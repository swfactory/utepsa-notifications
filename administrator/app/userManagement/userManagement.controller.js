;(function() {

    angular.module("utepsa-administrator").controller('userManagementController', userManagementController);

    userManagementController.$inject = ['$scope','resizeTemplate','userManagementService', 'status_code', '$localStorage','$state'];

    function userManagementController($scope,resizeTemplate, userManagementService, status_code,$localStorage,$state) {
        resizeTemplate.resize();
        $scope.role=[
            {
                "id":1,
                "name": "Súper Administrador"
            },
            {
                "id":2,
                "name": "Administrador"
            },
            {
                "id":3,
                "name": "Moderador"
            },
            {
                "id":4,
                "name": "Ejecutivo"
            },
            {
                "id":5,
                "name": "Seguridad"
            },
            {
                "id":6,
                "name": "Jefe de Carrera"
            },
            {
                "id":7,
                "name": "Docente"
            }
        ];
        $scope.roleSelected = "";
        $scope.messageAlert = "";
        $scope.alertError = false;
        $scope.resultPutAdministrator = "";
        $scope.itemUsersPermissions = "";
        $scope.userBuffer = "";
        $scope.searchUsersPermissions = [];
        $scope.newUsers = {};
        $scope.userName="";
        $scope.password = "";
        $scope.spinnerUserManagement = true;
        $scope.itemsPermissionsOfUser = "";
        $scope.permissionsOfUser = [];
        $scope.selectedUserPermissions = "";
        $scope.modifyPermissions = false;
        $scope.responsePutModifyPermissions = "";
        $scope.permissionsBuffer = "";
        $scope.deleteUserSelected = "";
        $scope.changePasswordForced=false;
        getUserManagement();

        function getUserManagement() {
            $scope.alertError = false;
            $scope.searchUsersPermissions = [];
            var promise = userManagementService.getUsersManagement();
            if(promise)
            {
                promise.then(function(result){
                    $scope.itemUsersPermissions = result;
                    $scope.spinnerUserManagement = false;
                    try{
                        if($scope.itemUsersPermissions.code == status_code.OK){
                            angular.forEach($scope.itemUsersPermissions.data, function (item) {
                                if(item.state)
                                {
                                    $scope.searchUsersPermissions.push(item);
                                }
                            });
                        }
                        if ($scope.itemUsersPermissions.status == -1) {
                            $scope.messageAlert = "  Problema de conexión";
                            $scope.alertError = true;
                        }
                    }catch(e){
                    }
                });
            }
        }

        $scope.editUser=function (user) {
            $scope.alertError = false;
            $scope.userBuffer = angular.copy(user);
            $("#editarUsuario").modal("show");
        };

        $scope.saveUser = function (user) {
            $scope.alertError=false;
            userManagementService.putUserManagemet(user, function (result) {
                $scope.resultPutAdministrator = result;
                if ($scope.resultPutAdministrator.data.code == status_code.OK) {
                    if($scope.userBuffer.id==$localStorage.currentAdministrator.idCredential){
                        $state.reload();
                    }
                    $("#editarUsuario").modal("hide");
                    $("#seCambioConExito").modal("show");
                    getUserManagement();
                }
                if($scope.resultPutAdministrator.data.code == status_code.NOT_MODIFIED){
                    $scope.messageAlert= "No fue posible modificar los datos del usuario.";
                    $scope.alertError=true;
                }
                if ($scope.resultPutAdministrator.status == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert="  Problema interno del servidor";
                    $scope.alertError = true;
                }
                if ($scope.resultPutAdministrator.status == -1) {
                    $scope.alertError = true;
                    $scope.messageAlert = "  Problema de conexión";
                }
            })
        };

        $scope.deleteUserSelectedInUserManagement = function () {
            userManagementService.deleteUserMangement($scope.deleteUserSelected, function (result) {
                $scope.alertError = false;
                $scope.resultDeleteAdministrator = result;
                if ($scope.resultDeleteAdministrator.data.code == status_code.OK) {
                    $("#confirmacionParaEliminar").modal("hide");
                    $("#seEliminoConExito").modal("show");
                    getUserManagement();
                }
                if ($scope.resultDeleteAdministrator.data.code == status_code.NOT_FOUND) {
                    $scope.messageAlert ="  El usuario no existe.";
                    $scope.alertError = true;
                }
                if ($scope.resultDeleteAdministrator.data.code == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert ="  Problema interno del servidor";
                    $scope.alertError = true;
                }
                if ($scope.resultDeleteAdministrator.status == -1) {
                    $scope.messageAlert = "  Problema de conexión";
                    $scope.alertError = true;
                }
            })
        };

        $scope.confirmDeleteUser = function (idCredential) {
            $("#confirmacionParaEliminar").modal("show");
            $scope.deleteUserSelected = angular.copy(idCredential);
        };

        $scope.newUser = function () {
            $scope.alertError = false;
            $("#nuevoUsuario").modal("show");
            $scope.newUsers = {};
        };

        $scope.saveNewUser = function (user,role,PasswordForced,userName) {
            userManagementService.postUserMangement(user, role,PasswordForced, userName, function (result) {
                $scope.alertError = false;
                $scope.resultPostAdministrator = result;
                if ($scope.resultPostAdministrator.data.code == status_code.CREATE) {
                    $("#nuevoUsuario").modal("hide");
                    $("#seCambioConExito").modal("show");
                    getUserManagement();
                    $scope.newUsers = {};
                }
                if ($scope.resultPostAdministrator.data.code == status_code.CONFLICT) {
                    $scope.messageAlert ="El nombre usuario ya existe. Porfavor elija otro.";
                    $scope.alertError = true;
                }
                if ($scope.resultPostAdministrator.data.code == status_code.NOT_FOUND) {
                    $scope.messageAlert ="No se pudo crear usuario.";
                    $scope.alertError = true;
                }
                if ($scope.resultPostAdministrator.status == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert="Problema interno del servidor";
                    $scope.alertError = true;
                }
                if ($scope.resultPostAdministrator.status == -1) {
                    $scope.messageAlert = "  Problema de conexión";
                    $scope.alertError = true;
                }
            })
        };

        $scope.showPermissions = function (users) {
            $scope.modifyPermissions = false;
            $scope.selectedUserPermissions = users;
            $scope.alertError = false;
            $("#permisosDeUsuario").modal("show");
            $scope.permissionsOfUser = [];
            var promise = userManagementService.permissionsUserManagement(users.id);
            if(promise){
                promise.then(function (result) {
                    $scope.itemsPermissionsOfUser = result;
                    if ($scope.itemsPermissionsOfUser.code == status_code.OK) {
                        angular.forEach($scope.itemsPermissionsOfUser.data, function (item) {
                            if(item)
                            {
                                $scope.permissionsOfUser.push(item);
                            }
                        });
                    }
                    if($scope.itemsPermissionsOfUser.code == status_code.NO_CONTENT){
                        $scope.alertError = true;
                        $scope.messageAlert = "El usuario no tiene ningun permiso asignado.";
                    }
                    if ($scope.itemsPermissionsOfUser.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                })
            }
        };

        $scope.editPermissions=function (userPermissions) {
            $scope.alertError = false;
            $scope.modifyPermissions = true;
            $scope.permissionsBuffer = angular.copy(userPermissions);
            $("#permisosDeUsuario").modal("hide");
            $("#editarpermisosDeUsuario").modal("show");
        };

        $scope.savePermissionsChanges = function (permissionResult) {
            $scope.alertError = false;
            var promise = userManagementService.putModifyPermissions(permissionResult);
            if(promise){
                promise.then(function (result) {
                    $scope.responsePutModifyPermissions = result;
                    if($scope.responsePutModifyPermissions.data.code == status_code.OK){
                        $scope.alertError = false;
                        $("#editarpermisosDeUsuario").modal("hide");
                        $("#seCambioConExito").modal("show");
                    }
                    if($scope.responsePutModifyPermissions.data.code == status_code.NOT_MODIFIED){
                        $scope.alertError = true;
                        $scope.messageAlert = "El permiso no fue modificado.";
                    }
                    if($scope.responsePutModifyPermissions.data.code == status_code.INTERNAL_SERVER_ERROR){
                        $scope.alertError = true;
                        $scope.messageAlert = "Error interno del servidor..";
                    }
                    if ($scope.responsePutModifyPermissions.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                })
            }
        };
    }

})();