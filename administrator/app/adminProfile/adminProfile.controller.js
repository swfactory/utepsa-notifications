/**
 * Created by CRISTIAN on 07/03/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('adminProfileController', adminProfileController);

    adminProfileController.$inject = ['$scope','resizeTemplate', 'adminProfileService', 'status_code','AuthenticationService'];

    function adminProfileController($scope, resizeTemplate, adminProfileService, status_code,AuthenticationService) {
        resizeTemplate.resize();
        $scope.itemsAdminProfile = [];
        $scope.adminProfile = [];
        $scope.spinnerAdminProfile = true;
        $scope.logout = logout;

        function logout(){
            AuthenticationService.Logout();
        }
        getAdminProfile();
        function getAdminProfile() {
            var promise = adminProfileService.getAdminProfile();
            if(promise){
                promise.then(function (result){
                    $scope.itemsAdminProfile = result;
                    $scope.spinnerAdminProfile = false;
                    if ($scope.itemsAdminProfile.code == status_code.OK) {
                        $scope.adminProfile =$scope.itemsAdminProfile.data;
                    }
                })
            }
        }
    }
})();