/**
 * Created by BURGOS on 20/09/2017.
 */
;(function () {
    angular.module('utepsa-administrator').controller('libraryOfUtepsaController', libraryOfUtepsaController);
    libraryOfUtepsaController.$inject = ['$scope', 'libraryOfUtepsaService', 'optionsStudent'];

    function libraryOfUtepsaController($scope, libraryOfUtepsaService, optionsStudent) {
        $scope.messageError = '';
        $scope.spinner = false;

        $scope.getSearchBooks = function(fullName) {
            $scope.allBooks = [];
            $scope.spinner = true;
            let promise = libraryOfUtepsaService.getSearchBooks(fullName);
            if(promise) {
                promise.then(function (result) {
                    console.log(result.data);
                    if(result.status == -1)
                        $scope.messageError = 'Problema de conexión';
                    if(result.code === 200)
                        $scope.allBooks = result.data;
                    if(result.code !== 200)
                        $scope.messageError = optionsStudent.verifyStatusCode(result.code, 'El libro')
                    $scope.spinner = false;
                })
            }
        };

        $scope.showInformationBook = function (book) {
            $scope.bookInformation = book;
            $('#informationBook').modal('show');
        }
    }
})();