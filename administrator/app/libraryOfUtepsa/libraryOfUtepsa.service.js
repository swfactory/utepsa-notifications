/**
 * Created by BURGOS on 20/09/2017.
 */
;(function () {
    angular.module('utepsa-administrator').service('libraryOfUtepsaService', libraryOfUtepsaService);
    libraryOfUtepsaService.$inject = ['$http', 'API'];

    function libraryOfUtepsaService($http, API) {

        function getSearchBooks(nameOfBook){
            return $http({
                method: 'GET',
                url: API.url+'Book/search?fullName=' + nameOfBook,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        let service = {
            getSearchBooks: getSearchBooks
        };

        return service;
    }
})();