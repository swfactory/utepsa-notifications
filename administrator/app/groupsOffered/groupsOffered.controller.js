/**
 * Created by BURGOS on 19/05/2017.
 */
(function () {
    angular.module("utepsa-administrator").controller('groupsOfferedController', groupsOfferedController);
    groupsOfferedController.$inject = ['$scope', 'groupsOfferedService', 'resizeTemplate', 'status_code'];
    function groupsOfferedController($scope, groupsOfferedService, resizeTemplate, status_code) {
        resizeTemplate.resize();
        //getGroupsOffered
        $scope.resultGetAcademicArea = [];
        $scope.itemsAcademicArea = {};
        getGroupsOffered();
        //getCareers
        $scope.resultGetCareers = [];
        $scope.itemsCareers = {};
        getCareers();
        //checkSelection
        $scope.optionSelect = '';
        $scope.academicArea = false;
        $scope.career = false;
        //getGroupsOfferedAcadeicArea
        $scope.resultGetGroupsOfferedAcademicArea = [];
        $scope.itemsGetGroupsOfferedAcademicArea = [];
        $scope.spinnerGroupsOfferedAcademicArea = false;
        $scope.AlertErrorGroupsOfferedAcademicArea= false;
        $scope.messageAlert = "No existe el grupo ofertado";
        //getGroupsOfferedCareer
        $scope.resultGetGroupsOfferedCareer = [];
        $scope.itemsGetGroupsOfferedCareer = [];
        $scope.spinnerGroupsOfferedCareer = false;
        $scope.AlertErrorGroupsOfferedCareer = false;

        $scope.stateColorOfGroupsOffered = false;

        function getGroupsOffered() {
            var promise = groupsOfferedService.getAcademicArea();
            if(promise){
                promise.then(function (result) {
                    $scope.resultGetAcademicArea = result;
                    if($scope.resultGetAcademicArea.data.code == status_code.OK){
                        $scope.itemsAcademicArea = $scope.resultGetAcademicArea;
                    }
                })
            }
        }

        function getCareers(){
            var promise = groupsOfferedService.getCareers();
            if(promise){
                promise.then(function (result) {
                    $scope.resultGetCareers = result;
                    if($scope.resultGetCareers.data.code == status_code.OK){
                        $scope.itemsCareers = $scope.resultGetCareers.data;
                    }
                })
            }
        }

        $scope.checkSelection = function (areaSelected) {
            if(areaSelected == "areaAcademica"){
                $scope.academicArea = true;
                $scope.career = false;
            }
            if(areaSelected == "carrera"){
                $scope.career = true;
                $scope.academicArea = false;
            }
            if(areaSelected == ""){
                $scope.career = false;
                $scope.academicArea = false;
            }
        };

        $scope.getGroupsOfferedAcadeicArea = function (idAcademicArea, semester) {
            $scope.spinnerGroupsOffered = true;
            $scope.stateColorOfGroupsOffered = 0;
            var promise = groupsOfferedService.getGroupsOfferedAcadeicArea(idAcademicArea, semester);
            if(promise){
                promise.then(function (result) {
                    $scope.itemsGetGroupsOfferedCareer = [];
                    $scope.itemsGetGroupsOfferedAcademicArea = [];
                    $scope.spinnerGroupsOfferedCareer = false;
                    $scope.AlertErrorGroupsOfferedCareer = false;
                    $scope.AlertErrorGroupsOfferedAcademicArea= false;
                    $scope.resultGetGroupsOfferedAcademicArea = result;
                    if($scope.resultGetGroupsOfferedAcademicArea.data.code == status_code.OK){
                        $scope.spinnerGroupsOffered = false;
                        $scope.spinnerGroupsOfferedAcademicArea = true;
                        angular.forEach($scope.resultGetGroupsOfferedAcademicArea.data.data, function (item) {
                            $scope.itemsGetGroupsOfferedAcademicArea.push(item);
                        })
                    }
                    if($scope.resultGetGroupsOfferedAcademicArea.data.code == status_code.NOT_FOUND){
                        $scope.AlertErrorGroupsOfferedAcademicArea = true;
                        $scope.spinnerGroupsOffered = false;
                    }
                })
            }
        };
        
        $scope.getGroupsOfferedCareer = function (idCareer, pensum, semester, module) {
            $scope.spinnerGroupsOffered = true;
            $scope.stateColorOfGroupsOffered = 0;
            var promise = groupsOfferedService.getGroupsOfferedCareers(idCareer, pensum, semester, module);
            if(promise){
                promise.then(function (result) {
                    $scope.itemsGetGroupsOfferedAcademicArea = [];
                    $scope.itemsGetGroupsOfferedCareer = [];
                    $scope.spinnerGroupsOfferedAcademicArea = false;
                    $scope.AlertErrorGroupsOfferedCareer = false;
                    $scope.AlertErrorGroupsOfferedAcademicArea= false;
                    $scope.resultGetGroupsOfferedCareer = result;
                    if($scope.resultGetGroupsOfferedCareer.data.code == status_code.OK){
                        $scope.spinnerGroupsOffered = false;
                        $scope.spinnerGroupsOfferedCareer = true;
                        angular.forEach($scope.resultGetGroupsOfferedCareer.data.data, function (item) {
                            $scope.itemsGetGroupsOfferedCareer.push(item);
                        })
                    }
                    if($scope.resultGetGroupsOfferedCareer.data.code == status_code.NOT_FOUND){
                        $scope.spinnerGroupsOffered = false;
                        $scope.AlertErrorGroupsOfferedCareer = true;
                    }
                })
            }
        };

        $scope.changeColorState = function (restult) {
            if(restult == 'course.name')
                $scope.stateColorOfGroupsOffered = 1;
            if(restult == 'course.academicArea.description')
                $scope.stateColorOfGroupsOffered = 2;
            if(restult == 'module')
                $scope.stateColorOfGroupsOffered = 3;
            if(restult == 'group')
                $scope.stateColorOfGroupsOffered = 4;
        }
    }
})();
