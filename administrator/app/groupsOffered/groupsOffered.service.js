/**
 * Created by BURGOS on 19/05/2017.
 */
(function () {
    angular.module("utepsa-administrator").factory('groupsOfferedService', groupsOfferedService);

    groupsOfferedService.$inject = ['$http', 'API'];

    function groupsOfferedService($http, API) {
        function getAcademicArea() {
            return $http({
                method: 'GET',
                url: API.url+'academicArea'
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        function getGroupsOfferedAcadeicArea(idAcademicArea, semester) {
            return $http({
                method: 'GET',
                url: API.url+'groupsOffered/academicArea/'+idAcademicArea+'/'+semester
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        function getCareers() {
            return $http({
                method: 'GET',
                url: API.url+'careers'
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        function getGroupsOfferedCareers(idCareer, pensum, semester, module) {
            return $http({
                method: 'GET',
                url: API.url+'groupsOffered/career/'+idCareer+'/'+pensum+'/'+semester+'?module='+module
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        var service = {
            getAcademicArea: getAcademicArea,
            getGroupsOfferedAcadeicArea: getGroupsOfferedAcadeicArea,
            getCareers: getCareers,
            getGroupsOfferedCareers: getGroupsOfferedCareers
        };

        return service;
    }
})();
