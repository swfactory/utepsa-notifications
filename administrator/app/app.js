;(function() {
  'use strict';

      angular.module("utepsa-administrator",['ui.router', 'ngStorage','angularMoment', 'angularUtils.directives.dirPagination', 'chart.js']);

  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("utepsa-administrator").config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$stateProvider','$urlRouterProvider', '$httpProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    $urlRouterProvider.otherwise('/');

    // routes
    $stateProvider
        .state('dashboard', {
            url: '/',
            parent: 'main',
            controller: 'DashboardController',
            templateUrl: 'app/dashboard/dashboard.html',
        })
        .state('main', {
            abstract: true,
            templateUrl: 'app/core/pages/main.html',
            data: {
              cssClassnames: 'hold-transition skin-blue sidebar-mini layout-boxed fixed'
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'app/auth/login.html',
            controller: 'LoginController',
            data: {
                cssClassnames: 'hold-transition login-page'
            }
        })
        .state('resetPassword', {
            url: '/resetPassword',
            controller: 'ResetPasswordController',
            templateUrl: 'app/auth/resetPassword/resetPassword.html',
            data: {
                cssClassnames: 'hold-transition login-page'
            }
        })
        .state('searchStudents', {
            url:'/searchStudents',
            parent: 'main',
            controller: 'searchStudentController',
            templateUrl:'app/searchStudents/searchStudents.html',
        })
        .state('notifications', {
            url: '/notifications',
            parent: 'main',
            controller: 'NotificationController',
            templateUrl: 'app/notifications/notifications.html'
        })
        .state('userManagement', {
            url: '/userManagement',
            parent: 'main',
            controller: 'userManagementController',
            templateUrl: 'app/userManagement/userManagement.html',
        })
        .state('adminProfile', {
            url: '/adminProfile',
            parent: 'main',
            controller: 'adminProfileController',
            templateUrl: 'app/adminProfile/adminProfile.html',
        })
        .state('groupsOffered', {
            url: '/groupsOffered',
            parent: 'main',
            controller: 'groupsOfferedController',
            templateUrl: 'app/groupsOffered/groupsOffered.html',
        })
        .state('reportByYear', {
            url: '/reportByYear',
            parent: 'main',
            controller: 'reportByYearController',
            templateUrl: 'app/reportByYear/reportByYear.html',
        })
        .state('featuredStudent', {
            url: '/featuredStudent',
            parent: 'main',
            controller: 'featuredStudentController',
            templateUrl: 'app/featuredStudent/featuredStudent.html',
        })
        .state('matrixProgrammingCourse', {
            url: '/programmingCourse',
            parent: 'main',
            controller: 'matrixProgrammingCourseController',
            templateUrl: 'app/matrixProgrammingCourse/matrixProgrammingCourse.html',
        })
        .state('libraryOfUtepsa', {
            url: '/libraryOfUtepsa',
            parent: 'main',
            controller: 'libraryOfUtepsaController',
            templateUrl: 'app/libraryOfUtepsa/libraryOfUtepsa.html',
        })
  }

  angular.module("utepsa-administrator").run(run);

  run.$inject = ['$rootScope', '$http', '$location', '$state', '$localStorage'];

  function run($rootScope, $http, $location, $state, $localStorage) {
      moment.lang('es', {
          monthsParseExact : true,
          weekdays : 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
          weekdaysShort : 'dom._lun._mar._mié._jue._vie._sáb.'.split('_'),
          weekdaysMin : 'do_lu_ma_mi_ju_vi_sá'.split('_'),
          weekdaysParseExact : true,
          longDateFormat : {
              LT : 'H:mm',
              LTS : 'H:mm:ss',
              L : 'DD/MM/YYYY',
              LL : 'D [de] MMMM [de] YYYY',
              LLL : 'D [de] MMMM [de] YYYY H:mm',
              LLLL : 'dddd, D [de] MMMM [de] YYYY H:mm'
          },
          calendar : {
              sameDay : function () {
                  return '[hoy a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
              },
              nextDay : function () {
                  return '[mañana a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
              },
              nextWeek : function () {
                  return 'dddd [a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
              },
              lastDay : function () {
                  return '[ayer a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
              },
              lastWeek : function () {
                  return '[el] dddd [pasado a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
              },
              sameElse : 'L'
          },
          relativeTime : {
              future : 'en %s',
              past : 'hace %s',
              s : 'unos segundos',
              m : 'un minuto',
              mm : '%d minutos',
              h : 'una hora',
              hh : '%d horas',
              d : 'un día',
              dd : '%d días',
              M : 'un mes',
              MM : '%d meses',
              y : 'un año',
              yy : '%d años'
          },
          dayOfMonthOrdinalParse : /\d{1,2}º/,
          ordinal : '%dº',
          week : {
              dow : 1, // Monday is the first day of the week.
              doy : 4  // The week that contains Jan 4th is the first week of the year.
          }
      });
      // keep user logged in after page refresh
      if (localStorage.getItem("currentAdministrator") === null) {
          $state.transitionTo("login");
      }else {
          if ($localStorage.currentAdministrator) {
              //$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
          }
      }

      // redirect to login page if not logged in and trying to access a restricted page
      $rootScope.$on('$locationChangeStart', function (event, next, current) {
          var publicPages = ['/login'];

          var restrictedPage = publicPages.indexOf($location.path()) === -1;

          if (restrictedPage && !$localStorage.currentAdministrator) {
              $state.transitionTo("login");
              event.preventDefault();
          }

          try {
              if ($localStorage.tempUser) {
                  $state.go("resetPassword");
                  event.preventDefault();
              }
          }
          catch(e){

          }
      });
  }

})();