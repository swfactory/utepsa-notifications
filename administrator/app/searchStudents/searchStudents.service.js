/**
 * Created by Android on 2/13/2017.
 */
;(function() {

    angular.module("utepsa-administrator").factory("searchStudentService", searchStudentService);

    searchStudentService.$inject = ['$http','API','$localStorage'];

    function searchStudentService($http,API,$localStorage) {

        function getStudent(criterio){
            return $http({
                method: 'GET',
                url: API.url+'student/search?registerNumber='+criterio,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getHistoryNotes(idStudent){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent+'/historyNotes',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getHistoryNotesByCourse(idStudent,idCourse){
            return $http({
                method: 'GET',
                url: API.url+'student/'+idStudent+'/historyNotes/course/'+idCourse,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getHistoryNotesByPensum(idStudent){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent+'/historyNotes/Pensum',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getDocumentsStudents(idStudent){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent+'/documents',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getCoursesRegistered(idStudent){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent+'/CourseRegister',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getCurrentSemester(){
            return $http({
                method: 'GET',
                url: API.url+'CurrentSemester',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        function getFinancialState(idStudent) {
            return $http({
                method: 'GET',
                url: API.url+'student/'+idStudent+'/FinancialState',
                withCredentials: true,
                headers:{
                    'Content-Type': 'application/json: charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            })
        }

        function getStudentProfile(idStudent){

            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function putUpdateInfoStudent(idStudent, callback) {
            return $http({
                method: 'PUT',
                url: API.url+'student/'+ idStudent +'/sync',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function (result) {
                callback(result);
            }).catch(function (error) {
                callback(error);
            })
        }

        function putResetPassword(idStudent){
            return $http({
                method: 'PUT',
                url: API.url+'resetPassword/'+idStudent+'/reset',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service={
            getStudent: getStudent,
            getHistoryNotes: getHistoryNotes,
            getHistoryNotesByPensum: getHistoryNotesByPensum,
            getDocumentsStudents: getDocumentsStudents,
            getCoursesRegistered: getCoursesRegistered,
            getCurrentSemester: getCurrentSemester,
            getFinancialState: getFinancialState,
            getStudentProfile: getStudentProfile,
            getHistoryNotesByCourse: getHistoryNotesByCourse,
            putUpdateInfoStudent: putUpdateInfoStudent,
            putResetPassword: putResetPassword
        };
        return service;
    }

})();
