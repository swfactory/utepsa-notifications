/**
 * Created by BURGOS on 31/08/2017.
 */
;(function () {
    angular.module('utepsa-administrator').controller('matrixProgrammingCourseController', matrixProgrammingCourseController);
    matrixProgrammingCourseController.$inject = ['$scope', 'matrixProgrammingCourseService', 'optionsStudent', 'searchStudentService', '$state'];

    function matrixProgrammingCourseController($scope, matrixProgrammingCourseService, optionsStudent, searchStudentService, $state) {
        $scope.dataIdCourse = [];
        let changeClassCssOnMatrixProgramming = [];
        let dataProgrammingCourseWithRepeatValue = [];
        $scope.messageError = '';

        $scope.getStudents = function (registerNumber) {
            $scope.spinnerSearchStudent = true;
            $scope.dataStudent = {};
            $scope.messageError = '';
            $scope.tabs = 'm1';
            $scope.description = 'Seleccione las carreras que desea programar y luego haz click sobre el botón enviar'
            $scope.updatingInfoStudent = {spinner: false, message: ''};
            let promise = matrixProgrammingCourseService.getStudent(optionsStudent.formatRegisterCode(registerNumber, 10));
            if(promise) {
                promise.then(function (result) {
                    if(result.data.code == 200) {
                        getMissingCourseByStudents(result.data.data.id);
                        $scope.dataStudent = {
                            id: result.data.data.id,
                            registerCode: result.data.data.registerNumber,
                            name: result.data.data.name,
                            fatherLastname: result.data.data.fatherLastname,
                            motherLastname: result.data.data.motherLastname,
                            faculty: result.data.data.career.faculty.name,
                            career: result.data.data.career.name
                        };
                        $scope.spinnerSearchStudent = false;
                    } else {
                        $scope.messageError = optionsStudent.verifyStatusCode(result.data.code, 'Estudiante');
                    }
                })
            }
        };

        function getMissingCourseByStudents(idStudent) {
            $scope.dataCourseMissing = {};
            let promise = matrixProgrammingCourseService.getMissingCourseByStudent(idStudent);
            if(promise) {
                promise.then(function (result) {
                    if(result.data.code == 200) {
                        $scope.dataCourseMissing = result.data.data;
                    }
                })
            }
        }

        $scope.preparingIdCourse = function (idCourse) {
            $('#local'+idCourse).css({'background-color': '#E0E0E0',
                'color': 'white'});
            if($scope.dataIdCourse.length === 0) {
                $scope.dataIdCourse.push(idCourse);
            } else if($scope.dataIdCourse.length >= 1) {
                verifyRepeatValue(idCourse);
            }
        };

        function verifyRepeatValue(idCourses) {
            let index = $scope.dataIdCourse.indexOf(idCourses);
            if($scope.dataIdCourse.indexOf(idCourses) > -1) {
                $scope.dataIdCourse.splice(index, 1);
                $('#local'+idCourses).css({'background-color': 'white',
                    'color': '#333'})
            } else
                $scope.dataIdCourse.push(idCourses);
        }

        $scope.groupsOfferedById = function () {
            $scope.spinnerCourseOffered = true;
                $scope.resultGroupsOfferedById = [];
            angular.forEach($scope.dataIdCourse, function (item) {
                let promise = matrixProgrammingCourseService.getGroupsOfferedByIdCourse(item);
                if(promise) {
                    promise.then(function (result) {
                        $('#local'+item).css({'background-color': 'white',
                            'color': '#333'});
                        $scope.resultGroupsOfferedById.push(result.data.data);
                    })
                }
            });
            $scope.spinnerCourseOffered = false;
            $scope.dataIdCourse = [];
            dataProgrammingCourseWithRepeatValue = [];
        };

        $scope.changeTabs = function (module) {
            $scope.tabs = module;
            if(module === 'programacionDelEstudiante')
                removeRepeatValueOnDataProgrammingCourse();
        };

        $scope.programmingCourse = function (course) {
            $('.id'+course.id).addClass('btn-primary textWhite');
            if(dataProgrammingCourseWithRepeatValue.length == 0) {
                changeClassCssOnMatrixProgramming.push(course.id);
                dataProgrammingCourseWithRepeatValue.push({id: course.id, name: course.course.name, group: course.group,
                    module: course.module, numberEnrolled: course.numberEnrolled, schedule: course.schedule.schedule
                });
            }
            else if(dataProgrammingCourseWithRepeatValue.length > 0) {
                verifyRepeatCode(course.id, course)
            }
        };

        function verifyRepeatCode(idCourses, course) {
            let index = changeClassCssOnMatrixProgramming.indexOf(idCourses);
            if(changeClassCssOnMatrixProgramming.indexOf(idCourses) > -1) {
                changeClassCssOnMatrixProgramming.splice(index, 1);
                $('.id'+ idCourses).removeClass('btn-primary textWhite');
            } else {
                changeClassCssOnMatrixProgramming.push(idCourses);
                dataProgrammingCourseWithRepeatValue.push({id: course.id, name: course.course.name, group: course.group,
                    module: course.module, numberEnrolled: course.numberEnrolled, schedule: course.schedule.schedule
                });
            }
        }

        function removeRepeatValueOnDataProgrammingCourse() {
            $scope.courseConfirmedOfStudent = [];
            changeClassCssOnMatrixProgramming = [];
            let obj ={};
            for(let i = 0; i < dataProgrammingCourseWithRepeatValue.length; i++) {
                if(!obj[dataProgrammingCourseWithRepeatValue[i]['id']])
                    obj[dataProgrammingCourseWithRepeatValue[i]['id']]= dataProgrammingCourseWithRepeatValue[i]
            }
            let dataProgrammingCourse = [];
            for(let key in obj) dataProgrammingCourse.push(obj[key])
            $scope.courseConfirmedOfStudent = angular.copy(dataProgrammingCourse)
            dataProgrammingCourseWithRepeatValue = [];
        }

        $scope.updateInfoStudent = function () {
            $scope.updatingInfoStudent = {spinner: true, message: ''};
            searchStudentService.putUpdateInfoStudent($scope.dataStudent.id, function (result) {
                if(result.data.code === 200)
                    $scope.updatingInfoStudent = {spinner: false, message: 'Se actualizo con éxito'};
                else {
                    $scope.updatingInfoStudent = {
                        spinner: false,
                        message: optionsStudent.verifyStatusCode(result.data.code, ' ')
                    }
                }
                $state.reload()
            })
        };

        $scope.exportToExcel = function () {
            $('#dvData').table2excel({
                name: 'Worksheet Name',
                filename: $scope.dataStudent.name,
                fileext: '.xls'
            })
        }
        //https://lmfresneda.github.io/2017/01/22/eliminar-duplicados-de-un-array-de-objetos/
        //refactorizar busqueda de valores repetidos (codigo repetido)
        //revisar posicion donde resetear valores
    }
})();