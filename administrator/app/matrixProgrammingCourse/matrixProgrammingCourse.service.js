/**
 * Created by BURGOS on 31/08/2017.
 */
;(function () {
    angular.module("utepsa-administrator").factory('matrixProgrammingCourseService', matrixProgrammingCourseService);
    matrixProgrammingCourseService.$inject = ['$http', 'API'];
    
    function matrixProgrammingCourseService($http, API) {

        function getStudent(registerNumber) {
            return $http({
                method: 'GET',
                url: API.url+'student/search?registerNumber=' + registerNumber,
                withCredentials: true
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        function getMissingCourseByStudent(idStudent) {
            return $http({
                method: 'GET',
                url: API.url+'student/'+ idStudent +'/MissingCourse',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        function getGroupsOfferedByIdCourse(idCourse) {
            return $http({
                method: 'GET',
                url: API.url+'groupsOffered/course/' + idCourse,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        return {
            getStudent: getStudent,
            getMissingCourseByStudent: getMissingCourseByStudent,
            getGroupsOfferedByIdCourse: getGroupsOfferedByIdCourse
        };
    }
})();