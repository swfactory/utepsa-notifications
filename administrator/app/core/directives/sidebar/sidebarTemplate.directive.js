'use strict';

angular.module('utepsa-administrator')
	.directive('sidebarTemplate',function(){
		return {
	        templateUrl:'app/core/directives/sidebar/sidebar.html',
	        restrict: 'A',
	        replace: false
    	}
	});

