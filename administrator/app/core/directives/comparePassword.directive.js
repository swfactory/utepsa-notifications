/**
 * Created by Android on 1/17/2017.
 */
(function () {
    'use strict';

    angular.module('utepsa-administrator').directive('matchPassword', matchPassword);

     function matchPassword() {
         return {
             require: "ngModel",
             scope: {
                 otherModelValue: "=matchPassword"
             },
             link: function(scope, element, attributes, ngModel) {

                 ngModel.$validators.matchPassword = function(modelValue) {
                     return modelValue == scope.otherModelValue;
                 };

                 scope.$watch("otherModelValue", function() {
                     ngModel.$validate();
                 });
             }
         };
    };
}());