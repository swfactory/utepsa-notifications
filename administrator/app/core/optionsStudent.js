/**
 * Created by BURGOS on 31/08/2017.
 */
angular.module('utepsa-administrator').factory('optionsStudent', optionsStudent);
optionsStudent.$inject = ['status_code'];

function optionsStudent(status_code) {

    function verifyStatusCode(code, subject) {
        if(code == status_code.NO_CONTENT)
            return subject + ' no encontrado. Por favor intentelo mas tarde.';
        if(code == status_code.NOT_FOUND)
            return subject + ' no existe.';
        if(code == status_code.CONFLICT || code == status_code.INTERNAL_SERVER_ERROR)
            return 'Problema interno del servior.';
    }

    function formatRegisterCode(accountOrRegisterCode, tamano) {
        return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
    }

    return {
        verifyStatusCode: verifyStatusCode,
        formatRegisterCode: formatRegisterCode
    }
}
