/**
 * Created by Android on 1/27/2017.
 */
;(function() {

    angular.module("utepsa-administrator").constant('status_code', {
        'OK': '200',
        'CREATE': '201',
        'NO_CONTENT': '204',
        'NOT_MODIFIED': '304',
        'UNAUTHORIZED': '401',
        'FORBIDDEN': '403',
        'NOT_FOUND': '404',
        'CONFLICT': '409',
        'INTERNAL_SERVER_ERROR': '500',
    });

})();
