;(function() {

	angular.module("utepsa-administrator").factory("resizeTemplate", resizeTemplate);

	
	
	function resizeTemplate(){
		function resize(){
			$.AdminLTE.layout.activate();
    		$.AdminLTE.layout.fix();
    		$.AdminLTE.layout.fixSidebar();
		}

		var service = {
			resize: resize
		};

		return service;
	}

})();