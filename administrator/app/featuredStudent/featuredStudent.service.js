;(function () {
    angular.module('utepsa-administrator').factory('featuredStudentService', featuredStudentService);
    featuredStudentService.$inject = ['$http', 'API'];

    function featuredStudentService($http, API) {

          function getBestAverage(year, idCareer, semester){
            return $http({
                method: 'GET',
                url: API.url+'FeaturedStudent/BestAverage/'+idCareer+'?year='+year+'&semester='+semester,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getCareers() {
            return $http({
                method: 'GET',
                url: API.url+'careers'
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        function getStudentPerExcellence(year, idCareer, semester){
            return $http({
                method: 'GET',
                url: API.url+'FeaturedStudent/StudentsParExcellence?semester='+year+'-'+semester+'&idCareer='+idCareer,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getHistoryNotesByStudentAndSemester(idStudent, semester){
            return $http({
                method: 'GET',
                url: API.url+'FeaturedStudent/'+idStudent+'/historyNotes?semester='+semester,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service = {
            getBestAverage: getBestAverage,
            getStudentPerExcellence: getStudentPerExcellence,
            getHistoryNotesByStudentAndSemester: getHistoryNotesByStudentAndSemester,
            getCareers: getCareers
        };

        return service;

    }
})();