;(function () {
    angular.module("utepsa-administrator").controller('featuredStudentController', featuredStudentController);

    featuredStudentController.$inject = ['$scope', 'featuredStudentService', 'status_code'];

    function featuredStudentController($scope, featuredStudentService, status_code) {
        $scope.spinnerFeaturedStudent = true;
        $scope.listStudentOfBestAverage = [];
        $scope.listStudentPerExcellence = [];
        $scope.element = [];
        $scope.element2 = [];
        $scope.student = [];
        $scope.studentExcellence = [];
        $scope.initRegisterStudent = [];
        $scope.initRegisterStudents = [];
        $scope.courses = [];
        $scope.showProfile = false;
        $scope.showProfileExcellence = false;
        $scope.spinnerShowInformationCourseStudent = false;
        $scope.careers = [];
        $scope.showInformation = false;
        $scope.spinnerInit = false;
        getCareers();

        $scope.getBestAverage = function (year, idCareer, semester) {
            $scope.spinnerInit = true;
            $scope.spinnerBoxBestAverage = true;
            var promise = featuredStudentService.getBestAverage(year, idCareer, semester);
            if(promise){
                promise.then(function (result) {
                    if(result.code == status_code.OK){
                        $scope.showProfile = false;
                        $scope.showProfileExcellence = false;
                        $scope.showInformation = true;
                        $scope.listStudentOfBestAverage = result.data;
                        $scope.spinnerFeaturedStudent = false;
                        $scope.spinnerBoxBestAverage = false;
                        getStudentExcellence(year, idCareer, semester);
                        $scope.spinnerInit = false;
                    }
                })
            }
        };

        function getCareers() {
            var promise = featuredStudentService.getCareers();
            if(promise){
                promise.then(function (result) {
                    if(result.data.code == status_code.OK)
                        $scope.careers = result.data;
                })
            }
        }

        $scope.element = function element(item){
            $scope.spinnerShowInformationCourseStudent = true;
            $scope.showProfile = true;
            $scope.student = item;
            $scope.initRegisterStudent = formatRegisterCode($scope.student.agendCode, 10);
            var promise = featuredStudentService.getHistoryNotesByStudentAndSemester($scope.student.idStudent, '2017-1');
            if(promise){
                promise.then(function (result) {
                    if(result.code == status_code.OK){
                        $scope.courses = result.data;
                        $scope.spinnerShowInformationCourseStudent = false;
                    }
                })
            }
        };

        function getStudentExcellence(year, idCareer, semester) {
            $scope.spinnerBoxStudentExcellence = true;
            var promise = featuredStudentService.getStudentPerExcellence(year, idCareer, semester);
            if(promise){
                promise.then(function (result) {
                    if(result.code == status_code.OK){
                        $scope.listStudentPerExcellence = result.data;
                        console.log($scope.listStudentPerExcellence);
                        $scope.spinnerBoxStudentExcellence = false;
                    }

                    if(result.code == status_code.NO_CONTENT){
                        $scope.spinnerBoxStudentExcellence = false;
                    }
                })
            }
        }

        $scope.element2 = function element2(item2){
            $scope.showProfileExcellence = true;
            $scope.studentExcellence = item2;
            $scope.initRegisterStudents = formatRegisterCode($scope.studentExcellence.agendCode, 10);
            console.log(item2);
        };

        var formatRegisterCode = function(accountOrRegisterCode, tamano) {
            var register = "";
            for(var i=0; i<tamano ; i++){
                if(accountOrRegisterCode.charAt(i) > 0){
                    if(accountOrRegisterCode.substring(i).length == 2){ //si el registro contiene 2
                        register= accountOrRegisterCode.substring(i-2,tamano-2);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 3){ //si el registro contiene 3
                        register= accountOrRegisterCode.substring(i-2,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 4){ //si el registro contiene 4
                        register= accountOrRegisterCode.substring(i-1,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 5){ //si el registro contiene 5
                        register= accountOrRegisterCode.substring(i,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 6){ //si el registro contiene 6
                        register= accountOrRegisterCode.substring(i,tamano-4);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 7){ //si el registro contiene 7
                        register= accountOrRegisterCode.substring(i,tamano-5);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 8){ //si el registro contiene 8
                        register= accountOrRegisterCode.substring(i,tamano-6);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 9){ //si el registro contiene 9
                        register= accountOrRegisterCode.substring(i,tamano-7);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 10){ //si el registro contiene 10
                        register= accountOrRegisterCode.substring(i,tamano-8);
                        break;
                    }
                }
            }
            return register;
        }
    }
})();