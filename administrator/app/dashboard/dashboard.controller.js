/**
 * Created by Android on 2/6/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', 'resizeTemplate', 'dashboardService', 'status_code','groupsOfferedService'];

    function DashboardController($scope, resizeTemplate, dashboardService, status_code,groupsOfferedService) {
        resizeTemplate.resize();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerDashboard=true;
        $scope.itemsExecuterReport="";
        $scope.executeReport="";
        $scope.alertErrorStudent = false;
        $scope.messageAlerStudent = "";
        $scope.alertErrorStudentActive = false;
        $scope.messageAlerStudentActive = "";

        getExecuteReport();
        function getExecuteReport() {
            $scope.alertError= false;
            $scope.alertErrorStudent = false;
            $scope.alertErrorStudentActive = false;
            var promise = dashboardService.getExecuteReport();
            if (promise) {
                promise.then(function (result) {
                    $scope.itemsExecuterReport=result;
                    $scope.spinnerDashboard=false;
                    if ($scope.itemsExecuterReport.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if($scope.itemsExecuterReport.code == 200){
                        $scope.executeReport= $scope.itemsExecuterReport.data;
                        getCareersByGrade();
                        averageByCareer($scope.executeReport);
                        getStudentsByModules($scope.executeReport);
                    }
                    if($scope.itemsExecuterReport.code == 409){
                        $scope.messageAlert= " No se pudo traer los datos.";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsExecuterReport.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsExecuterReport.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        $scope.careersByGrade = [];
        function getCareersByGrade() {
            let promise = groupsOfferedService.getCareers();
            if(promise){
                promise.then(function (result) {
                    if(result.data.code == status_code.OK)
                        $scope.careersByGrade = result.data;
                })
            }
        }

        $scope.labelsStudentsModulesByReportGeneral = [];
        $scope.dataStudentsModulesByReportGeneral = [];
        function getStudentsByModules(result) {
            angular.forEach(result.totalActiveStudentByModule, function (item) {
                $scope.labelsStudentsModulesByReportGeneral.push('Modulo ' + item.module);
                $scope.dataStudentsModulesByReportGeneral.push(item.studentRegister);
            });
        }

        $scope.selectedCareer="";
        $scope.showReportStudentByModule=false;
        $scope.itemsExecuterReportByCareer="";
        $scope.ExecuteReportByCareer="";
        $scope.spinnerReportStudentByModule=false;
        $scope.loadResource = true;

        $scope.studentByModuleSerie=[];
        $scope.stundentByModuleName=[];
        $scope.studentByModuleData=[];
        $scope.idCareer = '';

        $scope.studentsByModule= function (result) {
            if(result !== ""){
                $scope.idCareer = result;
                $scope.loadResource = true;
                $scope.studentByModuleSerie = ['Inscritos'];
                $scope.spinnerReportStudentByModule=true;
                var promise = dashboardService.getExecuteReportByCareer(result);
                if(promise){
                    promise.then(function (result) {
                        $scope.itemsExecuterReportByCareer=result;
                        $scope.spinnerReportStudentByModule=false;
                        if($scope.itemsExecuterReportByCareer.code === 200){
                            $scope.loadResource = false;
                            StudentPerCareer(result);
                            StudentActive(result);
                            StudentCareerManAndStudentCareerWoman(result);
                            StudentActiveManAndStudentActiveWoman(result);
                            StudentRegisterPerModule(result);
                            $scope.showReportStudentByModule=true;
                        }
                        if($scope.itemsExecuterReportByCareer.code == 409){
                            $scope.messageAlert= " No se pudo traer los datos.";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsExecuterReportByCareer.code == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsExecuterReportByCareer.status == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                    })
                }
            }
        };

        $scope.studentsPerCareer = "";
        function StudentPerCareer(result) {
            $scope.studentsPerCareer = "";
            $scope.studentsPerCareer = result.data.studentCareer;
        }

        $scope.studentActive = "";
        function StudentActive(result) {
            $scope.studentActive = "";
            $scope.studentActive = result.data.studentActive;
        }

        $scope.studentCareerMan = "";
        $scope.studentCareerWoman = "";
        $scope.labelsStudents = [];
        function StudentCareerManAndStudentCareerWoman(result) {
            $scope.alertErrorStudent = false;
            if($scope.studentsPerCareer > 0){
                $scope.studentCareerMan = "";
                $scope.studentCareerWoman = "";
                $scope.studentCareerMan = result.data.studentCareerMan;
                $scope.studentCareerWoman = result.data.studentCareerWoman;
                $scope.labelsStudents = ["Estudiantes registrados hombres","Estudiantes registrados mujeres"];
                $scope.dataStudents = [$scope.studentCareerMan, $scope.studentCareerWoman];
                $scope.type = 'pie';
            }
            else{
                $scope.alertErrorStudent = true;
                $scope.messageAlerStudent = "No hay estudiantes inscritos. Por favor elija otra carrera";
            }
        }

        $scope.studentActiveMan = "";
        $scope.studentActiveWoman = "";
        function StudentActiveManAndStudentActiveWoman(result) {
            $scope.alertErrorStudentActive = false;
            if($scope.studentActive > 0){
                $scope.studentActiveMan = "";
                $scope.studentActiveWoman = "";
                $scope.studentActiveMan = result.data.studentActiveMan;
                $scope.studentActiveWoman = result.data.studentActiveWoman;
                $scope.labelsActiveStudents = ["Estudiantes activos: hombres","Estudiantes activos: mujeres"];
                $scope.dataActiveStudents = [$scope.studentActiveMan, $scope.studentActiveWoman];
                $scope.type = 'pie';
            }
            else{
                $scope.studentActiveMan = "";
                $scope.studentActiveWoman = "";
                $scope.dataActiveStudents = [];
                $scope.alertErrorStudentActive = true;
                $scope.messageAlerStudentActive = "No hay estudiantes activos. Por favor elija otra carrera";
            }
        }

        $scope.labelsModule = [];
        $scope.dataStudentRegisterPerModule = [];
        function StudentRegisterPerModule(result) {
            $scope.dataStudentRegisterPerModule = [];
            $scope.labelsModule = [];
            angular.forEach(result.data.module, function (item) {
                $scope.labelsModule.push('Modulo '+item.module);
                $scope.dataStudentRegisterPerModule.push(item.studentRegister);
            });
        }

        $scope.averageCarrerSerie=[];
        $scope.averageCareerName=[];
        $scope.averageData=[];
        $scope.colorsOfAverageByCareer = [];
        $scope.average = 0.0;
        function averageByCareer(result) {
            let i = 0;
            let average = 0.0;
            $scope.colorsOfAverageByCareer = [];
            $scope.averageCarrerSerie = ['Promedio'];
            angular.forEach(result.dataCareers, function (item) {
                if(item.average != undefined){
                    i++;
                    average = average + parseFloat(parseFloat(item.average));
                    $scope.averageCareerName.push(item.nameCareer);
                    $scope.averageData.push(parseInt(item.average));
                    $scope.colorsOfAverageByCareer.push('#97BBCD', '#DCDCDC');
                }
            });
            $scope.average =( average / i).toFixed(2);
        }

        $scope.courseModule = function (points) {
            $scope.showItemsOfCourseMoudule = false;
            $scope.itemsCourseModule = [];
            let module = 0;
            angular.forEach(points, function (item) {
                module = item._index;
            });
            let promise = dashboardService.getCourseModule(module, $scope.idCareer);
            if(promise){
                promise.then(function (result) {
                    if(result.data.code == status_code.OK) {
                        $scope.itemsCourseModule = result.data.data;
                        $scope.showItemsOfCourseMoudule = true;
                    }
                })
            }
        }
    }
})();