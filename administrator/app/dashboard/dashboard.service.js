/**
 * Created by Android on 2/6/2017.
 */
;(function() {

    angular.module("utepsa-administrator").factory("dashboardService", dashboardService);

    dashboardService.$inject = ['$http','API','$localStorage'];

    function dashboardService($http,API){

        function getExecuteReport(){

            return $http({
                method: 'GET',
                url: API.url+'ExecutiveReport?semester=2017-1',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getExecuteReportByCareer(idCareer){

            return $http({
                method: 'GET',
                url: API.url+'ExecutiveReport/Career/'+idCareer+'?semester=2017-1',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getCourseModule(module, idCareer){
            return $http({
                method: 'GET',
                url: API.url + 'ExecutiveReport/CourseModule?semester=2017-1&module='+module+'&idCareer='+idCareer,
                withCredentials: true,
                headers:{
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            })
        }

        var service = {
            getExecuteReport: getExecuteReport,
            getExecuteReportByCareer:getExecuteReportByCareer,
            getCourseModule: getCourseModule
        };

        return service;
    }

})();