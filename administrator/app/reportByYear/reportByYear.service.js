/**
 * Created by Front-End on 17/07/2017.
 */
;(function () {
    angular.module('utepsa-administrator').factory('reportByYearService', reportByYearService);
    reportByYearService.$inject = ['$http', 'API'];

    function reportByYearService($http, API) {

        function getReportByYear(career, year){
            return $http({
                method: 'GET',
                url: API.url+'ExecutiveReport/Career/Year/'+ career +'?year='+ year,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service = {
            getReportByYear: getReportByYear
        };

        return service;

    }
})();