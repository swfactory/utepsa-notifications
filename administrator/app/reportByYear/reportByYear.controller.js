/**
 * Created by Front-End on 17/07/2017.
 */
;(function () {
    angular.module("utepsa-administrator").controller('reportByYearController', reportByYearController);

    reportByYearController.$inject = ['$scope', 'groupsOfferedService', 'reportByYearService', 'status_code'];

    function reportByYearController($scope, groupsOfferedService, reportByYearService, status_code) {
        $scope.careers = [];
        $scope.spinnerReportByYear = undefined;
        $scope.dataReportByYear = [];
        $scope.years = {};
        $scope.alertError = false;
        $scope.messageError = '';
        $scope.dataOfPercentageApprovedByCareer = {};
        $scope.dataOfPercentageDisapprovedByCareer = {};
        $scope.dataOfPercentageAbandonedByCareer = {};
        getCarers();

        function getCarers() {
            var promise = groupsOfferedService.getCareers();
            if(promise){
                promise.then(function (result) {
                    if(result.data.code == status_code.OK)
                        $scope.careers = result.data;
                })
            }
        }

        $scope.getReportByYear = function(career, year){
            if(year == undefined)
                year = 2017;
            $scope.spinnerReportByYear = true;
            $scope.years = {
                lessTwoYears: year-2,
                lessOneYears: year-1,
                year: year
            };
            $scope.dataReportByYear = [];
            var promise = reportByYearService.getReportByYear(career, year);
            if(promise){
                promise.then(function (result) {
                    $scope.dataOfPercentageApprovedByCareer = percentageOfApprovedStudent(result.data);
                    $scope.dataOfPercentageDisapprovedByCareer = percentageOfDisapprovedByCareer(result.data);
                    $scope.dataOfPercentageAbandonedByCareer = percentageOfAbandonedByCareer(result.data);
                    if(result.code == status_code.OK){
                        $scope.spinnerReportByYear = false;
                        $scope.dataReportByYear = result.data;
                        $scope.alertError = false;
                        $scope.messageError = '';
                    }
                    if(result.code == status_code.CONFLICT){
                        $scope.spinnerReportByYear = false;
                        $scope.alertError = true;
                        $scope.messageError = 'Conflicto para ejecutar el informe';
                    }
                    if(result.code == status_code.INTERNAL_SERVER_ERROR){
                        $scope.spinnerReportByYear = false;
                        $scope.alertError = true;
                        $scope.messageError = 'Problema interno del servidor';
                    }
                })
            }
        };

        function percentageOfApprovedStudent(result) {
            return {
                percentageOfLessTwoYear: ((result.numberOfCourseApprovedByCareer.yearnumber3 * 100)/result.numberOfCourseByCareer.yearnumber3).toFixed(2),
                percentageOfLessOneYear: ((result.numberOfCourseApprovedByCareer.yearnumber2 * 100)/result.numberOfCourseByCareer.yearnumber2).toFixed(2),
                percentageOfYear: ((result.numberOfCourseApprovedByCareer.yearnumber1 * 100)/result.numberOfCourseByCareer.yearnumber1).toFixed(2)
            };

        }

        function percentageOfDisapprovedByCareer(result) {
            return {
                percentageOfLessTwoYear: ((result.numberOfCourseDisapprovedByCareer.yearnumber3 * 100)/result.numberOfCourseByCareer.yearnumber3).toFixed(2),
                percentageOfLessOneYear: ((result.numberOfCourseDisapprovedByCareer.yearnumber2 * 100)/result.numberOfCourseByCareer.yearnumber2).toFixed(2),
                percentageOfYear: ((result.numberOfCourseDisapprovedByCareer.yearnumber1 * 100)/result.numberOfCourseByCareer.yearnumber1).toFixed(2)
            }
        }

        function percentageOfAbandonedByCareer(result) {
            return {
                percentageOfLessTwoYear: ((result.numberOfCourseAbandonedByCareer.yearnumber3 * 100)/result.numberOfCourseByCareer.yearnumber3).toFixed(2),
                percentageOfLessOneYear: ((result.numberOfCourseAbandonedByCareer.yearnumber2 * 100)/result.numberOfCourseByCareer.yearnumber2).toFixed(2),
                percentageOfYear: ((result.numberOfCourseAbandonedByCareer.yearnumber1 * 100)/result.numberOfCourseByCareer.yearnumber1).toFixed(2)
            }
        }

        $scope.exportToPDFButton = function () {
            let doc = new jsPDF();

            doc.fromHTML($('#generatePDF').get(0), 15, 15, {
                'width': 2000
            });
            doc.setFont('times', 'italic');
            doc.setFontSize(10);
            doc.save('ReporteAnual.pdf')
        };
    }
})();