/**
 * Created by Android on 2/6/2017.
 */
;(function() {

    angular.module("utepsa-administrator").factory("notificationsService", notificationsService);

    notificationsService.$inject = ['$http','API','$localStorage'];

    function notificationsService($http,API){

        function getCarrers(){

            return $http({
                method: 'GET',
                url: API.url+'careers/grade/PRE',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function postNotification(dateCreation, title, content, idCategory, target, idAudience, profile, callback){

            return $http({
                method: 'POST',
                url: API.url+'notifications',
                withCredentials: true,
                data:{
                    "dateCreation": dateCreation,
                    "idCreator": {
                        "id": profile.id,
                        "role": {
                            "id": profile.role.id
                        },
                        "profile": {
                            "id": profile.profile.id
                        }
                    },
                    "title": title,
                    "content": content,
                    "category": {
                        "id": idCategory
                    },
                    "audience": [
                        {
                            "target": target,
                            "type": {
                                "id": idAudience
                            }
                        }
                    ]
                },
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        function getNotifications() {

            return $http({
                method: 'GET',
                url: API.url + 'notifications',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function (result) {
                return result;
            }).catch(function (error) {
                return error;
            });
        }

        var service = {
            getCarrers: getCarrers,
            postNotification:postNotification,
            getNotifications:getNotifications
        };

        return service;
    }

})();