/**
 * Created by Android on 2/6/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('NotificationController', NotificationController);

    NotificationController.$inject = ['$scope', 'resizeTemplate', 'notificationsService', 'status_code', 'adminProfileService'];

    function NotificationController($scope, resizeTemplate, notificationsService, status_code, adminProfileService) {
        resizeTemplate.resize();
        $scope.typeOfNotification=[{
            "id":1,
            "name":'Noticias'
        }];
        $scope.messageAlert="";
        $scope.typeOfAudience = "";
        $scope.codigoRegistro = "";
        $scope.title = "";
        $scope.content = "";
        $scope.target = "";
        $scope.resultConsumtion = "";
        $scope.selectedCareer="";
        $scope.selectedTypeNotification="";
        $scope.careersByGrade = [];
        $scope.careersByGradeItems=null;
        $scope.itemsNotifications=[];
        $scope.notifications=[];
        $scope.spinnerNotifications=true;
        $scope.messageNotifications=false;
        $scope.alertNotifications=false;
        getProfileAdmin();

        var dateFull = new Date();
        var day = dateFull.getDate();
        var month = dateFull.getMonth();
        var year = dateFull.getFullYear();
        var hour = dateFull.getHours();
        var minutes = dateFull.getMinutes();
        var seconds = dateFull.getSeconds();

        var hourToTimestamp;
        function humanToTime(){
            var humDate = new Date(Date.UTC(year,
                (month),
                (day),
                (hour),
                (minutes),
                (seconds)));
            hourToTimestamp = (humDate.getTime()/1000.0);
        }

        var formatRegisterCode = function(accountOrRegisterCode, tamano) {
            return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
        }

        getCareersByGrade();
        function getCareersByGrade() {
            $scope.alertError= false;
            var promise = notificationsService.getCarrers();
            if (promise) {
                promise.then(function (result) {
                    $scope.careersByGradeItems = result;
                    if($scope.careersByGradeItems === undefined){
                        return;
                    }
                    for(var i = 0; i < $scope.careersByGradeItems.data.length; i++){
                        $scope.careersByGrade.push($scope.careersByGradeItems.data[i]);
                    }
                });
            }
        }

        Notifications();
        function Notifications() {
            $scope.alertError= false;
            $scope.notifications=[];
            var promise = notificationsService.getNotifications();
            if (promise) {
                promise.then(function (result) {
                    $scope.itemsNotifications = result;
                    $scope.spinnerNotifications=false;
                    if ($scope.itemsNotifications.status == -1) {
                        $scope.messageNotifications = "  Problema de conexión";
                        $scope.alertNotifications = true;
                    }
                    if($scope.itemsNotifications.data.code == status_code.OK){
                        angular.forEach($scope.itemsNotifications.data.data, function (item) {
                            $scope.notifications.push(item);
                        });
                        $scope.showTypePublicAudience = 0;
                }
                    if($scope.itemsNotifications.data.code == status_code.NOT_FOUND){
                        $scope.messageNotifications= " Notificaciones no encontradas.";
                        $scope.alertNotifications = true;
                    }
                    if ($scope.itemsNotifications.data.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageNotifications = "  Problema interno del servidor";
                        $scope.alertNotifications = true;
                    }
                    if ($scope.itemsNotifications.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageNotifications = "  Problema interno del servidor";
                        $scope.alertNotifications = true;
                    }
                });
            }
        }


        $scope.createNotification = createNotification;
        function createNotification() {
            $scope.alertError= 0;
            humanToTime();
            if( $scope.title!="" &&
                $scope.content!="" &&
                $scope.typeOfAudience!="" && $scope.typeOfAudience!=null){
                if($scope.typeOfAudience=='1'){//public
                     $scope.target = "PUBLIC";
                }
                if($scope.typeOfAudience=='2'){//student
                    if($scope.codigoRegistro!="" && $scope.codigoRegistro!=null ){
                        $scope.target = formatRegisterCode($scope.codigoRegistro,10);
                    }else{
                        $scope.alertError= 2;
                        $scope.messageAlert=" Ingrese un numero de registro.";
                    }

                }
                if($scope.typeOfAudience=='3'){//career
                    if($scope.selectedCareer!="" && $scope.selectedCareer!=null ){
                        $scope.target =  $scope.selectedCareer;
                    }else{
                        $scope.alertError= 2;
                        $scope.messageAlert=" Porfavor seleccione una Carrera.";
                    }
                }
                notificationsService.postNotification(hourToTimestamp, $scope.title, $scope.content, $scope.selectedTypeNotification, $scope.target, $scope.typeOfAudience, $scope.adminProfile, function (result) {
                    $scope.resultConsumtion = result;
                    if($scope.resultConsumtion.data.code == status_code.OK) {
                        $scope.alertError= 1;
                        $scope.messageAlert=" Notificacion enviada correctamente.";
                        cleanInputs();
                        Notifications();
                    }
                    if($scope.resultConsumtion.status == status_code || $scope.resultConsumtion.status==-1){
                        $scope.alertError= 2;
                        $scope.messageAlert=" Problema al enviar notificacion.";
                        cleanInputs();
                    }
                });

            }else{
                $scope.alertError= 2;
                $scope.messageAlert="  Porfavor Rellene todos los campos.";
            }

        }

        function cleanInputs() {
            $scope.typeOfAudience = "";
            $scope.codigoRegistro = "";
            $scope.title = "";
            $scope.content = "";
            $scope.target = "";
        }

        $scope.adminProfile = [];
        function getProfileAdmin() {
            $scope.adminProfile = [];
            let promise = adminProfileService.getAdminProfile();
            if(promise){
                promise.then(function (result) {
                    if (result.code == status_code.OK) {
                        $scope.adminProfile = result.data;
                    }
                })
            }
        }
    }

})();