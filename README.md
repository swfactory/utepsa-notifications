# Proyecto: Notificaciones UTEPSA
[Documentación](https://bitbucket.org/swfactory/utepsa-notifications/wiki/Home)

## Product Owner
- Carlos Draugialis <jefe.carrera.sistemas@utepsa.edu>

## Responsable
- Luis Roberto Pérez

## Equipo de desarrollo

- Luis Roberto Pérez <roberto.perez@swissbytes.ch>
- Timoteo Ponce <timoteo.ponce@swissbytes.ch>
- David Batista <david.batista.utepsa@gmail.com>
- Shigeo Tsukazan <shigeo.tsukazan.utepsa@gmail.com>
- Alexis Ardaya <alexis.ardaya.utepsa@gmail.com>
- Gerardo Figueroa <gerardo.figueroa.utepsa@gmail.com>
- Luana Chavez Añez <luana.chavez.utepsa@gmail.com>
- Cristian Alexis Burgos Mojica <cristian.burgos.utepsa@gmail.com>
- Lucas Suarez Blanco <lucas.suarez.utepsa@gmail.com>

## Requerimientos

- JDK 1.8
- Maven 3

## Ejecución

- Construir el proyecto
`$>cd server && mvn clean package`

- Ejecución
`$>java -jar target/notificationserver-1.0-SNAPSHOT.jar server config.yml`