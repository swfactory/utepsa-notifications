describe('Hola mundo', function () { 

	var msg = holaMundo();

	it('El mensaje no debe estar vacio', function () { 
		expect(msg).not.toBe(null);
		expect(msg).not.toBe("");
	}); 

	it('El mensaje debe ser Hola mundo', function () { 
		expect(msg).toBe("Hola mundo");
	}); 
}); 

