;(function() {

	angular.module("utepsa-notifications").factory("historyNotesService", historyNotesService);

	historyNotesService.$inject = ['$http','API','$localStorage'];

	function historyNotesService($http,API,$localStorage) {

		function getHistoryNotes(){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ $localStorage.currentUserWeb.student+'/historyNotes',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

		var service={
			getHistoryNotes: getHistoryNotes,
		};
		return service;
	}

})();