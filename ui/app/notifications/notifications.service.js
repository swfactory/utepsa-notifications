;(function() {

	angular.module("utepsa-notifications").factory("notificationsService", notificationsService);

	notificationsService.$inject = ['$http', 'API','$localStorage'];
	
	function notificationsService($http, API, $localStorage){

		function getNotifications(rows){
			return $http({
                method: 'GET',
                url: API.url + 'student/' + $localStorage.currentUserWeb.student +'/notifications/' + rows,
                withCredentials: true,
                headers: {
                'Content-Type': 'application/json; charset=utf-8'
                }
			}).then(function(result) {
			    return result.data;
			}).catch(function(error) {
                return error;
			});
		}

		var service = {
            getNotifications: getNotifications
		};

		return service;
	}

})();