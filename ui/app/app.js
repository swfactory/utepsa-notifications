;(function() {
  'use strict';

  angular.module("utepsa-notifications",['ngTouch','ui.router', 'ngStorage', 'infinite-scroll','angularMoment', 'angularUtils.directives.dirPagination']);

  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("utepsa-notifications").config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    $urlRouterProvider.otherwise('/');

    // routes
    $stateProvider
        .state('main', {
            abstract: true,
            templateUrl:'app/core/pages/main.html',
            data : {
              cssClassnames : 'hold-transition skin-red sidebar-mini layout-boxed fixed'
            }
        })
        .state('dashboard', {
            url: '/',
            parent: 'main',
            templateUrl: 'app/dashboard/dashboard.html',
        })
        .state('notifications', {
            url: '/notifications',
            parent: 'main',
            templateUrl: 'app/notifications/notifications.html',
            controller: 'NotificationsController'
        })
        .state('studentProfile', {
            url:'/studentProfile',
            parent: 'main',
            controller: 'StudentProfileController',
            templateUrl:'app/studentProfile/studentProfile.html',
        })
        .state('login', {
            url:'/login',
            controller: 'LoginController',
            controllerAs: 'LoginController',
            templateUrl:'app/core/pages/login.html',
            data : {
              cssClassnames : 'hold-transition login-page'
            }
        })
        .state('resetPassword', {
            url:'/resetPassword',
            controller: 'ResetPasswordController',
            controllerAs: 'ResetPasswordController',
            templateUrl:'app/auth/resetPassword/resetPassword.html',
            data : {
              cssClassnames : 'hold-transition login-page'
            }
        })
        .state('historyNotes', {
            url:'/historyNotes',
            parent: 'main',
            controller: 'HistoryNotesController',
            templateUrl:'app/historyNotes/historyNotes.html',
        })
        .state('documentsStudent', {
            url:'/documents',
            parent: 'main',
            controller: 'DocumentsStudentController',
            templateUrl:'app/documentsStudent/documentsStudent.html',
        })
        .state('coursesRegistered', {
            url:'/coursesRegistered',
            parent: 'main',
            controller: 'coursesRegisteredController',
            templateUrl:'app/currentSemester/currentSemester.html',
        })
        .state('financialState', {
            url:'/financialState',
            parent: 'main',
            controller:'financialStateController',
            templateUrl:'app/financialState/financialState.html',
        })
        .state('locaLibrary', {
            url:'/localLibrary',
            parent: 'main',
            controller:'localLibraryController',
            templateUrl:'app/localLibrary/localLibrary.html',
        })
        .state('virtualBooks', {
            url:'/virtualBooks',
            parent: 'main',
            controller:'virtualBooksController',
            templateUrl:'app/virtualBooks/virtualBooks.html',
        })
        .state('linksOfInterest', {
            url:'/linksOfInterest',
            parent: 'main',
            templateUrl:'app/core/pages/linksOfInterest.html',
        })
        .state('about', {
          url:'/about',
          parent: 'main',
          templateUrl:'app/core/pages/about.html'
        });
  }

    angular.module("utepsa-notifications").run(run);

    run.$inject = ['$rootScope', '$http','$location', '$state', '$localStorage'];

    function run($rootScope, $http, $location ,$state, $localStorage) {
        moment.lang('es', {
            monthsParseExact : true,
            weekdays : 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
            weekdaysShort : 'dom._lun._mar._mié._jue._vie._sáb.'.split('_'),
            weekdaysMin : 'do_lu_ma_mi_ju_vi_sá'.split('_'),
            weekdaysParseExact : true,
            longDateFormat : {
                LT : 'H:mm',
                LTS : 'H:mm:ss',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY H:mm',
                LLLL : 'dddd, D [de] MMMM [de] YYYY H:mm'
            },
            calendar : {
                sameDay : function () {
                    return '[hoy a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                nextDay : function () {
                    return '[mañana a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                nextWeek : function () {
                    return 'dddd [a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                lastDay : function () {
                    return '[ayer a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                lastWeek : function () {
                    return '[el] dddd [pasado a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                sameElse : 'L'
            },
            relativeTime : {
                future : 'en %s',
                past : 'hace %s',
                s : 'unos segundos',
                m : 'un minuto',
                mm : '%d minutos',
                h : 'una hora',
                hh : '%d horas',
                d : 'un día',
                dd : '%d días',
                M : 'un mes',
                MM : '%d meses',
                y : 'un año',
                yy : '%d años'
            },
            dayOfMonthOrdinalParse : /\d{1,2}º/,
            ordinal : '%dº',
            week : {
                dow : 1, // Monday is the first day of the week.
                doy : 4  // The week that contains Jan 4th is the first week of the year.
            }
        });
        document.addEventListener("backbutton", onBackKeyDown, false);
        function onBackKeyDown(e) {
            if($state.current.name !== 'dashboard'){
                e.preventDefault();
                hideAnyModals();
                $state.go('dashboard');
            }else{
                e.preventDefault();
                navigator.notification.confirm(
                    "Desea salir de la Aplicación ?",
                    onConfirm,
                    "",
                    ["NO","SI"]);
            }
        }

        function onConfirm(button) {
            if(button==2){
                navigator.app.exitApp();
            }else{
                $state.go('dashboard');
                return;
            }
        }

        // keep user logged in after page refresh
        if (localStorage.getItem("currentUserWeb") === null) {
            $state.transitionTo("login");
        }else {
            if ($localStorage.currentUserWeb) {
                //$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
            }
        }

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login'];

            var restrictedPage = publicPages.indexOf($location.path()) === -1;

            if (restrictedPage && !$localStorage.currentUserWeb) {
                $state.transitionTo("login");
                event.preventDefault();
            }

            try {
                if ($localStorage.tempUser) {
                    $state.go("resetPassword");
                    event.preventDefault();
                }
            }
            catch(e){

            }
        });
    }

})();