;(function () {
    angular.module('utepsa-notifications').factory('localLibraryService', localLibraryService);

    localLibraryService.$inject = ['$http', 'API'];

    function localLibraryService($http, API) {

        function getSearchBook(search) {
            return $http({
                method: 'GET',
                url: API.url + 'Book/search?fullName=' + search,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error.data;
            });
        }

        var service = {
            getSearchBook: getSearchBook
        }

        return service;
    }
})();
