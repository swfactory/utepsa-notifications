;(function() {

    angular.module("utepsa-notifications").controller('financialStateController', financialStateController);

    financialStateController.$inject = ['$scope', 'resizeTemplate', 'financialStateService' ,'status_code'];

    function financialStateController($scope, resizeTemplate, financialStateService, status_code) {
        $scope.itemsFinancialState=[];
        $scope.financialStateOfStudent=[];
        resizeTemplate.resize();
        getFinancialState();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerFinancialState= true;
        $scope.alertErrorFinancialState = false;

        function getFinancialState(){
            var promise = financialStateService.getFinancialStates();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsFinancialState = result;

                    $scope.spinnerFinancialState = false;
                    if ($scope.itemsFinancialState.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsFinancialState.data.code == status_code.OK) {
                        $scope.financialStateOfStudent = $scope.itemsFinancialState.data;
                    }
                    if ($scope.itemsFinancialState.data.code == status_code.NOT_FOUND) {
                        $scope.alertErrorFinancialState = true;
                    }
                    if ($scope.itemsFinancialState.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsFinancialState.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }
    }
})();
