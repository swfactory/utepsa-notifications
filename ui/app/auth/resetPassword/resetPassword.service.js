;(function() {

	angular.module("utepsa-notifications").factory("resetPasswordService", resetPasswordService);

    resetPasswordService.$inject = ['$http','API','$localStorage'];

	function resetPasswordService($http,API,$localStorage){

        function resetPassword(password,confirm_password,callback) {
            $http({
                method: 'PUT',
                url: API.url + 'credential/changePasswordForced',
                withCredentials: true,
                data:{id: $localStorage.tempUser.student, newPassword: password, confimNewPassword: confirm_password},
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                callback(result);
            })
            .catch(function(error) {
                callback(error);
            });
        }

		var service = {
            resetPassword: resetPassword
		};

		return service;
	}
})();