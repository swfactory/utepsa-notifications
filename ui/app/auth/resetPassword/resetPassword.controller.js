;(function() {

  angular.module("utepsa-notifications").controller('ResetPasswordController', ResetPasswordController);

    ResetPasswordController.$inject = ['resizeTemplate','resetPasswordService','$state','$scope','$localStorage','status_code'];

    function ResetPasswordController(resizeTemplate,resetPasswordService,$state,$scope, $localStorage,status_code) {
    resizeTemplate.resize();
    $scope.userTemp="";
    $scope.password = "";
    $scope.confirm_password="";
    $scope.messageAlert="";
    $scope.resetPasswords=resetPasswords;

    $scope.goToDashboard =function () {
        $state.go("dashboard");
        $('#mymodal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
    function resetPasswords(){
        $scope.spinnerResetPassword=true;
        $scope.alertError=false;
        resetPasswordService.resetPassword(CryptoJS.SHA512($scope.password).toString(), CryptoJS.SHA512($scope.password).toString(), function (result) {
            $scope.userTemp = result;
            $scope.spinnerResetPassword=false;
            if ($scope.userTemp.data.code == status_code.OK) {
                delete $localStorage.tempUser;
                $("#mymodal").modal("show");
                $("#mymodal").options({backdrop:'',keyboard:'false'});
            }
            if($scope.userTemp.data.code == status_code.NOT_MODIFIED){
                $scope.messageAlert="  No fue posible cambiar tu contraseña, porque la contraseña introducida es igual a la contraseña anterior.";
                $scope.alertError=true;
            }
            if($scope.userTemp.data.code == status_code.UNAUTHORIZED){
                $scope.messageAlert="  No estas autorizado para cambiar tu contraseña.";
                $scope.alertError=true;
            }
            if ($scope.userTemp.data.code == status_code.NOT_FOUND) {
                $scope.messageAlert ="  Usuario no existe.";
                $scope.alertError = true;
            }
            if ($scope.userTemp.data.code == status_code.INTERNAL_SERVER_ERROR) {
                $scope.messageAlert ="  Problema interno del servidor";
                $scope.alertError = true;
            }
            if ($scope.userTemp.status == status_code.INTERNAL_SERVER_ERROR) {
                $scope.messageAlert="  Problema interno del servidor";
                $scope.alertError = true;
            }
        });
    }
  }

})();