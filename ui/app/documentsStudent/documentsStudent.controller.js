;(function() {

    angular.module("utepsa-notifications").controller('DocumentsStudentController', DocumentsStudentController);

    DocumentsStudentController.$inject = ['$scope', 'resizeTemplate','documentsStudentService','status_code'];

    function DocumentsStudentController($scope, resizeTemplate, documentsStudentService,status_code) {
        $scope.documentsStudentItems = "";
        $scope.documentsStudent=[];
        resizeTemplate.resize();
        getDocumentsStudent();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerDocumentsStudent = true;
        $scope.mainDocumentsForStudentsBolivians = [];
        $scope.provisionalDocumentsForStudentsBolivians = [];
        $scope.isStudentBolivian = true;

        function getDocumentsStudent(){
            var promise = documentsStudentService.getDocumentsStudent();
            if(promise){
                promise.then(function (result) {
                    $scope.documentsStudentItems = result;
                    $scope.spinnerDocumentsStudent = false;
                    if ($scope.documentsStudentItems.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.code == status_code.OK) {
                        angular.forEach($scope.documentsStudentItems.data, function(item){
                            //Verifica si el estudiante es extranjero
                            if(item.document == 'PASAPORTE VIGENTE CON VISA CONSULAR DE ESTUDIANTE')
                                $scope.isStudentBolivian = false;
                            if(item.document == "FOTOCOPIA LEGALIZADA DEL TITULO DE BACHILLER")
                                $scope.mainDocumentsForStudentsBolivians.push(item);
                            if(item.document == "FOTOCOPIA  DE LA CEDULA DE IDENTIDAD")
                                $scope.mainDocumentsForStudentsBolivians.push(item);
                            if(item.document == "2 FOTOGRAFIAS DE 3 X 4 CM. COLOR (CON TRAJE FORMAL Y FONDO CLARO)")
                                $scope.mainDocumentsForStudentsBolivians.push(item);
                            if(item.document == "CERTIFICADO DE NACIMIENTO")
                                $scope.mainDocumentsForStudentsBolivians.push(item);
                            if((item.document != "FOTOCOPIA LEGALIZADA DEL TITULO DE BACHILLER" &&
                                item.document != "FOTOCOPIA  DE LA CEDULA DE IDENTIDAD" &&
                                item.document != "2 FOTOGRAFIAS DE 3 X 4 CM. COLOR (CON TRAJE FORMAL Y FONDO CLARO)" &&
                                item.document != "CERTIFICADO DE NACIMIENTO") && item.state == true)
                                $scope.provisionalDocumentsForStudentsBolivians.push(item);
                            $scope.documentsStudent.push(item);
                        });
                    }
                    if ($scope.documentsStudentItems.code == status_code.NOT_FOUND) {
                        $scope.messageAlert = "  Estudiante no encontrado";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        $scope.numberOfMainDocumentsDelivered = function () {
            var total = 0;
            angular.forEach($scope.mainDocumentsForStudentsBolivians, function (item) {
                if(item.state == true)
                    total++;
            });
            return total
        };

        $scope.numberDocumentsDelivered = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                if(item.state==true){
                    total ++;
                }
            });
            return total;
        };

        $scope.numberDocuments = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                total ++;
            });
            return total;
        };

        $scope.percentageOfDocumentsForStudentsBolivians = function () {
            var total = parseFloat(100/$scope.mainDocumentsForStudentsBolivians.length*$scope.numberOfMainDocumentsDelivered());
            return total.toFixed(2);
        };

        $scope.percentageOfDocuments = function(){
            var total = parseFloat(100/$scope.numberDocuments()*$scope.numberDocumentsDelivered());
            return total.toFixed(2);
        };
    }

})();