;(function() {

    angular.module("utepsa-notifications").factory("coursesRegisteredService", coursesRegisteredService);

    coursesRegisteredService.$inject = ['$http','API','$localStorage'];

    function coursesRegisteredService($http,API,$localStorage) {

        function getCoursesRegistered(){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ $localStorage.currentUserWeb.student+'/CourseRegister',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function getCurrentSemester(){
            return $http({
                method: 'GET',
                url: API.url+'CurrentSemester',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        var service={
            getCoursesRegistered: getCoursesRegistered,
            getCurrentSemester:getCurrentSemester
        };
        return service;
    }

})();