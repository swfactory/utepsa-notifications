;(function() {

    angular.module("utepsa-notifications").controller('coursesRegisteredController', coursesRegisteredController);

    coursesRegisteredController.$inject = ['$scope', 'resizeTemplate', 'coursesRegisteredService','status_code'];

    function coursesRegisteredController($scope, resizeTemplate, coursesRegisteredService,status_code) {
        $scope.itemsCoursesRegistered=[];
        $scope.coursesRegistered=[];
        $scope.itemsCurrentSemester=[];
        $scope.currentSemester=[];
        resizeTemplate.resize();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.currentModule=[];
        $scope.spinnerCoursesRegistered = true;
        $scope.spinnerMatter=false;

        CourseRegistered();
        function CourseRegistered(){
            var promise = coursesRegisteredService.getCoursesRegistered();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsCoursesRegistered = result;
                    $scope.spinnerCoursesRegistered = false;
                    $scope.spinnerMatter=true;
                    if ($scope.itemsCoursesRegistered.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.OK) {
                        $scope.spinnerMatter = false;
                        $scope.coursesRegistered =$scope.itemsCoursesRegistered;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.NO_CONTENT) {
                        $scope.messageAlert = "  No tienes materias programadas para este semestre";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        CurrentSemester();
        function CurrentSemester(){
            var promise = coursesRegisteredService.getCurrentSemester();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsCurrentSemester = result;
                    $scope.spinnerCoursesRegistered = false;
                    $scope.spinnerMatter= true;
                    if ($scope.itemsCurrentSemester.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCurrentSemester.data.code == status_code.OK) {
                        $scope.spinnerMatter = false;
                        $scope.currentSemester =$scope.itemsCurrentSemester.data;
                    }
                    if ($scope.itemsCurrentSemester.data.code == status_code.NO_CONTENT) {
                        $scope.messageAlert = "  Semestre actual no encontrado.";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCurrentSemester.data.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCurrentSemester.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }
    }
})();