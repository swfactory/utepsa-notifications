package com.utepsa.adapters.weather;

import com.utepsa.adapters.weather.Weather;
import com.utepsa.adapters.weather.WeatherAdapter;
import com.utepsa.config.ExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by roberto on 24/8/2016.
 */
public class WeatherAdapterTest {

    private WeatherAdapter adapter;

    @Before
    public void setUp() {
        ExternalServer weatherServer = new ExternalServer();
        weatherServer.setName("Weather");
        weatherServer.setUrl("http://api.apixu.com/v1/current.json");
        weatherServer.setKey("be57df18f3ae419581d224836162408");

        this.adapter = new WeatherAdapter(weatherServer);
    }

    @Test
    public void testGetWeatherForBolivia() throws IOException {
        String country = "Bolivia";
        Weather boliviaWeather = this.adapter.getLatestWeatherFor(country);
        Assert.assertEquals(boliviaWeather.getLocation().getCountry(), country);
    }
}
