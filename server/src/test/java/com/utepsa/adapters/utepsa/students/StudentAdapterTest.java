package com.utepsa.adapters.utepsa.students;

import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.weather.Weather;
import com.utepsa.config.ExternalServer;
import com.utepsa.config.NotificationServerConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by david on 4/10/16.
 */
public class StudentAdapterTest {
    private StudentAdapter studentAdapter;

    @Before
    public void SetUp() {
        ExternalServer externalServer = new ExternalServer();
        externalServer.setName("Utepsa");
        externalServer.setUrl("http://190.171.202.86/api/");

        this.studentAdapter = new StudentAdapter(externalServer);
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.studentAdapter.isServerUp());
    }

    @Test
    public void testGetAllStudents() throws IOException {
        List<StudentData> listStudents= this.studentAdapter.getAllStudents();

        Assert.assertNotNull(listStudents);
    }
}
