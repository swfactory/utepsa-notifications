package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.models.File;
import com.utepsa.config.NotificationServerConfig;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shigeots on 10-11-16.
 */
public class GetFileAcceptanceTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void requestGetAllFiles() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET All Files");

        Response response = client.target(
                String.format("http://localhost:%s/api/file", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void createAnfileWithPOST() throws Exception {

        /*File file = new File(1,"Guia estudiantil", "pdf", "http://utepsa.edu/guiamap.pdf", "01/01/2016",true);
        Entity n = Entity.json(file);

        JerseyClientConfiguration config = new JerseyClientConfiguration();
        config.setTimeout(Duration.seconds(20));

        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test createFile file");

        Response response = client
                .target(String.format("http://localhost:%d/api/file", RULE.getLocalPort()))
                .request()
                .post(Entity.json(file));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);*/
    }

    @Test
    public void requestGetFilesAvailable() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET All Files available");

        Response response = client.target(
                String.format("http://localhost:%s/api/file/available", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public  void requestAfiletWithValidIdWithGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test get file with valid id");

        Response response = client.target(
                String.format("http://localhost:%d/api/file/2", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public  void requestAfiletWithInvalidIdWithGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test get file with invalid id");

        Response response = client.target(
                String.format("http://localhost:%d/api/file/1000000000", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }
}
