package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.models.Course;
import com.utepsa.config.NotificationServerConfig;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 10-08-16.
 */
public class GetCourseAcceptanceTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void requestFindExistingCourse() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test get course with valid course code, career code and pensum code");

        Response response = client.target(
                String.format("http://localhost:%d/api/courses/00003/001/01", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void requestFindNotExistingCourse() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test get not existing course");

        Response response = client.target(
                String.format("http://localhost:%d/api/courses/00003/001/0asd", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    public void requestCreateCourse() {
        /*Course course = new Course(2, "00003", "AAD-140", "ADMINISTRACION 140", "001","02");
        Entity n = Entity.json(course);

        JerseyClientConfiguration config = new JerseyClientConfiguration();
        config.setTimeout(Duration.seconds(20));

        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test create course");

        Response response = client
                .target(String.format("http://localhost:%d/api/courses", RULE.getLocalPort()))
                .request()
                .post(Entity.json(course));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);*/
    }

    @Test
    public void requestGetCoursesByCareerAndPensum() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET All Courses by careerCode and pensum code");

        Response response = client.target(
                String.format("http://localhost:%s/api/courses/001/01", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }
}
