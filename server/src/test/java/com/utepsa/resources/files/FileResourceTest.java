package com.utepsa.resources.files;

import com.utepsa.models.File;
import com.utepsa.db.file.FileDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by shigeots on 11-11-16.
 */
public class FileResourceTest {
    /*private final static FileDAO fileDAO = new FileFakeDAO();

    private final static FileService fileservice = new FileService(fileDAO);

    @ClassRule
    public static final ResourceTestRule resouces = ResourceTestRule.builder()
            .addResource(new FileResource(fileservice))
            .build();

    private File file;
    private List<File> files;

    @Before
    public void setup() {
        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        files.add(new File(2,"Guia universitaria","pdf","https://utepsa.edu/guiamap2.pdf","09/09/2016",false));

        files.forEach(file -> this.fileDAO.createFile(file));
    }

    @Test
    public void getAllFilesWithGET() {
        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        files.add(new File(2,"Guia universitaria","pdf","https://utepsa.edu/guiamap2.pdf","09/09/2016",false));
        List<File> response = resouces.client().target("/file").request().get(new GenericType<List<File>>(){});
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(this.files);
    }

    @Test
    public void createFileWithPOST() {
        this.file = new File(3,"Guia Carrera","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",false);
        Response response = resouces.client().target("/file").request().post(Entity.json(this.file));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @Test
    public void getFilesAvailableWithGET() {
        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        List<File> response = resouces.client().target("/file/available").request().get(new GenericType<List<File>>(){});
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(this.files);
    }

    @Test
    public void findByIdWithGET() {
        File request = resouces.client().target("/file/1").request().get(File.class);
        assertThat(request).isEqualTo(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
    }

    @Test
    public void findByInvalidIdWithGET() {
        File request = resouces.client().target("/file/99999999999").request().get(File.class);
        assertThat(request).isNull();
    }*/
}
