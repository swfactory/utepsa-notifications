package com.utepsa.resources.files;

import com.utepsa.models.File;
import com.utepsa.db.file.FileDAO;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shigeots on 10-11-16.
 */
public class FileServiceTest {
    /*private FileDAO fileDAO;
    private FileService fileService;

    private File file;
    private List<File> files;

    @Before
    public void setup() {
        this.fileDAO = new FileFakeDAO();
        this.fileService = new FileService(fileDAO);

        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        files.add(new File(2,"Guia universitaria","pdf","https://utepsa.edu/guiamap2.pdf","09/09/2016",false));

        files.forEach(file -> this.fileDAO.createFile(file));
    }

    @Test
    public void getAllFiles() {
        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        files.add(new File(2,"Guia universitaria","pdf","https://utepsa.edu/guiamap2.pdf","09/09/2016",false));
        assertThat(fileService.getAllFiles()).isEqualTo(this.files);
    }

    @Test
    public void createFile() {
        this.file = new File(3,"Guia universitaria","pdf","https://utepsa.edu/guiamap2.pdf","09/09/2016",false);
        assertThat(fileService.createFile(this.file)).isNotNull();
    }

    @Test
    public void getFilesAvailables() {
        this.files = new ArrayList<>();
        files.add(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
        assertThat(fileService.getFilesAvailable()).isEqualTo(this.files);
    }

    @Test
    public void getFileById() {
        assertThat(fileService.findById(1)).isEqualTo(new File(1,"Guia estudiantil","pdf","https://utepsa.edu/guiamap1.pdf","09/09/2016",true));
    }

    @Test
    public void getFileByInvalidId() {
        assertThat(fileService.findById(1000000000)).isNull();
    }*/
}
