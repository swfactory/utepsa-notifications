package com.utepsa.resources.courses;

import com.utepsa.models.Course;
//import com.utepsa.db.courses.CourseDAO;
//import com.utepsa.db.courses.CourseFakeDAO;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Roberto Perez on 10-08-16.
 */
public class CourseServiceTest {
    /*
    private CourseDAO dao;
    private CoursesService coursesService;

    private Course course;
    private List<Course> courses;

    @Before
    public void setup() {
        this.dao = new CourseFakeDAO();
        this.coursesService = new CoursesService(dao);

        this.courses = new ArrayList<>();
        courses.add(new Course(1, "00003", "AAD-140", "ADMINISTRACION 140", "001","03"));
        courses.add(new Course(2, "00003", "AAD-140", "ADMINISTRACION 140", "002","02"));
        courses.add(new Course(3, "00003", "AAD-140", "ADMINISTRACION 140", "002","03"));

        courses.forEach(course -> this.dao.create(course));
    }

    @Test
    public void getExistingCourse() {
        this.course = new Course(2, "00003", "AAD-140", "ADMINISTRACION 140", "002","02");
        assertThat(this.coursesService.findCourse("00003","002","02")).isEqualTo(this.course).isNotNull();
    }

    @Test
    public void getNotExistingCourse() {
        assertThat(this.coursesService.findCourse("0000asd","000000","052222")).isNull();
    }

    @Test
    public void create() {
        this.course = new Course(4, "00003", "AAD-140", "ADMINISTRACION 140", "003","01");
        assertThat(this.coursesService.create(this.course)).isNotNull();
    }

    @Test
    public void getCoursesByCareerAndPensum() {
        this.courses = new ArrayList<>();
        courses.add(new Course(1, "00003", "AAD-140", "ADMINISTRACION 140", "001","03"));
        assertThat(this.coursesService.getCoursesByCareerAndPensum("001", "03")).isEqualTo(this.courses).isNotNull();
    }
    */
}
