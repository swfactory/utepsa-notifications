package com.utepsa.api.students;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Student;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class StudentTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Student student;

    @Before
    public void setUp() throws Exception {
        //this.student = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/student.json"), Student.class));

        assertThat(MAPPER.writeValueAsString(student)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/student.json"),
                Student.class)).isEqualTo(student);
    }
}
