package com.utepsa.api.files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.File;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shigeots on 10-11-16.
 */
public class FileTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private File file;

    @Before
    public void setup() throws Exception {
        //this.file = new File(1,"Guia estudiantil", "pdf", "http://utepsa.edu/guiamap.pdf", "01/01/2016",true);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/file.json"), File.class));

        assertThat(MAPPER.writeValueAsString(file)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/file.json"), File.class))
                .isEqualTo(this.file);
    }
}
