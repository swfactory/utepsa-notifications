--insert data student
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('0000376522','Shigeo','Tsukazan','Shiroma','26/01/1993','CI','6345872',77055568,3376153,'101','0000376522','shigeots','12345678','stsukazan.est.@utepsa.edu','');
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('000046885A', 'Geraldo', 'Figueroa','Zurita','19/02/1997','CI','9746660',78590523,3536889,'101','000046885A','geraldofz','12345678','gfigueroa.est@utepsa.edu', '');

--insert data notification
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Primera noticia', 'Esta es la primera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Segunda noticia', 'Esta es la segunda noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Tercera noticia', 'Esta es la tercera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Cuarta noticia', 'Esta es la Cuarta noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Quinta noticia', 'Esta es la Quinta noticia del sistema.', 'NOTICE');

--insert data audience
INSERT INTO Audience (notification_id, target, type) VALUES (1,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (2,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (3,'374201', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (4,'123123', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (5,'374201', 'STUDENT');

--insert data courses
INSERT INTO Course (id,courseCode, initials, semester, careerCode, pensumCode) VALUES (1,'00003','AAD-140','ADMINISTRACION 140','001','01');
INSERT INTO Course (id,courseCode, initials, semester, careerCode, pensumCode) VALUES (2,'00003','AAD-140','ADMINISTRACION 140','001','02');
INSERT INTO Course (id,courseCode, initials, semester, careerCode, pensumCode) VALUES (3,'00003','AAD-140','ADMINISTRACION 140','002','01');
INSERT INTO Course (id,courseCode, initials, semester, careerCode, pensumCode) VALUES (4,'00004','CAL-100','CALCULO 100','001','01');
INSERT INTO Course (id,courseCode, initials, semester, careerCode, pensumCode) VALUES (5,'00005','ORP-120','ORGANIZACION PERSONAL 120','001','01');

--Insert Data Careers
INSERT INTO Career (careerCode, semester, pensum, faculty) VALUES ('100', 'Ing. de Sistemas', '01', 'FECT');

--Insert Data File
INSERT INTO File (id, name, type, url, date, state) VALUES (1,'Guia estudiantil', 'pdf', 'http://utepsa.edu/guia_estudiantil.pdf', '08/08/2016', true);
INSERT INTO File (id, name, type, url, date, state) VALUES (2,'Guia universitaria', 'pdf', 'http://utepsa.edu/guia_universitaria.pdf', '08/08/2016', false);

--Insert Data studentsRegisters
INSERT INTO StudentRegister (fullName, registerCode, documentCode) VALUES ('David Batista', '0000403201', '9746661');
INSERT INTO StudentRegister (fullName, registerCode, documentCode) VALUES ('Cristian Burgos', '0000403120', '999446');
