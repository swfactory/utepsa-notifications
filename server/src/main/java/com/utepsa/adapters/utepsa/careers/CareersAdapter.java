package com.utepsa.adapters.utepsa.careers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 09/11/2016.
 */
public class CareersAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public CareersAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/Career").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String careerCode) {
        return String.format("%sCareer/%s",
                this.externalServer.getUrl(),
                careerCode);
    }

    public List<CareersDATA> getAllCareers() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<CareersDATA> careers;
            careers = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<CareersDATA>>(){});

            return careers;
        }
        return null;
    }

    public CareersDATA getCareersByCareerCode(String careerCode) throws IOException {
        String url = this.buildRequestUrl(careerCode);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            CareersDATA career = MAPPER.readValue(response.getEntity().getContent(), CareersDATA.class);
            return career;
        }

        return null;
    }
}
