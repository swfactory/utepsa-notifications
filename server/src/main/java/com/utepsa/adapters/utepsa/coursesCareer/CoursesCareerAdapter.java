package com.utepsa.adapters.utepsa.coursesCareer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by David-SW on 10/03/2017.
 */
public class CoursesCareerAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public CoursesCareerAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/Pensum").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String careerCode) {
        return String.format("%sPensum/%s",
                this.externalServer.getUrl(),
                careerCode);
    }

    public List<CoursesCareerData> getAll() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<CoursesCareerData> careers;
            careers = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<CoursesCareerData>>(){});

            return careers;
        }
        return null;
    }
}
