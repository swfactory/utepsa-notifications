package com.utepsa.adapters.utepsa.students;

/**
 * Created by david on 4/10/16.
 */
public class StudentData {

    private String agd_codigo;
    private String agd_appaterno;
    private String agd_apmaterno;
    private String agd_nombres;
    private String agd_fechanac;
    private String agd_docid;
    private String agd_docnro;
    private String agd_telf1;
    private String agd_telf2;
    private String crr_codigo;
    private String alm_registro;
    private String usr_login;
    private String clave;
    private String correo;
    private String agd_sexo;
    private String pns_codigo;
    private String estado;

    public StudentData() {
    }

    public String getAgd_codigo() {
        return agd_codigo;
    }

    public void setAgd_codigo(String agd_codigo) {
        this.agd_codigo = agd_codigo;
    }

    public String getAgd_appaterno() {
        return agd_appaterno;
    }

    public void setAgd_appaterno(String agd_appaterno) {
        this.agd_appaterno = agd_appaterno;
    }

    public String getAgd_apmaterno() {
        return agd_apmaterno;
    }

    public void setAgd_apmaterno(String agd_apmaterno) {
        this.agd_apmaterno = agd_apmaterno;
    }

    public String getAgd_nombres() {
        return agd_nombres;
    }

    public void setAgd_nombres(String agd_nombres) {
        this.agd_nombres = agd_nombres;
    }

    public String getAgd_fechanac() {
        return agd_fechanac;
    }

    public void setAgd_fechanac(String agd_fechanac) {
        this.agd_fechanac = agd_fechanac;
    }

    public String getAgd_docid() {
        return agd_docid;
    }

    public void setAgd_docid(String agd_docid) {
        this.agd_docid = agd_docid;
    }

    public String getAgd_docnro() {
        return agd_docnro;
    }

    public void setAgd_docnro(String agd_docnro) {
        this.agd_docnro = agd_docnro;
    }

    public String getAgd_telf1() {
        return agd_telf1;
    }

    public void setAgd_telf1(String agd_telf1) {
        this.agd_telf1 = agd_telf1;
    }

    public String getAgd_telf2() {
        return agd_telf2;
    }

    public void setAgd_telf2(String agd_telf2) {
        this.agd_telf2 = agd_telf2;
    }

    public String getCrr_codigo() {
        return crr_codigo;
    }

    public void setCrr_codigo(String crr_codigo) {
        this.crr_codigo = crr_codigo;
    }

    public String getAlm_registro() {
        return alm_registro;
    }

    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }

    public String getUsr_login() {
        return usr_login;
    }

    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getAgd_sexo() {
        return agd_sexo;
    }

    public void setAgd_sexo(String agd_sexo) {
        this.agd_sexo = agd_sexo;
    }

    public String getPns_codigo() {
        return pns_codigo;
    }

    public void setPns_codigo(String pns_codigo) {
        this.pns_codigo = pns_codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
