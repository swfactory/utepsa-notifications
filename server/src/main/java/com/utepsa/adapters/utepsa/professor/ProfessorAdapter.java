package com.utepsa.adapters.utepsa.professor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by Luana Chavez on 14/03/2017.
 */
public class ProfessorAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public ProfessorAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/Docente").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String registerCode) {
        return String.format("%s/Docente/%s",
                this.externalServer.getUrl(),
                registerCode);
    }

    public List<ProfessorData> getProfessors() throws IOException {
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<ProfessorData> professor;
            professor = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<ProfessorData>>(){});
            return professor;
        }
        return null;
    }

    public ProfessorData getProfessor(String registerCode) throws IOException {
        String url = this.buildRequestUrl(registerCode);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            ProfessorData professor = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<ProfessorData>(){});
            return professor;
        }
        return null;
    }
}
