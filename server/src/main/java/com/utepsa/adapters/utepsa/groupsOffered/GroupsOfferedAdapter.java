package com.utepsa.adapters.utepsa.groupsOffered;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by Gerardo on 25/04/2017.
 */
public class GroupsOfferedAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public GroupsOfferedAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/GruposOfertados/?semestre=2017-1").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String semester) {
        return String.format("%sGruposOfertados/?semestre=%s",
                this.externalServer.getUrl(),
                semester);
    }

    public List<GroupsOfferedData> getAllGroupsOffered(String semester) throws IOException{
        String url = this.buildRequestUrl(semester);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<GroupsOfferedData> groupsOfferedDatas;

            groupsOfferedDatas = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<GroupsOfferedData>>(){});

            return groupsOfferedDatas;
        }
        return null;
    }
}
