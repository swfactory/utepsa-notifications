package com.utepsa.adapters.utepsa.financialState;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Gerardo on 28/03/2017.
 */

public class FinancialStateData {
    @JsonProperty
    private String agd_codigo;
    @JsonProperty
    private double SaldoDeudor;
    @JsonProperty
    private double saldoenMora;
    @JsonProperty
    private double saldoaVencer;
    @JsonProperty
    private String FechaAVencer;

    public FinancialStateData(){

    }

    public FinancialStateData(String agd_codigo, double SaldoDeudor, double saldoenMora, double saldoaVencer, String FechaAVencer){
        this.agd_codigo = agd_codigo;
        this.SaldoDeudor = SaldoDeudor;
        this.saldoenMora = saldoenMora;
        this.saldoaVencer = saldoaVencer;
        this.FechaAVencer = FechaAVencer;
    }

    public String getAgd_codigo() {
        return agd_codigo;
    }

    public void setAgd_codigo(String agd_codigo) {
        this.agd_codigo = agd_codigo;
    }

    public double getSaldoDeudor() {
        return SaldoDeudor;
    }

    public void setSaldoDeudor(double saldoDeudor) {
        SaldoDeudor = saldoDeudor;
    }

    public double getSaldoenMora() {
        return saldoenMora;
    }

    public void setSaldoenMora(double saldoenMora) {
        this.saldoenMora = saldoenMora;
    }

    public double getSaldoaVencer() {
        return saldoaVencer;
    }

    public void setSaldoaVencer(double saldoaVencer) {
        this.saldoaVencer = saldoaVencer;
    }

    public String getFechaAVencer() {
        return FechaAVencer;
    }

    public void setFechaAVencer(String fechaAVencer) {
        this.FechaAVencer = fechaAVencer;
    }

    @Override
    public String toString() {
        return "FinancialStateData{" +
                "agd_codigo='" + agd_codigo + '\'' +
                ", SaldoDeudor=" + SaldoDeudor +
                ", saldoenMora=" + saldoenMora +
                ", saldoaVencer=" + saldoaVencer +
                ", FechaAVencer='" + FechaAVencer + '\'' +
                '}';
    }
}
