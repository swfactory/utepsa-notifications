package com.utepsa.adapters.utepsa.professor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Luana Chavez on 14/03/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfessorData {
    private String agd_codigo;
    private String agd_nombres;
    private String agd_appaterno;
    private String agd_apmaterno;

    public ProfessorData() {
    }

    public ProfessorData(String agd_codigo, String agd_nombres, String agd_appaterno, String agd_apmaterno) {
        this.agd_codigo = agd_codigo;
        this.agd_nombres = agd_nombres;
        this.agd_appaterno = agd_appaterno;
        this.agd_apmaterno = agd_apmaterno;
    }

    public String getAgd_codigo() {
        return agd_codigo;
    }

    public void setAgd_codigo(String agd_codigo) {
        this.agd_codigo = agd_codigo;
    }

    public String getAgd_nombres() {
        return agd_nombres;
    }

    public void setAgd_nombres(String agd_nombres) {
        this.agd_nombres = agd_nombres;
    }

    public String getAgd_appaterno() {
        return agd_appaterno;
    }

    public void setAgd_appaterno(String agd_appaterno) {
        this.agd_appaterno = agd_appaterno;
    }

    public String getAgd_apmaterno() {
        return agd_apmaterno;
    }

    public void setAgd_apmaterno(String agd_apmaterno) {
        this.agd_apmaterno = agd_apmaterno;
    }
}
