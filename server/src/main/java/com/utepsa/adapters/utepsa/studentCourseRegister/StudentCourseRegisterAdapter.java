package com.utepsa.adapters.utepsa.studentCourseRegister;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
public class StudentCourseRegisterAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public StudentCourseRegisterAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(100000).build();
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/StudentGroup/?registro=0000399654&semestre=2017-1").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String registerNumber,String semester) {
        return String.format("%s/StudentGroup/?registro=%s&semestre=%s",
                this.externalServer.getUrl(),
                registerNumber,
                semester);
    }

    private String buildRequestUrl(String semester) {
        return String.format("%sStudentGroup/?semestre=%s",
                this.externalServer.getUrl(),
                semester);
    }
    private String buildRequestUrl(String semester, boolean activo) {
        return String.format("%sStudentGroup/?semestre=%s&activo=true",
                this.externalServer.getUrl(),
                semester);
    }

    public List<StudentCourseRegisterData> getCourseRegisterByRegisterNumberAndSemester(String registerNumber, String semester) throws IOException{
        String url = this.buildRequestUrl(registerNumber,semester);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<StudentCourseRegisterData> studentCourseRegisterDatas;
            studentCourseRegisterDatas = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<StudentCourseRegisterData>>(){});

            return studentCourseRegisterDatas;
        }
        return null;
    }

    public  List<StudentCourseRegisterData> getStudentRegisteredBySemester( String semester) throws IOException {
        String url = this.buildRequestUrl(semester);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<StudentCourseRegisterData> studentsRegisters;
            studentsRegisters = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<StudentCourseRegisterData>>(){});

            return studentsRegisters;
        }
        return null;
    }

    public List<String> getActiveStudents(String semester) throws IOException {
        String url = this.buildRequestUrl(semester, true);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<String> studentsRegisters;
            studentsRegisters = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<String>>(){});

            return studentsRegisters;
        }
        return null;
    }
}
