package com.utepsa.adapters.utepsa.averageStudent;

/**
 * Created by Gerardo on 01/06/2017.
 */
public class AverageStudentDATA {

    private String alm_registro;
    private double promedio;
    private double promedio_ult_semestre;

    public AverageStudentDATA() {
    }

    public AverageStudentDATA(String alm_registro, double promedio, double promedio_ult_semestre) {
        this.alm_registro = alm_registro;
        this.promedio = promedio;
        this.promedio_ult_semestre = promedio_ult_semestre;
    }

    public String getAlm_registro() {
        return alm_registro;
    }

    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public double getPromedio_ult_semestre() {
        return promedio_ult_semestre;
    }

    public void setPromedio_ult_semestre(double promedio_ult_semestre) {
        this.promedio_ult_semestre = promedio_ult_semestre;
    }
}
