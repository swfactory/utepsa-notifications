package com.utepsa.adapters.utepsa.groupsOffered;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Gerardo on 25/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupsOfferedData {

    private String cod_facultad;
    private String semestre;
    private String modulo;
    private String mat_codigo;
    private String grupo;
    private String docente;
    private int grp_nroinscritos;
    private String aul_codigo;
    private String tur_codigo;

    public GroupsOfferedData(){

    }

    public GroupsOfferedData(String cod_facultad, String semestre, String modulo, String mat_codigo, String grupo, String docente,
                             int grp_nroinscritos, String aul_codigo, String tur_codigo){
        this.cod_facultad = cod_facultad;
        this.semestre = semestre;
        this.modulo = modulo;
        this.mat_codigo = mat_codigo;
        this.grupo = grupo;
        this.docente = docente;
        this.grp_nroinscritos = grp_nroinscritos;
        this.aul_codigo = aul_codigo;
        this.tur_codigo = tur_codigo;
    }

    public String getCod_facultad() {
        return cod_facultad;
    }

    public void setCod_facultad(String cod_facultad) {
        this.cod_facultad = cod_facultad;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getMat_codigo() {
        return mat_codigo;
    }

    public void setMat_codigo(String mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public int getGrp_nroinscritos() {
        return grp_nroinscritos;
    }

    public void setGrp_nroinscritos(int grp_nroinscritos) {
        this.grp_nroinscritos = grp_nroinscritos;
    }

    public String getAul_codigo() {
        return aul_codigo;
    }

    public void setAul_codigo(String aul_codigo) {
        this.aul_codigo = aul_codigo;
    }

    public String getTur_codigo() {
        return tur_codigo;
    }

    public void setTur_codigo(String tur_codigo) {
        this.tur_codigo = tur_codigo;
    }

    @Override
    public String toString() {
        return "GroupsOfferedData{" +
                "cod_facultad='" + cod_facultad + '\'' +
                ", semestre='" + semestre + '\'' +
                ", modulo='" + modulo + '\'' +
                ", mat_codigo='" + mat_codigo + '\'' +
                ", grupo='" + grupo + '\'' +
                ", docente='" + docente + '\'' +
                ", grp_nroinscritos='" + grp_nroinscritos + '\'' +
                ", aul_codigo='" + aul_codigo + '\'' +
                ", tur_codigo='" + tur_codigo + '\'' +
                '}';
    }
}
