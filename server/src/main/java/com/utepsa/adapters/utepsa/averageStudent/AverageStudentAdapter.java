package com.utepsa.adapters.utepsa.averageStudent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by Gerardo on 01/06/2017.
 */
public class AverageStudentAdapter {

    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public AverageStudentAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/estudiantespromedios").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String registerNumber) {
        return String.format("%sestudiantespromedios/%s",
                this.externalServer.getUrl(),
                registerNumber);
    }

    public List<AverageStudentDATA> getAllAverageStudent() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<AverageStudentDATA> averageStudentDATAS;
            averageStudentDATAS = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<AverageStudentDATA>>(){});

            return averageStudentDATAS;
        }
        return null;
    }

    public AverageStudentDATA getByRegisterNumber(String registerNumber) throws IOException {
        String url = this.buildRequestUrl(registerNumber);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            AverageStudentDATA averageStudentDATA = MAPPER.readValue(response.getEntity().getContent(), AverageStudentDATA.class);
            return averageStudentDATA;
        }

        return null;
    }
}
