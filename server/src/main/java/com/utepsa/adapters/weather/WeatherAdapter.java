package com.utepsa.adapters.weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

/*
 * Created by roberto on 24/8/2016.
 *
 * This adapter gets information from a public REST API
 * API KEY for Luis Roberto be57df18f3ae419581d224836162408
 * https://apixu.com
 * Example: http://api.apixu.com/v1/current.json?key=be57df18f3ae419581d224836162408&q=Bolivia
 */

public class WeatherAdapter {

    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public WeatherAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()).getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    public Weather getLatestWeatherFor(String country) throws IOException {

        HttpResponse response = this.doRequestTo(this.buildRequestUrl(country));

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            Weather weather = MAPPER.readValue(response.getEntity().getContent(), Weather.class);
            return weather;
        }

        return null;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    private String buildRequestUrl(String country) {
        return String.format("%s?key=%s&q=%s",
                this.externalServer.getUrl(),
                this.externalServer.getKey(),
                country);
    }
}
