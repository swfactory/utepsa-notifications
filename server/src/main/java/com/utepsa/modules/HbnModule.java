package com.utepsa.modules;

import com.google.inject.AbstractModule;
import com.utepsa.config.HbnBundle;
import com.utepsa.db.Profile.ProfileDAO;
import com.utepsa.db.Profile.ProfileRealDAO;
import com.utepsa.db.academicArea.AcademicAreaDAO;
import com.utepsa.db.academicArea.AcademicAreaRealDAO;
import com.utepsa.db.academicEvent.AcademicEventDAO;
import com.utepsa.db.academicEvent.AcademicEventRealDAO;
import com.utepsa.db.areaOfVirtualBooks.AreaOfVirtualBooksDAO;
import com.utepsa.db.areaOfVirtualBooks.AreaOfVirtualBooksRealDAO;
import com.utepsa.db.book.BookDAO;
import com.utepsa.db.book.BookRealDAO;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.career.CareerRealDAO;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.careerCourse.CareerCourseRealDAO;
import com.utepsa.db.categoryNotification.CategoryNotificationDAO;
import com.utepsa.db.categoryNotification.CategoryNotificationRealDAO;
import com.utepsa.db.census.CensusDAO;
import com.utepsa.db.census.CensusRealDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.course.CourseRealDAO;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.credential.CredentialRealDAO;
import com.utepsa.db.credentialAdministrator.CredentialAdministratorDAO;
import com.utepsa.db.credentialAdministrator.CredentialAdministratorRealDAO;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.db.currentSemester.CurrentSemesterRealDAO;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.document.DocumentRealDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.documentStudent.DocumentStudentRealDAO;
import com.utepsa.db.eventCareer.EventCareerDAO;
import com.utepsa.db.eventCareer.EventCareerRealDAO;
import com.utepsa.db.eventCourse.EventCourseDAO;
import com.utepsa.db.eventCourse.EventCourseRealDAO;
import com.utepsa.db.eventType.EventTypeDAO;
import com.utepsa.db.eventType.EventTypeRealDAO;
import com.utepsa.db.executiveReport.ExecutiveReportDAO;
import com.utepsa.db.executiveReport.ExecutiveReportRealDAO;
import com.utepsa.db.featuredStudent.FeaturedStudentDAO;
import com.utepsa.db.featuredStudent.FeaturedStudentRealDAO;
import com.utepsa.db.file.FileDAO;
import com.utepsa.db.file.FileRealDAO;
import com.utepsa.db.fileCareer.FileCareerDAO;
import com.utepsa.db.fileCareer.FileCareerRealDAO;
import com.utepsa.db.financialState.FinancialStateDAO;
import com.utepsa.db.financialState.FinancialStateRealDAO;
import com.utepsa.db.groupsOffered.GroupsOfferedDAO;
import com.utepsa.db.groupsOffered.GroupsOfferedRealDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.historyNote.HistoryNotesRealDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.notification.NotificationRealDAO;
import com.utepsa.db.permission.PermissionDAO;
import com.utepsa.db.permission.PermissionRealDAO;
import com.utepsa.db.permissionCredential.PermissionCredentialDAO;
import com.utepsa.db.permissionCredential.PermissionCredentialRealDAO;
import com.utepsa.db.permissionRole.PermissionRoleDAO;
import com.utepsa.db.permissionRole.PermissionRoleRealDAO;
import com.utepsa.db.photo.PhotoDAO;
import com.utepsa.db.photo.PhotoRealDAO;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.professor.ProfessorRealDAO;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.db.schedules.SchedulesRealDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.student.StudentRealDAO;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterDAO;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterRealDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogRealDAO;
import com.utepsa.db.virtualBooks.VirtualBooksDAO;
import com.utepsa.db.virtualBooks.VirtualBooksRealDAO;
import org.hibernate.SessionFactory;

/**
 * Created by David-SW on 16/05/2017.
 */
public class HbnModule extends AbstractModule {
    private final HbnBundle hbnBundle;

    public HbnModule(HbnBundle hbnBundle) {
        this.hbnBundle = hbnBundle;
    }

    @Override
    protected void configure() {
        bind(SessionFactory.class).toInstance(hbnBundle.getSessionFactory());
        bind(AcademicAreaDAO.class).toInstance(new AcademicAreaRealDAO(hbnBundle.getSessionFactory()));
        bind(CareerDAO.class).toInstance(new CareerRealDAO(hbnBundle.getSessionFactory()));
        bind(CareerCourseDAO.class).toInstance(new CareerCourseRealDAO(hbnBundle.getSessionFactory()));
        bind(CategoryNotificationDAO.class).toInstance(new CategoryNotificationRealDAO(hbnBundle.getSessionFactory()));
        bind(CourseDAO.class).toInstance(new CourseRealDAO(hbnBundle.getSessionFactory()));
        bind(CredentialDAO.class).toInstance(new CredentialRealDAO(hbnBundle.getSessionFactory()));
        bind(CredentialAdministratorDAO.class).toInstance(new CredentialAdministratorRealDAO(hbnBundle.getSessionFactory()));
        bind(CurrentSemesterDAO.class).toInstance(new CurrentSemesterRealDAO(hbnBundle.getSessionFactory()));
        bind(DocumentDAO.class).toInstance(new DocumentRealDAO(hbnBundle.getSessionFactory()));
        bind(DocumentStudentDAO.class).toInstance(new DocumentStudentRealDAO(hbnBundle.getSessionFactory()));
        bind(ExecutiveReportDAO.class).toInstance(new ExecutiveReportRealDAO(hbnBundle.getSessionFactory()));
        bind(FileDAO.class).toInstance(new FileRealDAO(hbnBundle.getSessionFactory()));
        bind(FinancialStateDAO.class).toInstance(new FinancialStateRealDAO(hbnBundle.getSessionFactory()));
        bind(GroupsOfferedDAO.class).toInstance(new GroupsOfferedRealDAO(hbnBundle.getSessionFactory()));
        bind(HistoryNotesDAO.class).toInstance(new HistoryNotesRealDAO(hbnBundle.getSessionFactory()));
        bind(NotificationDAO.class).toInstance(new NotificationRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionDAO.class).toInstance(new PermissionRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionCredentialDAO.class).toInstance(new PermissionCredentialRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionRoleDAO.class).toInstance(new PermissionRoleRealDAO(hbnBundle.getSessionFactory()));
        bind(ProfessorDAO.class).toInstance(new ProfessorRealDAO(hbnBundle.getSessionFactory()));
        bind(ProfileDAO.class).toInstance(new ProfileRealDAO(hbnBundle.getSessionFactory()));
        bind(SchedulesDAO.class).toInstance(new SchedulesRealDAO(hbnBundle.getSessionFactory()));
        bind(StudentDAO.class).toInstance(new StudentRealDAO(hbnBundle.getSessionFactory()));
        bind(StudentCourseRegisterDAO.class).toInstance(new StudentCourseRegisterRealDAO(hbnBundle.getSessionFactory()));
        bind(StudentMigrationlogDAO.class).toInstance(new StudentMigrationlogRealDAO(hbnBundle.getSessionFactory()));
        bind(FileCareerDAO.class).toInstance(new FileCareerRealDAO(hbnBundle.getSessionFactory()));
        bind(FeaturedStudentDAO.class).toInstance(new FeaturedStudentRealDAO(hbnBundle.getSessionFactory()));
        bind(BookDAO.class).toInstance(new BookRealDAO(hbnBundle.getSessionFactory()));
        bind(AcademicEventDAO.class).toInstance(new AcademicEventRealDAO(hbnBundle.getSessionFactory()));
        bind(PhotoDAO.class).toInstance(new PhotoRealDAO(hbnBundle.getSessionFactory()));
        bind(EventCareerDAO.class).toInstance(new EventCareerRealDAO(hbnBundle.getSessionFactory()));
        bind(EventCourseDAO.class).toInstance(new EventCourseRealDAO(hbnBundle.getSessionFactory()));
        bind(EventTypeDAO.class).toInstance(new EventTypeRealDAO(hbnBundle.getSessionFactory()));
        bind(AreaOfVirtualBooksDAO.class).toInstance(new AreaOfVirtualBooksRealDAO(hbnBundle.getSessionFactory()));
        bind(VirtualBooksDAO.class).toInstance(new VirtualBooksRealDAO(hbnBundle.getSessionFactory()));
        bind(CensusDAO.class).toInstance(new CensusRealDAO(hbnBundle.getSessionFactory()));
    }
}
