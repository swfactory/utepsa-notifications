package com.utepsa.modules;

import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.adapters.utepsa.averageStudent.AverageStudentAdapter;
import com.utepsa.adapters.utepsa.careers.CareersAdapter;
import com.utepsa.adapters.utepsa.courses.CoursesAdapter;
import com.utepsa.adapters.utepsa.coursesCareer.CoursesCareerAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.financialState.FinancialStateAdapter;
import com.utepsa.adapters.utepsa.groupsOffered.GroupsOfferedAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.professor.ProfessorAdapter;
import com.utepsa.adapters.utepsa.schedules.SchedulesAdapter;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.weather.WeatherAdapter;
import com.utepsa.config.NotificationServerConfig;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

/**
 * Created by David-SW on 16/05/2017.
 */
public class MainModule extends DropwizardAwareModule<NotificationServerConfig> {
    @Override
    protected void configure() {

        bind(CareersAdapter.class).toInstance(new CareersAdapter(configuration().getExternalSever("Utepsa")));
        bind(CoursesAdapter.class).toInstance(new CoursesAdapter(configuration().getExternalSever("Utepsa")));
        bind(CoursesCareerAdapter.class).toInstance(new CoursesCareerAdapter(configuration().getExternalSever("Utepsa")));
        bind(DocumentsStudentAdapter.class).toInstance(new DocumentsStudentAdapter(configuration().getExternalSever("Utepsa")));
        bind(FinancialStateAdapter.class).toInstance(new FinancialStateAdapter(configuration().getExternalSever("Utepsa")));
        bind(GroupsOfferedAdapter.class).toInstance(new GroupsOfferedAdapter(configuration().getExternalSever("Utepsa")));
        bind(HistoryNotesAdapter.class).toInstance(new HistoryNotesAdapter(configuration().getExternalSever("Utepsa")));
        bind(ProfessorAdapter.class).toInstance(new ProfessorAdapter(configuration().getExternalSever("Utepsa")));
        bind(SchedulesAdapter.class).toInstance(new SchedulesAdapter(configuration().getExternalSever("Utepsa")));
        bind(StudentCourseRegisterAdapter.class).toInstance(new StudentCourseRegisterAdapter(configuration().getExternalSever("Utepsa")));
        bind(StudentAdapter.class).toInstance(new StudentAdapter(configuration().getExternalSever("Utepsa")));
        bind(GCMAdapter.class).toInstance(new GCMAdapter(configuration().getExternalSever("GCM")));
        bind(WeatherAdapter.class).toInstance(new WeatherAdapter(configuration().getExternalSever("utepsa")));
        bind(AverageStudentAdapter.class).toInstance(new AverageStudentAdapter(configuration().getExternalSever("Utepsa")));
    }
}
