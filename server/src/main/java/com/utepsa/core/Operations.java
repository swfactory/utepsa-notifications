package com.utepsa.core;

import com.utepsa.api.response.BasicResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 27/01/2017.
 */
public class Operations {
    public static BasicResponse getResponseErrors(Exception e){
        BasicResponse response = new BasicResponse();
        response.setCode(StatusCode.INTERNAL_SERVER_ERROR);
        response.setMessage("If an error occurred our server, try later or contact an administrator.");

        List<String> errors = new ArrayList<>();
        if(!e.getMessage().isEmpty()){
            errors.add(e.getMessage());
        }
        if(e.getCause().getMessage().isEmpty()){
            errors.add(e.getCause().getMessage());
        }
        if(errors != null){
            response.setData(errors);
        }

        return  response;
    }

    public static long getCompareDate(Date laterDate, Date previusDate){

        final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
        long difference;

        difference = (laterDate.getTime() - previusDate.getTime() )/MILLSECS_PER_DAY;
        difference = difference + 1;
        return difference;
    }

    public static String encryptPassword(String passwordToHash){
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(passwordToHash.getBytes());
            byte byteData[] = md.digest();


            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1) hexString.append('0');
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hexString.toString();
    }

    public static String FormattedStringForRemoveZero(String RegisterNumber){

        String originalString = RegisterNumber;
        String result = originalString.replaceFirst ("^0*", "");
        return result;
    }
}
