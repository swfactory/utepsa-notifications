package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.core.Operations;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.credentialAdministrator.CredentialAdministratorDAO;
import com.utepsa.db.financialState.FinancialStateDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.*;
import com.utepsa.resources.notifications.NotificationService;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Gerardo on 06/04/2017.
 */
public class PeriodicDebtNotifications extends Task {

    private FinancialStateDAO financialStateDAO;
    private StudentDAO studentDAO;
    private NotificationService notificationService;
    private SessionFactory sessionFactory;
    private CredentialDAO credentialDAO;
    private CredentialAdministratorDAO credentialAdministratorDAO;

    @Inject
    public PeriodicDebtNotifications(SessionFactory sessionFactory, FinancialStateDAO financialStateDAO, StudentDAO studentDAO, NotificationService notificationService, CredentialDAO credentialDAO) {
        super("send_notifications_periodic");
        this.sessionFactory = sessionFactory;
        this.financialStateDAO = financialStateDAO;
        this.studentDAO = studentDAO;
        this.notificationService = notificationService;
        this.credentialDAO = credentialDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            String day = immutableMultimap.get("day").toArray()[0].toString();
            Transaction transaction = session.beginTransaction();
            try {
                sendNotifications(day);
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void sendNotifications(String day) throws Exception{
        int dayForNotifications = Integer.parseInt(day);
        List<FinancialState> financialStates = financialStateDAO.getAllFinancialStateWithFairValue();
        for (FinancialState financialState: financialStates) {
            Date currentDate = new Date();
            new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
            long difference = Operations.getCompareDate(financialState.getExpirationDate(), currentDate);
            if(difference == dayForNotifications){
                long student =  financialState.getIdStudent().getId();
                if(credentialDAO.getById(student).getGcmId() != null) {
                    long date = currentDate.getTime();
                    String expirationDateFormatted = new SimpleDateFormat("dd-MM-yy").format(financialState.getExpirationDate());
                    Set<NotificationAudience> audiences = Collections.newSetFromMap(new IdentityHashMap<NotificationAudience, Boolean>());
                    audiences.add(new NotificationAudience(studentDAO.getById(financialState.getIdStudent().getId()).getRegisterNumber(), new com.utepsa.models.TypeAudience(2, "STUDENT")));
                    Notification notification = new Notification(
                            0, date, credentialAdministratorDAO.getById(1),
                            "Deuda Pendiente!",
                            "Tiene " + dayForNotifications + " dias para pagar su deuda de: " + financialState.getDelinquentBalance() + " Fecha de vencimiento: " + expirationDateFormatted,
                            new CategoryNotification(1, "Notificacion", null),
                            audiences);
                    notificationService.createNotification(notification);
                }
            }
        }
    }
}
