package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.core.Migrationlog;
import com.utepsa.core.Operations;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.models.*;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 05/12/2016.
 */
public class StudentsTask  extends Task{
    private SessionFactory sessionFactory;
    private StudentAdapter studentAdapter;
    private StudentDAO studentDAO;
    private CareerDAO careerDAO;
    private CredentialDAO credentialDAO;
    private StudentMigrationlogDAO studentMigrationlogDAO;

    @Inject
    public StudentsTask(SessionFactory sessionFactory, StudentAdapter studentAdapter, StudentMigrationlogDAO studentMigrationlogDAO,StudentDAO studentDAO, CareerDAO careerDAO, CredentialDAO credentialDAO) {
        super("migration_students");
        this.sessionFactory = sessionFactory;
        this.studentDAO = studentDAO;
        this.studentAdapter = studentAdapter;
        this.careerDAO = careerDAO;
        this.credentialDAO = credentialDAO;
        this.studentMigrationlogDAO = studentMigrationlogDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();

        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                List<StudentData> listStudent = studentAdapter.getAllStudents();
                for(StudentData student : listStudent){
                    transaction = session.beginTransaction();
                    if(student == null) {
                        continue;
                    }
                    boolean synchronizedStudent = false;

                    do{
                        try{
                            if(isOnlineNet()){
                                long idStudent = this.registerStudent(student);
                                if(idStudent > 0){
                                    registerUserStudent(student, idStudent);
                                    registerStudentLog(idStudent,null,true);
                                }
                                synchronizedStudent = true;
                            }else{
                                Thread.sleep(5000);
                            }
                        }catch (Exception e){
                            Thread.sleep(5000);
                            printWriter.write(e.getMessage() + " \n");
                        }
                    }while (!synchronizedStudent);

                    transaction.commit();
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }
    public void updateStudentData(StudentData studentData) throws Exception
    {
        Career careerStudent = careerDAO.getByCodeUtepsa(studentData.getCrr_codigo().trim());
        Student student = new Student();
        student.setName(studentData.getAgd_nombres().trim());
        student.setFatherLastname(studentData.getAgd_appaterno().trim());
        student.setMotherLastname(studentData.getAgd_apmaterno().trim());
        student.setAgendCode(studentData.getAgd_codigo().trim());
        student.setState(studentData.getEstado().trim());
        if(studentData.getAgd_fechanac() == null){
            student.setBirthday(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date birthday = formaterTimeZone.parse(studentData.getAgd_fechanac().trim());
            student.setBirthday(birthday);
        }

        if(careerStudent != null){
            student.setCareer(careerStudent);
        }
        student.setRegisterNumber(studentData.getAlm_registro().trim());
        student.setDocumentNumber(studentData.getAgd_docnro().trim());
        student.setDocumentType(studentData.getAgd_docid().trim());
        student.setEmail1(studentData.getCorreo().trim());
        student.setPhoneNumber1(studentData.getAgd_telf1().trim());
        student.setPhoneNumber2(studentData.getAgd_telf2().trim());
        student.setGender(studentData.getAgd_sexo().trim());
        student.setPensum(Integer.parseInt(studentData.getPns_codigo()));
        student.setState(student.getState().trim());

    }

    public long registerStudent(StudentData studentData) throws Exception {
        if(studentDAO.getByRegisterNumber(studentData.getAlm_registro().trim()) != null)
        {
            this.updateStudentData(studentData);
            return -1;
        }
        Career careerStudent = careerDAO.getByCodeUtepsa(studentData.getCrr_codigo().trim());
        if(careerStudent == null){
            return -1;
        }

        Student student = new Student();
        student.setName(studentData.getAgd_nombres().trim());
        student.setFatherLastname(studentData.getAgd_appaterno().trim());
        student.setMotherLastname(studentData.getAgd_apmaterno().trim());
        student.setAgendCode(studentData.getAgd_codigo().trim());
        student.setState(studentData.getEstado().trim());
        if(studentData.getAgd_fechanac() == null){
            student.setBirthday(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date birthday = formaterTimeZone.parse(studentData.getAgd_fechanac().trim());
            student.setBirthday(birthday);
        }
        student.setCareer(careerStudent);
        student.setRegisterNumber(studentData.getAlm_registro().trim());
        student.setDocumentNumber(studentData.getAgd_docnro().trim());
        student.setDocumentType(studentData.getAgd_docid().trim());
        student.setEmail1(studentData.getCorreo().trim());
        student.setPhoneNumber1(studentData.getAgd_telf1().trim());
        student.setPhoneNumber2(studentData.getAgd_telf2().trim());
        student.setGender(studentData.getAgd_sexo().trim());
        student.setPensum(Integer.parseInt(studentData.getPns_codigo()));
        student.setState(student.getState().trim());

        return studentDAO.create(student);
    }

    public void registerUserStudent(StudentData studentData, long idStudent) throws Exception{
        Credential credential = new Credential();
        if(!studentData.getAgd_docnro().isEmpty()){
            credential.setPassword(Operations.encryptPassword(studentData.getAgd_docnro().trim()));
        }
        else{
            credential.setPassword(Operations.encryptPassword(studentData.getAlm_registro().trim()));
        }
        credential.setUsername(studentData.getAlm_registro().trim());
        credential.setRole(new Role(1));
        credential.setChangePasswordForced(true);
        credential.setState(true);
        credential.setStudent(new Student(idStudent));
        credentialDAO.create(credential);
    }

    public void registerStudentLog(long idStudent, String comments, boolean state) throws Exception {
        StudentMigrationlog studentMigrationlog = new StudentMigrationlog();
        studentMigrationlog.setStudent(new Student(idStudent));
        studentMigrationlog.setType(new TypeMigrationlog(Migrationlog.MIGRATION_STUDENT_WITH_CREDENTIAL));
        studentMigrationlog.setSource("TASK");
        Date date = new Date();
        studentMigrationlog.setDateExecuted(date);
        studentMigrationlog.setComments(comments);
        studentMigrationlog.setState(state);

        studentMigrationlogDAO.create(studentMigrationlog);
    }


    private boolean isOnlineNet(){
        try {
            Socket s = new Socket("www.google.com", 80);

            if(s.isConnected()){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}

