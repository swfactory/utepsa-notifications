package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.core.Operations;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Credential;
import com.utepsa.models.Student;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;

/**
 * Created by Gerardo on 11/05/2017.
 */
public class ResetPassword extends Task {

    private SessionFactory sessionFactory;
    private CredentialDAO credentialDAO;
    private StudentDAO studentDAO;

    @Inject
    public ResetPassword(SessionFactory sessionFactory, CredentialDAO credentialDAO, StudentDAO studentDAO) {
        super("reset_password");
        this.sessionFactory = sessionFactory;
        this.credentialDAO = credentialDAO;
        this.studentDAO = studentDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            String registerNumber = immutableMultimap.get("registerNumber").toArray()[0].toString();
            Transaction transaction = session.beginTransaction();
            try {
                this.resetPassword(registerNumber);
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    private void resetPassword (String registerNumber) throws Exception {
        if(studentDAO.getByRegisterNumber(registerNumber) != null){
            Student student = studentDAO.getByRegisterNumber(registerNumber);
            Credential credential = credentialDAO.getByIdStudent(student);
            credential.setGcmId(null);
            credential.setLastConnection(null);
            if(student.getDocumentNumber().isEmpty()){
                String formatted = Operations.FormattedStringForRemoveZero(student.getRegisterNumber().trim());
                credential.setPassword(Operations.encryptPassword(formatted));
            } else {
                credential.setPassword(Operations.encryptPassword(student.getDocumentNumber().trim()));
            }
            credential.setChangePasswordForced(true);
            credentialDAO.resetPasswordOfCredential(credential);
        } else {
            throw new Exception("Estudiante no existe");
        }
    }
}
