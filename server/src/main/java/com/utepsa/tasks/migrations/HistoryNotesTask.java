package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;
import com.utepsa.models.Student;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

/**
 * Created by David on 05/12/2016.
 */

public class HistoryNotesTask extends Task {
    private SessionFactory sessionFactory;
    private StudentMigrationlogDAO studentMigrationlogDAO;
    private HistoryNotesDAO historyNotesDAO;
    private HistoryNotesAdapter historyNotesAdapter;
    private StudentDAO studentDAO;
    private CourseDAO courseDAO;

    @Inject
    public HistoryNotesTask(SessionFactory sessionFactory,HistoryNotesAdapter historyNotesAdapter, StudentMigrationlogDAO studentMigrationlogDAO, HistoryNotesDAO historyNotesDAO, StudentDAO studentDAO, CourseDAO courseDAO) {
        super("migration_historynotes");
        this.studentMigrationlogDAO = studentMigrationlogDAO;
        this.sessionFactory = sessionFactory;
        this.historyNotesDAO = historyNotesDAO;
        this.studentDAO = studentDAO;
        this.courseDAO = courseDAO;
        this.historyNotesAdapter = historyNotesAdapter;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                List<Student> students = studentDAO.getAll();
                if(students.isEmpty()){
                    printWriter.write("No existen estudiantes \n");
                    return;
                }

                for(Student student : students){
                    transaction = session.beginTransaction();
                    if(student == null) {
                        continue;
                    }
                    boolean synchronizedStudent = false;

                    do{
                        try{
                            if(isOnlineNet()){
                                this.SyncHistoryNotes(student);
                                synchronizedStudent = true;
                            }else{
                                Thread.sleep(5000);
                            }
                        }catch (Exception e){
                            Thread.sleep(5000);
                            printWriter.write(e.getMessage() + " \n");
                        }
                    }while (!synchronizedStudent);

                    transaction.commit();
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }

    }

    public void SyncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDAO.getByStudent(student.getId());

        if(historyNotesApp.size() == 0 && historyNotesUtepsa.size() > 0){
            for(HistoryNotesData data : historyNotesUtepsa){
                createHistoryNote(data, student.getId());
                continue;
            }
            return;
        }

        if(historyNotesUtepsa.size() <= 0) return;

        for(HistoryNotesData data : historyNotesUtepsa){
            this.updateHistoryNote(data, historyNotesApp, student.getId());
            continue;
        }
    }

    private void createHistoryNote(HistoryNotesData data,long idStudent) throws Exception {
        if (data.getMat_codigo() != null && data.getMat_codigo() != "") {
            Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
            HistoryNotes historyNote = new HistoryNotes();
            historyNote.setIdStudent(idStudent);
            historyNote.setCourse(course);
            historyNote.setMinimunNote((int) data.getPln_notaminima());
            historyNote.setNote((int) data.getNot_nota());
            historyNote.setModule(data.getMdu_codigo().trim());
            historyNote.setSemester(data.getSem_codigo().trim());
            historyNotesDAO.create(historyNote);
        }
    }

    private void updateHistoryNote(HistoryNotesData data, List<HistoryNotes> historyNotesApp , long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        for(HistoryNotes historyNote: historyNotesApp){
            if (historyNote.getCourse().getId() != Long.parseLong(null))
            {
                if(historyNote.getSemester().equals(data.getSem_codigo()) &&( historyNote.getModule().equals(data.getMdu_codigo()) || historyNote.getModule().equals(null) || historyNote.getModule().equals(""))&& historyNote.getCourse().getId() == course.getId()){
                    if(historyNote.getNote() != data.getNot_nota()){
                        historyNote.setNote((int) data.getNot_nota());
                    }
                    return;
                }
            }
            continue;
        }

        this.createHistoryNote(data, idStudent);
    }

    private boolean isOnlineNet(){
        try {
            Socket s = new Socket("www.google.com", 80);

            if(s.isConnected()){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
