package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.coursesCareer.CoursesCareerAdapter;
import com.utepsa.adapters.utepsa.coursesCareer.CoursesCareerData;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.models.Career;
import com.utepsa.models.CareerCourse;
import com.utepsa.models.Course;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by David-SW on 10/03/2017.
 */
public class CareersCoursesTask extends Task {
    private SessionFactory sessionFactory;
    private CareerDAO careerDAO;
    private CourseDAO courseDAO;
    private CareerCourseDAO careerCourseDAO;
    private CoursesCareerAdapter coursesCareerAdapter;

    @Inject
    public CareersCoursesTask(SessionFactory sessionFactory, CoursesCareerAdapter coursesCareerAdapter, CareerDAO careerDAO, CourseDAO courseDAO, CareerCourseDAO careerCourseDAO) {
        super("migration_courses_career");
        this.coursesCareerAdapter = coursesCareerAdapter;
        this.sessionFactory = sessionFactory;
        this.careerDAO = careerDAO;
        this.courseDAO = courseDAO;
        this.careerCourseDAO = careerCourseDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerCoursesCareer();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerCoursesCareer() throws Exception {
        List<CoursesCareerData> coursesUtepsa = coursesCareerAdapter.getAll();
        for(CoursesCareerData coursesCareerData : coursesUtepsa) {
            Career career = careerDAO.getByCodeUtepsa(coursesCareerData.getCrr_codigo().trim());
            if(career == null) continue;

            Course course = courseDAO.getByCodeUtepsa(coursesCareerData.getMat_codigo().trim());
            if(course == null) continue;

            int pensum = Integer.parseInt(coursesCareerData.getPns_codigo().trim());
            if(careerCourseDAO.getCourseByCoursePensum(course.getId(), career.getId(), pensum) != null) continue;

            CareerCourse careerCourse = new CareerCourse();
            careerCourse.setCareer(career);
            careerCourse.setCourse(course);
            careerCourse.setPensum(pensum);
            careerCourse.setPensumLevel(coursesCareerData.getPns_nivel());
            careerCourse.setPensumOrder(coursesCareerData.getPns_orden());
            careerCourseDAO.create(careerCourse);
        }
    }
}
