package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.professor.ProfessorAdapter;
import com.utepsa.adapters.utepsa.professor.ProfessorData;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterData;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.models.*;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
public class StudentCourseRegisterTask extends Task{
    private SessionFactory sessionFactory;
    private StudentMigrationlogDAO studentMigrationlogDAO;
    private StudentCourseRegisterAdapter studentCourseRegisterAdapter;
    private CurrentSemesterDAO currentSemesterDAO;
    private StudentDAO studentDAO;
    private ProfessorDAO professorDAO;
    private SchedulesDAO schedulesDAO;
    private CourseDAO courseDAO;
    private StudentCourseRegisterDAO studentCourseRegisterDAO;
    private ProfessorAdapter professorAdapter;

    @Inject
    public StudentCourseRegisterTask(SessionFactory sessionFactory, StudentCourseRegisterAdapter studentCourseRegisterAdapter, ProfessorAdapter professorAdapter, StudentMigrationlogDAO studentMigrationlogDAO,
                                        CurrentSemesterDAO currentSemesterDAO, StudentDAO studentDAO , ProfessorDAO professorDAO,
                                        CourseDAO courseDAO, StudentCourseRegisterDAO studentCourseRegisterDAO, SchedulesDAO schedulesDAO) {
        super("migration_studentcourseregister");
        this.sessionFactory = sessionFactory;
        this.studentMigrationlogDAO = studentMigrationlogDAO;
        this.studentCourseRegisterAdapter = studentCourseRegisterAdapter;
        this.currentSemesterDAO = currentSemesterDAO;
        this.studentDAO = studentDAO;
        this.professorDAO = professorDAO;
        this.courseDAO = courseDAO;
        this.studentCourseRegisterDAO = studentCourseRegisterDAO;
        this.schedulesDAO = schedulesDAO;
        this.professorAdapter = professorAdapter;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                String semester = currentSemesterDAO.getCurrentSemester().getSemester();
                List<String> studentsRegister = studentCourseRegisterAdapter.getActiveStudents(semester);
                Student student;
                for (String registerStudent : studentsRegister) {
                    student = studentDAO.getByRegisterNumber(registerStudent);
                    if(student == null) {
                        continue;
                    }

                    boolean registered;
                    do{
                        transaction = session.beginTransaction();
                        try {
                            this.SyncStudentCourseRegisters(student ,semester.trim());
                            transaction.commit();
                            registered = true;
                        }
                        catch (Exception e) {
                            if (transaction != null) {
                                try {
                                    transaction.rollback();
                                } catch(Exception re) {
                                    System.err.println("Error when trying to rollback transaction:");
                                    re.printStackTrace();
                                }
                            }
                            registered = false;
                        }
                    }while (!registered);
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void SyncStudentCourseRegisters(Student student, String semester) throws Exception{
        List<StudentCourseRegister> studentCoursesRegistersApp =  studentCourseRegisterDAO.getAllCourseRegisterByStudent(student.getId(),semester);
        List<StudentCourseRegisterData> studentCoursesRegistersUtepsa = studentCourseRegisterAdapter.getCourseRegisterByRegisterNumberAndSemester(student.getRegisterNumber(), semester);

        if(studentCoursesRegistersApp.size() == 0 && studentCoursesRegistersUtepsa.size() > 0){
            for(StudentCourseRegisterData data : studentCoursesRegistersUtepsa){
                this.crateCoursesRegistered(data, student.getId());
            }

            return  ;
        }

        if(studentCoursesRegistersUtepsa.isEmpty()) return;


        if (studentCoursesRegistersUtepsa.size() > studentCoursesRegistersApp.size())
        {
            List<StudentCourseRegisterData> newStudentCourseRegister = studentCoursesRegistersUtepsa.stream().filter( e ->studentCoursesRegistersApp.stream().filter(a -> a.getCodeUtepsa() == e.getGrp_detalle_id()).count() == 0).collect(Collectors.toList());

            newStudentCourseRegister.stream().forEach(sr -> {
                try {
                    this.crateCoursesRegistered(sr,student.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }});
        }

        for(StudentCourseRegisterData data: studentCoursesRegistersUtepsa){
            updateStudentCourseRegistered(data, studentCoursesRegistersApp);
        }

        return;
    }

    public void crateCoursesRegistered(StudentCourseRegisterData studentCourseRegisterData, long idStudent) throws Exception {
        StudentCourseRegister studentCourseRegister = new StudentCourseRegister();

        Course course = courseDAO.getByCodeUtepsa(studentCourseRegisterData.getMat_codigo().trim());

        if(studentCourseRegisterData.getAgd_docente() == null){
            studentCourseRegister.setProfessor(null);
        }else{
            Professor professor = professorDAO.getProfessorByAgendCode(studentCourseRegisterData.getAgd_docente().trim());
            if(professor == null) {
                professor = registerProfessor(studentCourseRegisterData.getAgd_docente().trim());
            }
            studentCourseRegister.setProfessor(professor);
        }

        if(studentCourseRegisterData.getTur_codigo() == null){
            studentCourseRegister.setSchedule(null);
        }else{
            Schedule schedule = schedulesDAO.getByCodeUtepsa(studentCourseRegisterData.getTur_codigo().trim());
            studentCourseRegister.setSchedule(schedule);
        }

        studentCourseRegister.setIdStudent(idStudent);
        studentCourseRegister.setCourse(course);
        studentCourseRegister.setClassroom(studentCourseRegisterData.getAul_codigo().trim());
        studentCourseRegister.setGroup(studentCourseRegisterData.getGrp_grupo().trim());
        studentCourseRegister.setModule(studentCourseRegisterData.getMdu_codigo().trim());
        studentCourseRegister.setNumberEnrolled(studentCourseRegisterData.getGrp_nroinscritos());

        if(studentCourseRegisterData.getObservacion() == null){
            studentCourseRegister.setState(null);
        } else {
            studentCourseRegister.setState(studentCourseRegisterData.getObservacion().trim());
        }

        studentCourseRegister.setNote(studentCourseRegisterData.getNot_nota());
        studentCourseRegister.setSemester(studentCourseRegisterData.getSem_codigo().trim());
        studentCourseRegister.setCodeUtepsa(studentCourseRegisterData.getGrp_detalle_id());

        studentCourseRegisterDAO.create(studentCourseRegister);
    }

    private void updateStudentCourseRegistered(StudentCourseRegisterData data, List<StudentCourseRegister> studentCoursesRegistersApp) throws Exception{
        for(StudentCourseRegister studentCourseRegister : studentCoursesRegistersApp){
            if(studentCourseRegister.getCodeUtepsa() == data.getGrp_detalle_id()){
                studentCourseRegister.setGroup(data.getGrp_grupo().trim());
                studentCourseRegister.setClassroom(data.getAul_codigo().trim());
                studentCourseRegister.setNumberEnrolled(data.getGrp_nroinscritos());
                studentCourseRegister.setNote(data.getNot_nota());
                studentCourseRegister.setCodeUtepsa(data.getGrp_detalle_id());

                if(data.getAgd_docente() != null){
                    Professor professor = professorDAO.getProfessorByAgendCode(data.getAgd_docente().trim());
                    if(professor == null) {
                        professor = registerProfessor(data.getAgd_docente().trim());
                    }

                    if(data.getObservacion() == null){
                        studentCourseRegister.setState(null);
                    } else {
                        studentCourseRegister.setState(data.getObservacion().trim());
                    }

                    if(studentCourseRegister.getProfessor() == null){
                        studentCourseRegister.setProfessor(professor);
                    }else{
                        if(studentCourseRegister.getProfessor().getId() != professor.getId()){
                            studentCourseRegister.setProfessor(professor);
                        }
                    }
                }

                if(data.getTur_codigo() != null){
                    Schedule schedule = schedulesDAO.getByCodeUtepsa(data.getTur_codigo().trim());
                    if(studentCourseRegister.getSchedule() != null){
                        if(studentCourseRegister.getSchedule().getId() != schedule.getId()){
                            studentCourseRegister.setSchedule(schedule);
                        }
                    }else{
                        studentCourseRegister.setSchedule(schedule);
                    }
                }
            }
        }
    }

    public Professor registerProfessor(String registerCode) throws Exception {
        ProfessorData professorData = professorAdapter.getProfessor(registerCode);

        if(professorData == null) return null;

        Professor newProfessor = new Professor();
        newProfessor.setAgend_code(professorData.getAgd_codigo());
        newProfessor.setName(professorData.getAgd_nombres());
        newProfessor.setFather_lastname(professorData.getAgd_appaterno());
        newProfessor.setMother_lastname(professorData.getAgd_apmaterno());
        newProfessor.setState(true);
        professorDAO.create(newProfessor);
        return newProfessor;
    }
}
