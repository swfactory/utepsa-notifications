package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;
import com.utepsa.models.Student;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

/**
 * Created by David-SW on 13/04/2017.
 */
public class SyncHistoryNotes extends Task {
    private SessionFactory sessionFactory;
    private StudentMigrationlogDAO studentMigrationlogDAO;
    private StudentCourseRegisterAdapter studentCourseRegisterAdapter;
    private HistoryNotesDAO historyNotesDAO;
    private HistoryNotesAdapter historyNotesAdapter;
    private StudentDAO studentDAO;
    private CourseDAO courseDAO;

    @Inject
    public SyncHistoryNotes(SessionFactory sessionFactory, HistoryNotesAdapter historyNotesAdapter,StudentCourseRegisterAdapter studentCourseRegisterAdapter, StudentMigrationlogDAO studentMigrationlogDAO, HistoryNotesDAO historyNotesDAO, StudentDAO studentDAO, CourseDAO courseDAO) {
        super("sync_history_notes");
        this.studentMigrationlogDAO = studentMigrationlogDAO;
        this.sessionFactory = sessionFactory;
        this.historyNotesDAO = historyNotesDAO;
        this.studentDAO = studentDAO;
        this.courseDAO = courseDAO;
        this.historyNotesAdapter = historyNotesAdapter;
        this.studentCourseRegisterAdapter = studentCourseRegisterAdapter;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            ManagedSessionContext.bind(session);

            if(immutableMultimap.get("semester").isEmpty()){
                printWriter.write("Semester not found \n");
                return;
            }
            String semester = immutableMultimap.get("semester").toArray()[0].toString();


            List<String> students = studentCourseRegisterAdapter.getActiveStudents(semester);
            if(students.isEmpty()){
                printWriter.write("No existen estudiantes activos en este semestre \n");
                return;
            }

            for(String registerStudent : students){
                transaction = session.beginTransaction();

                Student student = studentDAO.getByRegisterNumber(registerStudent);
                if(student == null) {
                    continue;
                }
                boolean synchronizedStudent = false;

                do{
                    try{
                        if(isOnlineNet()){
                            this.SyncHistoryNotes(student);
                            synchronizedStudent = true;
                        }else{
                            Thread.sleep(5000);
                        }
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);

                transaction.commit();
            }

        }catch (Exception e){
            if ( transaction.getStatus() == TransactionStatus.ACTIVE ) {
                transaction.rollback();
            }
        }finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }

    }

    public void SyncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDAO.getByStudent(student.getId());

        if(historyNotesApp.size() == 0 && historyNotesUtepsa.size() > 0){
            for(HistoryNotesData data : historyNotesUtepsa){
                createHistoryNote(data, student.getId());
            }
            return;
        }

        if(historyNotesUtepsa.size() <= 0) return;

        for(HistoryNotesData data : historyNotesUtepsa){
            this.updateHistoryNote(data, historyNotesApp, student.getId());
        }
    }

    private void createHistoryNote(HistoryNotesData data,long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        HistoryNotes historyNote = new HistoryNotes();
        historyNote.setIdStudent(idStudent);
        historyNote.setCourse(course);
        historyNote.setMinimunNote((int) data.getPln_notaminima());
        historyNote.setNote((int) data.getNot_nota());
        historyNote.setModule(data.getMdu_codigo().trim());
        historyNote.setSemester(data.getSem_codigo().trim());
        historyNotesDAO.create(historyNote);
    }

    private void updateHistoryNote(HistoryNotesData data, List<HistoryNotes> historyNotesApp , long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        for(HistoryNotes historyNote: historyNotesApp){
            if(historyNote.getSemester().equals(data.getSem_codigo()) && historyNote.getModule().equals(data.getMdu_codigo()) && historyNote.getCourse().getId() == course.getId()){
                if(historyNote.getNote() != data.getNot_nota()){
                    historyNote.setNote((int) data.getNot_nota());
                }
                return;
            }
        }

        this.createHistoryNote(data, idStudent);
    }

    private boolean isOnlineNet(){
        try {
            Socket s = new Socket("www.google.com", 80);

            if(s.isConnected()){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
