package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.courses.CoursesAdapter;
import com.utepsa.adapters.utepsa.courses.CoursesDATA;
import com.utepsa.db.academicArea.AcademicAreaDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.models.AcademicArea;
import com.utepsa.models.Course;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by David on 28/01/2017.
 */
public class CoursesTask extends Task {
    private SessionFactory sessionFactory;
    private CourseDAO courseDAO;
    private AcademicAreaDAO academicAreaDAO;
    private CoursesAdapter coursesAdapter;

    @Inject
    public CoursesTask(SessionFactory sessionFactory,CoursesAdapter coursesAdapter, CourseDAO courseDAO, AcademicAreaDAO academicAreaDAO) {
        super("migration_courses");
        this.coursesAdapter = coursesAdapter;
        this.sessionFactory = sessionFactory;
        this.courseDAO = courseDAO;
        this.academicAreaDAO = academicAreaDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerCourses();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerCourses() throws Exception {
        List<CoursesDATA> coursesUtepsa = coursesAdapter.getAllCourses();
        for(CoursesDATA courseUtepsa : coursesUtepsa) {
            Course course = courseDAO.getByCodeUtepsa(courseUtepsa.getMat_codigo().trim());
            if (course == null){
                course = new Course();
                course.setName(courseUtepsa.getMat_descripcion().trim());
                course.setCodeUtepsa(courseUtepsa.getMat_codigo());
                course.setInitials(courseUtepsa.getMat_sigla().trim());
                if(courseUtepsa.getAac_codigo() == null || courseUtepsa.getAac_codigo().trim().isEmpty()){
                    course.setAcademicArea(null);
                } else {
                    AcademicArea academicArea = academicAreaDAO.getByCode(courseUtepsa.getAac_codigo().trim());
                    course.setAcademicArea(academicArea);
                }
                courseDAO.create(course);
            }else{
                if(courseUtepsa.getAac_codigo() == null || courseUtepsa.getAac_codigo().trim().isEmpty()){
                    course.setAcademicArea(null);
                } else {
                    AcademicArea academicArea = academicAreaDAO.getByCode(courseUtepsa.getAac_codigo().trim());
                    course.setAcademicArea(academicArea);
                }
            }
        }
    }


}
