package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.professor.ProfessorAdapter;
import com.utepsa.adapters.utepsa.professor.ProfessorData;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.models.Professor;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Luana Chavez on 14/03/2017.
 */
public class ProfessorTask extends Task{

    private SessionFactory sessionFactory;
    private ProfessorDAO professorDAO;
    private ProfessorAdapter professorAdapter;

    @Inject
    public ProfessorTask(SessionFactory sessionFactory, ProfessorAdapter professorAdapter, ProfessorDAO professorDAO) {
        super("migration_professor");
        this.sessionFactory = sessionFactory;
        this.professorDAO = professorDAO;
        this.professorAdapter = professorAdapter;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        ManagedSessionContext.bind(session);
        Transaction transaction = session.beginTransaction();
        try {
            try {
                List<ProfessorData> professorDatas = professorAdapter.getProfessors();
                if (professorDatas != null) {
                    for (ProfessorData professorData : professorDatas) {
                        if(professorDAO.getProfessorByAgendCode(professorData.getAgd_codigo())==null)
                        {
                            Professor newProfessor = new Professor();
                            newProfessor.setAgend_code(professorData.getAgd_codigo().trim());
                            newProfessor.setName(professorData.getAgd_nombres().trim());
                            newProfessor.setFather_lastname(professorData.getAgd_appaterno().trim());
                            newProfessor.setMother_lastname(professorData.getAgd_apmaterno().trim());
                            newProfessor.setState(true);
                            professorDAO.create(newProfessor);
                        }
                    }
                    transaction.commit();
                }
            } catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        }finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }
}
