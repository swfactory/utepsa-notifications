package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Document;
import com.utepsa.models.DocumentStudent;
import com.utepsa.models.Student;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 05/12/2016.
 */
public class DocumentsStudentTask extends Task {
    private SessionFactory sessionFactory;
    private DocumentStudentDAO documentStudentDAO;
    private StudentDAO studentDAO;
    private DocumentDAO documentDAO;
    private DocumentsStudentAdapter documentsStudentAdapter;

    @Inject
    public DocumentsStudentTask(SessionFactory sessionFactory,DocumentsStudentAdapter documentsStudentAdapter, DocumentStudentDAO documentStudentDAO, DocumentDAO documentDAO,StudentDAO studentDAO) {
        super("migration_documentsstudent");
        this.documentStudentDAO = documentStudentDAO;
        this.studentDAO = studentDAO;
        this.documentDAO = documentDAO;
        this.documentsStudentAdapter = documentsStudentAdapter;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
            try {
                ManagedSessionContext.bind(session);
                List <Student> students = studentDAO.getAll();
                for (Student student : students) {
                    transaction = session.beginTransaction();
                    if (student == null) {
                        continue;
                    }
                    boolean synchronizedStudent = false;

                    do {
                        try {
                            if (isOnlineNet()) {
                                List<DocumentsStudentData> listDocumentsStudentsData = documentsStudentAdapter.getAllDocumentsStudentForStudent(student.getRegisterNumber());
                                this.validatedDocumentsStudent(listDocumentsStudentsData, student);
                                synchronizedStudent = true;
                            } else {
                                Thread.sleep(5000);
                            }
                        } catch (Exception e) {
                            Thread.sleep(5000);
                            printWriter.write(e.getMessage() + " \n");
                        }
                    } while (!synchronizedStudent);

                    transaction.commit();
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void validatedDocumentsStudent(List<DocumentsStudentData> listDocumentsStudentData, Student student) throws  Exception {
        if(listDocumentsStudentData != null){
            if(listDocumentsStudentData.size() == 0){
                return;
            }

            List<DocumentStudent> documets = documentStudentDAO.getByStudent(student.getId());
            if (documets.size() == 0){
                for(DocumentsStudentData documentsStudentData : listDocumentsStudentData ){
                    this.createDocumentsStudent(documentsStudentData, student.getId());
                }
            }else {
                for(DocumentsStudentData documentsStudentData : listDocumentsStudentData ){
                    this.updateDocumentStudent(documets, documentsStudentData);
                }
            }
        }else{
            return;
        }
    }

    private void createDocumentsStudent(DocumentsStudentData documentsStudentData, long idStudent) throws Exception {
        DocumentStudent documentStudent = new DocumentStudent();

        Document document = documentDAO.getByCodeUtepsa(documentsStudentData.getDoc_codigo().trim());
        documentStudent.setIdStudent(idStudent);
        documentStudent.setTypePaper(documentsStudentData.getAlmdoc_tipopapel().trim());
        documentStudent.setDocument(document);

        if(documentsStudentData.getESTADO().trim().contains("ENTREGADO")){
            documentStudent.setState(true);
        }else {
            documentStudent.setState(false);
        }
        if(documentsStudentData.getAlmdoc_fechaentregar() == null){
            documentStudent.setDateDelivery(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateDelivery = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fechaentregar().trim());
            documentStudent.setDateDelivery(dateDelivery);
        }

        if(documentsStudentData.getAlmdoc_fecharecepcion() == null){
            documentStudent.setDateReceipt(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateReceipt = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fecharecepcion().trim());
            documentStudent.setDateDelivery(dateReceipt);
        }

        documentStudentDAO.create(documentStudent);
    }

    private boolean isOnlineNet(){
        try {
            Socket s = new Socket("www.google.com", 80);

            if(s.isConnected()){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    private void updateDocumentStudent(List<DocumentStudent> documets, DocumentsStudentData documentStudent) throws ParseException {
        for (DocumentStudent document : documets){
            if (document.getDocument().getCodeUtepsa().equals(documentStudent.getDoc_codigo())){
                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                if (documentStudent.getAlmdoc_fechaentregar() != null){
                    Date dateDelivery = formaterTimeZone.parse(documentStudent.getAlmdoc_fechaentregar());
                    document.setDateDelivery(dateDelivery);
                }
                if (documentStudent.getAlmdoc_fecharecepcion() != null){
                    Date dateReceipt = formaterTimeZone.parse(documentStudent.getAlmdoc_fecharecepcion());
                    document.setDateReceipt(dateReceipt);
                }
                document.setTypePaper(documentStudent.getAlmdoc_tipopapel());
                if (documentStudent.getESTADO().trim().contains("ENTREGADO")){
                    document.setState(true);
                }else{
                    document.setState(false);
                }
            }
        }
    }
}
