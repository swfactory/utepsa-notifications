package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 21/04/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.AcademicArea.getByCode",
                query = "SELECT a FROM AcademicArea a WHERE a.code = :code"
        ),
        @NamedQuery(
                name = "com.utepsa.models.AcademicArea.getAll",
                query = "SELECT a FROM AcademicArea a"
        )
})
@Table(name = "academic_area")
@ApiModel(value = "Academic Area entity", description = "Complete info of a entity academic area")
public class AcademicArea {

    private long id;
    private String code;
    private String description;

    public AcademicArea(){

    }

    public AcademicArea(long id, String code, String description){
        this.id = id;
        this.code = code;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Academic Area in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code", nullable = false)
    @ApiModelProperty(value = "The code of the Academic Area in application")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the Academic Area in application")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
