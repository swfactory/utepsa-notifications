package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by leonardo on 20-07-17.
 */
@Entity
@Table(name = "event_career")
@ApiModel(value = "event career", description = "the relation between Academic Event and Career")
public class EventCareer {

    private long id;
    private AcademicEvent idEvent;
    private Career idCareer;

    public EventCareer() {

    }

    public EventCareer(long id, AcademicEvent idEvent, Career idCareer) {
        this.id = id;
        this.idEvent = idEvent;
        this.idCareer = idCareer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of relation EventCareer in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(targetEntity = AcademicEvent.class)
    @JoinColumn(name = "id_event", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The relation between Academic Event and this table", required = true)
    public AcademicEvent getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(AcademicEvent idEvent) {
        this.idEvent = idEvent;
    }

    @ManyToOne(targetEntity = Career.class)
    @JoinColumn(name = "id_career", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The relation between Career and this table", required = true)
    public Career getIdCareer() {
        return idCareer;
    }

    public void setIdCareer(Career idCareer) {
        this.idCareer = idCareer;
    }

    @Override
    public String toString() {
        return String.format("eventCareer{id=%d,idEvent=%d,idCareer=%d}",
                this.id,this.idEvent,this.idCareer);
    }
}
