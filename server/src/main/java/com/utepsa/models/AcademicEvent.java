package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.awt.*;

/**
 * Created by leonardo on 20-07-17.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.AcademicEvent.getAll",
                query = "SELECT a FROM AcademicEvent a"
        )
})
@Table(name = "academic_event")
@ApiModel(value = "Academic Event", description = "Complete information of the academic event")
public class AcademicEvent {

    private long id;
    private CredentialAdministrator idCreator;
    private EventType idEventType;
    private String name;
    private String description;
    private String date;
    private int duration;
    private String hour;
    private int studentsQuantity;

    public AcademicEvent() {

    }

    public AcademicEvent(long id, CredentialAdministrator idCreator, EventType idEventType, String name, String description, String date, int duration, String hour, int studentsQuantity) {
        this.id = id;
        this.idCreator = idCreator;
        this.idEventType = idEventType;
        this.name = name;
        this.description = description;
        this.date = date;
        this.duration = duration;
        this.hour = hour;
        this.studentsQuantity = studentsQuantity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Academic Event in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(targetEntity = CredentialAdministrator.class)
    @JoinColumn(name = "id_creator", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The creator of the Academic Event in application", required = true)
    public CredentialAdministrator getIdCreator() {
        return idCreator;
    }

    public void setIdCreator(CredentialAdministrator idCreator) {
        this.idCreator = idCreator;
    }

    @OneToOne(targetEntity = EventType.class)
    @JoinColumn(name = "id_event_type", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The type of the Academic Event in application", required = true)
    public EventType getIdEventType() {
        return idEventType;
    }

    public void setIdEventType(EventType idEventType) {
        this.idEventType = idEventType;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Academic Event in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the Academic Event in application", required = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "date", nullable = false)
    @ApiModelProperty(value = "The date of the Academic Event", required = true)
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Basic
    @Column(name = "duration", nullable = false)
    @ApiModelProperty(value = "The duration of the Academic Event", required = true)
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "hour", nullable = false)
    @ApiModelProperty(value = "The hour of the beginning and the end of the Academic Event", required = true)
    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    @Basic
    @Column(name = "students_quantity", nullable = false)
    @ApiModelProperty(value = "The quantity of students who attend to the Academic Event", required = true)
    public int getStudentsQuantity() {
        return studentsQuantity;
    }

    public void setStudentsQuantity(int studentsQuantity) {
        this.studentsQuantity = studentsQuantity;
    }

    @Override
    public String toString() {
        return "AcademicEvent{" +
                "id=" + id +
                ", idCreator=" + idCreator +
                ", idEventType=" + idEventType +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", duration=" + duration +
                ", hour='" + hour + '\'' +
                ", studentsQuantity=" + studentsQuantity +
                '}';
    }
}
