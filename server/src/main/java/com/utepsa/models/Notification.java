package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Notification.getByStudent",
                query = "SELECT n FROM Notification n INNER JOIN n.audience a INNER JOIN a.type t "+
                        "WHERE (t.id = 1) OR " +
                        "(a.target = :registerNumber AND t.id = 2) OR " +
                        "(a.target = :idCareer AND t.id = 3) " +
                        "ORDER BY n.id DESC"

        )
})
@ApiModel(value = "notification entity", description = "Complete info of a entity notification")
public class Notification {

    private long id;
    private long dateCreation;
    private CredentialAdministrator idCreator;
    private String title;
    private String content;
    private CategoryNotification category;
    private Set<NotificationAudience> audience;

    public Notification() {
    }

    public Notification(long id, long dateCreation, CredentialAdministrator idCreator, String title, String content, CategoryNotification category, Set<NotificationAudience> audience) {
        this.id = id;
        this.dateCreation = dateCreation;
        this.idCreator = idCreator;
        this.title = title;
        this.content = content;
        this.category = category;
        this.audience = audience;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the notification in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "date_creation", nullable = false)
    @ApiModelProperty(value = "The dateCreation of the notification in application", required = true)
    public long getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(long dateCreation) {
        this.dateCreation = dateCreation;
    }

    @ManyToOne
    @JoinColumn(name = "id_creator", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The category of the notification in application", required = true)
    public CredentialAdministrator getIdCreator() {
        return idCreator;
    }

    public void setIdCreator(CredentialAdministrator idCreator) {
        this.idCreator = idCreator;
    }

    @Column(name = "title", nullable = false)
    @ApiModelProperty(value = "The title of the notification in application", required = true)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "content", nullable = false)
    @ApiModelProperty(value = "The content of the notification in application", required = true)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @ManyToOne
    @JoinColumn(name = "id_category", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The category of the notification in application", required = true)
    public CategoryNotification getCategory() {
        return category;
    }

    public void setCategory(CategoryNotification category) {
        this.category = category;
    }

    @NotNull
    @Column
    @ElementCollection(fetch = FetchType.EAGER, targetClass = NotificationAudience.class)
    @CollectionTable(name="notification_audience", joinColumns=@JoinColumn(name="id_notification"))
    @ApiModelProperty(value = "The audience of the Notification", required = true)
    public Set<NotificationAudience> getAudience() {
        return audience;
    }

    public void setAudience(Set<NotificationAudience> audience) {
        this.audience = audience;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", dateCreation=" + dateCreation +
                ", idCreator=" + idCreator +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", category=" + category +
                ", audience=" + audience +
                '}';
    }
}
