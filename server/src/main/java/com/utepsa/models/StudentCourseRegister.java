package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;


/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "student_course_register")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.StudentCourseRegister.getCourseRegisterByStudent",
                query = "SELECT s FROM StudentCourseRegister s WHERE s.semester = :semester AND s.idStudent = :idStudent AND (s.state = 'CONFIRMADA' OR s.state = 'PENDIENTE' OR s.state = 'A CONFIRMAR')"
        ),
        @NamedQuery(
                name = "com.utepsa.models.StudentCourseRegister.getAllCourseRegisterByStudent",
                query = "SELECT s FROM StudentCourseRegister s WHERE s.semester = :semester AND s.idStudent = :idStudent"
        ),
        @NamedQuery(
                name = "com.utepsa.models.StudentCourseRegister.getCourseRegisterById",
                query = "SELECT s FROM StudentCourseRegister s WHERE s.id = :id"
        ),
        @NamedQuery(
                name = "com.utepsa.models.StudentCourseRegister.deleteAllByStudent",
                query = "DELETE FROM StudentCourseRegister s WHERE s.idStudent = :idStudent AND s.semester = :semester"
        )
})
@ApiModel(value = "student_course_register entity", description = "Complete info of a entity student_course_register")
public class StudentCourseRegister {

    private long id;
    private long idStudent;
    private Course course;
    private Professor professor;
    private String semester;
    private String module;
    private String group;
    private Schedule schedule;
    private String turn;
    private String classroom;
    private long note;
    private long numberEnrolled;
    private long codeUtepsa;
    private String state;

    public StudentCourseRegister() {
    }

    public StudentCourseRegister(long id, long idStudent, Course course, Professor professor, String semester, String module, String group, Schedule schedule, String turn, String classroom, long note, long numberEnrolled, long codeUtepsa, String state) {
        this.id = id;
        this.idStudent = idStudent;
        this.course = course;
        this.professor = professor;
        this.semester = semester;
        this.module = module;
        this.group = group;
        this.schedule = schedule;
        this.turn = turn;
        this.classroom = classroom;
        this.note = note;
        this.numberEnrolled = numberEnrolled;
        this.codeUtepsa = codeUtepsa;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the course_student in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The student's registered course in application", required = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Basic
    @Column(name = "id_student", nullable = false)
    @ApiModelProperty(value = "The ID of the student in application", required = true)
    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    @ManyToOne
    @JoinColumn(name = "id_professor", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The course professor in application", required = true)
    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Basic
    @Column(name = "semester", nullable = false, length = 40)
    @ApiModelProperty(value = "The student's registered semester in application", required = true)
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "module", nullable = false, length = 20)
    @ApiModelProperty(value = "The student's registered course module in application", required = true)
    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Basic
    @Column(name = "group_course", length = 20)
    @ApiModelProperty(value = "The student's registered course group in application", required = true)
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @ManyToOne
    @JoinColumn(name = "schedule", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The schedule of the student's registered course in application", required = true)
    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Basic
    @Column(name = "turn", length = 50)
    @ApiModelProperty(value = "The tudent's registered course turn in application", required = true)
    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    @Basic
    @Column(name = "classroom", length = 20)
    @ApiModelProperty(value = "The registered classroom of the student in application", required = true)
    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    @Basic
    @Column(name = "number_enrolled", nullable = false)
    @ApiModelProperty(value = "Number of enrolled registered courses in application", required = true)
    public long getNumberEnrolled() {
        return numberEnrolled;
    }

    public void setNumberEnrolled(long numberEnrolled) {
        this.numberEnrolled = numberEnrolled;
    }

    @Basic
    @Column(name = "note", nullable = false)
    @ApiModelProperty(value = "The note of the course_student in application", required = true)
    public long getNote() {
        return note;
    }

    public void setNote(long note) {
        this.note = note;
    }


    @Basic
    @JsonIgnore
    @Column(name = "code_utepsa", nullable = false)
    public long getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(long codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "Registered Course Status in application", required = true)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("courseStudent{id=%d,course=%d,idStudent=%d,professor=%d,semester=%s,module=%s,group=%s,schedule=%s,turn=%s,classroom=%s,numberEnrolled=%d,state=%s}",
                this.id,this.course,this.idStudent,this.professor,this.semester,this.module,this.group,this.schedule,this.turn,this.classroom,this.numberEnrolled,this.state);
    }
}
