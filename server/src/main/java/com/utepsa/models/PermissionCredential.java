package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 13/02/2017.
 */
@Entity
@Table(name = "permission_credential")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.PermissionCredential.getByIdCredentialPermission",
                query = "SELECT u FROM PermissionCredential u " +
                        " INNER JOIN u.permission p WHERE u.credentialAdministrator = :idCredential AND p.id = :idPermission"
        ),
        @NamedQuery(
                name = "com.utepsa.models.PermissionCredential.getByIdCredential",
                query = "SELECT u FROM PermissionCredential u " +
                        " INNER JOIN u.permission p WHERE u.credentialAdministrator = :idCredential"
        )
})
@ApiModel(value = "Permission Credential entity", description = "Complete info of a entity permission credential")
public class PermissionCredential {
    private long id;
    private long credentialAdministrator;
    private Permission permission;
    private boolean state;

    public PermissionCredential (){

    }

    public PermissionCredential(long id, long credentialAdministrator, Permission permission, boolean state){
        this.id = id;
        this.credentialAdministrator = credentialAdministrator;
        this.permission = permission;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Permission Credential in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @JoinColumn(name = "id_credential", nullable = false)
    @Column(name = "id_credential")
    @ApiModelProperty(value = "The id credential administrator of the permission credential in application", required = true)
    public long getCredentialAdministrator() {
        return credentialAdministrator;
    }

    public void setCredentialAdministrator(long credentialAdministrator) {
        this.credentialAdministrator = credentialAdministrator;
    }

    @ManyToOne
    @JoinColumn(name = "id_permission", nullable = false)
    @ApiModelProperty(value = "The id permission of the permission credential in application", required = true)
    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the permission credential in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("PermissionCredential{id=%d,credentialAdministrator=%s,permission=%s,state=%s}",
                this.id,this.credentialAdministrator,this.permission,this.state);
    }
}