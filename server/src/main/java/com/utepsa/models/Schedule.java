package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 14/03/2017.
 */
@Entity
@Table(name = "schedule")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Schedule.getByCodeUtepsa",
                query = "SELECT s FROM Schedule s WHERE s.codeUtepsa = :codeUtepsa"
        )
})
@ApiModel(value = "Schedule entity", description = "Complete info of a entity Schedule")
public class Schedule {

    private long id;
    private String codeUtepsa;
    private String description;
    private String schedule;

    public Schedule(){

    }

    public Schedule(long id, String codeUtepsa, String description, String schedule){
        this.id = id;
        this.codeUtepsa = codeUtepsa;
        this.description = description;
        this.schedule = schedule;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Schedule in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Basic
    @Column(name = "code_utepsa", nullable = false)
    @ApiModelProperty(value = "The codeUtepsa of the Schedule in application")
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the Schedule in application")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "schedules", nullable = false)
    @ApiModelProperty(value = "The schedule of the Schedule in application")
    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return String.format("Schedule{id=%d,codeUtepsa=%s,description=%s,schedule=%s}",
                this.id,this.codeUtepsa,this.description,this.schedule);
    }
}
