package com.utepsa.models;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Embeddable
public class FileAudience {

    private String target;
    private TypeAudience type;

    public FileAudience() {
    }

    public FileAudience(String target, TypeAudience type) {
        this.target = target;
        this.type = type;
    }

    @Basic
    @Column(name = "target", nullable = false)
    @ApiModelProperty(value = "The target of the file_audience in application", required = true)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @ManyToOne
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The type of the file_audience in application", required = true)
    public TypeAudience getType() {
        return type;
    }

    public void setType(TypeAudience idType) {
        this.type = idType;
    }

    @Override
    public String toString() {
        return String.format("fileAudience{target=%s,type=%d}",
                this.target,this.type);
    }
}
