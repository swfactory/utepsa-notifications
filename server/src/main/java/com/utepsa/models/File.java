package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import javax.persistence.Entity;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.File.getAvailableByStudent",
                query = "SELECT f FROM File f " +
                        "INNER JOIN f.audience a INNER JOIN a.type t " +
                        " WHERE t.id = 1 OR " +
                        " (t.id = 3 AND a.target = :idCareer)" +
                        " AND f.state = true"
        )
})
@ApiModel(value = "file entity", description = "Complete info of a entity file")
public class File {

    private long id;
    private String name;
    private String description;
    private String url;
    private long dateUpload;
    private TypeFile type;
    private boolean state;
    private Set<FileAudience> audience;

    public File() {
    }

    public File(long id, String name, String description, String url, long dateUpload, TypeFile type, boolean state) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.dateUpload = dateUpload;
        this.type = type;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the file in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the file in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the file in application", required = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "url", nullable = false)
    @ApiModelProperty(value = "The url of the file in application", required = true)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "date_upload", nullable = false)
    @ApiModelProperty(value = "The dateUpload of the file in application", required = true)
    public long getDateUpload() {
        return dateUpload;
    }

    public void setDateUpload(long dateUpload) {
        this.dateUpload = dateUpload;
    }

    @ManyToOne
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The type of the file in application", required = true)
    public TypeFile getType() {
        return type;
    }

    public void setType(TypeFile type) {
        this.type = type;
    }

    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the file in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @NotNull
    @Column
    @ElementCollection(fetch = FetchType.EAGER, targetClass = FileAudience.class)
    @CollectionTable(name="file_audience", joinColumns=@JoinColumn(name="id_file"))
    @ApiModelProperty(value = "The audience of the File", required = true)
    public Set<FileAudience> getAudience() {
        return audience;
    }

    public void setAudience(Set<FileAudience> audience) {
        this.audience = audience;
    }

    @Override
    public String toString() {
        return String.format("file{id=%d,name=%s,description=%s,url=%s,dateUpload=%d,type=%d,state=%s}",
                this.id,this.name,this.description,this.url,this.dateUpload,this.type,this.state);
    }


}
