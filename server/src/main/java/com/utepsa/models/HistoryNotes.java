package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "history_note")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.historyNotes.getAllHistoryNotesByCourse",
                query = "SELECT h FROM HistoryNotes h WHERE h.idStudent = :idStudent AND h.course = :idCourse"
        ),
        @NamedQuery(
                name = "com.utepsa.models.historyNotes.getAllHistoryNotesByStudentAndSemester",
                query = "SELECT h FROM HistoryNotes h WHERE h.idStudent = :idStudent AND h.semester = :semester"
        )
})
@ApiModel(value = "HistoryNote entity", description = "Complete info of a entity HistoryNote")
public class HistoryNotes {

    private long id;
    private long idStudent;
    private Course course;
    private int note;
    private int minimunNote;
    private String semester;
    private String module;

    public HistoryNotes() {
    }

    public HistoryNotes(long id, long idStudent, Course course, int note, int minimunNote, String semester, String module) {
        this.id = id;
        this.idStudent = idStudent;
        this.course = course;
        this.note = note;
        this.minimunNote = minimunNote;
        this.semester = semester;
        this.module = module;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the history_note in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student", nullable = false)
    @ApiModelProperty(value = "The idStudent of the history_note in application", required = true)
    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The course of the history_note in application", required = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Basic
    @Column(name = "note", nullable = false)
    @ApiModelProperty(value = "The note of the history_note in application", required = true)
    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Basic
    @Column(name = "minimun_note", nullable = false)
    @ApiModelProperty(value = "The minimunNote of the history_note in application", required = true)
    public int getMinimunNote() {
        return minimunNote;
    }

    public void setMinimunNote(int minimunNote) {
        this.minimunNote = minimunNote;
    }

    @Basic
    @Column(name = "semester", nullable = false, length = 50)
    @ApiModelProperty(value = "The semester of the history_note in application", required = true)
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "module", nullable = false, length = 20)
    @ApiModelProperty(value = "The module of the history_note in application", required = true)
    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Override
    public String toString() {
        return String.format("historyNote{id=%d,idStudent=%d,course=%d,note=%d,minimunNote=%d,semester=%s,module=%s}",
                this.id,this.idStudent,this.course,this.note,this.minimunNote,this.semester,this.module);
    }
}
