package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Gerardo on 28/03/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.FinancialState.getByIdStudent",
                query = "SELECT u FROM FinancialState u WHERE u.idStudent = :idStudent"
        )
})
@Table(name = "financial_state")
@ApiModel(value = "Financial State entity", description = "Complete info of a entity financial state")
public class FinancialState {
    private long id;
    private Student idStudent;
    private double debitBalance;
    private double delinquentBalance;
    private double outstandingBalance;
    private Date expirationDate;

    public FinancialState(){

    }

    public FinancialState(long id, Student idStudent, double debitBalance, double delinquentBalance, double outstandingBalance, Date expirationDate){
        this.id = id;
        this.idStudent = idStudent;
        this.debitBalance = debitBalance;
        this.delinquentBalance = delinquentBalance;
        this.outstandingBalance = outstandingBalance;
        this.expirationDate = expirationDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Financial State in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_student", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The student of the financial state in application", required = true)
    public Student getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Student idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "debit_balance", nullable = false)
    @ApiModelProperty(value = "The debit balance of the Financial State in application")
    public double getDebitBalance() {
        return debitBalance;
    }

    public void setDebitBalance(double debitBalance) {
        this.debitBalance = debitBalance;
    }

    @Basic
    @Column(name = "delinquent_balance", nullable = false)
    @ApiModelProperty(value = "The delinquent balance of the Financial State in application")
    public double getDelinquentBalance() {
        return delinquentBalance;
    }

    public void setDelinquentBalance(double delinquentBalance) {
        this.delinquentBalance = delinquentBalance;
    }

    @Basic
    @Column(name = "outstanding_balance", nullable = false)
    @ApiModelProperty(value = "The outstanding balance of the Financial State in application")
    public double getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(double outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    @Basic
    @Column(name = "expiration_date", nullable = false)
    @ApiModelProperty(value = "The expiration date of the Financial State in application")
    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return String.format("financialState{id=%d,idStudent=%d,debitBalance=%d,delinquentBalance=%d,outstandingBalance=%d,expirationDate=%s}",
                this.id,this.idStudent,this.debitBalance,this.delinquentBalance,this.outstandingBalance,this.expirationDate);
    }
}
