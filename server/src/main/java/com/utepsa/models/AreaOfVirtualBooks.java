package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 20/09/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.AreaOfVirtualBooks.getAll",
                query = "SELECT a FROM AreaOfVirtualBooks a"
        )
})
@Table(name = "area_of_virtual_books")
@ApiModel(value = "Area Of Virtual Books", description = "Complete information of the area of virtual books")
public class AreaOfVirtualBooks {

    private long id;
    private String name;

    public AreaOfVirtualBooks(){

    }

    public AreaOfVirtualBooks(long id, String name){
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Area of Virtual Books in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Area Of Virtual Books in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AreaOfVirtualBooks{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
