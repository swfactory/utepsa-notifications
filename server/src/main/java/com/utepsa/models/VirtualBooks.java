package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 20/09/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.VirtualBooks.getAll",
                query = "SELECT v FROM VirtualBooks v"
        )
})
@Table(name = "virtual_books")
@ApiModel(value = "Virtual Books", description = "Complete information of the Virtual Books")
public class VirtualBooks {

    private long id;
    private String name;
    private String author;
    private String description;
    private AreaOfVirtualBooks areaOfVirtualBooks;
    private String url;

    public VirtualBooks(){ }

    public VirtualBooks(long id, String name, String author, String description, AreaOfVirtualBooks areaOfVirtualBooks, String url){
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
        this.areaOfVirtualBooks = areaOfVirtualBooks;
        this.url = url;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Virtual Books in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Virtual Books in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "author")
    @ApiModelProperty(value = "The author of the Virtual Books in application")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "description")
    @ApiModelProperty(value = "The description of the Virtual Books in application")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = "id_area", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The area of the virtual books in application", required = true)
    public AreaOfVirtualBooks getAreaOfVirtualBooks() {
        return areaOfVirtualBooks;
    }

    public void setAreaOfVirtualBooks(AreaOfVirtualBooks areaOfVirtualBooks) {
        this.areaOfVirtualBooks = areaOfVirtualBooks;
    }

    @Basic
    @Column(name = "url", nullable = false)
    @ApiModelProperty(value = "The url of the Virtual Books in application", required = true)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "VirtualBooks{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", areaOfVirtualBooks=" + areaOfVirtualBooks +
                ", url='" + url + '\'' +
                '}';
    }
}
