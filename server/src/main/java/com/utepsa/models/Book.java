package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;

/**
 * Created by Gerardo on 19/09/2017.
 */
@Entity()
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Book.getAll",
                query = "SELECT b FROM Book b"
        )
})
@ApiModel(value = "Book entity", description = "Complete info of a entity Book")
public class Book {
    private long id;
    private String author;
    private String title;
    private int quantity;
    private String publicationDate;
    private String content;
    private String location;
    private String description;
    private String editorial;
    private String pages;
    private String volumeNumber;
    private String numberOfTomes;
    private String isbn;
    private String numberEdition;

    public Book(){

    }

    public Book(long id, String author, String title, int quantity, String publicationDate, String content,
                String location, String description, String editorial, String pages, String volumeNumber, String numberOfTomes,
                String isbn, String numberEdition){
        this.id = id;
        this.author = author;
        this.title = title;
        this.quantity = quantity;
        this.publicationDate = publicationDate;
        this.content = content;
        this.location = location;
        this.description = description;
        this.editorial = editorial;
        this.pages = pages;
        this.volumeNumber = volumeNumber;
        this.numberOfTomes = numberOfTomes;
        this.isbn = isbn;
        this.numberEdition = numberEdition;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the book in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "author", nullable = false)
    @ApiModelProperty(value = "The author of the book in application", required = true)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "title", nullable = false)
    @ApiModelProperty(value = "The title of the book in application", required = true)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    @ApiModelProperty(value = "The quantity level of the book in application")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "publication_date", nullable = false)
    @ApiModelProperty(value = "The publication date of the book in application", required = true)
    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Basic
    @Column(name = "content", nullable = false)
    @ApiModelProperty(value = "The content of the book in application", required = true)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "location", nullable = false)
    @ApiModelProperty(value = "The location of the book in application", required = true)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the book in application", required = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "editorial", nullable = false)
    @ApiModelProperty(value = "The editorial of the book in application", required = true)
    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @Basic
    @Column(name = "pages", nullable = false)
    @ApiModelProperty(value = "The pages of the book in application", required = true)
    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    @Basic
    @Column(name = "volume_number", nullable = false)
    @ApiModelProperty(value = "The volume number of the book in application", required = true)
    public String getVolumeNumber() {
        return volumeNumber;
    }

    public void setVolumeNumber(String volumeNumber) {
        this.volumeNumber = volumeNumber;
    }

    @Basic
    @Column(name = "number_of_tomes", nullable = false)
    @ApiModelProperty(value = "The number of tomes of the book in application", required = true)
    public String getNumberOfTomes() {
        return numberOfTomes;
    }

    public void setNumberOfTomes(String numberOfTomes) {
        this.numberOfTomes = numberOfTomes;
    }

    @Basic
    @Column(name = "isbn", nullable = false)
    @ApiModelProperty(value = "The isbn of the book in application", required = true)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Basic
    @Column(name = "number_edition", nullable = false)
    @ApiModelProperty(value = "The number edition of the book in application", required = true)
    public String getNumberEdition() {
        return numberEdition;
    }

    public void setNumberEdition(String numberEdition) {
        this.numberEdition = numberEdition;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                ", publicationDate='" + publicationDate + '\'' +
                ", content='" + content + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", editorial='" + editorial + '\'' +
                ", pages='" + pages + '\'' +
                ", volumeNumber='" + volumeNumber + '\'' +
                ", numberOfTomes='" + numberOfTomes + '\'' +
                ", isbn='" + isbn + '\'' +
                ", numberEdition='" + numberEdition + '\'' +
                '}';
    }
}
