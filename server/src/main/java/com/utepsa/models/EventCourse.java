package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.eclipse.jetty.util.statistic.CounterStatistic;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by leonardo on 20-07-17.
 */
@Entity
@Table(name = "event_course")
@ApiModel(value = "event course", description = "the relation between Academic Event and Course")
public class EventCourse {

    private long id;
    private AcademicEvent idEvent;
    private Course idCourse;

    public EventCourse() {

    }

    public EventCourse(long id, AcademicEvent idEvent, Course idCourse) {
        this.id = id;
        this.idEvent = idEvent;
        this.idCourse = idCourse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of relation EventCourse in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(targetEntity = AcademicEvent.class)
    @JoinColumn(name = "id_event", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The relation between Academic Event and this table", required = true)
    public AcademicEvent getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(AcademicEvent idEvent) {
        this.idEvent = idEvent;
    }

    @ManyToOne(targetEntity = Course.class)
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The relation between Course and this table", required = true)
    public Course getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Course idCourse) {
        this.idCourse = idCourse;
    }

    @Override
    public String toString() {
        return String.format("eventCourse{id=%d,idEvent=%d,idCourse=%d}",
                this.id,this.idEvent,this.idCourse);
    }
}
