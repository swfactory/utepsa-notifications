package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by leonardo on 19-07-17.
 */
@Entity
@Table(name = "event_type")
@ApiModel(value = "event type", description = "the type of the academic event")
public class EventType {

    private long id;
    private String name;

    public EventType() {

    }

    public EventType(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the EventType in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Type of Event in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("evenType{id=%d,name=%s}",
                this.id,this.name);
    }
}
