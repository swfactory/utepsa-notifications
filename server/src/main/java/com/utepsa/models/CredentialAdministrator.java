package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;

/**
 * Created by Gerardo on 01/02/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.credentialAdministrator.login",
                query = "SELECT u FROM CredentialAdministrator u WHERE u.username = :username AND u.password = :password"
        ),
        @NamedQuery(
                name = "com.utepsa.models.credentialAdministrator.getById",
                query = "SELECT u FROM CredentialAdministrator u WHERE u.id= :id"
        ),
        @NamedQuery(
                name = "com.utepsa.models.credentialAdministrator.getAll",
                query = "SELECT u FROM CredentialAdministrator u"
        ),
        @NamedQuery(
                name = "com.utepsa.models.credentialAdministrator.getByUsername",
                query = "SELECT u FROM CredentialAdministrator u WHERE u.username = :username"
        )
})
@Table(name = "credential_administrator")
@ApiModel(value = "Credential Administrator entity", description = "Complete info of a entity credential for administrator")
public class CredentialAdministrator {
    private long id;
    private String username;
    private String password;
    private String token;
    private String gcmId;
    private String lastConnection;
    private boolean state;
    private Role role;
    private Profile profile;
    private boolean changePasswordForced;

    public CredentialAdministrator (){

    }

    public CredentialAdministrator(long id, String username, String password, String token, String gcmId, String lastConnection, boolean state, Profile profile, Role role, boolean changePasswordForced){
        this.id = id;
        this.username = username;
        this.password = password;
        this.token = token;
        this.gcmId = gcmId;
        this.lastConnection = lastConnection;
        this.state = state;
        this.role = role;
        this.profile = profile;
        this.changePasswordForced = changePasswordForced;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Credential Administrator in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false)
    @ApiModelProperty(value = "The username of the Credential Administrator in application", required = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Basic
    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password of the Credential Administrator in application")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "token", nullable = false)
    @ApiModelProperty(value = "The token of the Credential Administrator")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "gcm_id", nullable = false)
    @ApiModelProperty(value = "The gcmId of the Credential Administrator")
    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    @Basic
    @Column(name = "last_connection", nullable = false)
    @ApiModelProperty(value = "The LastConnection of the Credential Administrator")
    public String getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(String lastConnection) {
        this.lastConnection = lastConnection;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the Credential Administrator", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The role of the Credential Administrator in application", required = true)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "id_profile", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The profile of the Credential Administrator in application", required = true)
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Basic
    @Column(name = "change_password_forced", nullable = false)
    @ApiModelProperty(value = "The change password of the Credential Administrator in application", required = true)
    public boolean isChangePasswordForced() {
        return changePasswordForced;
    }

    public void setChangePasswordForced(boolean changePasswordForced) {
        this.changePasswordForced = changePasswordForced;
    }

    @Override
    public String toString() {
        return String.format("credentialAdministrator{id=%d,username=%s,password=%s,token=%s,gcmId=%s,lastConnection=%s,state=%s,role%d,profile=%d,changePasswordForced=%s}",
                this.id,this.username,this.password,this.token,this.gcmId,this.lastConnection,this.state,this.role,this.profile,this.changePasswordForced);
    }
}
