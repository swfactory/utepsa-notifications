package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;

/**
 * Created by Gerardo on 25/04/2017.
 */
@Entity
@Table(name = "groups_offered")
@ApiModel(value = "Groups Offered entity", description = "Complete info of a entity Groups Offered")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.GroupsOffered.getBySemesterCourseGroupAndModule",
                query = "SELECT gr FROM GroupsOffered gr WHERE gr.semester = :semester AND gr.course = :course AND gr.group = :group AND gr.module = :module"
        ),
        @NamedQuery(
                name = "com.utepsa.models.GroupsOffered.getByCourseAndSemester",
                query = "SELECT gr FROM GroupsOffered gr WHERE gr.semester = :semester AND gr.course = :course"
        )
})
public class GroupsOffered {

    private long id;
    private Course course;
    private Professor professor;
    private Schedule schedule;
    private String semester;
    private String module;
    private String group;
    private int numberEnrolled;
    private String classroom;

    public GroupsOffered(){

    }

    public GroupsOffered(long id, Course course, Professor professor, Schedule schedule, String semester, String module, String group, int numberEnrolled, String classroom){
        this.id = id;
        this.course = course;
        this.professor = professor;
        this.schedule = schedule;
        this.semester = semester;
        this.module = module;
        this.group = group;
        this.numberEnrolled = numberEnrolled;
        this.classroom = classroom;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Groups Offered in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The Course of the Groups Offered in application", required = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @ManyToOne
    @JoinColumn(name = "id_professor", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The Professor of the Groups Offered in application", required = true)
    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @ManyToOne
    @JoinColumn(name = "id_schedule", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The Schedule of the Groups Offered in application", required = true)
    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Basic
    @Column(name = "semester", nullable = false)
    @ApiModelProperty(value = "The semester of the Groups Offered in application")
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "module", nullable = false)
    @ApiModelProperty(value = "The module of the Groups Offered in application")
    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Basic
    @Column(name = "group_course", nullable = false)
    @ApiModelProperty(value = "The group of the Groups Offered in application")
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Basic
    @Column(name = "number_enrolled", nullable = false)
    @ApiModelProperty(value = "The number enrolled of the Groups Offered in application")
    public int getNumberEnrolled() {
        return numberEnrolled;
    }

    public void setNumberEnrolled(int numberEnrolled) {
        this.numberEnrolled = numberEnrolled;
    }

    @Basic
    @Column(name = "classroom", nullable = false)
    @ApiModelProperty(value = "The classroom of the Groups Offered in application")
    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    @Override
    public String toString() {
        return String.format("groupsOffered{id=%d,course=%d,professor=%d,schedule=%d,semester=%s,module=%s,group=%s,numberEnrolled=%d,classroom=%s}",
                this.id,this.course,this.professor,this.schedule,this.semester,this.module,this.group,this.numberEnrolled,this.classroom);
    }
}
