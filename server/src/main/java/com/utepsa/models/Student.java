package com.utepsa.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utepsa.api.serializers.StudentSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "student")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.student.getByAgendCode",
                query = "SELECT s FROM Student s WHERE s.agendCode = :agendCode"
        )
})
@ApiModel(value = "student entity", description = "Complete info of a entity student")
@JsonSerialize(using = StudentSerializer.class)
public class Student {

    private long id;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private Date birthday;
    private String gender;
    private String email1;
    private String email2;
    private String phoneNumber1;
    private String phoneNumber2;
    private String registerNumber;
    private String agendCode;
    private String documentType;
    private String documentNumber;
    private int pensum;
    private Career career;
    private String firstSemester;
    private String state;

    public Student() {
    }

    public Student(long id) {
        this.id = id;
    }

    public Student(long id, String name, String fatherLastname, String motherLastname, Date birthday, String gender, String email1, String email2, String phoneNumber1, String phoneNumber2, String registerNumber, String agendCode, String documentType, String documentNumber, int pensum, Career career, String firstSemester, String state) {
        this.id = id;
        this.name = name;
        this.fatherLastname = fatherLastname;
        this.motherLastname = motherLastname;
        this.birthday = birthday;
        this.gender = gender;
        this.email1 = email1;
        this.email2 = email2;
        this.phoneNumber1 = phoneNumber1;
        this.phoneNumber2 = phoneNumber2;
        this.registerNumber = registerNumber;
        this.agendCode = agendCode;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.pensum = pensum;
        this.career = career;
        this.firstSemester = firstSemester;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the student in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the student in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "father_lastname", nullable = false)
    @ApiModelProperty(value = "The fatherLastname of the student in application", required = true)
    public String getFatherLastname() {
        return fatherLastname;
    }

    public void setFatherLastname(String fatherLastname) {
        this.fatherLastname = fatherLastname;
    }

    @Basic
    @Column(name = "mother_lastname", nullable = false)
    @ApiModelProperty(value = "The motherLastname of the student in application", required = true)
    public String getMotherLastname() {
        return motherLastname;
    }

    public void setMotherLastname(String motherLastname) {
        this.motherLastname = motherLastname;
    }

    @Basic
    @Column(name = "birthday", nullable = true)
    @ApiModelProperty(value = "The birthday of the student in application", required = true)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "gender", nullable = false)
    @ApiModelProperty(value = "The gender of the student in application", required = true)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "email1", nullable = false)
    @ApiModelProperty(value = "The email1 of the student in application", required = true)
    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    @Basic
    @Column(name = "email2", nullable = true)
    @ApiModelProperty(value = "The email2 of the student in application", required = true)
    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    @Basic
    @Column(name = "phone_number1", nullable = false)
    @ApiModelProperty(value = "The phoneNumber1 of the student in application", required = true)
    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    @Basic
    @Column(name = "phone_number2", nullable = false)
    @ApiModelProperty(value = "The phoneNumber2 of the student in application", required = true)
    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    @Basic
    @Column(name = "register_number", nullable = false, length = 20)
    @ApiModelProperty(value = "The registerNumber of the student in application", required = true)
    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    @Basic
    @Column(name = "agend_code", nullable = false, length = 20)
    @ApiModelProperty(value = "The agendCode of the student in application", required = true)
    public String getAgendCode() {
        return agendCode;
    }

    public void setAgendCode(String agendCode) {
        this.agendCode = agendCode;
    }

    @Basic
    @Column(name = "document_type", nullable = false, length = 50)
    @ApiModelProperty(value = "The documentType of the student in application", required = true)
    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @Basic
    @Column(name = "document_number", nullable = false, length = 30)
    @ApiModelProperty(value = "The documentNumber of the student in application", required = true)
    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @Basic
    @Column(name = "pensum", nullable = false, length = 5)
    @ApiModelProperty(value = "The pensum of the student in application", required = true)
    public int getPensum() {
        return pensum;
    }

    public void setPensum(int pensum) {
        this.pensum = pensum;
    }

    @ManyToOne
    @JoinColumn(name = "id_career", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The career of the student in application", required = true)
    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    @Transient
    public String getFirstSemester() {
        return firstSemester;
    }

    public void setFirstSemester(String firstSemester) {
        this.firstSemester = firstSemester;
    }


    @Basic
    @Column(name = "state", nullable = false, length = 5)
    @ApiModelProperty(value = "The state of the student in application", required = true)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("student{registerNumber=%s,agendCode=%s,documentType=%s,documentNumber=%s,pensum=%s,career=%d}",
                this.registerNumber,this.agendCode,this.documentType,this.documentNumber,this.pensum,this.career);
    }
}
