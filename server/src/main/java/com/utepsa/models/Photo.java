package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by leonardo on 20-07-17.
 */
@Entity
@ApiModel(value = "photo entity", description = "Complete info of a photo")
public class Photo {

    private long id;
    private AcademicEvent idEvento;
    private String direction;

    public Photo() {

    }

    public Photo(long id, AcademicEvent idEvento, String direction) {
        this.id = id;
        this.idEvento = idEvento;
        this.direction = direction;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Photo in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(targetEntity = AcademicEvent.class)
    @JoinColumn(name = "id_event", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The event of the photo in application", required = true)
    public AcademicEvent getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(AcademicEvent idEvento) {
        this.idEvento = idEvento;
    }

    @Column(name = "direction", nullable = false)
    @ApiModelProperty(value = "The direction of the photo", required = true)
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
         return String.format("file{id=%d,id_event=%d,direction=%s}",
                this.id,this.idEvento,this.direction);

    }
}
