package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import javax.persistence.Entity;


/**
 * Created by Leonardo on 15/05/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.FileCareer.getAllFilesByIdCareer",
                query = "SELECT f FROM FileCareer f " +
                        "INNER JOIN f.career c " +
                        " WHERE c.id = :idCareer"
        )
})
@Table(name = "file_career")
@ApiModel(value = "file_career entity", description = "All the documentation per career")
public class FileCareer {
    private long id;
    private File file;
    private Career career;

    public FileCareer() {}

    public FileCareer(long id, File file, Career career) {
        this.id = id;
        this.file = file;
        this.career = career;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the documentation per career", required = true)
    public long getId() {return id;}
    public void setId(long id) {this.id = id;}

    @ManyToOne
    @JoinColumn(name = "id_file", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The id of the file", required = true)
    public File getFile() {return file;}
    public void setFile(File file) {this.file = file;}


    @ManyToOne
    @JoinColumn(name = "id_career", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The id of the career", required = true)
    public Career getCareer() {return career;}
    public void setCareer(Career career) {this.career = career;}
}
