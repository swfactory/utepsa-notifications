package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;


/**
 * Created by David on 22/01/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Career.getByCodeUtepsa",
                query = "SELECT c FROM Career c WHERE codeUtepsa = :codeUtepsa"
        ),
        @NamedQuery(
                name = "com.utepsa.models.Career.getByGrade",
                query = "select c from Career c INNER JOIN c.faculty f " +
                        "WHERE f.grade = :grade"
        )
})
@ApiModel(value = "Career entity", description = "Complete info of a entity Career")
public class Career {

    private long id;
    private String codeUtepsa;
    private String name;
    private int currentPensum;
    private Faculty faculty;
    private long sameCareer;
    private String plan;
    private boolean careerActive;

    public Career() {
    }

    public Career(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Career in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    @Basic
    @Column(name = "code_utepsa", nullable = true, length = 10)
    @ApiModelProperty(value = "The code of the Career in Utepsa")
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    @ApiModelProperty(value = "The name of the Career in Utepsa", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "current_pensum", nullable = false)
    @ApiModelProperty(value = "The current pensum of the Career in Utepsa", required = true)
    public int getCurrentPensum() {
        return currentPensum;
    }

    public void setCurrentPensum(int currentPensum) {
        this.currentPensum = currentPensum;
    }

    @ManyToOne
    @JoinColumn(name = "id_faculty", referencedColumnName = "id", nullable = false)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Basic
    @Column(name = "same_career", nullable = false)
    @ApiModelProperty(value = "The same career of the Career in Utepsa", required = true)
    public long getSameCareer() {
        return sameCareer;
    }

    public void setSameCareer(long same_career) {
        this.sameCareer = same_career;
    }

    @Basic
    @Column(name = "plan", nullable = false)
    @ApiModelProperty(value = "The plan of the Career in Utepsa", required = true)
    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }


    @Basic
    @Column(name = "career_active", nullable = false)
    @ApiModelProperty(value = "The career active of the Career in Utepsa", required = true)
    public boolean isCareerActive() {
        return careerActive;
    }
    public void setCareerActive(boolean careerActive) {
        this.careerActive = careerActive;
    }
}

