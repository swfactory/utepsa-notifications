package com.utepsa.api.views;

import org.hibernate.annotations.Subselect;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Subselect("select * from view_history_note")
public class ViewHistoryNote {
    private Long idStudent;
    private String course;
    private Integer note;
    private Integer minimunNote;
    private String semester;
    private String module;

    public ViewHistoryNote() {
    }

    @Id
    @GeneratedValue
    @Column(name = "id_student", updatable = false, nullable = false)
    public Long getIdStudent() {
        return idStudent;
    }

    @Column(name = "course", nullable = true, length = 255)
    public String getCourse() {
        return course;
    }

    @Column(name = "note", nullable = true)
    public Integer getNote() {
        return note;
    }

    @Column(name = "minimun_note", nullable = true)
    public Integer getMinimunNote() {
        return minimunNote;
    }

    @Column(name = "semester", nullable = true, length = 50)
    public String getSemester() {
        return semester;
    }

    @Column(name = "module", nullable = true, length = 20)
    public String getModule() {
        return module;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewHistoryNote that = (ViewHistoryNote) o;

        if (idStudent != null ? !idStudent.equals(that.idStudent) : that.idStudent != null) return false;
        if (course != null ? !course.equals(that.course) : that.course != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (minimunNote != null ? !minimunNote.equals(that.minimunNote) : that.minimunNote != null) return false;
        if (semester != null ? !semester.equals(that.semester) : that.semester != null) return false;
        if (module != null ? !module.equals(that.module) : that.module != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent != null ? idStudent.hashCode() : 0;
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (minimunNote != null ? minimunNote.hashCode() : 0);
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + (module != null ? module.hashCode() : 0);
        return result;
    }
}
