package com.utepsa.api.custom;

/**
 * Created by Gerardo on 24/08/2017.
 */
public class MissingCourse {
    private long idCourse;
    private String name;
    private int pensumLevel;

    public MissingCourse() {
    }

    public long getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(long idCourse) {
        this.idCourse = idCourse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPensumLevel() {
        return pensumLevel;
    }

    public void setPensumLevel(int pensumLevel) {
        this.pensumLevel = pensumLevel;
    }
}
