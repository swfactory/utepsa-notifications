package com.utepsa.api.custom.census;

/**
 * Created by Luana Chavez on 04/10/2017.
 */
public class MasculineAnswers {

    private String response;
    private Long quantity;

    public MasculineAnswers() {
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
