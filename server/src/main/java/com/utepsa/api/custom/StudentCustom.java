package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 12/05/2017.
 */
public class StudentCustom {
    private long idStudent;
    private long idCareer;
    private String gender;

    public StudentCustom() {
    }

    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    public long getIdCareer() {
        return idCareer;
    }

    public void setIdCareer(long idCareer) {
        this.idCareer = idCareer;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}

