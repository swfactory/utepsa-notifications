package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 31/07/2017.
 */
public class CareerAverageReport {
    private String nameCareer;
    private String  average;

    public CareerAverageReport() {
    }

    public String getNameCareer() {
        return nameCareer;
    }

    public void setNameCareer(String nameCareer) {
        this.nameCareer = nameCareer;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }
}
