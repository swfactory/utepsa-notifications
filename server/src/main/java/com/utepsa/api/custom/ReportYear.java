package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 07/07/2017.
 */
public class ReportYear {
    private String yearnumber1;
    private String yearnumber2;
    private String yearnumber3;

    public String getYearnumber1() {
        return yearnumber1;
    }

    public void setYearnumber1(String yearnumber1) {
        this.yearnumber1 = yearnumber1;
    }

    public String getYearnumber2() {
        return yearnumber2;
    }

    public void setYearnumber2(String yearnumber2) {
        this.yearnumber2 = yearnumber2;
    }

    public String getYearnumber3() {
        return yearnumber3;
    }

    public void setYearnumber3(String yearnumber3) {
        this.yearnumber3 = yearnumber3;
    }
}
