package com.utepsa.api.custom;

import java.math.BigInteger;

/**
 * Created by Gerardo on 09/08/2017.
 */
public class BestAveragePerSemester {
    private Long idStudent;
    private double average;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private String registerNumber;
    private String agendCode;
    private long carriedCourse;
    private long pensum;
    private String firstSemester;
    private BigInteger totalCarriedCourse;
    private BigInteger careerTotalCourse;

    public BestAveragePerSemester(){

    }

    public Long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Long idStudent) {
        this.idStudent = idStudent;
    }
    
    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherLastname() {
        return fatherLastname;
    }

    public void setFatherLastname(String fatherLastname) {
        this.fatherLastname = fatherLastname;
    }

    public String getMotherLastname() {
        return motherLastname;
    }

    public void setMotherLastname(String motherLastname) {
        this.motherLastname = motherLastname;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getAgendCode() {
        return agendCode;
    }

    public void setAgendCode(String agendCode) {
        this.agendCode = agendCode;
    }

    public long getCarriedCourse() {
        return carriedCourse;
    }

    public void setCarriedCourse(long carriedCourse) {
        this.carriedCourse = carriedCourse;
    }

    public long getPensum() {
        return pensum;
    }

    public void setPensum(long pensum) {
        this.pensum = pensum;
    }

    public String getFirstSemester() {
        return firstSemester;
    }

    public void setFirstSemester(String firstSemester) {
        this.firstSemester = firstSemester;
    }

    public BigInteger getTotalCarriedCourse() {
        return totalCarriedCourse;
    }

    public void setTotalCarriedCourse(BigInteger totalCarriedCourse) {
        this.totalCarriedCourse = totalCarriedCourse;
    }

    public BigInteger getCareerTotalCourse() {
        return careerTotalCourse;
    }

    public void setCareerTotalCourse(BigInteger careerTotalCourse) {
        this.careerTotalCourse = careerTotalCourse;
    }
}
