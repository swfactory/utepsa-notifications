package com.utepsa.api.custom;

import java.util.List;

/**
 * Created by Luana Chavez on 24/04/2017.
 */
public class DataCareer {
    private long idCareer;
    private long sameCareer;
    private String plan;
    private String nameCareer;
    private String average;
    private Long studentCareer;
    private Long studentCareerMan;
    private Long studentCareerWoman;
    private Long studentActive;
    private Long studentActiveMan;
    private Long studentActiveWoman;
    private Long semesterCourses;
    private Long approvedCourses;
    private Long disapprovedCourses;
    private Long abandonedCourses;
    private List<ModulesReport> module;
    private List<StudentParExcellence> studentParExcellences;

    public DataCareer() {
    }

    public long getIdCareer() {
        return idCareer;
    }

    public void setIdCareer(long idCareer) {
        this.idCareer = idCareer;
    }

    public long getSameCareer() {
        return sameCareer;
    }

    public void setSameCareer(long sameCareer) {
        this.sameCareer = sameCareer;
    }

    public String getNameCareer() {
        return nameCareer;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public void setNameCareer(String nameCareer) {
        this.nameCareer = nameCareer;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public Long getStudentCareer() {
        return studentCareer;
    }

    public void setStudentCareer(Long studentCareer) {
        this.studentCareer = studentCareer;
    }

    public Long getStudentCareerMan() {
        return studentCareerMan;
    }

    public void setStudentCareerMan(Long studentCareerMan) {
        this.studentCareerMan = studentCareerMan;
    }

    public Long getStudentCareerWoman() {
        return studentCareerWoman;
    }

    public void setStudentCareerWoman(Long studentCareerWoman) {
        this.studentCareerWoman = studentCareerWoman;
    }

    public Long getStudentActive() {
        return studentActive;
    }

    public void setStudentActive(Long studentActive) {
        this.studentActive = studentActive;
    }

    public Long getStudentActiveMan() {
        return studentActiveMan;
    }

    public void setStudentActiveMan(Long studentActiveMan) {
        this.studentActiveMan = studentActiveMan;
    }

    public Long getStudentActiveWoman() {
        return studentActiveWoman;
    }

    public void setStudentActiveWoman(Long studentActiveWoman) {
        this.studentActiveWoman = studentActiveWoman;
    }

    public Long getSemesterCourses() {
        return semesterCourses;
    }

    public void setSemesterCourses(Long semesterCourses) {
        this.semesterCourses = semesterCourses;
    }

    public Long getApprovedCourses() {
        return approvedCourses;
    }

    public void setApprovedCourses(Long approvedCourses) {
        this.approvedCourses = approvedCourses;
    }

    public Long getDisapprovedCourses() {
        return disapprovedCourses;
    }

    public void setDisapprovedCourses(Long disapprovedCourses) {
        this.disapprovedCourses = disapprovedCourses;
    }

    public Long getAbandonedCourses() {
        return abandonedCourses;
    }

    public void setAbandonedCourses(Long abandonedCourses) {
        this.abandonedCourses = abandonedCourses;
    }

    public List<ModulesReport> getModule() {
        return module;
    }

    public void setModule(List<ModulesReport> module) {
        this.module = module;
    }

    public List<StudentParExcellence> getStudentParExcellences() {
        return studentParExcellences;
    }

    public void setStudentParExcellences(List<StudentParExcellence> studentParExcellences) {
        this.studentParExcellences = studentParExcellences;
    }
}
