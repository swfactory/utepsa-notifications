package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 24/07/2017.
 */
public class CourseModuleReport {

    private Long studentRegister;
    private String courseGroup;
    private String courseName;
    private String scheduleCourse;

    public Long getStudentRegister() {
        return studentRegister;
    }

    public void setStudentRegister(Long studentRegister) {
        this.studentRegister = studentRegister;
    }

    public String getCourseGroup() {
        return courseGroup;
    }

    public void setCourseGroup(String courseGroup) {
        this.courseGroup = courseGroup;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getScheduleCourse() {
        return scheduleCourse;
    }

    public void setScheduleCourse(String scheduleCourse) {
        this.scheduleCourse = scheduleCourse;
    }
}
