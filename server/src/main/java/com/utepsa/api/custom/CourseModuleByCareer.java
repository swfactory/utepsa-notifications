package com.utepsa.api.custom;

import java.util.List;

/**
 * Created by Luana Chavez on 24/07/2017.
 */
public class CourseModuleByCareer {
    private List<CourseModuleReport> courseModuleCareer;

    public List<CourseModuleReport> getCourseModuleCareer() {
        return courseModuleCareer;
    }

    public void setCourseModuleCareer(List<CourseModuleReport> courseModuleCareer) {
        this.courseModuleCareer = courseModuleCareer;
    }
}
