package com.utepsa.api.custom.census;

import java.util.List;

/**
 * Created by Luana Chavez on 04/10/2017.
 */
public class Answers {
    private List<FemenineAnswers> isWorkingFemenine;
    private List<MasculineAnswers> isWorkingMasculine;
    private List<FemenineAnswers> anotherLanguageFemenine;
    private List<MasculineAnswers> anotherLanguageMasculine;
    private List<FemenineAnswers> bilingualSchoolFemenine;
    private List<MasculineAnswers> bilingualSchoolMasculine;
    private List<FemenineAnswers> communicationUniversityStudentFemenine;
    private List<MasculineAnswers> communicationUniversityStudentMasculine;
    private List<FemenineAnswers> suggestionsMetFemenine;
    private List<MasculineAnswers> suggestionsMetMasculine;
    private List<FemenineAnswers> courseProgrammingFemenine;
    private List<MasculineAnswers> courseProgrammingMasculine;
    private List<FemenineAnswers> accessCourseNoteFemenine;
    private List<MasculineAnswers> accessCourseNoteMasculine;
    private List<FemenineAnswers> classroomsFemenine;
    private List<MasculineAnswers> classroomsMasculine;
    private List<FemenineAnswers> laboratoriesFemenine;
    private List<MasculineAnswers> laboratoriesMasculine;
    private List<FemenineAnswers> laboratoriesAssignmentFemenine;
    private List<MasculineAnswers> laboratoriesAssignmentMasculine;
    private List<FemenineAnswers> libraryServiceFemenine;
    private List<MasculineAnswers> libraryServiceMasculine;
    private List<FemenineAnswers> treatmentPlatformStaffFemenine;
    private List<MasculineAnswers> treatmentPlatformStaffMasculine;
    private List<FemenineAnswers> quickAttentionPlatformResponseFemenine;
    private List<MasculineAnswers> quickAttentionPlatformResponseMasculine;
    private List<FemenineAnswers> knowledgePlatformStaffFemenine;
    private List<MasculineAnswers> knowledgePlatformStaffMasculine;
    private List<FemenineAnswers> platformStaffResponseFemenine;
    private List<MasculineAnswers> platformStaffResponseMasculine;
    private List<FemenineAnswers> careerInformationFemenine;
    private List<MasculineAnswers> careerInformationMasculine;
    private List<FemenineAnswers> paymentInformationFemenine;
    private List<MasculineAnswers> paymentInformationMasculine;
    private List<FemenineAnswers> punctualityClassesFemenine;
    private List<MasculineAnswers> punctualityClassesMasculine;
    private List<FemenineAnswers> knowledgeCourseFemenine;
    private List<MasculineAnswers> knowledgeCourseMasculine;
    private List<FemenineAnswers> usedToolsClassesFemenine;
    private List<MasculineAnswers> usedToolsClassesMasculine;
    private List<FemenineAnswers> supportMaterialFemenine;
    private List<MasculineAnswers> supportMaterialMasculine;
    private List<FemenineAnswers> criteriaEvaluationFemenine;
    private List<MasculineAnswers> criteriaEvaluationMasculine;
    private List<FemenineAnswers> professionalExperienceFemenine;
    private List<MasculineAnswers> professionalExperienceMasculine;
    private List<FemenineAnswers> knowledgeStudyPlanFemenine;
    private List<MasculineAnswers> knowledgeStudyPlanMasculine;
    private List<FemenineAnswers> upportHeadCareerFemenine;
    private List<MasculineAnswers> upportHeadCareerMasculine;
    private List<FemenineAnswers> expertProgramCourseFemenine;
    private List<MasculineAnswers> expertProgramCourseMasculine;
    private List<FemenineAnswers> contributionExpertProgramCourseFemenine;
    private List<MasculineAnswers> contributionExpertProgramCourseMasculine;
    private List<FemenineAnswers> contentCourseTopicFemenine;
    private List<MasculineAnswers> contentCourseTopicMasculine;
    private List<FemenineAnswers> adequateCoursePaymentFemenine;
    private List<MasculineAnswers> adequateCoursePaymentMasculine;
    private List<FemenineAnswers> jetsParticipationFemenine;
    private List<MasculineAnswers> jetsParticipationMasculine;
    private List<FemenineAnswers> contributionJetsParticipationFemenine;
    private List<MasculineAnswers> contributionJetsParticipationMasculine;
    private List<FemenineAnswers> sadisfaccionContributionJetsParticipationFemenine;
    private List<MasculineAnswers> sadisfaccionContributionJetsParticipationMasculine;
    private List<FemenineAnswers> knowledgeUniversitySocialResponsabilityFemenine;
    private List<MasculineAnswers> knowledgeUniversitySocialResponsabilityMasculine;
    private List<FemenineAnswers> contributionUniversitySocialResponsabilityFemenine;
    private List<MasculineAnswers> contributionUniversitySocialResponsabilityMasculine;
    private List<FemenineAnswers> knowledgeUniversityInternacionalizationFemenine;
    private List<MasculineAnswers> knowledgeUniversityInternacionalizationMasculine;
    private List<FemenineAnswers> contributionUniversityInternacionalizationFemenine;
    private List<MasculineAnswers> contributionUniversityInternacionalizationMasculine;
    private List<FemenineAnswers> studentUniversityPrideFemenine;
    private List<MasculineAnswers> studentUniversityPrideMasculine;
    private List<FemenineAnswers> professionalImageFemenine;
    private List<MasculineAnswers> professionalImageMasculine;
    private List<FemenineAnswers> careerQualityFemenine;
    private List<MasculineAnswers> careerQualityMasculine;
    private List<FemenineAnswers> infrastructureFemenine;
    private List<MasculineAnswers> infrastructureMasculine;
    private List<FemenineAnswers> classScheduleFemenine;
    private List<MasculineAnswers> classScheduleMasculine;
    private List<FemenineAnswers> prestigeInstitutionFemenine;
    private List<MasculineAnswers> prestigeInstitutionMasculine;
    private List<FemenineAnswers> greatTeachersFemenine;
    private List<MasculineAnswers> greatTeachersMasculine;
    private List<FemenineAnswers> modularSystemFemenine;
    private List<MasculineAnswers> modularSystemMasculine;
    private List<FemenineAnswers> accesiblePriceFemenine;
    private List<MasculineAnswers> accesiblePriceMasculine;
    private List<FemenineAnswers> referencesFemenine;
    private List<MasculineAnswers> referencesMasculine;
    private List<FemenineAnswers> laboratoriesEquipmentFemenine;
    private List<MasculineAnswers> laboratoriesEquipmentMasculine;
    private List<FemenineAnswers> communicationFemenine;
    private List<MasculineAnswers> communicationMasculine;
    private List<FemenineAnswers> graduateGreatStudentFemenine;
    private List<MasculineAnswers> graduateGreatStudentMasculine;
    private List<FemenineAnswers> adequateRequirementStudiesFemenine;
    private List<MasculineAnswers> adequateRequirementStudiesMasculine;
    private List<FemenineAnswers> studentServiceFemenine;
    private List<MasculineAnswers> studentServiceMasculine;
    private List<FemenineAnswers> reputationMiddleCityFemenine;
    private List<MasculineAnswers> reputationMiddleCityMasculine;
    private List<FemenineAnswers> universitySupervisionFemenine;
    private List<MasculineAnswers> universitySupervisionMasculine;
    private List<FemenineAnswers> universityRecommendationFemenine;
    private List<MasculineAnswers> universityRecommendationMasculine;
    private List<FemenineAnswers> qualificationTeachersFemenine;
    private List<MasculineAnswers> qualificationTeachersMasculine;
    private List<FemenineAnswers> typeLanguageFemenine;
    private List<MasculineAnswers> typeLanguageMasculine;

    public Answers() {
    }

    public List<FemenineAnswers> getIsWorkingFemenine() {
        return isWorkingFemenine;
    }

    public void setIsWorkingFemenine(List<FemenineAnswers> isWorkingFemenine) {
        this.isWorkingFemenine = isWorkingFemenine;
    }

    public List<MasculineAnswers> getIsWorkingMasculine() {
        return isWorkingMasculine;
    }

    public void setIsWorkingMasculine(List<MasculineAnswers> isWorkingMasculine) {
        this.isWorkingMasculine = isWorkingMasculine;
    }

    public List<FemenineAnswers> getAnotherLanguageFemenine() {
        return anotherLanguageFemenine;
    }

    public void setAnotherLanguageFemenine(List<FemenineAnswers> anotherLanguageFemenine) {
        this.anotherLanguageFemenine = anotherLanguageFemenine;
    }

    public List<MasculineAnswers> getAnotherLanguageMasculine() {
        return anotherLanguageMasculine;
    }

    public void setAnotherLanguageMasculine(List<MasculineAnswers> anotherLanguageMasculine) {
        this.anotherLanguageMasculine = anotherLanguageMasculine;
    }

    public List<FemenineAnswers> getBilingualSchoolFemenine() {
        return bilingualSchoolFemenine;
    }

    public void setBilingualSchoolFemenine(List<FemenineAnswers> bilingualSchoolFemenine) {
        this.bilingualSchoolFemenine = bilingualSchoolFemenine;
    }

    public List<MasculineAnswers> getBilingualSchoolMasculine() {
        return bilingualSchoolMasculine;
    }

    public void setBilingualSchoolMasculine(List<MasculineAnswers> bilingualSchoolMasculine) {
        this.bilingualSchoolMasculine = bilingualSchoolMasculine;
    }

    public List<FemenineAnswers> getCommunicationUniversityStudentFemenine() {
        return communicationUniversityStudentFemenine;
    }

    public void setCommunicationUniversityStudentFemenine(List<FemenineAnswers> communicationUniversityStudentFemenine) {
        this.communicationUniversityStudentFemenine = communicationUniversityStudentFemenine;
    }

    public List<MasculineAnswers> getCommunicationUniversityStudentMasculine() {
        return communicationUniversityStudentMasculine;
    }

    public void setCommunicationUniversityStudentMasculine(List<MasculineAnswers> communicationUniversityStudentMasculine) {
        this.communicationUniversityStudentMasculine = communicationUniversityStudentMasculine;
    }

    public List<FemenineAnswers> getSuggestionsMetFemenine() {
        return suggestionsMetFemenine;
    }

    public void setSuggestionsMetFemenine(List<FemenineAnswers> suggestionsMetFemenine) {
        this.suggestionsMetFemenine = suggestionsMetFemenine;
    }

    public List<MasculineAnswers> getSuggestionsMetMasculine() {
        return suggestionsMetMasculine;
    }

    public void setSuggestionsMetMasculine(List<MasculineAnswers> suggestionsMetMasculine) {
        this.suggestionsMetMasculine = suggestionsMetMasculine;
    }

    public List<FemenineAnswers> getCourseProgrammingFemenine() {
        return courseProgrammingFemenine;
    }

    public void setCourseProgrammingFemenine(List<FemenineAnswers> courseProgrammingFemenine) {
        this.courseProgrammingFemenine = courseProgrammingFemenine;
    }

    public List<MasculineAnswers> getCourseProgrammingMasculine() {
        return courseProgrammingMasculine;
    }

    public void setCourseProgrammingMasculine(List<MasculineAnswers> courseProgrammingMasculine) {
        this.courseProgrammingMasculine = courseProgrammingMasculine;
    }

    public List<FemenineAnswers> getAccessCourseNoteFemenine() {
        return accessCourseNoteFemenine;
    }

    public void setAccessCourseNoteFemenine(List<FemenineAnswers> accessCourseNoteFemenine) {
        this.accessCourseNoteFemenine = accessCourseNoteFemenine;
    }

    public List<MasculineAnswers> getAccessCourseNoteMasculine() {
        return accessCourseNoteMasculine;
    }

    public void setAccessCourseNoteMasculine(List<MasculineAnswers> accessCourseNoteMasculine) {
        this.accessCourseNoteMasculine = accessCourseNoteMasculine;
    }

    public List<FemenineAnswers> getClassroomsFemenine() {
        return classroomsFemenine;
    }

    public void setClassroomsFemenine(List<FemenineAnswers> classroomsFemenine) {
        this.classroomsFemenine = classroomsFemenine;
    }

    public List<MasculineAnswers> getClassroomsMasculine() {
        return classroomsMasculine;
    }

    public void setClassroomsMasculine(List<MasculineAnswers> classroomsMasculine) {
        this.classroomsMasculine = classroomsMasculine;
    }

    public List<FemenineAnswers> getLaboratoriesFemenine() {
        return laboratoriesFemenine;
    }

    public void setLaboratoriesFemenine(List<FemenineAnswers> laboratoriesFemenine) {
        this.laboratoriesFemenine = laboratoriesFemenine;
    }

    public List<MasculineAnswers> getLaboratoriesMasculine() {
        return laboratoriesMasculine;
    }

    public void setLaboratoriesMasculine(List<MasculineAnswers> laboratoriesMasculine) {
        this.laboratoriesMasculine = laboratoriesMasculine;
    }

    public List<FemenineAnswers> getLaboratoriesAssignmentFemenine() {
        return laboratoriesAssignmentFemenine;
    }

    public void setLaboratoriesAssignmentFemenine(List<FemenineAnswers> laboratoriesAssignmentFemenine) {
        this.laboratoriesAssignmentFemenine = laboratoriesAssignmentFemenine;
    }

    public List<MasculineAnswers> getLaboratoriesAssignmentMasculine() {
        return laboratoriesAssignmentMasculine;
    }

    public void setLaboratoriesAssignmentMasculine(List<MasculineAnswers> laboratoriesAssignmentMasculine) {
        this.laboratoriesAssignmentMasculine = laboratoriesAssignmentMasculine;
    }

    public List<FemenineAnswers> getLibraryServiceFemenine() {
        return libraryServiceFemenine;
    }

    public void setLibraryServiceFemenine(List<FemenineAnswers> libraryServiceFemenine) {
        this.libraryServiceFemenine = libraryServiceFemenine;
    }

    public List<MasculineAnswers> getLibraryServiceMasculine() {
        return libraryServiceMasculine;
    }

    public void setLibraryServiceMasculine(List<MasculineAnswers> libraryServiceMasculine) {
        this.libraryServiceMasculine = libraryServiceMasculine;
    }

    public List<FemenineAnswers> getTreatmentPlatformStaffFemenine() {
        return treatmentPlatformStaffFemenine;
    }

    public void setTreatmentPlatformStaffFemenine(List<FemenineAnswers> treatmentPlatformStaffFemenine) {
        this.treatmentPlatformStaffFemenine = treatmentPlatformStaffFemenine;
    }

    public List<MasculineAnswers> getTreatmentPlatformStaffMasculine() {
        return treatmentPlatformStaffMasculine;
    }

    public void setTreatmentPlatformStaffMasculine(List<MasculineAnswers> treatmentPlatformStaffMasculine) {
        this.treatmentPlatformStaffMasculine = treatmentPlatformStaffMasculine;
    }

    public List<FemenineAnswers> getQuickAttentionPlatformResponseFemenine() {
        return quickAttentionPlatformResponseFemenine;
    }

    public void setQuickAttentionPlatformResponseFemenine(List<FemenineAnswers> quickAttentionPlatformResponseFemenine) {
        this.quickAttentionPlatformResponseFemenine = quickAttentionPlatformResponseFemenine;
    }

    public List<MasculineAnswers> getQuickAttentionPlatformResponseMasculine() {
        return quickAttentionPlatformResponseMasculine;
    }

    public void setQuickAttentionPlatformResponseMasculine(List<MasculineAnswers> quickAttentionPlatformResponseMasculine) {
        this.quickAttentionPlatformResponseMasculine = quickAttentionPlatformResponseMasculine;
    }

    public List<FemenineAnswers> getKnowledgePlatformStaffFemenine() {
        return knowledgePlatformStaffFemenine;
    }

    public void setKnowledgePlatformStaffFemenine(List<FemenineAnswers> knowledgePlatformStaffFemenine) {
        this.knowledgePlatformStaffFemenine = knowledgePlatformStaffFemenine;
    }

    public List<MasculineAnswers> getKnowledgePlatformStaffMasculine() {
        return knowledgePlatformStaffMasculine;
    }

    public void setKnowledgePlatformStaffMasculine(List<MasculineAnswers> knowledgePlatformStaffMasculine) {
        this.knowledgePlatformStaffMasculine = knowledgePlatformStaffMasculine;
    }

    public List<FemenineAnswers> getPlatformStaffResponseFemenine() {
        return platformStaffResponseFemenine;
    }

    public void setPlatformStaffResponseFemenine(List<FemenineAnswers> platformStaffResponseFemenine) {
        this.platformStaffResponseFemenine = platformStaffResponseFemenine;
    }

    public List<MasculineAnswers> getPlatformStaffResponseMasculine() {
        return platformStaffResponseMasculine;
    }

    public void setPlatformStaffResponseMasculine(List<MasculineAnswers> platformStaffResponseMasculine) {
        this.platformStaffResponseMasculine = platformStaffResponseMasculine;
    }

    public List<FemenineAnswers> getCareerInformationFemenine() {
        return careerInformationFemenine;
    }

    public void setCareerInformationFemenine(List<FemenineAnswers> careerInformationFemenine) {
        this.careerInformationFemenine = careerInformationFemenine;
    }

    public List<MasculineAnswers> getCareerInformationMasculine() {
        return careerInformationMasculine;
    }

    public void setCareerInformationMasculine(List<MasculineAnswers> careerInformationMasculine) {
        this.careerInformationMasculine = careerInformationMasculine;
    }

    public List<FemenineAnswers> getPaymentInformationFemenine() {
        return paymentInformationFemenine;
    }

    public void setPaymentInformationFemenine(List<FemenineAnswers> paymentInformationFemenine) {
        this.paymentInformationFemenine = paymentInformationFemenine;
    }

    public List<MasculineAnswers> getPaymentInformationMasculine() {
        return paymentInformationMasculine;
    }

    public void setPaymentInformationMasculine(List<MasculineAnswers> paymentInformationMasculine) {
        this.paymentInformationMasculine = paymentInformationMasculine;
    }

    public List<FemenineAnswers> getPunctualityClassesFemenine() {
        return punctualityClassesFemenine;
    }

    public void setPunctualityClassesFemenine(List<FemenineAnswers> punctualityClassesFemenine) {
        this.punctualityClassesFemenine = punctualityClassesFemenine;
    }

    public List<MasculineAnswers> getPunctualityClassesMasculine() {
        return punctualityClassesMasculine;
    }

    public void setPunctualityClassesMasculine(List<MasculineAnswers> punctualityClassesMasculine) {
        this.punctualityClassesMasculine = punctualityClassesMasculine;
    }

    public List<FemenineAnswers> getKnowledgeCourseFemenine() {
        return knowledgeCourseFemenine;
    }

    public void setKnowledgeCourseFemenine(List<FemenineAnswers> knowledgeCourseFemenine) {
        this.knowledgeCourseFemenine = knowledgeCourseFemenine;
    }

    public List<MasculineAnswers> getKnowledgeCourseMasculine() {
        return knowledgeCourseMasculine;
    }

    public void setKnowledgeCourseMasculine(List<MasculineAnswers> knowledgeCourseMasculine) {
        this.knowledgeCourseMasculine = knowledgeCourseMasculine;
    }

    public List<FemenineAnswers> getUsedToolsClassesFemenine() {
        return usedToolsClassesFemenine;
    }

    public void setUsedToolsClassesFemenine(List<FemenineAnswers> usedToolsClassesFemenine) {
        this.usedToolsClassesFemenine = usedToolsClassesFemenine;
    }

    public List<MasculineAnswers> getUsedToolsClassesMasculine() {
        return usedToolsClassesMasculine;
    }

    public void setUsedToolsClassesMasculine(List<MasculineAnswers> usedToolsClassesMasculine) {
        this.usedToolsClassesMasculine = usedToolsClassesMasculine;
    }

    public List<FemenineAnswers> getSupportMaterialFemenine() {
        return supportMaterialFemenine;
    }

    public void setSupportMaterialFemenine(List<FemenineAnswers> supportMaterialFemenine) {
        this.supportMaterialFemenine = supportMaterialFemenine;
    }

    public List<MasculineAnswers> getSupportMaterialMasculine() {
        return supportMaterialMasculine;
    }

    public void setSupportMaterialMasculine(List<MasculineAnswers> supportMaterialMasculine) {
        this.supportMaterialMasculine = supportMaterialMasculine;
    }

    public List<FemenineAnswers> getCriteriaEvaluationFemenine() {
        return criteriaEvaluationFemenine;
    }

    public void setCriteriaEvaluationFemenine(List<FemenineAnswers> criteriaEvaluationFemenine) {
        this.criteriaEvaluationFemenine = criteriaEvaluationFemenine;
    }

    public List<MasculineAnswers> getCriteriaEvaluationMasculine() {
        return criteriaEvaluationMasculine;
    }

    public void setCriteriaEvaluationMasculine(List<MasculineAnswers> criteriaEvaluationMasculine) {
        this.criteriaEvaluationMasculine = criteriaEvaluationMasculine;
    }

    public List<FemenineAnswers> getProfessionalExperienceFemenine() {
        return professionalExperienceFemenine;
    }

    public void setProfessionalExperienceFemenine(List<FemenineAnswers> professionalExperienceFemenine) {
        this.professionalExperienceFemenine = professionalExperienceFemenine;
    }

    public List<MasculineAnswers> getProfessionalExperienceMasculine() {
        return professionalExperienceMasculine;
    }

    public void setProfessionalExperienceMasculine(List<MasculineAnswers> professionalExperienceMasculine) {
        this.professionalExperienceMasculine = professionalExperienceMasculine;
    }

    public List<FemenineAnswers> getKnowledgeStudyPlanFemenine() {
        return knowledgeStudyPlanFemenine;
    }

    public void setKnowledgeStudyPlanFemenine(List<FemenineAnswers> knowledgeStudyPlanFemenine) {
        this.knowledgeStudyPlanFemenine = knowledgeStudyPlanFemenine;
    }

    public List<MasculineAnswers> getKnowledgeStudyPlanMasculine() {
        return knowledgeStudyPlanMasculine;
    }

    public void setKnowledgeStudyPlanMasculine(List<MasculineAnswers> knowledgeStudyPlanMasculine) {
        this.knowledgeStudyPlanMasculine = knowledgeStudyPlanMasculine;
    }

    public List<FemenineAnswers> getUpportHeadCareerFemenine() {
        return upportHeadCareerFemenine;
    }

    public void setUpportHeadCareerFemenine(List<FemenineAnswers> upportHeadCareerFemenine) {
        this.upportHeadCareerFemenine = upportHeadCareerFemenine;
    }

    public List<MasculineAnswers> getUpportHeadCareerMasculine() {
        return upportHeadCareerMasculine;
    }

    public void setUpportHeadCareerMasculine(List<MasculineAnswers> upportHeadCareerMasculine) {
        this.upportHeadCareerMasculine = upportHeadCareerMasculine;
    }

    public List<FemenineAnswers> getExpertProgramCourseFemenine() {
        return expertProgramCourseFemenine;
    }

    public void setExpertProgramCourseFemenine(List<FemenineAnswers> expertProgramCourseFemenine) {
        this.expertProgramCourseFemenine = expertProgramCourseFemenine;
    }

    public List<MasculineAnswers> getExpertProgramCourseMasculine() {
        return expertProgramCourseMasculine;
    }

    public void setExpertProgramCourseMasculine(List<MasculineAnswers> expertProgramCourseMasculine) {
        this.expertProgramCourseMasculine = expertProgramCourseMasculine;
    }

    public List<FemenineAnswers> getContributionExpertProgramCourseFemenine() {
        return contributionExpertProgramCourseFemenine;
    }

    public void setContributionExpertProgramCourseFemenine(List<FemenineAnswers> contributionExpertProgramCourseFemenine) {
        this.contributionExpertProgramCourseFemenine = contributionExpertProgramCourseFemenine;
    }

    public List<MasculineAnswers> getContributionExpertProgramCourseMasculine() {
        return contributionExpertProgramCourseMasculine;
    }

    public void setContributionExpertProgramCourseMasculine(List<MasculineAnswers> contributionExpertProgramCourseMasculine) {
        this.contributionExpertProgramCourseMasculine = contributionExpertProgramCourseMasculine;
    }

    public List<FemenineAnswers> getContentCourseTopicFemenine() {
        return contentCourseTopicFemenine;
    }

    public void setContentCourseTopicFemenine(List<FemenineAnswers> contentCourseTopicFemenine) {
        this.contentCourseTopicFemenine = contentCourseTopicFemenine;
    }

    public List<MasculineAnswers> getContentCourseTopicMasculine() {
        return contentCourseTopicMasculine;
    }

    public void setContentCourseTopicMasculine(List<MasculineAnswers> contentCourseTopicMasculine) {
        this.contentCourseTopicMasculine = contentCourseTopicMasculine;
    }

    public List<FemenineAnswers> getAdequateCoursePaymentFemenine() {
        return adequateCoursePaymentFemenine;
    }

    public void setAdequateCoursePaymentFemenine(List<FemenineAnswers> adequateCoursePaymentFemenine) {
        this.adequateCoursePaymentFemenine = adequateCoursePaymentFemenine;
    }

    public List<MasculineAnswers> getAdequateCoursePaymentMasculine() {
        return adequateCoursePaymentMasculine;
    }

    public void setAdequateCoursePaymentMasculine(List<MasculineAnswers> adequateCoursePaymentMasculine) {
        this.adequateCoursePaymentMasculine = adequateCoursePaymentMasculine;
    }

    public List<FemenineAnswers> getJetsParticipationFemenine() {
        return jetsParticipationFemenine;
    }

    public void setJetsParticipationFemenine(List<FemenineAnswers> jetsParticipationFemenine) {
        this.jetsParticipationFemenine = jetsParticipationFemenine;
    }

    public List<MasculineAnswers> getJetsParticipationMasculine() {
        return jetsParticipationMasculine;
    }

    public void setJetsParticipationMasculine(List<MasculineAnswers> jetsParticipationMasculine) {
        this.jetsParticipationMasculine = jetsParticipationMasculine;
    }

    public List<FemenineAnswers> getContributionJetsParticipationFemenine() {
        return contributionJetsParticipationFemenine;
    }

    public void setContributionJetsParticipationFemenine(List<FemenineAnswers> contributionJetsParticipationFemenine) {
        this.contributionJetsParticipationFemenine = contributionJetsParticipationFemenine;
    }

    public List<MasculineAnswers> getContributionJetsParticipationMasculine() {
        return contributionJetsParticipationMasculine;
    }

    public void setContributionJetsParticipationMasculine(List<MasculineAnswers> contributionJetsParticipationMasculine) {
        this.contributionJetsParticipationMasculine = contributionJetsParticipationMasculine;
    }

    public List<FemenineAnswers> getSadisfaccionContributionJetsParticipationFemenine() {
        return sadisfaccionContributionJetsParticipationFemenine;
    }

    public void setSadisfaccionContributionJetsParticipationFemenine(List<FemenineAnswers> sadisfaccionContributionJetsParticipationFemenine) {
        this.sadisfaccionContributionJetsParticipationFemenine = sadisfaccionContributionJetsParticipationFemenine;
    }

    public List<MasculineAnswers> getSadisfaccionContributionJetsParticipationMasculine() {
        return sadisfaccionContributionJetsParticipationMasculine;
    }

    public void setSadisfaccionContributionJetsParticipationMasculine(List<MasculineAnswers> sadisfaccionContributionJetsParticipationMasculine) {
        this.sadisfaccionContributionJetsParticipationMasculine = sadisfaccionContributionJetsParticipationMasculine;
    }

    public List<FemenineAnswers> getKnowledgeUniversitySocialResponsabilityFemenine() {
        return knowledgeUniversitySocialResponsabilityFemenine;
    }

    public void setKnowledgeUniversitySocialResponsabilityFemenine(List<FemenineAnswers> knowledgeUniversitySocialResponsabilityFemenine) {
        this.knowledgeUniversitySocialResponsabilityFemenine = knowledgeUniversitySocialResponsabilityFemenine;
    }

    public List<MasculineAnswers> getKnowledgeUniversitySocialResponsabilityMasculine() {
        return knowledgeUniversitySocialResponsabilityMasculine;
    }

    public void setKnowledgeUniversitySocialResponsabilityMasculine(List<MasculineAnswers> knowledgeUniversitySocialResponsabilityMasculine) {
        this.knowledgeUniversitySocialResponsabilityMasculine = knowledgeUniversitySocialResponsabilityMasculine;
    }

    public List<FemenineAnswers> getContributionUniversitySocialResponsabilityFemenine() {
        return contributionUniversitySocialResponsabilityFemenine;
    }

    public void setContributionUniversitySocialResponsabilityFemenine(List<FemenineAnswers> contributionUniversitySocialResponsabilityFemenine) {
        this.contributionUniversitySocialResponsabilityFemenine = contributionUniversitySocialResponsabilityFemenine;
    }

    public List<MasculineAnswers> getContributionUniversitySocialResponsabilityMasculine() {
        return contributionUniversitySocialResponsabilityMasculine;
    }

    public void setContributionUniversitySocialResponsabilityMasculine(List<MasculineAnswers> contributionUniversitySocialResponsabilityMasculine) {
        this.contributionUniversitySocialResponsabilityMasculine = contributionUniversitySocialResponsabilityMasculine;
    }

    public List<FemenineAnswers> getKnowledgeUniversityInternacionalizationFemenine() {
        return knowledgeUniversityInternacionalizationFemenine;
    }

    public void setKnowledgeUniversityInternacionalizationFemenine(List<FemenineAnswers> knowledgeUniversityInternacionalizationFemenine) {
        this.knowledgeUniversityInternacionalizationFemenine = knowledgeUniversityInternacionalizationFemenine;
    }

    public List<MasculineAnswers> getKnowledgeUniversityInternacionalizationMasculine() {
        return knowledgeUniversityInternacionalizationMasculine;
    }

    public void setKnowledgeUniversityInternacionalizationMasculine(List<MasculineAnswers> knowledgeUniversityInternacionalizationMasculine) {
        this.knowledgeUniversityInternacionalizationMasculine = knowledgeUniversityInternacionalizationMasculine;
    }

    public List<FemenineAnswers> getContributionUniversityInternacionalizationFemenine() {
        return contributionUniversityInternacionalizationFemenine;
    }

    public void setContributionUniversityInternacionalizationFemenine(List<FemenineAnswers> contributionUniversityInternacionalizationFemenine) {
        this.contributionUniversityInternacionalizationFemenine = contributionUniversityInternacionalizationFemenine;
    }

    public List<MasculineAnswers> getContributionUniversityInternacionalizationMasculine() {
        return contributionUniversityInternacionalizationMasculine;
    }

    public void setContributionUniversityInternacionalizationMasculine(List<MasculineAnswers> contributionUniversityInternacionalizationMasculine) {
        this.contributionUniversityInternacionalizationMasculine = contributionUniversityInternacionalizationMasculine;
    }

    public List<FemenineAnswers> getStudentUniversityPrideFemenine() {
        return studentUniversityPrideFemenine;
    }

    public void setStudentUniversityPrideFemenine(List<FemenineAnswers> studentUniversityPrideFemenine) {
        this.studentUniversityPrideFemenine = studentUniversityPrideFemenine;
    }

    public List<MasculineAnswers> getStudentUniversityPrideMasculine() {
        return studentUniversityPrideMasculine;
    }

    public void setStudentUniversityPrideMasculine(List<MasculineAnswers> studentUniversityPrideMasculine) {
        this.studentUniversityPrideMasculine = studentUniversityPrideMasculine;
    }

    public List<FemenineAnswers> getProfessionalImageFemenine() {
        return professionalImageFemenine;
    }

    public void setProfessionalImageFemenine(List<FemenineAnswers> professionalImageFemenine) {
        this.professionalImageFemenine = professionalImageFemenine;
    }

    public List<MasculineAnswers> getProfessionalImageMasculine() {
        return professionalImageMasculine;
    }

    public void setProfessionalImageMasculine(List<MasculineAnswers> professionalImageMasculine) {
        this.professionalImageMasculine = professionalImageMasculine;
    }

    public List<FemenineAnswers> getCareerQualityFemenine() {
        return careerQualityFemenine;
    }

    public void setCareerQualityFemenine(List<FemenineAnswers> careerQualityFemenine) {
        this.careerQualityFemenine = careerQualityFemenine;
    }

    public List<MasculineAnswers> getCareerQualityMasculine() {
        return careerQualityMasculine;
    }

    public void setCareerQualityMasculine(List<MasculineAnswers> careerQualityMasculine) {
        this.careerQualityMasculine = careerQualityMasculine;
    }

    public List<FemenineAnswers> getInfrastructureFemenine() {
        return infrastructureFemenine;
    }

    public void setInfrastructureFemenine(List<FemenineAnswers> infrastructureFemenine) {
        this.infrastructureFemenine = infrastructureFemenine;
    }

    public List<MasculineAnswers> getInfrastructureMasculine() {
        return infrastructureMasculine;
    }

    public void setInfrastructureMasculine(List<MasculineAnswers> infrastructureMasculine) {
        this.infrastructureMasculine = infrastructureMasculine;
    }

    public List<FemenineAnswers> getClassScheduleFemenine() {
        return classScheduleFemenine;
    }

    public void setClassScheduleFemenine(List<FemenineAnswers> classScheduleFemenine) {
        this.classScheduleFemenine = classScheduleFemenine;
    }

    public List<MasculineAnswers> getClassScheduleMasculine() {
        return classScheduleMasculine;
    }

    public void setClassScheduleMasculine(List<MasculineAnswers> classScheduleMasculine) {
        this.classScheduleMasculine = classScheduleMasculine;
    }

    public List<FemenineAnswers> getPrestigeInstitutionFemenine() {
        return prestigeInstitutionFemenine;
    }

    public void setPrestigeInstitutionFemenine(List<FemenineAnswers> prestigeInstitutionFemenine) {
        this.prestigeInstitutionFemenine = prestigeInstitutionFemenine;
    }

    public List<MasculineAnswers> getPrestigeInstitutionMasculine() {
        return prestigeInstitutionMasculine;
    }

    public void setPrestigeInstitutionMasculine(List<MasculineAnswers> prestigeInstitutionMasculine) {
        this.prestigeInstitutionMasculine = prestigeInstitutionMasculine;
    }

    public List<FemenineAnswers> getGreatTeachersFemenine() {
        return greatTeachersFemenine;
    }

    public void setGreatTeachersFemenine(List<FemenineAnswers> greatTeachersFemenine) {
        this.greatTeachersFemenine = greatTeachersFemenine;
    }

    public List<MasculineAnswers> getGreatTeachersMasculine() {
        return greatTeachersMasculine;
    }

    public void setGreatTeachersMasculine(List<MasculineAnswers> greatTeachersMasculine) {
        this.greatTeachersMasculine = greatTeachersMasculine;
    }

    public List<FemenineAnswers> getModularSystemFemenine() {
        return modularSystemFemenine;
    }

    public void setModularSystemFemenine(List<FemenineAnswers> modularSystemFemenine) {
        this.modularSystemFemenine = modularSystemFemenine;
    }

    public List<MasculineAnswers> getModularSystemMasculine() {
        return modularSystemMasculine;
    }

    public void setModularSystemMasculine(List<MasculineAnswers> modularSystemMasculine) {
        this.modularSystemMasculine = modularSystemMasculine;
    }

    public List<FemenineAnswers> getAccesiblePriceFemenine() {
        return accesiblePriceFemenine;
    }

    public void setAccesiblePriceFemenine(List<FemenineAnswers> accesiblePriceFemenine) {
        this.accesiblePriceFemenine = accesiblePriceFemenine;
    }

    public List<MasculineAnswers> getAccesiblePriceMasculine() {
        return accesiblePriceMasculine;
    }

    public void setAccesiblePriceMasculine(List<MasculineAnswers> accesiblePriceMasculine) {
        this.accesiblePriceMasculine = accesiblePriceMasculine;
    }

    public List<FemenineAnswers> getReferencesFemenine() {
        return referencesFemenine;
    }

    public void setReferencesFemenine(List<FemenineAnswers> referencesFemenine) {
        this.referencesFemenine = referencesFemenine;
    }

    public List<MasculineAnswers> getReferencesMasculine() {
        return referencesMasculine;
    }

    public void setReferencesMasculine(List<MasculineAnswers> referencesMasculine) {
        this.referencesMasculine = referencesMasculine;
    }

    public List<FemenineAnswers> getLaboratoriesEquipmentFemenine() {
        return laboratoriesEquipmentFemenine;
    }

    public void setLaboratoriesEquipmentFemenine(List<FemenineAnswers> laboratoriesEquipmentFemenine) {
        this.laboratoriesEquipmentFemenine = laboratoriesEquipmentFemenine;
    }

    public List<MasculineAnswers> getLaboratoriesEquipmentMasculine() {
        return laboratoriesEquipmentMasculine;
    }

    public void setLaboratoriesEquipmentMasculine(List<MasculineAnswers> laboratoriesEquipmentMasculine) {
        this.laboratoriesEquipmentMasculine = laboratoriesEquipmentMasculine;
    }

    public List<FemenineAnswers> getCommunicationFemenine() {
        return communicationFemenine;
    }

    public void setCommunicationFemenine(List<FemenineAnswers> communicationFemenine) {
        this.communicationFemenine = communicationFemenine;
    }

    public List<MasculineAnswers> getCommunicationMasculine() {
        return communicationMasculine;
    }

    public void setCommunicationMasculine(List<MasculineAnswers> communicationMasculine) {
        this.communicationMasculine = communicationMasculine;
    }

    public List<FemenineAnswers> getGraduateGreatStudentFemenine() {
        return graduateGreatStudentFemenine;
    }

    public void setGraduateGreatStudentFemenine(List<FemenineAnswers> graduateGreatStudentFemenine) {
        this.graduateGreatStudentFemenine = graduateGreatStudentFemenine;
    }

    public List<MasculineAnswers> getGraduateGreatStudentMasculine() {
        return graduateGreatStudentMasculine;
    }

    public void setGraduateGreatStudentMasculine(List<MasculineAnswers> graduateGreatStudentMasculine) {
        this.graduateGreatStudentMasculine = graduateGreatStudentMasculine;
    }

    public List<FemenineAnswers> getAdequateRequirementStudiesFemenine() {
        return adequateRequirementStudiesFemenine;
    }

    public void setAdequateRequirementStudiesFemenine(List<FemenineAnswers> adequateRequirementStudiesFemenine) {
        this.adequateRequirementStudiesFemenine = adequateRequirementStudiesFemenine;
    }

    public List<MasculineAnswers> getAdequateRequirementStudiesMasculine() {
        return adequateRequirementStudiesMasculine;
    }

    public void setAdequateRequirementStudiesMasculine(List<MasculineAnswers> adequateRequirementStudiesMasculine) {
        this.adequateRequirementStudiesMasculine = adequateRequirementStudiesMasculine;
    }

    public List<FemenineAnswers> getStudentServiceFemenine() {
        return studentServiceFemenine;
    }

    public void setStudentServiceFemenine(List<FemenineAnswers> studentServiceFemenine) {
        this.studentServiceFemenine = studentServiceFemenine;
    }

    public List<MasculineAnswers> getStudentServiceMasculine() {
        return studentServiceMasculine;
    }

    public void setStudentServiceMasculine(List<MasculineAnswers> studentServiceMasculine) {
        this.studentServiceMasculine = studentServiceMasculine;
    }

    public List<FemenineAnswers> getReputationMiddleCityFemenine() {
        return reputationMiddleCityFemenine;
    }

    public void setReputationMiddleCityFemenine(List<FemenineAnswers> reputationMiddleCityFemenine) {
        this.reputationMiddleCityFemenine = reputationMiddleCityFemenine;
    }

    public List<MasculineAnswers> getReputationMiddleCityMasculine() {
        return reputationMiddleCityMasculine;
    }

    public void setReputationMiddleCityMasculine(List<MasculineAnswers> reputationMiddleCityMasculine) {
        this.reputationMiddleCityMasculine = reputationMiddleCityMasculine;
    }

    public List<FemenineAnswers> getUniversitySupervisionFemenine() {
        return universitySupervisionFemenine;
    }

    public void setUniversitySupervisionFemenine(List<FemenineAnswers> universitySupervisionFemenine) {
        this.universitySupervisionFemenine = universitySupervisionFemenine;
    }

    public List<MasculineAnswers> getUniversitySupervisionMasculine() {
        return universitySupervisionMasculine;
    }

    public void setUniversitySupervisionMasculine(List<MasculineAnswers> universitySupervisionMasculine) {
        this.universitySupervisionMasculine = universitySupervisionMasculine;
    }

    public List<FemenineAnswers> getUniversityRecommendationFemenine() {
        return universityRecommendationFemenine;
    }

    public void setUniversityRecommendationFemenine(List<FemenineAnswers> universityRecommendationFemenine) {
        this.universityRecommendationFemenine = universityRecommendationFemenine;
    }

    public List<MasculineAnswers> getUniversityRecommendationMasculine() {
        return universityRecommendationMasculine;
    }

    public void setUniversityRecommendationMasculine(List<MasculineAnswers> universityRecommendationMasculine) {
        this.universityRecommendationMasculine = universityRecommendationMasculine;
    }

    public List<FemenineAnswers> getQualificationTeachersFemenine() {
        return qualificationTeachersFemenine;
    }

    public void setQualificationTeachersFemenine(List<FemenineAnswers> qualificationTeachersFemenine) {
        this.qualificationTeachersFemenine = qualificationTeachersFemenine;
    }

    public List<MasculineAnswers> getQualificationTeachersMasculine() {
        return qualificationTeachersMasculine;
    }

    public void setQualificationTeachersMasculine(List<MasculineAnswers> qualificationTeachersMasculine) {
        this.qualificationTeachersMasculine = qualificationTeachersMasculine;
    }

    public List<FemenineAnswers> getTypeLanguageFemenine() {
        return typeLanguageFemenine;
    }

    public void setTypeLanguageFemenine(List<FemenineAnswers> typeLanguageFemenine) {
        this.typeLanguageFemenine = typeLanguageFemenine;
    }

    public List<MasculineAnswers> getTypeLanguageMasculine() {
        return typeLanguageMasculine;
    }

    public void setTypeLanguageMasculine(List<MasculineAnswers> typeLanguageMasculine) {
        this.typeLanguageMasculine = typeLanguageMasculine;
    }
}
