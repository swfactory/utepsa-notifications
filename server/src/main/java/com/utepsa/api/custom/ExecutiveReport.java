package com.utepsa.api.custom;

import java.util.List;

/**
 * Created by Luana Chavez on 18/04/2017.
 */
public class ExecutiveReport {

    private String semester;
    private long total;
    private long totalActive;
    private Long totalSemesterCourses;
    private Long totalApprovedCourses;
    private Long totalDisapprovedCourses;
    private Long totalAbandonedCourses;
    private List<CareerAverageReport> dataCareers;
    private List<ModulesReport> totalActiveStudentByModule;

    public ExecutiveReport() {
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTotalActive() {
        return totalActive;
    }

    public void setTotalActive(long totalActive) {
        this.totalActive = totalActive;
    }

    public Long getTotalSemesterCourses() {
        return totalSemesterCourses;
    }

    public void setTotalSemesterCourses(Long totalSemesterCourses) {
        this.totalSemesterCourses = totalSemesterCourses;
    }

    public Long getTotalApprovedCourses() {
        return totalApprovedCourses;
    }

    public void setTotalApprovedCourses(Long totalApprovedCourses) {
        this.totalApprovedCourses = totalApprovedCourses;
    }

    public Long getTotalDisapprovedCourses() {
        return totalDisapprovedCourses;
    }

    public void setTotalDisapprovedCourses(Long totalDisapprovedCourses) {
        this.totalDisapprovedCourses = totalDisapprovedCourses;
    }

    public Long getTotalAbandonedCourses() {
        return totalAbandonedCourses;
    }

    public void setTotalAbandonedCourses(Long totalAbandonedCourses) {
        this.totalAbandonedCourses = totalAbandonedCourses;
    }

    public List<CareerAverageReport> getDataCareers() {
        return dataCareers;
    }

    public void setDataCareers(List<CareerAverageReport> dataCareers) {
        this.dataCareers = dataCareers;
    }

    public List<ModulesReport> getTotalActiveStudentByModule() {
        return totalActiveStudentByModule;
    }

    public void setTotalActiveStudentByModule(List<ModulesReport> totalActiveStudentByModule) {
        this.totalActiveStudentByModule = totalActiveStudentByModule;
    }
}
