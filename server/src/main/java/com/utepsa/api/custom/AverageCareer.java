package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 16/05/2017.
 */
public class AverageCareer {
    private Long idCareer;
    private double average_student;

    public AverageCareer() {
    }

    public Long getIdCareer() {
        return idCareer;
    }

    public void setIdCareer(Long idCareer) {
        this.idCareer = idCareer;
    }

    public double getAverage_student() {
        return average_student;
    }

    public void setAverage_student(double average_student) {
        this.average_student = average_student;
    }
}
