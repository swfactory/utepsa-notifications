package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 23/06/2017.
 */
public class ReportByYear {
    private ReportYear numberOfStudentsByYear;
    private ReportYear averageByYear;
    private ReportYear numberOfStudentsInFirstSemester;
    private ReportYear numberOfStudentsInSecondSemester;
    private ReportYear numberOfStudentsInSummer;
    private ReportYear numberOfStudentsInWinter;
    private ReportYear numberOfStudentsInscribe;
    private ReportYear numberOfCourse;
    private ReportYear numberOfCourseApproved;
    private ReportYear numberOfCourseDisapproved;
    private ReportYear numberOfCourseAbandoned;
    private ReportYear numberOfCourseByCareer;
    private ReportYear numberOfCourseApprovedByCareer;
    private ReportYear numberOfCourseDisapprovedByCareer;
    private ReportYear numberOfCourseAbandonedByCareer;

    public ReportYear getNumberOfStudentsByYear() {
        return numberOfStudentsByYear;
    }

    public void setNumberOfStudentsByYear(ReportYear numberOfStudentsByYear) {
        this.numberOfStudentsByYear = numberOfStudentsByYear;
    }

    public ReportYear getAverageByYear() {
        return averageByYear;
    }

    public void setAverageByYear(ReportYear averageByYear) {
        this.averageByYear = averageByYear;
    }

    public ReportYear getNumberOfStudentsInFirstSemester() {
        return numberOfStudentsInFirstSemester;
    }

    public void setNumberOfStudentsInFirstSemester(ReportYear numberOfStudentsInFirstSemester) {
        this.numberOfStudentsInFirstSemester = numberOfStudentsInFirstSemester;
    }

    public ReportYear getNumberOfStudentsInSecondSemester() {
        return numberOfStudentsInSecondSemester;
    }

    public void setNumberOfStudentsInSecondSemester(ReportYear numberOfStudentsInSecondSemester) {
        this.numberOfStudentsInSecondSemester = numberOfStudentsInSecondSemester;
    }

    public ReportYear getNumberOfStudentsInSummer() {
        return numberOfStudentsInSummer;
    }

    public void setNumberOfStudentsInSummer(ReportYear numberOfStudentsInSummer) {
        this.numberOfStudentsInSummer = numberOfStudentsInSummer;
    }

    public ReportYear getNumberOfStudentsInWinter() {
        return numberOfStudentsInWinter;
    }

    public void setNumberOfStudentsInWinter(ReportYear numberOfStudentsInWinter) {
        this.numberOfStudentsInWinter = numberOfStudentsInWinter;
    }

    public ReportYear getNumberOfStudentsInscribe() {
        return numberOfStudentsInscribe;
    }

    public void setNumberOfStudentsInscribe(ReportYear numberOfStudentsInscribe) {
        this.numberOfStudentsInscribe = numberOfStudentsInscribe;
    }

    public ReportYear getNumberOfCourseApproved() {
        return numberOfCourseApproved;
    }

    public void setNumberOfCourseApproved(ReportYear numberOfCourseApproved) {
        this.numberOfCourseApproved = numberOfCourseApproved;
    }

    public ReportYear getNumberOfCourseDisapproved() {
        return numberOfCourseDisapproved;
    }

    public void setNumberOfCourseDisapproved(ReportYear numberOfCourseDisapproved) {
        this.numberOfCourseDisapproved = numberOfCourseDisapproved;
    }

    public ReportYear getNumberOfCourseAbandoned() {
        return numberOfCourseAbandoned;
    }

    public void setNumberOfCourseAbandoned(ReportYear numberOfCourseAbandoned) {
        this.numberOfCourseAbandoned = numberOfCourseAbandoned;
    }

    public ReportYear getNumberOfCourseApprovedByCareer() {
        return numberOfCourseApprovedByCareer;
    }

    public void setNumberOfCourseApprovedByCareer(ReportYear numberOfCourseApprovedByCareer) {
        this.numberOfCourseApprovedByCareer = numberOfCourseApprovedByCareer;
    }

    public ReportYear getNumberOfCourseDisapprovedByCareer() {
        return numberOfCourseDisapprovedByCareer;
    }

    public void setNumberOfCourseDisapprovedByCareer(ReportYear numberOfCourseDisapprovedByCareer) {
        this.numberOfCourseDisapprovedByCareer = numberOfCourseDisapprovedByCareer;
    }

    public ReportYear getNumberOfCourseAbandonedByCareer() {
        return numberOfCourseAbandonedByCareer;
    }

    public void setNumberOfCourseAbandonedByCareer(ReportYear numberOfCourseAbandonedByCareer) {
        this.numberOfCourseAbandonedByCareer = numberOfCourseAbandonedByCareer;
    }

    public ReportYear getNumberOfCourse() {
        return numberOfCourse;
    }

    public void setNumberOfCourse(ReportYear numberOfCourse) {
        this.numberOfCourse = numberOfCourse;
    }

    public ReportYear getNumberOfCourseByCareer() {
        return numberOfCourseByCareer;
    }

    public void setNumberOfCourseByCareer(ReportYear numberOfCourseByCareer) {
        this.numberOfCourseByCareer = numberOfCourseByCareer;
    }
}
