package com.utepsa.api.custom;


import org.hibernate.annotations.Entity;

/**
 * Created by Luana Chavez on 07/04/2017.
 */
@Entity
public class HistoryNotesCustom {
    private Long idStudent;
    private Long idCourse;
    private String course;
    private Long note;
    private Long minimunNote;
    private String semester;
    private String module;
    private Long pensumLevel;
    private Long pensumOrder;

    public HistoryNotesCustom() {
    }

    public Long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Long idStudent) {
        this.idStudent = idStudent;
    }

    public Long getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Long getNote() {
        return note;
    }

    public void setNote(Long note) {
        this.note = note;
    }

    public Long getMinimunNote() {
        return minimunNote;
    }

    public void setMinimunNote(Long minimunNote) {
        this.minimunNote = minimunNote;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Long getPensumLevel() {
        return pensumLevel;
    }

    public void setPensumLevel(Long pensumLevel) {
        this.pensumLevel = pensumLevel;
    }

    public Long getPensumOrder() {
        return pensumOrder;
    }

    public void setPensumOrder(Long pensumOrder) {
        this.pensumOrder = pensumOrder;
    }
}
