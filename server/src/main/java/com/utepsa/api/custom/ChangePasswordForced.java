package com.utepsa.api.custom;

/**
 * Created by David on 27/01/2017.
 */
public class ChangePasswordForced {
    long id;
    String newPassword;
    String confimNewPassword;

    public ChangePasswordForced() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfimNewPassword() {
        return confimNewPassword;
    }

    public void setConfimNewPassword(String confimNewPassword) {
        this.confimNewPassword = confimNewPassword;
    }
}
