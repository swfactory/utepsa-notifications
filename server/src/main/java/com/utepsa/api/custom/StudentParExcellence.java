package com.utepsa.api.custom;

import java.math.BigInteger;

/**
 * Created by Luana Chavez on 03/08/2017.
 */
public class StudentParExcellence {
    private Long id;
    private String registerNumber;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private String agendCode;
    private double average;
    private String firstSemester;
    private BigInteger totalCarriedCourse;
    private BigInteger careerTotalCourse;
    private long pensum;
    private BigInteger numberOfSemesters;


    public StudentParExcellence() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherLastname() {
        return fatherLastname;
    }

    public void setFatherLastname(String fatherLastname) {
        this.fatherLastname = fatherLastname;
    }

    public String getMotherLastname() {
        return motherLastname;
    }

    public void setMotherLastname(String motherLastname) {
        this.motherLastname = motherLastname;
    }

    public String getAgendCode() {
        return agendCode;
    }

    public void setAgendCode(String agendCode) {
        this.agendCode = agendCode;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getFirstSemester() {
        return firstSemester;
    }

    public void setFirstSemester(String firstSemester) {
        this.firstSemester = firstSemester;
    }

    public BigInteger getTotalCarriedCourse() {
        return totalCarriedCourse;
    }

    public void setTotalCarriedCourse(BigInteger totalCarriedCourse) {
        this.totalCarriedCourse = totalCarriedCourse;
    }

    public BigInteger getCareerTotalCourse() {
        return careerTotalCourse;
    }

    public void setCareerTotalCourse(BigInteger careerTotalCourse) {
        this.careerTotalCourse = careerTotalCourse;
    }

    public long getPensum() {
        return pensum;
    }

    public void setPensum(long pensum) {
        this.pensum = pensum;
    }

    public BigInteger getNumberOfSemesters() {
        return numberOfSemesters;
    }

    public void setNumberOfSemesters(BigInteger numberOfSemesters) {
        this.numberOfSemesters = numberOfSemesters;
    }
}
