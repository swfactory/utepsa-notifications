package com.utepsa.config;

import com.utepsa.api.views.ViewGcmStudent;
import com.utepsa.models.*;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;

/**
 * Created by David-SW on 16/05/2017.
 */
public class HbnBundle  extends HibernateBundle<NotificationServerConfig> {
    public HbnBundle() {
        super(Career.class, Course.class, CareerCourse.class, Faculty.class, NotificationAudience.class,
                Notification.class, CategoryNotification.class, TypeAudience.class, Student.class, HistoryNotes.class,
                Document.class, DocumentStudent.class, Credential.class,
                File.class, FileAudience.class, TypeFile.class, ViewGcmStudent.class, Role.class,
                TypeMigrationlog.class, StudentMigrationlog.class, CredentialAdministrator.class, Profile.class,
                PermissionCredential.class, PermissionRole.class, Permission.class, Professor.class, CurrentSemester.class,
                StudentCourseRegister.class, Professor.class, CurrentSemester.class, StudentCourseRegister.class,
                Schedule.class, FinancialState.class, AcademicArea.class, GroupsOffered.class, FileCareer.class, Book.class, AcademicEvent.class,
                EventType.class, Photo.class, EventCareer.class, EventCourse.class, AreaOfVirtualBooks.class, VirtualBooks.class);
    }

    @Override
    public PooledDataSourceFactory getDataSourceFactory(NotificationServerConfig configuration) {
        return configuration.getDataSourceFactory();
    }
}
