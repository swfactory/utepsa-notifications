package com.utepsa.resources.user;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.custom.User;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Gerardo on 08/11/2017.
 */
@Path(UserResource.SERVICE_PATH)
@Api(value = UserResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    public static final String SERVICE_PATH = "user/";

    private final UserService service;

    @Inject
    public UserResource(UserService service){
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("professor/")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiOperation( value = "Allows a student to initiate a session", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Signed in successfully"),
            @ApiResponse(code = StatusCode.FORBIDDEN, message = "You must change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse loginUserProfessor(@ApiParam(value = "Username needed to login", required = true)
                                   @HeaderParam("register") String register) {
        try {
            return service.getByAgendCodeProfessor(register.toUpperCase());
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("student/")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiOperation( value = "Allows a student to initiate a session", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Signed in successfully"),
            @ApiResponse(code = StatusCode.FORBIDDEN, message = "You must change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse loginUserStudent(@ApiParam(value = "Username needed to login", required = true)
                                   @HeaderParam("register") String register) {
        try {
            return service.getByStudentRegister(register.toUpperCase());
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }
}
