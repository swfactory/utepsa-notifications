package com.utepsa.resources.user;

import com.google.inject.Inject;
import com.utepsa.api.custom.User;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Professor;
import com.utepsa.models.Student;

/**
 * Created by Gerardo on 08/11/2017.
 */
public class UserService {
    private final StudentDAO studentDAO;
    private final ProfessorDAO professorDAO;

    @Inject
    public UserService(StudentDAO studentDAO, ProfessorDAO professorDAO){
        this.studentDAO = studentDAO;
        this.professorDAO = professorDAO;
    }

    public BasicResponse getByStudentRegister (String register) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            User user = new User();
            if(studentDAO.getByRegisterNumber(register.trim()) != null){
                Student student = studentDAO.getByRegisterNumber(register.trim());
                user.setName(student.getName().trim());
                user.setFatherLastname(student.getFatherLastname().trim());
                user.setMotherLastname(student.getMotherLastname().trim());
                user.setRegister(student.getRegisterNumber());

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(user);
            }  else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("User not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getByAgendCodeProfessor(String agendCode){
        BasicResponse basicResponse = new BasicResponse();
        try {
            User user = new User();
            if(professorDAO.getProfessorByAgendCode(agendCode.trim()) != null){
                Professor professor = professorDAO.getProfessorByAgendCode(agendCode.trim());
                user.setName(professor.getName().trim());
                user.setFatherLastname(professor.getFather_lastname().trim());
                user.setMotherLastname(professor.getMother_lastname().trim());
                user.setRegister(professor.getAgend_code());

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(user);
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("User not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
}
