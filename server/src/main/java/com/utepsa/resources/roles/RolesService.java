package com.utepsa.resources.roles;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.permissionRole.PermissionRoleDAO;
import com.utepsa.models.PermissionRole;
import java.util.List;
/**
 * Created by Gerardo on 14/02/2017.
 */
public class RolesService {
    private final PermissionRoleDAO permissionRoleDAO;

    @Inject
    public RolesService(PermissionRoleDAO permissionRoleDAO) {
        this.permissionRoleDAO = permissionRoleDAO;
    }

    public BasicResponse getByIdRole(long idRole){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<PermissionRole> permissionRoles = permissionRoleDAO.getByIdRole(idRole);
            if(permissionRoles.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no permission role to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(permissionRoles);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
    public BasicResponse modify(List<PermissionRole> permissionRoles){
        BasicResponse basicResponse = new BasicResponse();
        try{
            if(permissionRoles != null){
                permissionRoleDAO.modify(permissionRoles);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("modify correctly");
            } else {
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It was not modified, Some param was null");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
