package com.utepsa.resources.careers;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Career;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.models.FileCareer;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
/**
 * Created by David on 08/11/2016.
 */
@Path(CareersResource.SERVICE_PATH)
@Api(value = CareersResource.SERVICE_PATH, description = "Operations about Careers")
@Produces(MediaType.APPLICATION_JSON)
public class CareersResource {
    public static final String SERVICE_PATH = "careers/";
    private final CareersService service;

    @Inject
    public CareersResource(CareersService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all Careers", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Careers found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no careers to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getAll() {
        return service.getAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("grade/{grade}")
    @ApiOperation( value = "Find a Careers By Grade (Postgraduate or Undergraduate)", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Careers found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no careers to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse findByCareerCode(@ApiParam(value = "Grade to search", required = true)
                                              @PathParam("grade") String grade){
        return service.findByGrade(grade);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("{idCareer}/files")
    @ApiOperation( value = "Find every career's file)", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Files found on the selected career"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No files were found on the selected career"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "The career doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})

    public BasicResponse findFilesByCareer(@ApiParam(value = "Files to search by career", required = true)
                                          @PathParam("idCareer") long idCareer){
        return service.getFilesPerId(idCareer);
    }

}
