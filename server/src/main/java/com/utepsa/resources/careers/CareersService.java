package com.utepsa.resources.careers;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.fileCareer.FileCareerDAO;
import com.utepsa.models.Career;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.models.FileCareer;
import java.util.List;

/**
 * Created by David on 08/11/2016.
 */
public class CareersService {
    private final CareerDAO careerDAO;
    private final FileCareerDAO fileCareerDAO;

    @Inject
    public CareersService(CareerDAO careerDAO, FileCareerDAO fileCareerDAO) {
        this.careerDAO = careerDAO;
        this.fileCareerDAO = fileCareerDAO;
    }

    public FileCareerDAO getDao() {
        return fileCareerDAO;
    }

    public FileCareerDAO getFileCareerDAO() {return fileCareerDAO;}

    public BasicResponse findByGrade(String grade){
        BasicResponse basicResponse = new BasicResponse();
        try{
            List<Career> careers = careerDAO.getByGrade(grade);
            if(careers == null || careers.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(careers);
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAll(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Career> careers = careerDAO.getAll();
            if(careers.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no careers to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(careers);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getFilesPerId(long idCareer)
    {
        BasicResponse basicResponse = new BasicResponse();
        try{
            Career career = careerDAO.getById(idCareer);
            if(career == null)
            {
                basicResponse.setMessage("The career doesn't exist");
                basicResponse.setCode(StatusCode.NOT_FOUND);
                return basicResponse;
            }
            List<FileCareer> fileCareers = fileCareerDAO.getAllFilesPerCareer(idCareer);
            if(fileCareers == null || fileCareers.size() == 0){
                basicResponse.setMessage("No files were found on the selected career");
                basicResponse.setCode(StatusCode.NO_CONTENT);
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(fileCareers);
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
