package com.utepsa.resources.credential;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.custom.ChangePasswordForced;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by david on 5/8/16.
 */
@Path(CredentialResource.SERVICE_PATH)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = CredentialResource.SERVICE_PATH, description = "Operations about Auth")
public class CredentialResource {
    public static final String SERVICE_PATH = "credential/";

    private final CredentialService service;

    @Inject
    public CredentialResource(CredentialService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiOperation( value = "Allows a student to initiate a session", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Signed in successfully"),
            @ApiResponse(code = StatusCode.FORBIDDEN, message = "You must change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse loginUser(@ApiParam(value = "Username needed to login", required = true)
                                       @HeaderParam("username") String username,
                                   @ApiParam(value = "Password needed to login", required = true)
                                        @HeaderParam("password") String password) {
        try {
            return service.login(username.toUpperCase(), password);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idCredential}/registerGcm/{gcmId}")
    @ApiOperation( value = "Register GCM of Student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Register Successful"),
            @ApiResponse(code = 406, message = "Invalid data supplied"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse registerGcmIdStudent(
            @ApiParam(value = "Credential ID needed to find the credential", required = true)
            @PathParam("idCredential") int idCredential,
            @ApiParam(value = "GCM ID obtained from google service", required = true)
            @PathParam("gcmId") String gcmId) {
        return service.registerGcmId(idCredential,gcmId);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/changePasswordForced/")
    @ApiOperation( value = "Changes a user's password and set a new one.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Password changed correctly."),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "It was not possible to change the password, Because the new password is the same as the old one."),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "It was not possible to change the password, because the confirmation of the password is not the same as the new password."),
            @ApiResponse(code = StatusCode.UNAUTHORIZED, message = "You are not authorized to change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse changePasswordForced(@ApiParam(value = "Received in JSON format.", required = true)
                                                          ChangePasswordForced changePasswordForced) {
        return service.changePasswordForced(changePasswordForced);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/resetPassword")
    @ApiOperation( value = "Changes a user's password and set a new one.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Password reset correctly."),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "It was not possible to change the password, Because the user does not exist."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "credential doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public  BasicResponse resetPassword(@ApiParam(value = "ID needed to find the credential", required = true)
                                            @PathParam("id") long id,
                                            @QueryParam("password") final String password,
                                            @QueryParam("confirmPassword") final String confirmPassword,
                                            @QueryParam("forcedPassword") final boolean forcedPassword){
        return service.resetPassword(id,password,confirmPassword,forcedPassword);
    }
}
