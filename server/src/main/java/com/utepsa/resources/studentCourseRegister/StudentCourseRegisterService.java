package com.utepsa.resources.studentCourseRegister;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterDAO;
import com.utepsa.models.StudentCourseRegister;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
public class StudentCourseRegisterService {
    private final StudentCourseRegisterDAO studentCourseRegisterDAO;

    @Inject
    public StudentCourseRegisterService(StudentCourseRegisterDAO studentCourseRegisterDAO) {
        this.studentCourseRegisterDAO = studentCourseRegisterDAO;
    }

    public BasicResponse getCourseRegisterById(long id)
    {
        BasicResponse basicResponse =new BasicResponse();
        try{
            StudentCourseRegister studentCourseRegister = studentCourseRegisterDAO.getCourseRegisterById(id);
            if (studentCourseRegister !=null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(studentCourseRegister);
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Course Register not found");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
}
