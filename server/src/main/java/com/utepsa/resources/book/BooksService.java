package com.utepsa.resources.book;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.book.BookDAO;
import com.utepsa.models.Book;

import java.util.List;

/**
 * Created by Gerardo on 19/09/2017.
 */
public class BooksService {
    private BookDAO bookDAO;

    @Inject
    public BooksService(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    public BasicResponse createBook(Book book) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            bookDAO.create(book);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("create book correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getById(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Book book = bookDAO.getById(id);
            if(book != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(book);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This book was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAll() {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Book> books = bookDAO.getAll();
            if(books != null && books.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(books);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No books are available");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse searchBook(String fullName) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Book> books = bookDAO.searchBook(fullName);
            if(books != null && books.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(books);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No books are available");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
