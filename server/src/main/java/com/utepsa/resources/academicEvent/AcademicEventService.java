package com.utepsa.resources.academicEvent;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.academicEvent.AcademicEventDAO;
import com.utepsa.db.eventCareer.EventCareerDAO;
import com.utepsa.db.eventCourse.EventCourseDAO;
import com.utepsa.db.photo.PhotoDAO;
import com.utepsa.models.AcademicEvent;
import com.utepsa.models.EventCareer;
import com.utepsa.models.EventCourse;
import com.utepsa.models.Photo;

import java.awt.*;
import java.util.List;

/**
 * Created by Leonardo on 22/07/2017.
 */
public class AcademicEventService {

    private AcademicEventDAO academicEventDAO;
    private PhotoDAO photoDAO;
    private EventCareerDAO eventCareerDAO;
    private EventCourseDAO eventCourseDAO;

    @Inject
    public AcademicEventService(AcademicEventDAO academicEventDAO, PhotoDAO photoDAO, EventCareerDAO eventCareerDAO, EventCourseDAO eventCourseDAO) {
        this.academicEventDAO = academicEventDAO;
        this.photoDAO = photoDAO;
        this.eventCareerDAO = eventCareerDAO;
        this.eventCourseDAO = eventCourseDAO;
    }

    public BasicResponse getAll(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<AcademicEvent> academicEvents = academicEventDAO.getAll();
            if(academicEvents.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no academic events to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(academicEvents);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getByCreator(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<AcademicEvent> academicEventsByCreator = academicEventDAO.getByCreator(id);
            if(academicEventsByCreator.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("The indicated creator has not made an event yet");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(academicEventsByCreator);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getByCourse(long idCourse, String year){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<AcademicEvent> academicEventsByCourse = academicEventDAO.getByCourse(idCourse, year);
            if(academicEventsByCourse.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There is no event related to the indicated course");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(academicEventsByCourse);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getByCareer(long idCareer, String year){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<AcademicEvent> academicEventsByCareer = academicEventDAO.getByCareer(idCareer, year);
            if(academicEventsByCareer.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There is no event related to the indicated career");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(academicEventsByCareer);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse createEvent(AcademicEvent academicEvent) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            academicEventDAO.create(academicEvent);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("Event created correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse createPhoto(Photo photo) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            photoDAO.create(photo);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("Photo created correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse registerEventCareer(EventCareer eventCareer) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            eventCareerDAO.create(eventCareer);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("Event per Career registered correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse registerEventCourse(EventCourse eventCourse) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            eventCourseDAO.create(eventCourse);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("Event per Course registered correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getPhotoByEvent(long idEvent){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Photo> photosByEvent = photoDAO.getByAcademicEvent(idEvent);
            if(photosByEvent.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("The indicated event does not have photos");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(photosByEvent);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
