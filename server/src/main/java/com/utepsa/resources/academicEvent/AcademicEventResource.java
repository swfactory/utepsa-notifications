package com.utepsa.resources.academicEvent;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.AcademicEvent;
import com.utepsa.models.Photo;
import com.utepsa.models.EventCourse;
import com.utepsa.models.EventCareer;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Leonardo on 22/07/2017.
 */
@Path(AcademicEventResource.SERVICE_PATH)
@Api(value = AcademicEventResource.SERVICE_PATH, description = "Academic events related operations")
@Produces(MediaType.APPLICATION_JSON)
public class AcademicEventResource {
    public static final String SERVICE_PATH = "academicEvent/";

    private AcademicEventService academicEventService;
    @Inject
    public AcademicEventResource(AcademicEventService academicEventService) {
        this.academicEventService = academicEventService;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all Academic events", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Academic event found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no academic events to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getAll() {
        return academicEventService.getAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("course/{idCourse}/{year}")
    @ApiOperation( value = "Find Events by course ", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Events found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no events to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getByCourseCode(@ApiParam(value = "Event to search by course", required = true)
                                          @PathParam("idCourse") Long idCourse,
                                         @ApiParam(value = "Event to search by career", required = true)
                                         @PathParam("year") String year){
        return academicEventService.getByCourse(idCourse, year);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("career/{idCareer}/{year}")
    @ApiOperation( value = "Find Events by career ", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Events found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no events to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getByCareerCode(@ApiParam(value = "Event to search by career", required = true)
                                         @PathParam("idCareer") Long idCareer,
                                         @ApiParam(value = "Event to search by career", required = true)
                                         @PathParam("year") String year){
        return academicEventService.getByCareer(idCareer, year);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idCreator}")
    @ApiOperation( value = "Find Events by creator", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Events found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no events to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getByCreator(@ApiParam(value = "Event to search by creator", required = true)
                                         @PathParam("idCreator") Long idCreator){
        return academicEventService.getByCreator(idCreator);
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/createEvent")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create an event", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The event has been created"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse createEvent(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) AcademicEvent academicEvent ) {
        return academicEventService.createEvent(academicEvent);
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/registerCareer")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Register an event by career", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The event has been correctly registered by career"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse registerEventByCareer(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) EventCareer eventCareer ) {
        return academicEventService.registerEventCareer(eventCareer);
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/registerCourse")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Register an event by course", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The event has been correctly registered by course"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse registerEventByCourse(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) EventCourse eventCourse ) {
        return academicEventService.registerEventCourse(eventCourse);
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/uploadPhoto")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Upload a photo", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The photo has been uploaded"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse uploadPhoto(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) Photo photo ) {
        return academicEventService.createPhoto(photo);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idEvent}/photos")
    @ApiOperation( value = "Find photos by events", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Photos found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no photos to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getPhotosByEvent(@ApiParam(value = "photos to search by event", required = true)
                                      @PathParam("idEvent") Long idEvent){
        return academicEventService.getPhotoByEvent(idEvent);
    }

}