package com.utepsa.resources.groupsOffered;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.academicArea.AcademicAreaDAO;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.db.groupsOffered.GroupsOfferedDAO;
import com.utepsa.models.Course;
import com.utepsa.models.CurrentSemester;
import com.utepsa.models.GroupsOffered;
import java.util.List;

/**
 * Created by Gerardo on 26/04/2017.
 */
public class GroupsOfferedService {

    private GroupsOfferedDAO groupsOfferedDAO;
    private CareerDAO careerDAO;
    private AcademicAreaDAO academicAreaDAO;
    private CurrentSemesterDAO currentSemesterDAO;
    private CourseDAO courseDAO;

    @Inject
    public GroupsOfferedService(GroupsOfferedDAO groupsOfferedDAO, CareerDAO careerDAO, AcademicAreaDAO academicAreaDAO,
                                CurrentSemesterDAO currentSemesterDAO, CourseDAO courseDAO){
        this.groupsOfferedDAO = groupsOfferedDAO;
        this.careerDAO = careerDAO;
        this.academicAreaDAO = academicAreaDAO;
        this.currentSemesterDAO = currentSemesterDAO;
        this.courseDAO = courseDAO;
    }

    public BasicResponse getByCareerSemesterAndModule(long idCareer, int pensum, String semester, String module) throws Exception{
        BasicResponse basicResponse = new BasicResponse();
            if(careerDAO.getById(idCareer) == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("career doesn't exist");
                return  basicResponse;
            }

            if(semester == null) {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("semester is empty");
                return basicResponse;
            }

            List<GroupsOffered> groupsOffereds;
            if(module == null){
                groupsOffereds = groupsOfferedDAO.getByCareerAndSemester(idCareer, pensum, semester);
            }else {
                groupsOffereds = groupsOfferedDAO.getByCareerSemesterAndModule(idCareer, pensum, semester, module);
            }

            if(groupsOffereds.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(groupsOffereds);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This Groups Offered was not found");
            }
        return basicResponse;
    }

    public BasicResponse getByAcademicAreaAndModule(long idAcademicArea, String semester, String module){
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(semester == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("semester is empty");
                return basicResponse;
            }

            if(academicAreaDAO.getById(idAcademicArea) == null){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("Academic area doesn't exist");
                return basicResponse;
            }

            List<GroupsOffered> groupsOffereds;
            if(module != null){
                groupsOffereds = groupsOfferedDAO.getByAcademicAreaAndSemester(idAcademicArea, semester, module);
            } else {
                groupsOffereds = groupsOfferedDAO.getByAcademicArea(idAcademicArea, semester);
            }

            if(groupsOffereds.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(groupsOffereds);
                return basicResponse;
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This Groups Offered was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getByCourseAndSemester(long idCourse){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Course course = courseDAO.getById(idCourse);
            CurrentSemester currentSemester = currentSemesterDAO.getCurrentSemester();
            List<GroupsOffered> groupsOffereds = groupsOfferedDAO.getByCourseAndSemester(course, currentSemester.getSemester());

            if(course == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Course not exist");
            }

            if(groupsOffereds.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(groupsOffereds);
            } else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("Groups Offered Not Content");
            }
        } catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }

        return basicResponse;
    }
}
