package com.utepsa.resources.groupsOffered;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gerardo on 26/04/2017.
 */
@Path(GroupsOfferedResource.SERVICE_PATH)
@Api(value = GroupsOfferedResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class GroupsOfferedResource {
    public final static String SERVICE_PATH = "/groupsOffered";
    private final GroupsOfferedService service;

    @Inject
    public GroupsOfferedResource(GroupsOfferedService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/career/{idCareer}/{pensum}/{semester}/")
    @ApiOperation( value = "Gets the Groups Offered by career and semester", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Groups Offered found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Groups Offered not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getByCareerAndSemester(@ApiParam(value = "Groups Offered ID needed to find the career", required = true)
                                                    @PathParam("idCareer") long idCareer,
                                                @ApiParam(value = "Groups Offered ID needed to find the pensum", required = true)
                                                    @PathParam("pensum") int pensum,
                                                @ApiParam(value = "Groups Offered ID needed to find the semester", required = true)
                                                    @PathParam("semester") String semester,
                                                @ApiParam(value = "Groups Offered ID needed to find the module")
                                                    @QueryParam("module") String module ) throws Exception {
        return service.getByCareerSemesterAndModule(idCareer, pensum, semester, module);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/academicArea/{idAcademicArea}/{semester}")
    @ApiOperation( value = "Gets the Groups Offered by academic area", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Groups Offered found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Groups Offered not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getByAcademicArea(@ApiParam(value = "Groups Offered ID needed to find the academic area", required = true)
                                                @PathParam("idAcademicArea") long idAcademicArea,
                                           @ApiParam(value = "Groups Offered ID needed to find the semester", required = true)
                                                @PathParam("semester") String semester,
                                           @ApiParam(value = "Groups Offered ID needed to find the module")
                                                @QueryParam("module") String module) {
        return service.getByAcademicAreaAndModule(idAcademicArea, semester, module);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/course/{idCourse}")
    @ApiOperation(value = "Gets the Groups Offered by academic area", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Groups Offered found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "Groups Offered not content"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Course not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getByCourseAndCurrentSemester(@ApiParam(value = "Groups Offered ID needed to find the course", required = true)
                                                           @PathParam("idCourse") long idCourse){
        return service.getByCourseAndSemester(idCourse);
    }
}
