package com.utepsa.resources.census;

import com.google.inject.Inject;
import com.utepsa.api.custom.census.Answers;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.census.CensusDAO;

/**
 * Created by Luana Chavez on 04/10/2017.
 */
public class CensusService {
    private final CensusDAO censusDAO;

    @Inject
    public CensusService(CensusDAO censusDAO) {
        this.censusDAO = censusDAO;
    }

    public BasicResponse generalReport(){
        BasicResponse basicResponse = new BasicResponse();
        Answers answers = censusDAO.generalReport();
        try{
            if (answers != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(answers);
            }

            if(answers == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Census not found");
            }

        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }


    public BasicResponse careerReport(long idCareer){
        BasicResponse basicResponse = new BasicResponse();
        Answers answers = censusDAO.careerReport(idCareer);
        try{
            if (answers != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(answers);
            }

            if(answers == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Census Not Found");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }
}
