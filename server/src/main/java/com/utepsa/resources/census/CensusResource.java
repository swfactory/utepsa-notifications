package com.utepsa.resources.census;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 04/10/2017.
 */

@Path(CensusResource.SERVICE_PATH)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = CensusResource.SERVICE_PATH, description = "Report from Census")
public class CensusResource {

    public static final String SERVICE_PATH = "census/";

    private final CensusService service;

    @Inject
    public CensusResource(CensusService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("general")
    @ApiOperation( value = "Gets the General Report", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Answers found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Answers not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getGeneralReport() {
        return service.generalReport();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("career/{idCareer}")
    @ApiOperation( value = "Gets the Career Report", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Answers found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Answers not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getCareerReport(@ApiParam(value = "Grade to search", required = true)
                                             @PathParam("idCareer") long idCareer) {
        return service.careerReport(idCareer);
    }
}
