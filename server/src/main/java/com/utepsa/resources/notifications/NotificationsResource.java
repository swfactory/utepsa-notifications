package com.utepsa.resources.notifications;


import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Notification;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by DAvid on 22/07/2016.
 */
@Path(NotificationsResource.SERVICE_PATH)
@Api(value = NotificationsResource.SERVICE_PATH, description = "Operations about Notifications")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationsResource {

    public static final String SERVICE_PATH = "notifications/";
    private final NotificationService service;

    @Inject
    public NotificationsResource(NotificationService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification created correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Service GCM not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse createNotification(@ApiParam(value = "JSON format is received with the notification structure.", required = true) Notification notification) {
        return service.createNotification(notification);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Get Notification by ID", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification obtained correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Notification not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse getNotificationById(@ApiParam(value = "Notification ID needed to find the notification", required = true)
                                                 @PathParam("id") LongParam id) {
        try {
            return service.getNotificationById(id.get());
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification obtained correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Notification not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse getAllNotifications() {
        try {
            return service.getAllNotifications();
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("/{id}/update/")
    @ApiOperation( value = "update Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification updated correctly"),
            @ApiResponse(code=StatusCode.NOT_MODIFIED,message="Notification doesn't updated, because this category doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse UpdateNotifications(@ApiParam(value = "Notification ID needed to find the notification", required = true)
                                             @PathParam("id") long id,
                                             @ApiParam(value = "Notification Content needed to find the notification", required = true)
                                             @QueryParam("content") String content,
                                             @ApiParam(value = "Notification ID Category needed to find the notification", required = true)
                                             @QueryParam("idCategory") long idCategory) {
        try {
            return service.updateNotifications(id,content,idCategory);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/Creator/{IdCreator}")
    @ApiOperation( value =  "get Notifications by creator", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification founded correctly"),
            @ApiResponse(code=StatusCode.NOT_FOUND,message="Notification doesn't founded, because this category doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getNotificationsByCreator (@ApiParam(value = "Creator ID needed to find the notification", required = true)
                                                    @PathParam("idCreator") long idCreator){
        try{
            return service.getNotificationsByCreator(idCreator);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}