package com.utepsa.resources.notifications;

import com.google.inject.Inject;
import com.utepsa.adapters.gcm.Data;
import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.adapters.gcm.NotificationGCM;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.core.TypeAudience;
import com.utepsa.db.categoryNotification.CategoryNotificationDAO;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Notification;
import com.utepsa.models.NotificationAudience;
import com.utepsa.models.Student;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by DAvid on 22/07/2016.
 */
public class NotificationService {

    private NotificationDAO notificationDAO;
    private StudentDAO daoStudent;
    private GCMAdapter adapter;
    private CredentialDAO credentialDAO;
    private CategoryNotificationDAO categoryNotificationDAO;

    @Inject
    public NotificationService(NotificationDAO notificationDAO, GCMAdapter gcmAdapter, StudentDAO daoStudent, CredentialDAO credentialDAO,CategoryNotificationDAO categoryNotificationDAO) {
        this.notificationDAO = notificationDAO;
        this.adapter= gcmAdapter;
        this.daoStudent = daoStudent;
        this.credentialDAO = credentialDAO;
        this.categoryNotificationDAO =categoryNotificationDAO;
    }

    public BasicResponse getNotificationById(long id) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Notification notification = notificationDAO.getById(id);
            if(notification != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(notification);
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Notification not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse createNotification(Notification notification) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(notificationDAO.create(notification) > 0){
                basicResponse = this.pushNotificationGcm(notification);
                basicResponse.setMessage("Notification created correctly");
            }else{
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
                basicResponse.setMessage("Notification not created");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    private BasicResponse pushNotificationGcm(Notification notification) throws IOException {
        BasicResponse basicResponse = new BasicResponse();
        basicResponse.setCode(StatusCode.OK);
        NotificationGCM notificationGCM= new NotificationGCM();
        Set<NotificationAudience> notificationAudience = notification.getAudience();
        NotificationAudience audience = (NotificationAudience) notificationAudience.toArray()[0];

        List<String> students = null;
        try {
            students = this.getViewGcmStudents(audience.getType().getId(), audience.getTarget());
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.NOT_FOUND);
            basicResponse.setMessage("Student not found");
        }

        if(students != null && students.size()!=0){
            String[] gcmIds = new String[students.size()];
            gcmIds = students.toArray(gcmIds);
            Set<String> ids = new HashSet<>(Arrays.asList(gcmIds));

            notificationGCM.setNotification(new Data(notification.getTitle(), notification.getContent()));
            notificationGCM.setRegistration_ids(ids);
            try{
                this.adapter.sendNotification(notificationGCM);

            }catch (Exception e){
                basicResponse.setCode(StatusCode.CONFLICT);
                basicResponse.setMessage("Service GCM not found");
            }
        }
        return basicResponse;
    }

    private List<String> getViewGcmStudents(long typeAudience, String target) throws Exception {

        if(typeAudience == TypeAudience.STUDENT){
            Student student = daoStudent.getByRegisterNumber(target);
            if(student == null) return null;
            return credentialDAO.getGcmIdByStudent(student.getId());
        }
        if(typeAudience == TypeAudience.CAREER){
            return credentialDAO.getGcmIdByCareer( Long.parseLong(target));
        }

        return credentialDAO.getAllGcmId();
    }

    public BasicResponse getAllNotifications() throws Exception
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Notification> notifications = notificationDAO.getAll();
            if(notifications.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(notifications);
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Notification not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse updateNotifications(long id,String content,long idCategory) throws Exception
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            CategoryNotification categoryNotification = categoryNotificationDAO.getById(idCategory);
            if (categoryNotification!=null)
            {
                notificationDAO.updateNotifications(id,content,categoryNotification);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("Notification updated correctly");
            }
            else{
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("Notification doesn't updated, because this category doesn't exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public  BasicResponse getNotificationsByCreator(long idCreator) throws Exception
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Notification> notifications = notificationDAO.getByCreator(idCreator);
            if (notifications.size() > 0)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(notifications);
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Notification doesn't found");
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
