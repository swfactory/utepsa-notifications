package com.utepsa.resources.executiveReport;

import com.google.inject.Inject;
import com.utepsa.api.custom.*;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.executiveReport.ExecutiveReportDAO;
import com.utepsa.models.Career;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public class ExecutiveReportService {
    private final ExecutiveReportDAO executiveReportDAO;
    private final CareerDAO careerDao;

    @Inject
    public ExecutiveReportService(ExecutiveReportDAO executiveReportDAO, CareerDAO careerDao) {
        this.executiveReportDAO = executiveReportDAO;
        this.careerDao = careerDao;
    }

    public BasicResponse getQuantityOfStudent(String semester){
        BasicResponse basicResponse = new BasicResponse();
        try {
            ExecutiveReport executiveReport = new ExecutiveReport();
            List<CareerAverageReport> dataCareers = new ArrayList<>();
            List<StudentCustom> studentCustoms = executiveReportDAO.getStudentCustom();
            executiveReport.setSemester(semester);
            executiveReport.setTotal(studentCustoms.stream().count());
            executiveReport.setTotalActive(executiveReportDAO.getActiveStudents(semester.trim()));
            executiveReport.setTotalSemesterCourses(executiveReportDAO.getTotalSemesterCourses(semester.trim()));
            executiveReport.setTotalApprovedCourses(executiveReportDAO.getTotalApprovedCourses(semester.trim()));
            executiveReport.setTotalDisapprovedCourses(executiveReportDAO.getTotalDisapprovedCourses(semester.trim()));
            executiveReport.setTotalAbandonedCourses(executiveReportDAO.getTotalAbandonedCourses(semester.trim()));
            Stream<Career> careers = careerDao.getAll().stream();
            careers.forEach( career -> {
                CareerAverageReport newDataCareer = new CareerAverageReport();
                newDataCareer.setNameCareer(career.getName());
                try {
                    newDataCareer.setAverage(executiveReportDAO.getAverageByCareer(semester.trim(), career.getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dataCareers.add(newDataCareer);
            });
            executiveReport.setDataCareers(dataCareers);
            List<String> modules = new ArrayList(Arrays.asList("0","1","2","3", "4", "5"));
            Collections.sort(modules);
            Iterator iterator = modules.iterator();
            List<ModulesReport> modulesReports = new ArrayList<>();
            modules.stream().forEach(module ->{
                String moduleSemester = String.valueOf(iterator.next());
                ModulesReport modulesReport = new ModulesReport();
                try {
                    modulesReport.setStudentRegister(executiveReportDAO.getTotalStudentByModule(semester.trim(), module.trim()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                modulesReport.setModule(moduleSemester);
                modulesReports.add(modulesReport);
            });
            executiveReport.setTotalActiveStudentByModule(modulesReports);

            if (executiveReport != null){
                executiveReport.setDataCareers(dataCareers);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(executiveReport);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getQuantityOfStudentByCareer(Long idCareer, String semester){
        BasicResponse basicResponse = new BasicResponse();
        try
        {
            List<StudentCustom> studentCustoms = executiveReportDAO.getStudentCustom();
            Career career = careerDao.getById(idCareer);
            DataCareer newDataCareer = new DataCareer();
            newDataCareer.setIdCareer(career.getId());
            newDataCareer.setSameCareer(career.getSameCareer());
            newDataCareer.setPlan(career.getPlan());
            newDataCareer.setNameCareer(career.getName());
            try {
                newDataCareer.setAverage(executiveReportDAO.getAverageByCareer(semester.trim(), career.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            newDataCareer.setStudentCareer(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId()).count());
            newDataCareer.setStudentCareerMan(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId() && student.getGender().toUpperCase().equals("M")).count());
            newDataCareer.setStudentCareerWoman(newDataCareer.getStudentCareer()- newDataCareer.getStudentCareerMan());
            newDataCareer.setStudentActive(executiveReportDAO.getActiveStudentsByCareer(semester.trim(), career.getId()));
            newDataCareer.setStudentActiveMan(executiveReportDAO.getActiveStudentsByGender(semester.trim(), career.getId(), "M"));
            newDataCareer.setStudentActiveWoman(executiveReportDAO.getActiveStudentsByGender(semester.trim(), career.getId(), "F"));
            newDataCareer.setSemesterCourses(executiveReportDAO.getSemesterCourses(semester.trim(), career.getId()));
            newDataCareer.setApprovedCourses(executiveReportDAO.getApprovedCourses(semester.trim(), career.getId()));
            newDataCareer.setDisapprovedCourses(executiveReportDAO.getDisapprovedCourses(semester.trim(), career.getId()));
            newDataCareer.setAbandonedCourses(executiveReportDAO.getAbandonedCourses(semester.trim(), career.getId()));

            List<String> modules = new ArrayList(Arrays.asList("0","1","2","3", "4", "5"));
            Collections.sort(modules);
            Iterator iterator = modules.iterator();
            List<ModulesReport> modulesReports = new ArrayList<>();
            modules.stream().forEach(module ->{
                String moduleSemester = String.valueOf(iterator.next());
                ModulesReport modulesReport = new ModulesReport();
                try {
                    modulesReport.setStudentRegister(executiveReportDAO.getActiveStudentsByModule(semester.trim(), career.getId(), moduleSemester.toString().trim()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                modulesReport.setModule(moduleSemester);
                modulesReports.add(modulesReport);
            });
            newDataCareer.setModule(modulesReports);
            if (newDataCareer != null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(newDataCareer);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getQuantityOfStudentByYear(String year){
        BasicResponse basicResponse = new BasicResponse();
        try
        {
            ReportByYear reportByYear = new ReportByYear();
            reportByYear.setNumberOfStudentsByYear(executiveReportDAO.getActiveStudentsByYear(year.trim()));
            reportByYear.setNumberOfStudentsInFirstSemester(executiveReportDAO.getActiveStudentsByFirstSemester(year.trim()));
            reportByYear.setNumberOfStudentsInSecondSemester(executiveReportDAO.getActiveStudentsBySecondSemester(year.trim()));
            reportByYear.setNumberOfStudentsInSummer(executiveReportDAO.getActiveStudentsBySummer(year.trim()));
            reportByYear.setNumberOfStudentsInWinter(executiveReportDAO.getActiveStudentsByWinter(year.trim()));
            reportByYear.setNumberOfCourse(executiveReportDAO.getTotalCoursesByYear(year.trim()));
            reportByYear.setNumberOfCourseDisapproved(executiveReportDAO.getTotalDisapprovedCoursesByYear(year.trim()));
            reportByYear.setNumberOfCourseApproved(executiveReportDAO.getTotalApprovedCoursesByYear(year.trim()));
            reportByYear.setNumberOfCourseAbandoned(executiveReportDAO.getTotalAbandonedCoursesByYear(year.trim()));
            if (reportByYear != null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(reportByYear);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }


    public BasicResponse getQuantityOfStudentByYearAndCareer(String year, Long idCareer){
        BasicResponse basicResponse = new BasicResponse();
        try
        {
            ReportByYear reportByYear = new ReportByYear();
            reportByYear.setNumberOfStudentsByYear(executiveReportDAO.getActiveStudentsByYearAndCareer(year.trim(), idCareer));
            reportByYear.setAverageByYear(executiveReportDAO.getAverageByYear(year.trim(), idCareer));
            reportByYear.setNumberOfStudentsInFirstSemester(executiveReportDAO.getActiveStudentsByFirstSemesterAndCareer(year.trim(), idCareer));
            reportByYear.setNumberOfStudentsInSecondSemester(executiveReportDAO.getActiveStudentsBySecondSemesterAndCareer(year.trim(), idCareer));
            reportByYear.setNumberOfStudentsInSummer(executiveReportDAO.getActiveStudentsBySummerAndCareer(year.trim(), idCareer));
            reportByYear.setNumberOfStudentsInWinter(executiveReportDAO.getActiveStudentsByWinterAndCareer(year.trim(), idCareer));
            reportByYear.setNumberOfCourseByCareer(executiveReportDAO.getCoursesByYear(year.trim(), idCareer));
            reportByYear.setNumberOfCourseApprovedByCareer(executiveReportDAO.getApprovedCoursesByYear(year.trim(), idCareer));
            reportByYear.setNumberOfCourseDisapprovedByCareer(executiveReportDAO.getDisapprovedCoursesByYear(year.trim(), idCareer));
            reportByYear.setNumberOfCourseAbandonedByCareer(executiveReportDAO.getAbandonedCoursesByYear(year.trim(), idCareer));
            if (reportByYear != null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(reportByYear);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getCourseModuleReport(String semester, String module, Long idCareer)
    {
        BasicResponse basicResponse = new BasicResponse();

        try
        {
            List<CourseModuleReport> courseModule = executiveReportDAO.getCourseModule(semester.trim(), module.trim(), idCareer);
            if (courseModule.size() > 0)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(courseModule);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }
}
