package com.utepsa.resources.students;

import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.adapters.utepsa.financialState.FinancialStateAdapter;
import com.utepsa.adapters.utepsa.financialState.FinancialStateData;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.adapters.utepsa.professor.ProfessorAdapter;
import com.utepsa.adapters.utepsa.professor.ProfessorData;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterData;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.api.custom.HistoryNotesCustom;
import com.utepsa.api.custom.MissingCourse;
import com.utepsa.api.custom.StudentHistoryNotes;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.GlobalConfig;
import com.utepsa.core.StatusCode;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.file.FileDAO;
import com.utepsa.db.financialState.FinancialStateDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterDAO;
import com.utepsa.models.*;
import com.utepsa.resources.groupsOffered.GroupsOfferedService;
import ru.vyarus.dropwizard.guice.module.context.stat.Stat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by roberto on 12/7/2016.
 */
public class StudentsService {

    private final StudentDAO dao;
    private final HistoryNotesDAO historyNotesDao;
    private final CareerCourseDAO careerCourseDAO;
    private final NotificationDAO notificationDAO;
    private final DocumentStudentDAO documentStudentDAO;
    private final DocumentDAO documentDAO;
    private final FileDAO fileDAO;
    private final CurrentSemesterDAO currentSemesterDAO;
    private final StudentCourseRegisterDAO studentCourseRegisterDAO;
    private final HistoryNotesAdapter historyNotesAdapter;
    private final DocumentsStudentAdapter documentsStudentAdapter;
    private final StudentAdapter studentAdapter;
    private final CourseDAO courseDAO;
    private final CareerDAO careerDAO;
    private final FinancialStateDAO financialStateDAO;
    private final ProfessorDAO professorDAO;
    private final SchedulesDAO schedulesDAO;
    private final ProfessorAdapter professorAdapter;
    private final StudentCourseRegisterAdapter studentCourseRegisterAdapter;
    private final FinancialStateAdapter financialStateAdapter;

    @Inject
    public StudentsService(StudentCourseRegisterAdapter studentCourseRegisterAdapter,FinancialStateAdapter financialStateAdapter , ProfessorAdapter professorAdapter,
                           DocumentsStudentAdapter documentsStudentAdapter, HistoryNotesAdapter historyNotesAdapter, StudentAdapter studentAdapter, StudentDAO dao, HistoryNotesDAO historyNotesDao,
                           CareerCourseDAO careerCourseDAO, NotificationDAO notificationDAO, DocumentStudentDAO documentStudentDAO,
                           DocumentDAO documentDAO, FileDAO fileDAO, CurrentSemesterDAO currentSemesterDAO, StudentCourseRegisterDAO studentCourseRegisterDAO,
                           CourseDAO courseDAO, CareerDAO careerDAO, FinancialStateDAO financialStateDAO, ProfessorDAO professorDAO, SchedulesDAO schedulesDAO) {
        this.dao = dao;
        this.historyNotesDao = historyNotesDao;
        this.careerCourseDAO = careerCourseDAO;
        this.notificationDAO = notificationDAO;
        this.documentStudentDAO = documentStudentDAO;
        this.documentDAO = documentDAO;
        this.fileDAO = fileDAO;
        this.currentSemesterDAO = currentSemesterDAO;
        this.studentCourseRegisterDAO = studentCourseRegisterDAO;
        this.historyNotesAdapter = historyNotesAdapter;
        this.documentsStudentAdapter = documentsStudentAdapter;
        this.studentAdapter = studentAdapter;
        this.courseDAO = courseDAO;
        this.careerDAO = careerDAO;
        this.financialStateDAO = financialStateDAO;
        this.professorAdapter = professorAdapter;
        this.studentCourseRegisterAdapter = studentCourseRegisterAdapter;
        this.professorDAO = professorDAO;
        this.schedulesDAO = schedulesDAO;
        this.financialStateAdapter = financialStateAdapter;
    }

    public BasicResponse getById (long idStudent) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(student);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getNotificationsByStudent(long idStudent, int startRow) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                List<Notification> notifications = notificationDAO.getByStudent(student.getRegisterNumber(),student.getCareer().getId(), GlobalConfig.NOTIFICATIONS_PER_STUDENT,startRow);
                if(notifications != null && notifications.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(notifications);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("There are no notifications to display");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getDocumentsByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                List<DocumentStudent> documents = documentStudentDAO.getByStudent(idStudent);
                if(documents != null && documents.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(documents);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("This student does not have any documents registered");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getHistoryNotesByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Student student = dao.getById(idStudent);
            List<HistoryNotes> historyNotes = historyNotesDao.getByStudent(idStudent);
            if (historyNotes != null)
            {
                List<CareerCourse> coursesByCareer = careerCourseDAO.getCoursesByCareer(student.getCareer().getId(), student.getPensum());
                List<Course> coursesMissing = historyNotesDao.getCoursesMissingByStudent(idStudent, student.getCareer().getId(), student.getPensum());
                List<Double> summary = getStudentSummary(historyNotes,null);

                double progressPercentage = (summary.get(0) * 100) / (double) coursesByCareer.size();

                StudentHistoryNotes studentHistoryNotes = new StudentHistoryNotes();
                studentHistoryNotes.setApproved(summary.get(0).intValue());
                studentHistoryNotes.setDisapproved(summary.get(1).intValue());
                studentHistoryNotes.setAverage(summary.get(2));
                studentHistoryNotes.setTotal(coursesByCareer.size());
                studentHistoryNotes.setProgressPercentage(progressPercentage);
                studentHistoryNotes.setCourses(historyNotes);
                studentHistoryNotes.setCoursesMissing(coursesMissing);

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(studentHistoryNotes);
                return basicResponse;
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getHistoryNotesByPensum(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Student student = dao.getById(idStudent);
            List<HistoryNotesCustom> historyNotes = historyNotesDao.getHistoryNotesByPensum(idStudent,student.getCareer().getId(),student.getPensum());
            if (historyNotes!=null)
            {
                List<CareerCourse> coursesByCareer = careerCourseDAO.getCoursesByCareer(student.getCareer().getId(), student.getPensum());
                List<Double> summary = getStudentSummary(null,historyNotes);

                double progressPercentage = (summary.get(0) * 100) / (double) coursesByCareer.size();

                StudentHistoryNotes studentHistoryNotes = new StudentHistoryNotes();
                studentHistoryNotes.setApproved(summary.get(0).intValue());
                studentHistoryNotes.setDisapproved(summary.get(1).intValue());
                studentHistoryNotes.setAverage(summary.get(2));
                studentHistoryNotes.setTotal(coursesByCareer.size());
                studentHistoryNotes.setProgressPercentage(progressPercentage);
                studentHistoryNotes.setCourses(historyNotes);

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(studentHistoryNotes);
                return basicResponse;
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
    private List<Double> getStudentSummary(List<HistoryNotes> courses,List<HistoryNotesCustom> coursesCustom){
        List<Double> summary = new ArrayList<>();
        double approved = 0;
        double disapproved = 0;
        double note = 0;
        double average = 0;
        double total = 0;

        if (courses!=null)
        {
            for(HistoryNotes course: courses){
                total++;
                note = note + course.getNote();
                if(course.getNote() >= course.getMinimunNote()){
                    approved++;
                }else{
                    disapproved++;
                }
            }
        }
        else{
            for(HistoryNotesCustom courseCustom: coursesCustom){
                if(courseCustom.getNote() == null || courseCustom.getMinimunNote()==null) continue;
                total++;
                note = note + courseCustom.getNote();
                if(courseCustom.getNote() >= courseCustom.getMinimunNote()){
                    approved++;
                }else{
                    disapproved++;
                }
            }
        }
        average = note / total;
        summary.add(approved);
        summary.add(disapproved);
        summary.add(average);

        return summary;
    }

    public BasicResponse getFilesByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                List<File> files = fileDAO.getAvailableByStudent(student.getCareer().getId());
                if(files.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(files);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("This student has no files to display");
                }
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return basicResponse;
    }

    public BasicResponse getStudentByRegisterNumber(String registerNumber){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getByRegisterNumber(registerNumber);
            if(student != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(student);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getCourseRegisterByStudent(long idStudent)
    {
        BasicResponse basicResponse=new BasicResponse();
        try {
            CurrentSemester semester = currentSemesterDAO.getCurrentSemester();
            if(semester != null){
                List<StudentCourseRegister> studentCourseRegisters = studentCourseRegisterDAO.getAllCourseRegisterByStudent(idStudent,semester.getSemester());
                if(studentCourseRegisters.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(studentCourseRegisters);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("This CurrentSemester has no Student Course Register to display");
                }
            }else{
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
                basicResponse.setMessage("Current semester not found");
            }

        }catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse updateStudentData(long idStudent)
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            StudentData studentData = studentAdapter.getStudent(student.getRegisterNumber());
            if (studentData != null && student != null)
            {
                student.setName(studentData.getAgd_nombres().trim());
                student.setFatherLastname(studentData.getAgd_appaterno().trim());
                student.setMotherLastname(studentData.getAgd_apmaterno().trim());
                student.setPhoneNumber1(studentData.getAgd_telf1().trim());
                student.setPhoneNumber2(studentData.getAgd_telf2().trim());
                student.setEmail1(studentData.getCorreo().trim());
                syncHistoryNotes(student);
                syncDocuments(student);
                SyncStudentCourseRegisters(student, currentSemesterDAO.getCurrentSemester().getSemester());
                SyncFinancialState(student.getId());

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("Students course registers updated");
            }
            else
            {
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
                basicResponse.setMessage("This data has no Student to display");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public void syncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDao.getByStudent(student.getId());

        if (historyNotesUtepsa.size() > historyNotesApp.size())
        {
            List<HistoryNotesData> newCourses = historyNotesUtepsa.stream().filter( e ->historyNotesApp.stream().filter(a -> a.getSemester().equals(e.getSem_codigo())
                    && (a.getModule().equals(e.getMdu_codigo()) || a.getModule().isEmpty()) && a.getCourse().getCodeUtepsa().equals(e.getMat_codigo())).count() == 0).collect(Collectors.toList());

            newCourses.stream().forEach(hn -> {
                try {
                    this.createHistoryNote(hn,student.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }});
        }

        historyNotesApp.stream().forEach(app -> {
                    historyNotesUtepsa.stream()
                    .filter(utepsa -> app.getSemester().equals(utepsa.getSem_codigo()) && app.getModule().equals(utepsa.getMdu_codigo()) && app.getCourse().getCodeUtepsa().equals(utepsa.getMat_codigo()))
                    .map(e -> e).findFirst().ifPresent(
                            a -> {
                                app.setNote((int) a.getNot_nota());
                            }
                    );
        });
    }

    private void createHistoryNote(HistoryNotesData data,long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        HistoryNotes historyNote = new HistoryNotes();
        historyNote.setIdStudent(idStudent);
        historyNote.setCourse(course);
        historyNote.setMinimunNote((int) data.getPln_notaminima());
        historyNote.setNote((int) data.getNot_nota());
        historyNote.setModule(data.getMdu_codigo().trim());
        historyNote.setSemester(data.getSem_codigo().trim());
        historyNotesDao.create(historyNote);
    }

    public void syncDocuments(Student student) throws Exception {
        List<DocumentStudent> documentsStudentApp = documentStudentDAO.getByStudent(student.getId());
        List<DocumentsStudentData> documentsStudentUtepsa = documentsStudentAdapter.getAllDocumentsStudentForStudent(student.getRegisterNumber());

        for(DocumentsStudentData data: documentsStudentUtepsa){

            if(documentsStudentUtepsa.size() <= 0) return;

            if(documentsStudentApp.size() == 0 && documentsStudentUtepsa.size() > 0){
                DocumentStudent documentStudent = new DocumentStudent();
                if(data.getAlmdoc_fechaentregar() == null){
                    documentStudent.setDateDelivery(null);
                } else {
                    SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fechaentregar().trim());
                    documentStudent.setDateDelivery(dateDelivery);
                }

                if(data.getAlmdoc_fecharecepcion() == null){
                    documentStudent.setDateReceipt(null);
                } else {
                    SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fecharecepcion().trim());
                    documentStudent.setDateReceipt(dateDelivery);
                }

                documentStudent.setIdStudent(student.getId());
                documentStudent.setDocument(documentDAO.getByCodeUtepsa(data.getDoc_codigo()));
                documentStudent.setTypePaper(data.getAlmdoc_tipopapel());

                boolean state;
                if(data.getESTADO().trim().contains("ENTREGADO")){
                    state = true;
                }else{
                    state = false;
                }

                documentStudent.setState(state);
                documentStudentDAO.create(documentStudent);
            }

            for(DocumentStudent documentStudent: documentsStudentApp){
                    if(!documentStudent.getTypePaper().equals(data.getAlmdoc_tipopapel().trim())){
                        documentStudent.setTypePaper(data.getAlmdoc_tipopapel().trim());
                    }

                    SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                    if(data.getAlmdoc_fechaentregar() != null){
                        Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fechaentregar().trim());

                        if(documentStudent.getDateDelivery() != dateDelivery){
                            documentStudent.setDateDelivery(dateDelivery);
                        }
                    }

                    if(data.getAlmdoc_fecharecepcion() != null){
                        Date dateReceipt = formaterTimeZone.parse(data.getAlmdoc_fecharecepcion().trim());
                        if(documentStudent.getDateReceipt() != dateReceipt){
                            documentStudent.setDateReceipt(dateReceipt);
                        }
                    }

                    boolean state;
                    if(data.getESTADO().trim().contains("ENTREGADO")){
                        state = true;
                    }else{
                        state = false;
                    }

                    if(documentStudent.isState() != state){
                        documentStudent.setState(state);
                    }
                    return;
                }
        }
    }

    public BasicResponse getFinancialStateOfStudent(long student){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student1 = dao.getById(student);
            FinancialState financialState = financialStateDAO.getByIdStudent(student1);
            if(financialState != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(financialState);
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student doesn't exist");
            }
        }catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getByFullName(String fullName, String registerNumber)
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            if (fullName != null && registerNumber != null)
            {
                basicResponse.setCode(StatusCode.CONFLICT);
                basicResponse.setMessage("The search can only be done by a parameter");
                return basicResponse;
            }
            if (fullName == null && registerNumber == null)
            {
                basicResponse.setCode(StatusCode.CONFLICT);
                basicResponse.setMessage("Please enter a parameter");
                return basicResponse;
            }
            List<Student> students;
            if(fullName != null && registerNumber == null){
                students = dao.searchStudentByCareerFullnameOrRegisterNumber(fullName.toUpperCase());
            }
            else{
                return getStudentByRegisterNumber(registerNumber.toUpperCase());
            }
            if (students.size() > 0)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(students);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("Student does't exist");
            }
        }catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getAllHistoryNotesByCourse(long idStudent, long idCourse) throws Exception{
        BasicResponse basicResponse = new BasicResponse();
        try {
            Course course = courseDAO.getById(idCourse);
            List<HistoryNotes> getAllHistoryNotesByCourse = historyNotesDao.getAllHistoryNotesByCourse(idStudent, course);
            if(dao.getById(idStudent) != null){
                if(getAllHistoryNotesByCourse.size() != 0){
                    basicResponse.setData(getAllHistoryNotesByCourse);
                    basicResponse.setCode(StatusCode.OK);
                } else {
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("The student doesn't have this course");
                }
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student doesn't exist");
            }
        } catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public void SyncStudentCourseRegisters(Student student, String semester) throws Exception{
        List<StudentCourseRegister> studentCoursesRegistersApp =  studentCourseRegisterDAO.getAllCourseRegisterByStudent(student.getId(), semester);
        List<StudentCourseRegisterData> studentCoursesRegistersUtepsa = studentCourseRegisterAdapter.getCourseRegisterByRegisterNumberAndSemester(student.getRegisterNumber(), semester);

        if(studentCoursesRegistersApp.size() == 0 && studentCoursesRegistersUtepsa.size() > 0){
            for(StudentCourseRegisterData data : studentCoursesRegistersUtepsa){
                this.createStudentCourseRegistered(data, student.getId());
            }
            return;
        }

        if(studentCoursesRegistersUtepsa.isEmpty()) return;


        if (studentCoursesRegistersUtepsa.size() > studentCoursesRegistersApp.size())
        {
            List<StudentCourseRegisterData> newStudentCourseRegister = studentCoursesRegistersUtepsa.stream().filter( e ->studentCoursesRegistersApp.stream().filter(a -> a.getCodeUtepsa() == e.getGrp_detalle_id()).count() == 0).collect(Collectors.toList());

            newStudentCourseRegister.stream().forEach(sr -> {
                try {
                    this.createStudentCourseRegistered(sr,student.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }});
        }

        for(StudentCourseRegisterData data: studentCoursesRegistersUtepsa){
            this.updateStudentCourseRegistered(data, studentCoursesRegistersApp);
        }

        return;
    }

    private void createStudentCourseRegistered(StudentCourseRegisterData data, long idStudent) throws Exception{
        StudentCourseRegister studentCourseRegister = new StudentCourseRegister();

        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());

        if(data.getAgd_docente() == null){
            studentCourseRegister.setProfessor(null);
        }else{
            Professor professor = professorDAO.getProfessorByAgendCode(data.getAgd_docente().trim());
            if(professor == null) {
                professor = registerProfessor(data.getAgd_docente().trim());
            }
            studentCourseRegister.setProfessor(professor);
        }

        if(data.getTur_codigo() == null){
            studentCourseRegister.setSchedule(null);
        }else{
            Schedule schedule = schedulesDAO.getByCodeUtepsa(data.getTur_codigo().trim());
            studentCourseRegister.setSchedule(schedule);
        }

        studentCourseRegister.setIdStudent(idStudent);
        studentCourseRegister.setCourse(course);
        studentCourseRegister.setClassroom(data.getAul_codigo().trim());
        studentCourseRegister.setGroup(data.getGrp_grupo().trim());
        studentCourseRegister.setModule(data.getMdu_codigo().trim());
        studentCourseRegister.setNumberEnrolled(data.getGrp_nroinscritos());
        if(data.getObservacion() != null){
            studentCourseRegister.setState(data.getObservacion().trim());
        } else {
            studentCourseRegister.setState(null);
        }
        studentCourseRegister.setNote(data.getNot_nota());
        studentCourseRegister.setSemester(data.getSem_codigo().trim());
        studentCourseRegister.setCodeUtepsa(data.getGrp_detalle_id());

        studentCourseRegisterDAO.create(studentCourseRegister);
    }

    private void updateStudentCourseRegistered(StudentCourseRegisterData data, List<StudentCourseRegister> studentCoursesRegistersApp) throws Exception{
        for(StudentCourseRegister studentCourseRegister : studentCoursesRegistersApp){
            if(studentCourseRegister.getCodeUtepsa() == data.getGrp_detalle_id()){
                studentCourseRegister.setGroup(data.getGrp_grupo().trim());
                studentCourseRegister.setClassroom(data.getAul_codigo().trim());
                studentCourseRegister.setNumberEnrolled(data.getGrp_nroinscritos());
                studentCourseRegister.setNote(data.getNot_nota());
                if(data.getObservacion() != null){
                    studentCourseRegister.setState(data.getObservacion().trim());
                } else {
                    studentCourseRegister.setState(null);
                }
                studentCourseRegister.setCodeUtepsa(data.getGrp_detalle_id());

                if(data.getAgd_docente() != null){
                    Professor professor = professorDAO.getProfessorByAgendCode(data.getAgd_docente().trim());
                    if(professor == null) {
                        professor = registerProfessor(data.getAgd_docente().trim());
                    }
                    if(studentCourseRegister.getProfessor() == null){
                        studentCourseRegister.setProfessor(professor);
                    }else{
                        if(studentCourseRegister.getProfessor().getId() != professor.getId()){
                            studentCourseRegister.setProfessor(professor);
                        }
                    }
                }

                if(data.getTur_codigo() != null){
                    Schedule schedule = schedulesDAO.getByCodeUtepsa(data.getTur_codigo().trim());
                    if(studentCourseRegister.getSchedule() != null){
                        if(studentCourseRegister.getSchedule().getId() != schedule.getId()){
                            studentCourseRegister.setSchedule(schedule);
                        }
                    }else{
                        studentCourseRegister.setSchedule(schedule);
                    }
                }
            }
        }
        return;
    }

    private Professor registerProfessor(String registerCode) throws Exception {
        ProfessorData professorData = professorAdapter.getProfessor(registerCode);

        if(professorData == null) return null;

        Professor newProfessor = new Professor();
        newProfessor.setAgend_code(professorData.getAgd_codigo());
        newProfessor.setName(professorData.getAgd_nombres());
        newProfessor.setFather_lastname(professorData.getAgd_appaterno());
        newProfessor.setMother_lastname(professorData.getAgd_apmaterno());
        newProfessor.setState(true);
        professorDAO.create(newProfessor);
        return newProfessor;
    }

    public void SyncFinancialState(long idStudent) throws Exception {
        Student student = dao.getById(idStudent);
        if(student == null){
            return;
        }

        if(financialStateAdapter.getFinancialState(student.getAgendCode()) != null){
            FinancialStateData financialStateData = financialStateAdapter.getFinancialState(student.getAgendCode());
            FinancialState financialStateApp = financialStateDAO.getByIdStudent(student);
            if(financialStateApp == null)
                financialStateApp = new FinancialState();
            financialStateApp.setIdStudent(student);
            financialStateApp.setDebitBalance(financialStateData.getSaldoDeudor());
            financialStateApp.setDelinquentBalance(financialStateData.getSaldoenMora());
            financialStateApp.setOutstandingBalance(financialStateData.getSaldoaVencer());
            if(financialStateData.getFechaAVencer() == null){
                financialStateApp.setExpirationDate(null);
            } else {
                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                Date dateDelivery = formaterTimeZone.parse(financialStateData.getFechaAVencer().trim());
                financialStateApp.setExpirationDate(dateDelivery);
            }

            financialStateDAO.create(financialStateApp);
        }
    }

    public BasicResponse getMissingCourseByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            List<MissingCourse> missingCourses = dao.getMissingCoursesByStudent(student.getId(), student.getPensum(), student.getCareer().getId());
            if(dao.getById(idStudent) != null){
                if(missingCourses.size() != 0){
                    basicResponse.setData(missingCourses);
                    basicResponse.setCode(StatusCode.OK);
                } else {
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("The student doesn't have this course");
                }
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student doesn't exist");
            }
        } catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getGroupsOfferedByStudent(long idCareer, int pensum, String module) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Career career = careerDAO.getById(idCareer);
            if(career == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Career not found");
            } else {
                List<GroupsOffered> groupsOffereds = dao.getGroupsOfferedByStudent(idCareer, pensum, module);
                if(groupsOffereds.size() == 0){
                    basicResponse.setMessage("List empty");
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                } else {
                    basicResponse.setUniqueResult(groupsOffereds);
                    basicResponse.setCode(StatusCode.OK);
                    return basicResponse;
                }
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
}
