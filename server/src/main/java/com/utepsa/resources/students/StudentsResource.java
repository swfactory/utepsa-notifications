package com.utepsa.resources.students;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.HistoryNotes;
import com.utepsa.models.Student;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by roberto on 12/7/2016.
 */
@Path(StudentsResource.SERVICE_PATH)
@Api(value = StudentsResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class StudentsResource {
    public static final String SERVICE_PATH = "student/";

    private final StudentsService service;

    @Inject
    public StudentsResource(StudentsService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}")
    @ApiOperation( value = "Get Student by id", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getStudentById(@ApiParam(value = "Student ID needed to find the student", required = true)
                                            @PathParam("idStudent") long idStudent) {
            return service.getById(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/notifications/{numberRow}")
    @ApiOperation( value = "Gets notifications of a Student by id", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notifications obtained correctly"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no notifications to display"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getNotificationsByStudent(
            @ApiParam(value = "Student ID needed to find the student", required = true)
            @PathParam("idStudent") long idStudent,
            @ApiParam(value = "Row number to get", required = true)
            @PathParam("numberRow") int numRow) {
        return service.getNotificationsByStudent(idStudent, numRow);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("{idStudent}/historyNotes")
    @ApiOperation( value = "Get the History Notes of a Student by id and chronological order or pensum order", response = HistoryNotes.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student history notes were successfully obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getHistoryNotesByStudentChronological(@ApiParam(value = "Student ID needed to find the student", required = true)
                                                      @PathParam("idStudent") long idStudent){
        return service.getHistoryNotesByStudent(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("{idStudent}/historyNotes/Pensum")
    @ApiOperation( value = "Get the History Notes of a Student by id and chronological order or pensum order", response = HistoryNotes.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student history notes were successfully obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getHistoryNotesByStudentAndPensum(@ApiParam(value = "Student ID needed to find the student", required = true)
                                                               @PathParam("idStudent") long idStudent){
        return service.getHistoryNotesByPensum(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/documents")
    @ApiOperation( value = "Find DocumentsStudent by register code", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student documents were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "This student does not have any documents registered"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "This order doesn't exist, please select: C or P"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getDocumentsStudent (@ApiParam(value = "Student ID needed to find the student", required = true)
                                                  @PathParam("idStudent") long idStudent) {
            return service.getDocumentsByStudent(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/files")
    @ApiOperation( value = "Gets files by student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student files were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "This student has no files to display"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getFilesByStudent (@ApiParam(value = "ID of Student that needs to be fetched", required = true)
                                                            @PathParam("idStudent") long idStudent) {
        return service.getFilesByStudent(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/search/")
    @ApiOperation( value = "Get student by register number or full name", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student files were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "Student does't exist"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "The search can only be done by a parameter"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse searchStudent (@ApiParam(value = "registerNumber of Student that needs to be fetched by register")
                                        @QueryParam("registerNumber") String registerNumber,
                                        @ApiParam(value = "Full Name of Student that needs to be fetched by name")
                                        @QueryParam("fullName") String fullName) {
        return service.getByFullName(fullName,registerNumber);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/CourseRegister")
    @ApiOperation( value = "Gets registered courses by student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student Student Course Register were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "This Current Semester has no Student Course Register to display"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Current Semester not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getCourseRegisterByStudent (@ApiParam(value = "ID of Student that needs to be fetched", required = true)
                                            @PathParam("idStudent") long idStudent) {
        return service.getCourseRegisterByStudent(idStudent);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("{idStudent}/sync")
    @ApiOperation( value = "sync datas by student", response = BasicResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = StatusCode.OK, message = "Student data updated."),
        @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "This data has no Student to display"),
        @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse  updateStudentData(@ApiParam(value = "id student of Student that needs to be fetched", required = true)
        @PathParam("idStudent") long  idStudent) {
            return service.updateStudentData(idStudent);
        }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/FinancialState")
    @ApiOperation( value = "Gets Financial state of student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Students' financial statements were obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getFinancialStateOfStudent (@ApiParam(value = "ID of Student that needs to be fetched", required = true)
                                                     @PathParam("idStudent") long idStudent) {
        return service.getFinancialStateOfStudent(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("{idStudent}/historyNotes/course/{idCourse}")
    @ApiOperation( value = "Get History notes by course", response = HistoryNotes.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student history notes were successfully obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student doesn't exist"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "The student doesn't have this course"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getAllHistoryNotesByCourse(@ApiParam(value = "Student ID needed to find the student", required = true)
                                                 @PathParam("idStudent") long idStudent,
                                                 @ApiParam(value = "Course ID needed to find the course", required = true)
                                                 @PathParam("idCourse") long idCourse) throws Exception {
        return service.getAllHistoryNotesByCourse(idStudent, idCourse);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/MissingCourse")
    @ApiOperation( value = "Get Missing Course By Student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "The student doesn't have this course"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getMissingCourseByStudent(@ApiParam(value = "Student ID needed to find the student", required = true)
                                        @PathParam("idStudent") long idStudent) {
        return service.getMissingCourseByStudent(idStudent);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idCareer}/{pensum}/GroupsOffered")
    @ApiOperation( value = "Get Missing Course By Student", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "The student doesn't have this course"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student doesn't exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getGroupsOfferedByStudent(@ApiParam(value = "Career ID needed to find the student", required = true)
                                                        @PathParam("idCareer") long idCareer,
                                                   @ApiParam(value = "Pensum ID needed to find the student", required = true)
                                                        @PathParam("pensum") int pensum,
                                                   @ApiParam(value = "Pensum ID needed to find the student", required = true)
                                                        @QueryParam("module") String module) {
        return service.getGroupsOfferedByStudent(idCareer, pensum, module);
    }
}
