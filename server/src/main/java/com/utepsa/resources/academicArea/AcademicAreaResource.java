package com.utepsa.resources.academicArea;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gerardo on 30/04/2017.
 */
@Path(AcademicAreaResource.SERVICE_PATH)
@Api(value = AcademicAreaResource.SERVICE_PATH, description = "Operations about academic area")
@Produces(MediaType.APPLICATION_JSON)
public class AcademicAreaResource {
    public static final String SERVICE_PATH = "academicArea/";

    private AcademicAreaService academicAreaService;
    @Inject
    public AcademicAreaResource(AcademicAreaService academicAreaService){
        this.academicAreaService = academicAreaService;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all Academic area", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Academic area found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no academic area to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getAll() {
        return academicAreaService.getAll();
    }
}
