package com.utepsa.resources.featuredStudent;

import com.google.inject.Inject;
import com.utepsa.api.custom.BestAveragePerSemester;
import com.utepsa.api.custom.StudentParExcellence;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.featuredStudent.FeaturedStudentDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Career;
import com.utepsa.models.HistoryNotes;

import java.util.List;

/**
 * Created by Gerardo on 11/08/2017.
 */
public class FeaturedStudentService {

    private final FeaturedStudentDAO featuredStudentDAO;
    private final HistoryNotesDAO historyNotesDAO;
    private final StudentDAO studentDAO;
    private final CareerDAO careerDAO;

    @Inject
    public FeaturedStudentService(FeaturedStudentDAO featuredStudentDAO, HistoryNotesDAO historyNotesDAO, StudentDAO studentDAO, CareerDAO careerDAO){
        this.featuredStudentDAO = featuredStudentDAO;
        this.historyNotesDAO = historyNotesDAO;
        this.studentDAO = studentDAO;
        this.careerDAO = careerDAO;
    }

    public BasicResponse getBestAverageByCareer(String year, String semester, long idCareer){
        BasicResponse basicResponse = new BasicResponse();
        try{
            String station = "";
            List<BestAveragePerSemester> bestAveragePerSemesters = null;
            Career career = careerDAO.getById(idCareer);
            if(career == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Career not found");
                return basicResponse;
            }

            if(station == "1" && year != null || !year.isEmpty()){
                station = "V";
                bestAveragePerSemesters  = featuredStudentDAO.getBestAverageStudentsBySemesterAndCareer(year, semester, idCareer, station);
            }

            if(station == "2" && year != null || !year.isEmpty()){
                station = "I";
                bestAveragePerSemesters = featuredStudentDAO.getBestAverageStudentsBySemesterAndCareer(year, semester, idCareer, station);
            }

            if(bestAveragePerSemesters.size() != 0 || bestAveragePerSemesters != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(bestAveragePerSemesters);
                basicResponse.setMessage("Report correctly execute");
            } else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No data to display");
            }
        } catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getStudentParExcellence(String semester, long idCareer){
        BasicResponse basicResponse = new BasicResponse();
        try{
            List<StudentParExcellence> studentParExcellence = featuredStudentDAO.getStudentParExcellence(semester, idCareer);
            if(studentParExcellence.size() != 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(studentParExcellence);
                basicResponse.setMessage("Report correctly execute");
            } else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No data to display");
            }
        } catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getHistoryNotesByStudentAndSemester(long idStudent, String semester) throws Exception{
        BasicResponse basicResponse = new BasicResponse();
        try{
            List<HistoryNotes> historyNotes = historyNotesDAO.getAllHistoryNotesByStudentAndSemester(idStudent, semester);

            if(studentDAO.getById(idStudent) == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("student not found");
            }

            if(historyNotes.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("List empty");
            }

            if(historyNotes.size() > 0 && studentDAO.getById(idStudent) != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(historyNotes);
            }

        } catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }

        return basicResponse;
    }
}
