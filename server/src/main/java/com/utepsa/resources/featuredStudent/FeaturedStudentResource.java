package com.utepsa.resources.featuredStudent;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gerardo on 11/08/2017.
 */
@Path(FeaturedStudentResource.SERVICE_PATH)
@Api(value = FeaturedStudentResource.SERVICE_PATH, description = "Operations about ExecutiveReport")
@Produces(MediaType.APPLICATION_JSON)
public class FeaturedStudentResource {

    public static final String SERVICE_PATH = "FeaturedStudent/";
    private final FeaturedStudentService service;

    @Inject
    public FeaturedStudentResource(FeaturedStudentService service){
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/BestAverage/{idCareer}")
    @ApiOperation( value = "Get the report of the best semester averages per career")
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Report correctly execute"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Conflict to execute report"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getQuantityOfStudent(@ApiParam(value = "semester needed to find the quantity student", required = true)
                                                @QueryParam("year") String year,
                                              @ApiParam(value = "idCareer needed to find the quantity student", required = true)
                                                @QueryParam("semester") String semester,
                                              @ApiParam(value = "idCareer needed to find the quantity student", required = true)
                                                  @PathParam("idCareer") long idCareer) {
        return service.getBestAverageByCareer(year, semester, idCareer);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/StudentsParExcellence")
    @ApiOperation( value = "Get the report of the best semester averages per career")
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Report correctly execute"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Conflict to execute report"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getStudentsParExcellence(@ApiParam(value = "semester needed to find the quantity student", required = true)
                                              @QueryParam("semester")String semester,
                                              @ApiParam(value = "idCareer needed to find the quantity student", required = true)
                                              @QueryParam("idCareer") long idCareer) {
        return service.getStudentParExcellence(semester, idCareer);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/historyNotes")
    @ApiOperation(value = "Get the history notes of the student by semester")
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "History notes correctly execute"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "student not found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "list empty"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getHistoryNotesByStudentAndSemester(@ApiParam(value = "semester needed to find the quantity student", required = true)
                                                                 @PathParam("idStudent") long idStudent,
                                                             @ApiParam(value = "semester needed to find the quantity student", required = true)
                                                                @QueryParam("semester") String semester) throws Exception {
        return service.getHistoryNotesByStudentAndSemester(idStudent, semester);
    }
}
