package com.utepsa.resources.currentSemester;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 13/03/2017.
 */
@Path("/CurrentSemester")
@Api(value = "Current Semester", description = "Operations about Current Semester")
@Produces(MediaType.APPLICATION_JSON)
public class CurrentSemesterResource {
    private final CurrentSemesterService service;

    @Inject
    public CurrentSemesterResource(CurrentSemesterService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get Current Semester", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Current Semester files were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "This current semester has no files to display"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Current Semester not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getCurrentSemester () {
        return service.getCurrentSemester();
    }
}
