package com.utepsa.resources.virtualBooks;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.areaOfVirtualBooks.AreaOfVirtualBooksDAO;
import com.utepsa.db.virtualBooks.VirtualBooksDAO;
import com.utepsa.models.VirtualBooks;

import java.util.List;

/**
 * Created by Gerardo on 20/09/2017.
 */
public class VirtualBooksService {
    private AreaOfVirtualBooksDAO areaOfVirtualBooksDAO;
    private VirtualBooksDAO virtualBooksDAO;

    @Inject
    public VirtualBooksService(AreaOfVirtualBooksDAO areaOfVirtualBooksDAO, VirtualBooksDAO virtualBooksDAO){
        this.areaOfVirtualBooksDAO = areaOfVirtualBooksDAO;
        this.virtualBooksDAO = virtualBooksDAO;
    }

    public BasicResponse create(VirtualBooks virtualBooks) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(areaOfVirtualBooksDAO.getById(virtualBooks.getAreaOfVirtualBooks().getId()) != null){
                virtualBooksDAO.create(virtualBooks);
                basicResponse.setCode(StatusCode.CREATED);
                basicResponse.setMessage("Virtual Book created correctly");
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Area not exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getById(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            VirtualBooks virtualBook = virtualBooksDAO.getById(id);
            if(virtualBook != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(virtualBook);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This virtual book was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAll() {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<VirtualBooks> virtualBook = virtualBooksDAO.getAll();
            if(virtualBook != null && virtualBook.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(virtualBook);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No virtual books are available");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse searchBook(String fullName) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<VirtualBooks> virtualBooks = virtualBooksDAO.searchBooks(fullName);
            if(virtualBooks != null && virtualBooks.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(virtualBooks);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No virtual books are available");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
