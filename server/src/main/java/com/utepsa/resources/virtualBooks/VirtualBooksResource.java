package com.utepsa.resources.virtualBooks;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.VirtualBooks;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gerardo on 20/09/2017.
 */
@Path(VirtualBooksResource.SERVICE_PATH)
@Api(value = VirtualBooksResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class VirtualBooksResource {
    public static final String SERVICE_PATH = "virtualBooks/";

    private final VirtualBooksService service;

    @Inject
    public VirtualBooksResource(VirtualBooksService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation(value = "Gets all available Virtual Books", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "List of Books found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No files are available")})
    public BasicResponse getAll() {
        return service.getAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Gets the Virtual Book by id", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Book found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Book not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getFilesById(@ApiParam(value = "Book ID needed to find the book", required = true)
                                      @PathParam("id") LongParam id) {
        return service.getById(id.get());
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a virtual book", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The book has been created"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse create(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) VirtualBooks virtualBooks) {
        return service.create(virtualBooks);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/search/")
    @ApiOperation( value = "Get books by title, author, description and content", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Books were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "book does't exist"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "The search can only be done by a parameter"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse searchBook (@ApiParam(value = "Full Name of book that needs to be fetched by name")
                                        @QueryParam("fullName") String fullName) {
        return service.searchBook(fullName);
    }
}
