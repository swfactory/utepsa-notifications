package com.utepsa.resources.resetPassword;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Credential;
import com.utepsa.models.Student;

/**
 * Created by Gerardo on 11/07/2017.
 */
public class ResetPasswordService {

    private final CredentialDAO credentialDAO;
    private final StudentDAO studentDAO;

    @Inject
    public ResetPasswordService(CredentialDAO credentialDAO, StudentDAO studentDAO){
        this.credentialDAO = credentialDAO;
        this.studentDAO = studentDAO;
    }

    public BasicResponse resetPassword(long idStudent) throws Exception {
        BasicResponse basicResponse = new BasicResponse();
        Student student = studentDAO.getById(idStudent);
        if(student == null){
            basicResponse.setCode(StatusCode.NO_CONTENT);
            basicResponse.setMessage("Student not exist");
            return basicResponse;
        }

        Credential credential = credentialDAO.getByIdStudent(student);
        if(credential == null){
            basicResponse.setCode(StatusCode.NO_CONTENT);
            basicResponse.setMessage("Credential not exist");
            return basicResponse;
        } else {
            credential.setGcmId(null);
            credential.setLastConnection(null);
            credential.setChangePasswordForced(true);
            if(student.getDocumentNumber().isEmpty()){
                String formatted = Operations.FormattedStringForRemoveZero(student.getRegisterNumber().trim());
                credential.setPassword(Operations.encryptPassword(formatted));
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage(formatted);
            } else {
                credential.setPassword(Operations.encryptPassword(student.getDocumentNumber().trim()));
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage(student.getDocumentNumber().trim());
            }
            credentialDAO.resetPasswordOfCredential(credential);

            return basicResponse;
        }
    }
}
