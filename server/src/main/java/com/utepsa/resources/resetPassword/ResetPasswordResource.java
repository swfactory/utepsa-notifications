package com.utepsa.resources.resetPassword;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gerardo on 11/07/2017.
 */
@Path(ResetPasswordResource.SERVICE_PATH)
@Api(value = ResetPasswordResource.SERVICE_PATH, description = "Operations by reset password")
@Produces(MediaType.APPLICATION_JSON)
public class ResetPasswordResource {
    public static final String SERVICE_PATH = "resetPassword/";

    private final ResetPasswordService service;

    @Inject
    public ResetPasswordResource(ResetPasswordService service){
        this.service = service;
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("{idStudent}/reset")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Modifies the profile of an administrator's credentials.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The administrator credential profile was modified"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse resetPassword(
            @ApiParam(value = "ID Student", required = true) @PathParam("idStudent") long idStudent) throws Exception {
        return service.resetPassword(idStudent);
    }
}
