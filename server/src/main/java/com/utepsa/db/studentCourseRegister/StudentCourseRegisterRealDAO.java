package com.utepsa.db.studentCourseRegister;

import com.google.inject.Inject;
import com.utepsa.models.StudentCourseRegister;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
public class StudentCourseRegisterRealDAO extends AbstractDAO<StudentCourseRegister> implements StudentCourseRegisterDAO {
    @Inject
    public StudentCourseRegisterRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(StudentCourseRegister studentCourseRegister) throws Exception {
        return persist(studentCourseRegister).getId();
    }

    @Override
    public List<StudentCourseRegister> getCourseRegisterByStudent(long idStudent, String semester)throws Exception {
        return list(namedQuery("com.utepsa.models.StudentCourseRegister.getCourseRegisterByStudent").setParameter("semester", semester).setParameter("idStudent", idStudent));
    }

    @Override
    public List<StudentCourseRegister> getAllCourseRegisterByStudent(long idStudent, String semester) throws Exception {
        return list(namedQuery("com.utepsa.models.StudentCourseRegister.getAllCourseRegisterByStudent").setParameter("semester", semester).setParameter("idStudent", idStudent));
    }

    @Override
    public StudentCourseRegister getCourseRegisterById(long id) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.StudentCourseRegister.getCourseRegisterById").setParameter("id", id));

    }

    @Override
    public void deleteAllByStudentSemester(long idStudent, String semester) throws Exception {
        namedQuery("com.utepsa.models.StudentCourseRegister.deleteAllByStudent").setParameter("idStudent", idStudent).setParameter("semester", semester).executeUpdate();
    }
}
