package com.utepsa.db.virtualBooks;

import com.google.inject.Inject;
import com.utepsa.models.VirtualBooks;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 20/09/2017.
 */
public class VirtualBooksRealDAO extends AbstractDAO<VirtualBooks> implements VirtualBooksDAO {

    @Inject
    public VirtualBooksRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(VirtualBooks virtualBooks) throws Exception {
        final VirtualBooks created = persist(virtualBooks);
        if(created == null) throw new Exception("The Virtual Book was not created.");
        return created.getId();
    }

    @Override
    public VirtualBooks getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<VirtualBooks> getAll() throws Exception {
        return list(namedQuery("com.utepsa.models.VirtualBooks.getAll"));
    }

    @Override
    public List<VirtualBooks> searchBooks(String query) throws Exception {
        query = query.replace(" ", "%");

        return currentSession().createSQLQuery("SELECT v.\"id\", v.name, v.author, v.description, v.id_area, v.url " +
                    "FROM virtual_books As v INNER JOIN area_of_virtual_books As av on v.id_area = av.id " +
                    "WHERE (v.author || v.name || v.description || av.name) ILIKE '%" + query + "%' ORDER BY v.name ASC").addEntity(VirtualBooks.class).list();
    }
}
