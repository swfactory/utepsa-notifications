package com.utepsa.db.virtualBooks;

import com.utepsa.models.VirtualBooks;

import java.util.List;

/**
 * Created by Gerardo on 20/09/2017.
 */
public interface VirtualBooksDAO {
    long create(VirtualBooks virtualBooks) throws Exception;
    VirtualBooks getById(long id) throws Exception;
    List<VirtualBooks> getAll() throws Exception;
    List<VirtualBooks> searchBooks(String query) throws Exception;
}
