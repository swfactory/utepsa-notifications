package com.utepsa.db.groupsOffered;

import com.google.inject.Inject;
import com.utepsa.models.Course;
import com.utepsa.models.GroupsOffered;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * Created by Gerardo on 25/04/2017.
 */
public class GroupsOfferedRealDAO extends AbstractDAO<GroupsOffered> implements GroupsOfferedDAO {

    @Inject
    public GroupsOfferedRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(GroupsOffered groupsOffered) throws Exception{
        final GroupsOffered created = persist(groupsOffered);
        if(created == null) throw new Exception("Groups Offered was not created.");
        return created.getId();
    }

    @Override
    public void update(GroupsOffered groupsOffered) throws Exception{
        GroupsOffered groupsOffereds = get(groupsOffered.getId());
        persist(groupsOffereds);
    }

    @Override
    public GroupsOffered getBySemesterCourseGroupAndModule(String semester, Course course, String group, String module) throws Exception{
        return uniqueResult(namedQuery("com.utepsa.models.GroupsOffered.getBySemesterCourseGroupAndModule").setParameter("semester", semester).setParameter("course", course).setParameter("group", group).setParameter("module", module));
    }

    @Override
    public List<GroupsOffered> getByCareerAndSemester (long idCareer, int pensum, String semester) throws Exception {
        return currentSession().createSQLQuery(String.format("SELECT gro.*\n" +
                " FROM groups_offered gro\n" +
                " INNER JOIN (SELECT cc.id_course FROM career_course cc WHERE cc.id_career = '%d' and cc.pensum = '%d') cr ON cr.id_course = gro.id_course\n" +
                " WHERE gro.semester = '%s'", idCareer, pensum, semester)).addEntity(GroupsOffered.class).list();
    }

    @Override
    public List<GroupsOffered> getByCareerSemesterAndModule (long idCareer, int pensum, String semester, String module) throws Exception {
        return currentSession().createSQLQuery(String.format("SELECT gro.*\n" +
                " FROM groups_offered gro\n" +
                " INNER JOIN (SELECT cc.id_course FROM career_course cc WHERE cc.id_career = '%d' and cc.pensum = '%d') cr ON cr.id_course = gro.id_course\n" +
                " WHERE gro.semester = '%s' AND gro.module = '%s'", idCareer, pensum, semester, module)).addEntity(GroupsOffered.class).list();
    }

    @Override
    public List<GroupsOffered> getByAcademicArea(long idAcademicArea, String semester) throws Exception{
        return currentSession().createSQLQuery(String.format("SELECT gro.* FROM groups_offered gro\n" +
                " INNER JOIN (SELECT c.id FROM course c WHERE c.id_academic_area = '%d' ) cr on cr.id = gro.id_course\n" +
                " WHERE gro.semester = '%s'", idAcademicArea, semester)).addEntity(GroupsOffered.class).list();
    }

    @Override
    public List<GroupsOffered> getByAcademicAreaAndSemester(long idAcademicArea, String semester, String module) throws Exception{
        return currentSession().createSQLQuery(String.format("SELECT gro.* FROM groups_offered gro\n" +
                " INNER JOIN (SELECT cc.id FROM course cc WHERE cc.id_academic_area = '%d' ) cr on cr.id = gro.id_course\n" +
                " WHERE gro.semester = '%s' AND gro.module = '%s'", idAcademicArea, semester, module)).addEntity(GroupsOffered.class).list();
    }

    @Override
    public List<GroupsOffered> getByCourseAndSemester (Course course, String semester) throws Exception{
        return list(namedQuery("com.utepsa.models.GroupsOffered.getByCourseAndSemester")
                .setParameter("course", course).setParameter("semester", semester));
    }
}
