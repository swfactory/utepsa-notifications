package com.utepsa.db.groupsOffered;

import com.utepsa.models.Course;
import com.utepsa.models.GroupsOffered;

import java.util.List;

/**
 * Created by Gerardo on 25/04/2017.
 */
public interface GroupsOfferedDAO {
    long create(GroupsOffered groupsOffered) throws Exception;
    void update(GroupsOffered groupsOffered) throws Exception;
    GroupsOffered getBySemesterCourseGroupAndModule(String semester, Course idCourse, String groupCourse, String module) throws Exception;
    List<GroupsOffered> getByCareerAndSemester (long idCareer, int pensum, String semester) throws Exception;
    List<GroupsOffered> getByCareerSemesterAndModule (long idCareer, int pensum, String semester, String module) throws Exception;
    List<GroupsOffered> getByAcademicArea(long idAcademicArea, String semester) throws Exception;
    List<GroupsOffered> getByAcademicAreaAndSemester(long idAcademicArea, String semester, String module) throws Exception;
    List<GroupsOffered> getByCourseAndSemester (Course course, String semester) throws Exception;
}
