package com.utepsa.db.documentStudent;

import com.google.inject.Inject;
import com.utepsa.models.DocumentStudent;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentStudentRealDAO extends AbstractDAO<DocumentStudent> implements DocumentStudentDAO {

    @Inject
    public DocumentStudentRealDAO(SessionFactory factory) {super (factory);}

    @Override
    public boolean create(DocumentStudent documentsStudent) throws Exception {
        final DocumentStudent created = persist(documentsStudent);
        if (created == null) throw new Exception("The documentStudent was not created.");

        return  true;
    }

    @Override
    public List<DocumentStudent> getByStudent(long idStudent) {
        return list(namedQuery("com.utepsa.models.DocumentStudent.getDocumentStudentById").setParameter("idStudent", idStudent));
    }
}
