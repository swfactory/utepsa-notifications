package com.utepsa.db.academicEvent;
import com.google.inject.Inject;
import com.utepsa.models.AcademicEvent;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by leonardo on 21-07-17.
 */
public class AcademicEventRealDAO extends AbstractDAO<AcademicEvent> implements AcademicEventDAO {

    @Inject
    public AcademicEventRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public AcademicEvent getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public long create(AcademicEvent academicEvent) throws Exception{
        return persist(academicEvent).getId();
    }

    @Override
    public List<AcademicEvent> getByCreator(long idCreator) throws Exception{
        return currentSession().createSQLQuery(String.format("SELECT id,id_creator,id_event_type,name,description,date,duration,hour, students_quantity FROM academic_event where academic_event.id_creator = %d", idCreator)).addEntity(AcademicEvent.class).list();
    }

    @Override
    public List<AcademicEvent> getByCareer(long idCareer, String year) throws Exception{

        return currentSession().createSQLQuery("SELECT academic_event.\"id\",id_creator,id_event_type,academic_event.name,description,date,duration,hour, students_quantity " +
                "FROM academic_event, event_career, career where event_career.id_career = '"+idCareer+"' AND academic_event.date LIKE '%"+year+"%' " +
                "GROUP BY academic_event.id,id_creator,id_event_type,academic_event.name, description, date, duration, hour, students_quantity").addEntity(AcademicEvent.class).list();
    }

    @Override
    public List<AcademicEvent> getByCourse (long idCourse, String year) throws Exception{
        return currentSession().createSQLQuery("SELECT academic_event.id,id_creator,id_event_type,academic_event.name,description,date,duration,hour, students_quantity " +
                "FROM academic_event, event_course,career where event_course.id_course = '"+idCourse+"' and academic_event.\"date\" like '%"+year+"%' " +
                "group by academic_event.id,id_creator,id_event_type,academic_event.name,description,date,duration,hour, students_quantity").addEntity(AcademicEvent.class).list();
    }

    @Override
    public List<AcademicEvent> getAll () throws Exception{
        return  currentSession().createCriteria(AcademicEvent.class).list();
    }
}
