package com.utepsa.db.academicEvent;
import com.utepsa.models.AcademicEvent;

import java.util.List;

/**
 * Created by leonardo on 21-07-17.
 */
public interface AcademicEventDAO {
    AcademicEvent getById(long id) throws Exception;
    long create(AcademicEvent academicEvent) throws Exception;
    List<AcademicEvent> getByCreator(long id) throws Exception;
    List<AcademicEvent> getByCareer(long id, String year) throws Exception;
    List<AcademicEvent> getByCourse (long id, String year) throws Exception;
    List<AcademicEvent> getAll () throws Exception;
}
