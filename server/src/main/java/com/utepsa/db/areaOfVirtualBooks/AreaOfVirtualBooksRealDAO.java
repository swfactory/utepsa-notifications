package com.utepsa.db.areaOfVirtualBooks;

import com.google.inject.Inject;
import com.utepsa.models.AreaOfVirtualBooks;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 20/09/2017.
 */
public class AreaOfVirtualBooksRealDAO extends AbstractDAO<AreaOfVirtualBooks> implements AreaOfVirtualBooksDAO {

    @Inject
    public AreaOfVirtualBooksRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(AreaOfVirtualBooks areaOfVirtualBooks) throws Exception {
        final AreaOfVirtualBooks created = persist(areaOfVirtualBooks);
        if(created == null) throw new Exception("The area of virtual books was not created.");
        return created.getId();
    }

    @Override
    public AreaOfVirtualBooks getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<AreaOfVirtualBooks> getAll() throws Exception {
        return list(namedQuery("com.utepsa.models.AreaOfVirtualBooks.getAll"));
    }
}
