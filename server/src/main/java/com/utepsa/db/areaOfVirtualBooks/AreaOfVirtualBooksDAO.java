package com.utepsa.db.areaOfVirtualBooks;

import com.utepsa.models.AreaOfVirtualBooks;

import java.util.List;

/**
 * Created by Gerardo on 20/09/2017.
 */
public interface AreaOfVirtualBooksDAO {
    long create(AreaOfVirtualBooks areaOfVirtualBooks) throws Exception;
    AreaOfVirtualBooks getById(long id) throws Exception;
    List<AreaOfVirtualBooks> getAll() throws Exception;
}
