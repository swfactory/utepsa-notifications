package com.utepsa.db.financialState;

import com.utepsa.models.FinancialState;
import com.utepsa.models.Student;
import java.util.List;

/**
 * Created by Gerardo on 29/03/2017.
 */
public interface FinancialStateDAO {
    long create(FinancialState financialState) throws Exception;
    FinancialState getByIdStudent(Student idStudent) throws Exception;
    void updateFinancialState(FinancialState financialState) throws Exception;
    List<FinancialState> getAllFinancialStateWithFairValue()throws Exception;
}
