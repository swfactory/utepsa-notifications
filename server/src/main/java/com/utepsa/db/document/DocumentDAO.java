package com.utepsa.db.document;

import com.utepsa.models.Document;

import java.util.List;

/**
 * Created by David on 22/01/2017.
 */
public interface DocumentDAO {
    long create(Document document) throws Exception;
    Document getById(long id);
    Document getByCodeUtepsa(String codeUtepsa) throws Exception;
}
