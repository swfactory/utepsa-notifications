package com.utepsa.db.document;

import com.google.inject.Inject;
import com.utepsa.models.Document;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by David on 22/01/2017.
 */
public class DocumentRealDAO extends AbstractDAO<Document> implements DocumentDAO{
    @Inject
    public DocumentRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Document document) throws Exception {
        final Document created = persist(document);
        if(created == null) throw new Exception("The career was not created.");
        return created.getId();
    }

    @Override
    public Document getById(long id) {
        return get(id);
    }

    @Override
    public Document getByCodeUtepsa(String codeUtepsa) throws Exception {
        return (Document) currentSession().createCriteria(Document.class)
                .add(Restrictions.eq("codeUtepsa", codeUtepsa))
                .uniqueResult();
    }
}
