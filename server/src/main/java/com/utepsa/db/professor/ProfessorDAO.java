package com.utepsa.db.professor;

import com.utepsa.models.Professor;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
public interface ProfessorDAO {

    Professor getProfessorByAgendCode(String agendCode);
    long create(Professor professor) throws Exception;
}
