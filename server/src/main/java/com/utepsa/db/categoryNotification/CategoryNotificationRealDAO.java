package com.utepsa.db.categoryNotification;

import com.google.inject.Inject;
import com.utepsa.models.CategoryNotification;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Luana Chavez on 11/04/2017.
 */
public class CategoryNotificationRealDAO extends AbstractDAO<CategoryNotification> implements CategoryNotificationDAO {
    @Inject
    public CategoryNotificationRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public CategoryNotification getById(long id) throws Exception {
        return get(id);
    }
}
