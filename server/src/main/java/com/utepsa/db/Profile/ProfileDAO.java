package com.utepsa.db.Profile;

import com.utepsa.models.Profile;
import java.util.List;

/**
 * Created by Gerardo on 02/02/2017.
 */
public interface ProfileDAO {
    long create(Profile profile) throws Exception;
    void modify (Profile profile) throws Exception;
    List<Profile> getAll() throws Exception;
    Profile getById(long id) throws Exception;
}
