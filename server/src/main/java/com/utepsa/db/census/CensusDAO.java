package com.utepsa.db.census;

import com.utepsa.api.custom.census.Answers;

/**
 * Created by Luana Chavez on 04/10/2017.
 */
public interface CensusDAO {
    Answers generalReport();
    Answers careerReport(long idCareer);
}
