package com.utepsa.db.census;

import com.google.inject.Inject;
import com.utepsa.api.custom.census.Answers;
import com.utepsa.api.custom.census.FemenineAnswers;
import com.utepsa.api.custom.census.MasculineAnswers;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.List;

/**
 * Created by Luana Chavez on 04/10/2017.
 */
public class CensusRealDAO extends AbstractDAO<Answers> implements CensusDAO{

    @Inject
    public CensusRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Answers generalReport() {
        Answers generalAnswers = new Answers();
        SQLQuery femenineIsWorking = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'F' AND UPPER(is_working) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(is_working) = 'NO'");

        femenineIsWorking.addScalar("quantity", LongType.INSTANCE);
        femenineIsWorking.addScalar("response", StringType.INSTANCE);

        femenineIsWorking.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineIsWorkingCustoms = femenineIsWorking.list();

        SQLQuery masculineIsWorking = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'M' AND UPPER(is_working) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(is_working) = 'NO'");

        masculineIsWorking.addScalar("quantity", LongType.INSTANCE);
        masculineIsWorking.addScalar("response", StringType.INSTANCE);

        masculineIsWorking.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineIsWorkingCustoms = masculineIsWorking.list();

        SQLQuery femenineSuggestionsMet = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where suggestions_met = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where suggestions_met = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where suggestions_met = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where suggestions_met = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where suggestions_met = '5' and gender = 'F'");

        femenineSuggestionsMet.addScalar("quantity", LongType.INSTANCE);
        femenineSuggestionsMet.addScalar("response", StringType.INSTANCE);

        femenineSuggestionsMet.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSuggestionsMetCustoms = femenineSuggestionsMet.list();

        SQLQuery masculineSuggestionsMet = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where suggestions_met = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where suggestions_met = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where suggestions_met = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where suggestions_met = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where suggestions_met = '5' and gender = 'M'");

        masculineSuggestionsMet.addScalar("quantity", LongType.INSTANCE);
        masculineSuggestionsMet.addScalar("response", StringType.INSTANCE);

        masculineSuggestionsMet.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSuggestionsMetCustoms = masculineSuggestionsMet.list();

        SQLQuery masculineCourseProgramming = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where course_programming = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where course_programming = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where course_programming = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where course_programming = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where course_programming = '5' and gender = 'M'");

        masculineCourseProgramming.addScalar("quantity", LongType.INSTANCE);
        masculineCourseProgramming.addScalar("response", StringType.INSTANCE);

        masculineCourseProgramming.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCourseProgrammingCustoms = masculineCourseProgramming.list();

        SQLQuery femenineCourseProgramming = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where course_programming = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where course_programming = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where course_programming = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where course_programming = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where course_programming = '5' and gender = 'F'");

        femenineCourseProgramming.addScalar("quantity", LongType.INSTANCE);
        femenineCourseProgramming.addScalar("response", StringType.INSTANCE);

        femenineCourseProgramming.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCourseProgrammingCustoms = femenineCourseProgramming.list();

        SQLQuery femenineAnotherLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(another_language) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student WHERE gender = 'F' AND UPPER(another_language) = 'SI'");

        femenineAnotherLanguage.addScalar("quantity", LongType.INSTANCE);
        femenineAnotherLanguage.addScalar("response", StringType.INSTANCE);

        femenineAnotherLanguage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAnotherLanguageCustoms = femenineAnotherLanguage.list();

        SQLQuery masculineAnotherLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(another_language) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student WHERE gender = 'M' AND UPPER(another_language) = 'SI'");

        masculineAnotherLanguage.addScalar("quantity", LongType.INSTANCE);
        masculineAnotherLanguage.addScalar("response", StringType.INSTANCE);

        masculineAnotherLanguage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAnotherLanguageCustoms = masculineAnotherLanguage.list();

        SQLQuery masculineBilingualSchool = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(bilingual_school) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student WHERE gender = 'M' AND UPPER(bilingual_school) = 'SI'");

        masculineBilingualSchool.addScalar("quantity", LongType.INSTANCE);
        masculineBilingualSchool.addScalar("response", StringType.INSTANCE);

        masculineBilingualSchool.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineBilingualSchoolCustoms = masculineBilingualSchool.list();

        SQLQuery femenineBilingualSchool = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(bilingual_school) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student WHERE gender = 'F' AND UPPER(bilingual_school) = 'SI'");

        femenineBilingualSchool.addScalar("quantity", LongType.INSTANCE);
        femenineBilingualSchool.addScalar("response", StringType.INSTANCE);

        femenineBilingualSchool.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineBilingualSchoolCustoms = femenineBilingualSchool.list();

        SQLQuery femenineAccesCourseNote = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where access_course_note = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where access_course_note = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where access_course_note = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where access_course_note = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where access_course_note = '5' and gender = 'F'");

        femenineAccesCourseNote.addScalar("quantity", LongType.INSTANCE);
        femenineAccesCourseNote.addScalar("response", StringType.INSTANCE);

        femenineAccesCourseNote.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAccessCourseNoteCustoms = femenineAccesCourseNote.list();

        SQLQuery masculineAccesCourseNote = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where access_course_note = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where access_course_note = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where access_course_note = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where access_course_note = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where access_course_note = '5' and gender = 'M'");

        masculineAccesCourseNote.addScalar("quantity", LongType.INSTANCE);
        masculineAccesCourseNote.addScalar("response", StringType.INSTANCE);

        masculineAccesCourseNote.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAccessCourseNoteCustoms = masculineAccesCourseNote.list();

        SQLQuery masculineClassrooms = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where classrooms = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where classrooms = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where classrooms = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where classrooms = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where classrooms = '5' and gender = 'M'");

        masculineClassrooms.addScalar("quantity", LongType.INSTANCE);
        masculineClassrooms.addScalar("response", StringType.INSTANCE);

        masculineClassrooms.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineClassroomsCustoms = masculineClassrooms.list();

        SQLQuery femenineClassrooms = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where classrooms = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where classrooms = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where classrooms = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where classrooms = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where classrooms = '5' and gender = 'F'");

        femenineClassrooms.addScalar("quantity", LongType.INSTANCE);
        femenineClassrooms.addScalar("response", StringType.INSTANCE);

        femenineClassrooms.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineClassroomsCustoms = femenineClassrooms.list();

        SQLQuery femenineLaboratories = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where laboratories = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories = '5' and gender = 'F'");

        femenineLaboratories.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratories.addScalar("response", StringType.INSTANCE);

        femenineLaboratories.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesCustoms = femenineLaboratories.list();

        SQLQuery masculineLaboratories = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where laboratories = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories = '5' and gender = 'M'");

        masculineLaboratories.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratories.addScalar("response", StringType.INSTANCE);

        masculineLaboratories.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesCustoms = masculineLaboratories.list();

        SQLQuery masculineLaboratoriesAssignments = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where laboratories_assignment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories_assignment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories_assignment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories_assignment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories_assignment = '5' and gender = 'M'");

        masculineLaboratoriesAssignments.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratoriesAssignments.addScalar("response", StringType.INSTANCE);

        masculineLaboratoriesAssignments.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesAssignmentsCustoms = masculineLaboratoriesAssignments.list();

        SQLQuery femenineLaboratoriesAssignments = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where laboratories_assignment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories_assignment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories_assignment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories_assignment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories_assignment = '5' and gender = 'F'");

        femenineLaboratoriesAssignments.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratoriesAssignments.addScalar("response", StringType.INSTANCE);

        femenineLaboratoriesAssignments.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesAssignmentsCustoms = femenineLaboratoriesAssignments.list();

        SQLQuery femenineLibraryService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where library_service = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where library_service = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where library_service = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where library_service = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where library_service = '5' and gender = 'F'");

        femenineLibraryService.addScalar("quantity", LongType.INSTANCE);
        femenineLibraryService.addScalar("response", StringType.INSTANCE);

        femenineLibraryService.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLibraryServiceCustoms = femenineLibraryService.list();

        SQLQuery masculineLibraryService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where library_service = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where library_service = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where library_service = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where library_service = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where library_service = '5' and gender = 'M'");

        masculineLibraryService.addScalar("quantity", LongType.INSTANCE);
        masculineLibraryService.addScalar("response", StringType.INSTANCE);

        masculineLibraryService.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLibraryServiceCustoms = masculineLibraryService.list();

        SQLQuery masculineTreatmentPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where treatment_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where treatment_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where treatment_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where treatment_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where treatment_platform_staff = '5' and gender = 'M'");

        masculineTreatmentPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineTreatmentPlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineTreatmentPlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineTreatmentPlatformStaffCustoms = masculineTreatmentPlatformStaff.list();

        SQLQuery femenineTreatmentPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where treatment_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where treatment_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where treatment_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where treatment_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where treatment_platform_staff = '5' and gender = 'F'");

        femenineTreatmentPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineTreatmentPlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineTreatmentPlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineTreatmentPlatformStaffCustoms = femenineTreatmentPlatformStaff.list();

        SQLQuery femenineQuickAttentionPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where quick_attention_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where quick_attention_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where quick_attention_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where quick_attention_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where quick_attention_platform_staff = '5' and gender = 'F'");

        femenineQuickAttentionPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineQuickAttentionPlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineQuickAttentionPlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineQuickAttentionPlatformStaffCustoms = femenineQuickAttentionPlatformStaff.list();

        SQLQuery masculineQuickAttentionPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where quick_attention_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where quick_attention_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where quick_attention_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where quick_attention_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where quick_attention_platform_staff = '5' and gender = 'M'");

        masculineQuickAttentionPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineQuickAttentionPlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineQuickAttentionPlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineQuickAttentionPlatformStaffCustoms = masculineQuickAttentionPlatformStaff.list();

        SQLQuery masculineKnowledgePlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_platform_staff = '5' and gender = 'M'");

        masculineKnowledgePlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgePlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineKnowledgePlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgePlatformStaffCustoms = masculineKnowledgePlatformStaff.list();

        SQLQuery femenineKnowledgePlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_platform_staff = '5' and gender = 'F'");

        femenineKnowledgePlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgePlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineKnowledgePlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgePlatformStaffCustoms = femenineKnowledgePlatformStaff.list();

        SQLQuery femeninePlatformStaffResponse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where platform_staff_response = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where platform_staff_response = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where platform_staff_response = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where platform_staff_response = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where platform_staff_response = '5' and gender = 'F'");

        femeninePlatformStaffResponse.addScalar("quantity", LongType.INSTANCE);
        femeninePlatformStaffResponse.addScalar("response", StringType.INSTANCE);

        femeninePlatformStaffResponse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePlatformStaffResponseCustoms = femeninePlatformStaffResponse.list();

        SQLQuery masculinePlatformStaffResponse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where platform_staff_response = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where platform_staff_response = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where platform_staff_response = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where platform_staff_response = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where platform_staff_response = '5' and gender = 'M'");

        masculinePlatformStaffResponse.addScalar("quantity", LongType.INSTANCE);
        masculinePlatformStaffResponse.addScalar("response", StringType.INSTANCE);

        masculinePlatformStaffResponse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePlatformStaffResponseCustoms = masculinePlatformStaffResponse.list();

        SQLQuery masculineCareerInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where career_information = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where career_information = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where career_information = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where career_information = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where career_information = '5' and gender = 'M'");

        masculineCareerInformation.addScalar("quantity", LongType.INSTANCE);
        masculineCareerInformation.addScalar("response", StringType.INSTANCE);

        masculineCareerInformation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCareerInformationCustoms = masculineCareerInformation.list();

        SQLQuery femenineCareerInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where career_information = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where career_information = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where career_information = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where career_information = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where career_information = '5' and gender = 'F'");

        femenineCareerInformation.addScalar("quantity", LongType.INSTANCE);
        femenineCareerInformation.addScalar("response", StringType.INSTANCE);

        femenineCareerInformation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCareerInformationCustoms = femenineCareerInformation.list();

        SQLQuery femeninePaymentInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where payment_information = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where payment_information = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where payment_information = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where payment_information = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where payment_information = '5' and gender = 'F'");

        femeninePaymentInformation.addScalar("quantity", LongType.INSTANCE);
        femeninePaymentInformation.addScalar("response", StringType.INSTANCE);

        femeninePaymentInformation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePaymentInformationCustoms = femeninePaymentInformation.list();

        SQLQuery masculinePaymentInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where payment_information = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where payment_information = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where payment_information = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where payment_information = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where payment_information = '5' and gender = 'M'");

        masculinePaymentInformation.addScalar("quantity", LongType.INSTANCE);
        masculinePaymentInformation.addScalar("response", StringType.INSTANCE);

        masculinePaymentInformation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePaymentInformationCustoms = masculinePaymentInformation.list();

        SQLQuery masculinePunctualityClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where punctuality_classes = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where punctuality_classes = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where punctuality_classes = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where punctuality_classes = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where punctuality_classes = '5' and gender = 'M'");

        masculinePunctualityClasses.addScalar("quantity", LongType.INSTANCE);
        masculinePunctualityClasses.addScalar("response", StringType.INSTANCE);

        masculinePunctualityClasses.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePunctualityClassesCustoms = masculinePunctualityClasses.list();

        SQLQuery femeninePunctualityClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where punctuality_classes = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where punctuality_classes = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where punctuality_classes = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where punctuality_classes = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where punctuality_classes = '5' and gender = 'F'");

        femeninePunctualityClasses.addScalar("quantity", LongType.INSTANCE);
        femeninePunctualityClasses.addScalar("response", StringType.INSTANCE);

        femeninePunctualityClasses.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePunctualityClassesCustoms = femeninePunctualityClasses.list();

        SQLQuery femenineKnowledgeCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_course = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_course = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_course = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_course = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_course = '5' and gender = 'F'");

        femenineKnowledgeCourse.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeCourse.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeCourseCustoms = femenineKnowledgeCourse.list();

        SQLQuery masculineKnowledgeCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_course = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_course = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_course = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_course = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_course = '5' and gender = 'M'");

        masculineKnowledgeCourse.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeCourse.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeCourseCustoms = masculineKnowledgeCourse.list();

        SQLQuery masculineUsedToolsClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where used_tools_classes = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where used_tools_classes = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where used_tools_classes = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where used_tools_classes = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where used_tools_classes = '5' and gender = 'M'");

        masculineUsedToolsClasses.addScalar("quantity", LongType.INSTANCE);
        masculineUsedToolsClasses.addScalar("response", StringType.INSTANCE);

        masculineUsedToolsClasses.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUsedToolsClassesCustoms = masculineUsedToolsClasses.list();

        SQLQuery femenineUsedToolsClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where used_tools_classes = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where used_tools_classes = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where used_tools_classes = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where used_tools_classes = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where used_tools_classes = '5' and gender = 'F'");

        femenineUsedToolsClasses.addScalar("quantity", LongType.INSTANCE);
        femenineUsedToolsClasses.addScalar("response", StringType.INSTANCE);

        femenineUsedToolsClasses.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUsedToolsClassesCustoms = femenineUsedToolsClasses.list();

        SQLQuery femenineSupportMaterial = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where support_material = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where support_material = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where support_material = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where support_material = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where support_material = '5' and gender = 'F'");

        femenineSupportMaterial.addScalar("quantity", LongType.INSTANCE);
        femenineSupportMaterial.addScalar("response", StringType.INSTANCE);

        femenineSupportMaterial.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSupportMaterialCustoms = femenineSupportMaterial.list();

        SQLQuery masculineSupportMaterial = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where support_material = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where support_material = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where support_material = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where support_material = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where support_material = '5' and gender = 'M'");

        masculineSupportMaterial.addScalar("quantity", LongType.INSTANCE);
        masculineSupportMaterial.addScalar("response", StringType.INSTANCE);

        masculineSupportMaterial.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSupportMaterialCustoms = masculineSupportMaterial.list();

        SQLQuery masculineCriteriaEvaluation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where criteria_evaluation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where criteria_evaluation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where criteria_evaluation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where criteria_evaluation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where criteria_evaluation = '5' and gender = 'M'");

        masculineCriteriaEvaluation.addScalar("quantity", LongType.INSTANCE);
        masculineCriteriaEvaluation.addScalar("response", StringType.INSTANCE);

        masculineCriteriaEvaluation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCriteriaEvaluationCustoms = masculineCriteriaEvaluation.list();

        SQLQuery femenineCriteriaEvaluation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where criteria_evaluation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where criteria_evaluation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where criteria_evaluation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where criteria_evaluation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where criteria_evaluation = '5' and gender = 'F'");

        femenineCriteriaEvaluation.addScalar("quantity", LongType.INSTANCE);
        femenineCriteriaEvaluation.addScalar("response", StringType.INSTANCE);

        femenineCriteriaEvaluation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCriteriaEvaluationCustoms = femenineCriteriaEvaluation.list();

        SQLQuery femenineProfesionalExperience = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where professional_experience = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where professional_experience = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where professional_experience = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where professional_experience = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where professional_experience = '5' and gender = 'F'");

        femenineProfesionalExperience.addScalar("quantity", LongType.INSTANCE);
        femenineProfesionalExperience.addScalar("response", StringType.INSTANCE);

        femenineProfesionalExperience.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineProfesionalExperienceCustoms = femenineProfesionalExperience.list();

        SQLQuery masculineProfesionalExperience = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where professional_experience = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where professional_experience = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where professional_experience = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where professional_experience = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where professional_experience = '5' and gender = 'M'");

        masculineProfesionalExperience.addScalar("quantity", LongType.INSTANCE);
        masculineProfesionalExperience.addScalar("response", StringType.INSTANCE);

        masculineProfesionalExperience.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineProfesionalExperienceCustoms = masculineProfesionalExperience.list();

        SQLQuery masculineKnowledgeStudyPlan = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_study_plan = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_study_plan = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_study_plan = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_study_plan = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_study_plan = '5' and gender = 'M'");

        masculineKnowledgeStudyPlan.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeStudyPlan.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeStudyPlan.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeStudyPlanCustoms = masculineKnowledgeStudyPlan.list();

        SQLQuery femenineKnowledgeStudyPlan = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_study_plan = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_study_plan = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_study_plan = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_study_plan = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_study_plan = '5' and gender = 'F'");

        femenineKnowledgeStudyPlan.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeStudyPlan.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeStudyPlan.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeStudyPlanCustoms = femenineKnowledgeStudyPlan.list();

        SQLQuery femenineUpportHeadCareer = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where upport_head_career = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where upport_head_career = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where upport_head_career = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where upport_head_career = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where upport_head_career = '5' and gender = 'F'");

        femenineUpportHeadCareer.addScalar("quantity", LongType.INSTANCE);
        femenineUpportHeadCareer.addScalar("response", StringType.INSTANCE);

        femenineUpportHeadCareer.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUpportHeadCareerCustoms = femenineUpportHeadCareer.list();

        SQLQuery masculineUpportHeadCareer = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where upport_head_career = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where upport_head_career = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where upport_head_career = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where upport_head_career = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where upport_head_career = '5' and gender = 'M'");

        masculineUpportHeadCareer.addScalar("quantity", LongType.INSTANCE);
        masculineUpportHeadCareer.addScalar("response", StringType.INSTANCE);

        masculineUpportHeadCareer.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUpportHeadCareerCustoms = masculineUpportHeadCareer.list();

        SQLQuery masculineExpertProgramCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where expert_program_course = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where expert_program_course = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where expert_program_course = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where expert_program_course = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where expert_program_course = '5' and gender = 'M'");

        masculineExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        masculineExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        masculineExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineExpertProgramCourseCustoms = masculineExpertProgramCourse.list();

        SQLQuery femenineExpertProgramCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where expert_program_course = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where expert_program_course = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where expert_program_course = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where expert_program_course = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where expert_program_course = '5' and gender = 'F'");

        femenineExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        femenineExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        femenineExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineExpertProgramCourseCustoms = femenineExpertProgramCourse.list();

        SQLQuery femenineContributionExpertProgramCourse = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'F' AND UPPER(contribution_expert_program_course) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(contribution_expert_program_course) = 'NO'");

        femenineContributionExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        femenineContributionExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        femenineContributionExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionExpertProgramCourseCustoms = femenineContributionExpertProgramCourse.list();

        SQLQuery masculineContributionExpertProgramCourse = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'M' AND UPPER(contribution_expert_program_course) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(contribution_expert_program_course) = 'NO'");

        masculineContributionExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        masculineContributionExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        masculineContributionExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionExpertProgramCourseCustoms = masculineContributionExpertProgramCourse.list();

        SQLQuery femenineContentCourseTopics = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where content_course_topics = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where content_course_topics = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where content_course_topics = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where content_course_topics = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where content_course_topics = '5' and gender = 'F'");

        femenineContentCourseTopics.addScalar("quantity", LongType.INSTANCE);
        femenineContentCourseTopics.addScalar("response", StringType.INSTANCE);

        femenineContentCourseTopics.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContentCourseTopicsCustoms = femenineContentCourseTopics.list();

        SQLQuery masculineContentCourseTopics = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where content_course_topics = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where content_course_topics = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where content_course_topics = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where content_course_topics = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where content_course_topics = '5' and gender = 'M'");

        masculineContentCourseTopics.addScalar("quantity", LongType.INSTANCE);
        masculineContentCourseTopics.addScalar("response", StringType.INSTANCE);

        masculineContentCourseTopics.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContentCourseTopicsCustoms = masculineContentCourseTopics.list();

        SQLQuery masculineAdequateCoursePayment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where adequate_course_payment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where adequate_course_payment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where adequate_course_payment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where adequate_course_payment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where adequate_course_payment = '5' and gender = 'M'");

        masculineAdequateCoursePayment.addScalar("quantity", LongType.INSTANCE);
        masculineAdequateCoursePayment.addScalar("response", StringType.INSTANCE);

        masculineAdequateCoursePayment.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAdequateCoursePaymentCustoms = masculineAdequateCoursePayment.list();

        SQLQuery femenineAdequateCoursePayment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where adequate_course_payment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where adequate_course_payment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where adequate_course_payment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where adequate_course_payment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where adequate_course_payment = '5' and gender = 'F'");

        femenineAdequateCoursePayment.addScalar("quantity", LongType.INSTANCE);
        femenineAdequateCoursePayment.addScalar("response", StringType.INSTANCE);

        masculineAdequateCoursePayment.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAdequateCoursePaymentCustoms = femenineAdequateCoursePayment.list();

        SQLQuery femenineJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where jets_participation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where jets_participation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where jets_participation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where jets_participation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where jets_participation = '5' and gender = 'F'");

        femenineJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineJetsParticipationCustoms = femenineJetsParticipation.list();

        SQLQuery masculineJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where jets_participation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where jets_participation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where jets_participation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where jets_participation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where jets_participation = '5' and gender = 'M'");

        masculineJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineJetsParticipationCustoms = masculineJetsParticipation.list();

        SQLQuery masculineContributionJetsParticipation = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'M' AND UPPER(contribution_jets_participation) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(contribution_jets_participation) = 'NO'");

        masculineContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionJetsParticipationCustoms = masculineContributionJetsParticipation.list();

        SQLQuery femenineContributionJetsParticipation = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'F' AND UPPER(contribution_jets_participation) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(contribution_jets_participation) = 'NO'");

        femenineContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionJetsParticipationCustoms = femenineContributionJetsParticipation.list();

        SQLQuery masculineSadisfaccionContributionJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where sadisfaccion_contribution_jets_participation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where sadisfaccion_contribution_jets_participation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where sadisfaccion_contribution_jets_participation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where sadisfaccion_contribution_jets_participation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where sadisfaccion_contribution_jets_participation = '5' and gender = 'M'");

        masculineSadisfaccionContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineSadisfaccionContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineSadisfaccionContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSadisfaccionContributionJetsParticipationCustoms = masculineSadisfaccionContributionJetsParticipation.list();

        SQLQuery femenineSadisfaccionContributionJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where sadisfaccion_contribution_jets_participation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where sadisfaccion_contribution_jets_participation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where sadisfaccion_contribution_jets_participation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where sadisfaccion_contribution_jets_participation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where sadisfaccion_contribution_jets_participation = '5' and gender = 'F'");

        femenineSadisfaccionContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineSadisfaccionContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineSadisfaccionContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSadisfaccionContributionJetsParticipationCustoms = femenineSadisfaccionContributionJetsParticipation.list();

        SQLQuery femenineKnowledgeUniversitySocialResponsability = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'F' AND UPPER(knowledge_university_social_responsibility) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(knowledge_university_social_responsibility) = 'NO'");

        femenineKnowledgeUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeUniversitySocialResponsabilityCustoms = femenineKnowledgeUniversitySocialResponsability.list();

        SQLQuery masculineKnowledgeUniversitySocialResponsability = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'M' AND UPPER(knowledge_university_social_responsibility) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(knowledge_university_social_responsibility) = 'NO'");

        masculineKnowledgeUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeUniversitySocialResponsabilityCustoms = masculineKnowledgeUniversitySocialResponsability.list();

        SQLQuery femenineContributionUniversitySocialResponsability = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where contribution_university_social_responsibility = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where contribution_university_social_responsibility = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where contribution_university_social_responsibility = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where contribution_university_social_responsibility = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where contribution_university_social_responsibility = '5' and gender = 'F'");

        femenineContributionUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        femenineContributionUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        femenineContributionUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionUniversitySocialResponsabilityCustoms = femenineContributionUniversitySocialResponsability.list();

        SQLQuery maculineContributionUniversitySocialResponsability = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where contribution_university_social_responsibility = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where contribution_university_social_responsibility = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where contribution_university_social_responsibility = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where contribution_university_social_responsibility = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where contribution_university_social_responsibility = '5' and gender = 'M'");

        maculineContributionUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        maculineContributionUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        maculineContributionUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> maculineContributionUniversitySocialResponsabilityCustoms = maculineContributionUniversitySocialResponsability.list();

        SQLQuery maculineKnowledgeUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_university_internationalization = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_university_internationalization = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_university_internationalization = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_university_internationalization = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_university_internationalization = '5' and gender = 'M'");

        maculineKnowledgeUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        maculineKnowledgeUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        maculineKnowledgeUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> maculineKnowledgeUniversityInternacionalizationCustoms = maculineKnowledgeUniversityInternacionalization.list();

        SQLQuery femenineKnowledgeUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where knowledge_university_internationalization = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where knowledge_university_internationalization = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where knowledge_university_internationalization = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where knowledge_university_internationalization = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where knowledge_university_internationalization = '5' and gender = 'F'");

        femenineKnowledgeUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeUniversityInternacionalizationCustoms = femenineKnowledgeUniversityInternacionalization.list();

        SQLQuery femenineContributionUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where contribution_university_internationalization = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where contribution_university_internationalization = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where contribution_university_internationalization = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where contribution_university_internationalization = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where contribution_university_internationalization = '5' and gender = 'F'");

        femenineContributionUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        femenineContributionUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        femenineContributionUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionUniversityInternacionalizationCustoms = femenineContributionUniversityInternacionalization.list();

        SQLQuery masculineContributionUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where contribution_university_internationalization = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where contribution_university_internationalization = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where contribution_university_internationalization = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where contribution_university_internationalization = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where contribution_university_internationalization = '5' and gender = 'M'");

        masculineContributionUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        masculineContributionUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        masculineContributionUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionUniversityInternacionalizationCustoms = masculineContributionUniversityInternacionalization.list();

        SQLQuery masculineStudentUniversityPride = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where student_university_pride = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where student_university_pride = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where student_university_pride = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where student_university_pride = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where student_university_pride = '5' and gender = 'M'");

        masculineStudentUniversityPride.addScalar("quantity", LongType.INSTANCE);
        masculineStudentUniversityPride.addScalar("response", StringType.INSTANCE);

        masculineStudentUniversityPride.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineStudentUniversityPrideCustoms = masculineStudentUniversityPride.list();

        SQLQuery femenineStudentUniversityPride = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where student_university_pride = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where student_university_pride = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where student_university_pride = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where student_university_pride = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where student_university_pride = '5' and gender = 'F'");

        femenineStudentUniversityPride.addScalar("quantity", LongType.INSTANCE);
        femenineStudentUniversityPride.addScalar("response", StringType.INSTANCE);

        femenineStudentUniversityPride.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineStudentUniversityPrideCustoms = femenineStudentUniversityPride.list();

        SQLQuery femenineProfessionalImage = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where professional_image = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where professional_image = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where professional_image = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where professional_image = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where professional_image = '5' and gender = 'F'");

        femenineProfessionalImage.addScalar("quantity", LongType.INSTANCE);
        femenineProfessionalImage.addScalar("response", StringType.INSTANCE);

        femenineProfessionalImage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineProfessionalImageCustoms = femenineProfessionalImage.list();

        SQLQuery masculineProfessionalImage = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where professional_image = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where professional_image = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where professional_image = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where professional_image = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where professional_image = '5' and gender = 'M'");

        masculineProfessionalImage.addScalar("quantity", LongType.INSTANCE);
        masculineProfessionalImage.addScalar("response", StringType.INSTANCE);

        masculineProfessionalImage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineProfessionalImageCustoms = masculineProfessionalImage.list();

        SQLQuery masculineCareerQuality = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where career_quality = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where career_quality = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where career_quality = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where career_quality = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where career_quality = '5' and gender = 'M'");

        masculineCareerQuality.addScalar("quantity", LongType.INSTANCE);
        masculineCareerQuality.addScalar("response", StringType.INSTANCE);

        masculineCareerQuality.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCareerQualityCustoms = masculineCareerQuality.list();

        SQLQuery femenineCareerQuality = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where career_quality = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where career_quality = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where career_quality = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where career_quality = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where career_quality = '5' and gender = 'F'");

        femenineCareerQuality.addScalar("quantity", LongType.INSTANCE);
        femenineCareerQuality.addScalar("response", StringType.INSTANCE);

        femenineCareerQuality.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCareerQualityCustoms = femenineCareerQuality.list();

        SQLQuery femenineInfrastructure = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where infrastructure = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where infrastructure = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where infrastructure = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where infrastructure = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where infrastructure = '5' and gender = 'F'");

        femenineInfrastructure.addScalar("quantity", LongType.INSTANCE);
        femenineInfrastructure.addScalar("response", StringType.INSTANCE);

        femenineInfrastructure.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineInfrastructureCustoms = femenineInfrastructure.list();

        SQLQuery masculineInfrastructure = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where infrastructure = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where infrastructure = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where infrastructure = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where infrastructure = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where infrastructure = '5' and gender = 'M'");

        masculineInfrastructure.addScalar("quantity", LongType.INSTANCE);
        masculineInfrastructure.addScalar("response", StringType.INSTANCE);

        masculineInfrastructure.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineInfrastructureCustoms = masculineInfrastructure.list();

        SQLQuery masculineClassSchedule = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where class_schedule = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where class_schedule = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where class_schedule = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where class_schedule = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where class_schedule = '5' and gender = 'M'");

        masculineClassSchedule.addScalar("quantity", LongType.INSTANCE);
        masculineClassSchedule.addScalar("response", StringType.INSTANCE);

        masculineClassSchedule.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineClassScheduleCustoms = masculineClassSchedule.list();

        SQLQuery femenineClassSchedule = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where class_schedule = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where class_schedule = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where class_schedule = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where class_schedule = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where class_schedule = '5' and gender = 'F'");

        femenineClassSchedule.addScalar("quantity", LongType.INSTANCE);
        femenineClassSchedule.addScalar("response", StringType.INSTANCE);

        femenineClassSchedule.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineClassScheduleCustoms = femenineClassSchedule.list();

        SQLQuery femeninePrestigeInstitution = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where prestige_institution = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where prestige_institution = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where prestige_institution = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where prestige_institution = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where prestige_institution = '5' and gender = 'F'");

        femeninePrestigeInstitution.addScalar("quantity", LongType.INSTANCE);
        femeninePrestigeInstitution.addScalar("response", StringType.INSTANCE);

        femeninePrestigeInstitution.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePrestigeInstitutionCustoms = femeninePrestigeInstitution.list();

        SQLQuery masculinePrestigeInstitution = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where prestige_institution = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where prestige_institution = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where prestige_institution = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where prestige_institution = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where prestige_institution = '5' and gender = 'M'");

        masculinePrestigeInstitution.addScalar("quantity", LongType.INSTANCE);
        masculinePrestigeInstitution.addScalar("response", StringType.INSTANCE);

        masculinePrestigeInstitution.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePrestigeInstitutionCustoms = masculinePrestigeInstitution.list();

        SQLQuery masculineGreatTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where great_teachers = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where great_teachers = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where great_teachers = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where great_teachers = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where great_teachers = '5' and gender = 'M'");

        masculineGreatTeachers.addScalar("quantity", LongType.INSTANCE);
        masculineGreatTeachers.addScalar("response", StringType.INSTANCE);

        masculineGreatTeachers.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineGreatTeachersCustoms = masculineGreatTeachers.list();

        SQLQuery femenineGreatTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where great_teachers = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where great_teachers = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where great_teachers = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where great_teachers = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where great_teachers = '5' and gender = 'F'");

        femenineGreatTeachers.addScalar("quantity", LongType.INSTANCE);
        femenineGreatTeachers.addScalar("response", StringType.INSTANCE);

        femenineGreatTeachers.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineGreatTeachersCustoms = femenineGreatTeachers.list();

        SQLQuery femenineModularSystem = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where modular_system = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where modular_system = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where modular_system = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where modular_system = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where modular_system = '5' and gender = 'F'");

        femenineModularSystem.addScalar("quantity", LongType.INSTANCE);
        femenineModularSystem.addScalar("response", StringType.INSTANCE);

        femenineModularSystem.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineModularSystemCustoms = femenineModularSystem.list();

        SQLQuery masculineModularSystem = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where modular_system = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where modular_system = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where modular_system = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where modular_system = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where modular_system = '5' and gender = 'M'");

        masculineModularSystem.addScalar("quantity", LongType.INSTANCE);
        masculineModularSystem.addScalar("response", StringType.INSTANCE);

        masculineModularSystem.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineModularSystemCustoms = masculineModularSystem.list();

        SQLQuery masculineAccesiblePrice = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where accesible_price = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where accesible_price = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where accesible_price = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where accesible_price = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where accesible_price = '5' and gender = 'M'");

        masculineAccesiblePrice.addScalar("quantity", LongType.INSTANCE);
        masculineAccesiblePrice.addScalar("response", StringType.INSTANCE);

        masculineAccesiblePrice.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAccesiblePriceCustoms = masculineAccesiblePrice.list();

        SQLQuery femenineAccesiblePrice = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where accesible_price = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where accesible_price = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where accesible_price = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where accesible_price = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where accesible_price = '5' and gender = 'F'");

        femenineAccesiblePrice.addScalar("quantity", LongType.INSTANCE);
        femenineAccesiblePrice.addScalar("response", StringType.INSTANCE);

        femenineAccesiblePrice.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAccesiblePriceCustoms = femenineAccesiblePrice.list();

        SQLQuery femenineReferences = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where census_student.references = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where census_student.references = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where census_student.references = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where census_student.references = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where census_student.references = '5' and gender = 'F'");

        femenineReferences.addScalar("quantity", LongType.INSTANCE);
        femenineReferences.addScalar("response", StringType.INSTANCE);

        femenineReferences.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineReferencesCustoms = femenineReferences.list();

        SQLQuery masculineReferences = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where census_student.references = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where census_student.references = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where census_student.references = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where census_student.references = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where census_student.references = '5' and gender = 'M'");

        masculineReferences.addScalar("quantity", LongType.INSTANCE);
        masculineReferences.addScalar("response", StringType.INSTANCE);

        masculineReferences.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineReferencesCustoms = masculineReferences.list();

        SQLQuery femenineLaboratoriesEquipment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where laboratories_equipment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories_equipment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories_equipment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories_equipment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories_equipment = '5' and gender = 'F'");

        femenineLaboratoriesEquipment.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratoriesEquipment.addScalar("response", StringType.INSTANCE);

        femenineLaboratoriesEquipment.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesEquipmentCustoms = femenineLaboratoriesEquipment.list();

        SQLQuery masculineLaboratoriesEquipment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where laboratories_equipment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where laboratories_equipment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where laboratories_equipment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where laboratories_equipment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where laboratories_equipment = '5' and gender = 'M'");

        masculineLaboratoriesEquipment.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratoriesEquipment.addScalar("response", StringType.INSTANCE);

        masculineLaboratoriesEquipment.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesEquipmentCustoms = masculineLaboratoriesEquipment.list();

        SQLQuery masculineComunication = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where comunication = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where comunication = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where comunication = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where comunication = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where comunication = '5' and gender = 'M'");

        masculineComunication.addScalar("quantity", LongType.INSTANCE);
        masculineComunication.addScalar("response", StringType.INSTANCE);

        masculineComunication.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineComunicationCustoms = masculineComunication.list();

        SQLQuery femenineComunication = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where comunication = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where comunication = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where comunication = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where comunication = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where comunication = '5' and gender = 'F'");

        femenineComunication.addScalar("quantity", LongType.INSTANCE);
        femenineComunication.addScalar("response", StringType.INSTANCE);

        femenineComunication.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineComunicationCustoms = femenineComunication.list();

        SQLQuery femenineGraduateGreatStudent = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where graduate_great_student = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where graduate_great_student = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where graduate_great_student = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where graduate_great_student = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where graduate_great_student = '5' and gender = 'F'");

        femenineGraduateGreatStudent.addScalar("quantity", LongType.INSTANCE);
        femenineGraduateGreatStudent.addScalar("response", StringType.INSTANCE);

        femenineGraduateGreatStudent.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineGraduateGreatStudentCustoms = femenineGraduateGreatStudent.list();

        SQLQuery masculineGraduateGreatStudent = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where graduate_great_student = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where graduate_great_student = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where graduate_great_student = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where graduate_great_student = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where graduate_great_student = '5' and gender = 'M'");

        masculineGraduateGreatStudent.addScalar("quantity", LongType.INSTANCE);
        masculineGraduateGreatStudent.addScalar("response", StringType.INSTANCE);

        masculineGraduateGreatStudent.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineGraduateGreatStudentCustoms = masculineGraduateGreatStudent.list();

        SQLQuery masculineAdequateRequirementStudies = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where adequate_requirement_studies = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where adequate_requirement_studies = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where adequate_requirement_studies = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where adequate_requirement_studies = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where adequate_requirement_studies = '5' and gender = 'M'");

        masculineAdequateRequirementStudies.addScalar("quantity", LongType.INSTANCE);
        masculineAdequateRequirementStudies.addScalar("response", StringType.INSTANCE);

        masculineAdequateRequirementStudies.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAdequateRequirementStudiesCustoms = masculineAdequateRequirementStudies.list();

        SQLQuery femenineAdequateRequirementStudies = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where adequate_requirement_studies = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where adequate_requirement_studies = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where adequate_requirement_studies = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where adequate_requirement_studies = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where adequate_requirement_studies = '5' and gender = 'F'");

        femenineAdequateRequirementStudies.addScalar("quantity", LongType.INSTANCE);
        femenineAdequateRequirementStudies.addScalar("response", StringType.INSTANCE);

        femenineAdequateRequirementStudies.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAdequateRequirementStudiesCustoms = femenineAdequateRequirementStudies.list();

        SQLQuery femenineQualificationTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where qualification_teachers = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where qualification_teachers = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where qualification_teachers = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where qualification_teachers = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where qualification_teachers = '5' and gender = 'F'");

        femenineQualificationTeachers.addScalar("quantity", LongType.INSTANCE);
        femenineQualificationTeachers.addScalar("response", StringType.INSTANCE);

        femenineQualificationTeachers.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineQualificationTeachersCustoms = femenineQualificationTeachers.list();

        SQLQuery masculineQualificationTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where qualification_teachers = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where qualification_teachers = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where qualification_teachers = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where qualification_teachers = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where qualification_teachers = '5' and gender = 'M'");

        masculineQualificationTeachers.addScalar("quantity", LongType.INSTANCE);
        masculineQualificationTeachers.addScalar("response", StringType.INSTANCE);

        masculineQualificationTeachers.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineQualificationTeachersCustoms = masculineQualificationTeachers.list();

        SQLQuery masculineStudentService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where student_service = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where student_service = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where student_service = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where student_service = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where student_service = '5' and gender = 'M'");

        masculineStudentService.addScalar("quantity", LongType.INSTANCE);
        masculineStudentService.addScalar("response", StringType.INSTANCE);

        masculineStudentService.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineStudentServiceCustoms = masculineStudentService.list();

        SQLQuery femenineStudentService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where student_service = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where student_service = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where student_service = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where student_service = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where student_service = '5' and gender = 'F'");

        femenineStudentService.addScalar("quantity", LongType.INSTANCE);
        femenineStudentService.addScalar("response", StringType.INSTANCE);

        femenineStudentService.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineStudentServiceCustoms = femenineStudentService.list();

        SQLQuery femenineReputationMiddleCity = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where reputation_middle_city = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where reputation_middle_city = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where reputation_middle_city = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where reputation_middle_city = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where reputation_middle_city = '5' and gender = 'F'");

        femenineReputationMiddleCity.addScalar("quantity", LongType.INSTANCE);
        femenineReputationMiddleCity.addScalar("response", StringType.INSTANCE);

        femenineReputationMiddleCity.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineReputationMiddleCityCustoms = femenineReputationMiddleCity.list();

        SQLQuery masculineReputationMiddleCity = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where reputation_middle_city = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where reputation_middle_city = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where reputation_middle_city = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where reputation_middle_city = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where reputation_middle_city = '5' and gender = 'M'");

        masculineReputationMiddleCity.addScalar("quantity", LongType.INSTANCE);
        masculineReputationMiddleCity.addScalar("response", StringType.INSTANCE);

        masculineReputationMiddleCity.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineReputationMiddleCityCustoms = masculineReputationMiddleCity.list();

        SQLQuery masculineUniversitySupervision = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where university_supervision = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where university_supervision = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where university_supervision = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where university_supervision = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where university_supervision = '5' and gender = 'M'");

        masculineUniversitySupervision.addScalar("quantity", LongType.INSTANCE);
        masculineUniversitySupervision.addScalar("response", StringType.INSTANCE);

        masculineUniversitySupervision.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUniversitySupervisionCustoms = masculineUniversitySupervision.list();

        SQLQuery femenineUniversitySupervision = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where university_supervision = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where university_supervision = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where university_supervision = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where university_supervision = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where university_supervision = '5' and gender = 'F'");

        femenineUniversitySupervision.addScalar("quantity", LongType.INSTANCE);
        femenineUniversitySupervision.addScalar("response", StringType.INSTANCE);

        femenineUniversitySupervision.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUniversitySupervisionCustoms = femenineUniversitySupervision.list();

        SQLQuery femenineUniversityRecommendation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where university_recommendation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where university_recommendation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where university_recommendation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where university_recommendation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where university_recommendation = '5' and gender = 'F'");

        femenineUniversityRecommendation.addScalar("quantity", LongType.INSTANCE);
        femenineUniversityRecommendation.addScalar("response", StringType.INSTANCE);

        femenineUniversityRecommendation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUniversityRecommendationCustoms = femenineUniversityRecommendation.list();

        SQLQuery masculineUniversityRecommendation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where university_recommendation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where university_recommendation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where university_recommendation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where university_recommendation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where university_recommendation = '5' and gender = 'M'");

        masculineUniversityRecommendation.addScalar("quantity", LongType.INSTANCE);
        masculineUniversityRecommendation.addScalar("response", StringType.INSTANCE);

        masculineUniversityRecommendation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUniversityRecommendationCustoms = masculineUniversityRecommendation.list();

        SQLQuery femenineTypeLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'F' AND UPPER(type_language) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'F' AND UPPER(type_language) = 'NO'");

        femenineTypeLanguage.addScalar("quantity", LongType.INSTANCE);
        femenineTypeLanguage.addScalar("response", StringType.INSTANCE);

        femenineTypeLanguage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineTypeLanguageCustoms = femenineTypeLanguage.list();

        SQLQuery masculineTypeLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student WHERE gender = 'M' AND UPPER(type_language) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student WHERE gender = 'M' AND UPPER(type_language) = 'NO'");

        masculineTypeLanguage.addScalar("quantity", LongType.INSTANCE);
        masculineTypeLanguage.addScalar("response", StringType.INSTANCE);

        masculineTypeLanguage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineTypeLanguageCustoms = masculineTypeLanguage.list();

        generalAnswers.setTypeLanguageMasculine(masculineTypeLanguageCustoms);
        generalAnswers.setTypeLanguageFemenine(femenineTypeLanguageCustoms);
        generalAnswers.setUniversityRecommendationMasculine(masculineUniversityRecommendationCustoms);
        generalAnswers.setUniversityRecommendationFemenine(femenineUniversityRecommendationCustoms);
        generalAnswers.setUniversitySupervisionFemenine(femenineUniversitySupervisionCustoms);
        generalAnswers.setUniversitySupervisionMasculine(masculineUniversitySupervisionCustoms);
        generalAnswers.setReputationMiddleCityMasculine(masculineReputationMiddleCityCustoms);
        generalAnswers.setReputationMiddleCityFemenine(femenineReputationMiddleCityCustoms);
        generalAnswers.setStudentServiceFemenine(femenineStudentServiceCustoms);
        generalAnswers.setStudentServiceMasculine(masculineStudentServiceCustoms);
        generalAnswers.setQualificationTeachersMasculine(masculineQualificationTeachersCustoms);
        generalAnswers.setQualificationTeachersFemenine(femenineQualificationTeachersCustoms);
        generalAnswers.setAdequateRequirementStudiesFemenine(femenineAdequateRequirementStudiesCustoms);
        generalAnswers.setAdequateRequirementStudiesMasculine(masculineAdequateRequirementStudiesCustoms);
        generalAnswers.setGraduateGreatStudentMasculine(masculineGraduateGreatStudentCustoms);
        generalAnswers.setGraduateGreatStudentFemenine(femenineGraduateGreatStudentCustoms);
        generalAnswers.setCommunicationFemenine(femenineComunicationCustoms);
        generalAnswers.setCommunicationMasculine(masculineComunicationCustoms);
        generalAnswers.setLaboratoriesEquipmentMasculine(masculineLaboratoriesEquipmentCustoms);
        generalAnswers.setLaboratoriesEquipmentFemenine(femenineLaboratoriesEquipmentCustoms);
        generalAnswers.setReferencesMasculine(masculineReferencesCustoms);
        generalAnswers.setReferencesFemenine(femenineReferencesCustoms);
        generalAnswers.setAccesiblePriceFemenine(femenineAccesiblePriceCustoms);
        generalAnswers.setAccesiblePriceMasculine(masculineAccesiblePriceCustoms);
        generalAnswers.setModularSystemMasculine(masculineModularSystemCustoms);
        generalAnswers.setModularSystemFemenine(femenineModularSystemCustoms);
        generalAnswers.setGreatTeachersFemenine(femenineGreatTeachersCustoms);
        generalAnswers.setGreatTeachersMasculine(masculineGreatTeachersCustoms);
        generalAnswers.setPrestigeInstitutionMasculine(masculinePrestigeInstitutionCustoms);
        generalAnswers.setPrestigeInstitutionFemenine(femeninePrestigeInstitutionCustoms);
        generalAnswers.setClassScheduleFemenine(femenineClassScheduleCustoms);
        generalAnswers.setClassScheduleMasculine(masculineClassScheduleCustoms);
        generalAnswers.setInfrastructureMasculine(masculineInfrastructureCustoms);
        generalAnswers.setInfrastructureFemenine(femenineInfrastructureCustoms);
        generalAnswers.setCareerQualityFemenine(femenineCareerQualityCustoms);
        generalAnswers.setCareerQualityMasculine(masculineCareerQualityCustoms);
        generalAnswers.setProfessionalImageMasculine(masculineProfessionalImageCustoms);
        generalAnswers.setProfessionalImageFemenine(femenineProfessionalImageCustoms);
        generalAnswers.setStudentUniversityPrideFemenine(femenineStudentUniversityPrideCustoms);
        generalAnswers.setStudentUniversityPrideMasculine(masculineStudentUniversityPrideCustoms);
        generalAnswers.setContributionUniversityInternacionalizationMasculine(masculineContributionUniversityInternacionalizationCustoms);
        generalAnswers.setContributionUniversityInternacionalizationFemenine(femenineContributionUniversityInternacionalizationCustoms);
        generalAnswers.setKnowledgeUniversityInternacionalizationFemenine(femenineKnowledgeUniversityInternacionalizationCustoms);
        generalAnswers.setKnowledgeUniversityInternacionalizationMasculine(maculineKnowledgeUniversityInternacionalizationCustoms);
        generalAnswers.setContributionUniversitySocialResponsabilityMasculine(maculineContributionUniversitySocialResponsabilityCustoms);
        generalAnswers.setContributionUniversitySocialResponsabilityFemenine(femenineContributionUniversitySocialResponsabilityCustoms);
        generalAnswers.setKnowledgeUniversitySocialResponsabilityFemenine(femenineKnowledgeUniversitySocialResponsabilityCustoms);
        generalAnswers.setKnowledgeUniversitySocialResponsabilityMasculine(masculineKnowledgeUniversitySocialResponsabilityCustoms);
        generalAnswers.setSadisfaccionContributionJetsParticipationFemenine(femenineSadisfaccionContributionJetsParticipationCustoms);
        generalAnswers.setSadisfaccionContributionJetsParticipationMasculine(masculineSadisfaccionContributionJetsParticipationCustoms);
        generalAnswers.setContributionJetsParticipationFemenine(femenineContributionJetsParticipationCustoms);
        generalAnswers.setContributionJetsParticipationMasculine(masculineContributionJetsParticipationCustoms);
        generalAnswers.setIsWorkingFemenine(femenineIsWorkingCustoms);
        generalAnswers.setIsWorkingMasculine(masculineIsWorkingCustoms);
        generalAnswers.setSuggestionsMetFemenine(femenineSuggestionsMetCustoms);
        generalAnswers.setSuggestionsMetMasculine(masculineSuggestionsMetCustoms);
        generalAnswers.setCourseProgrammingMasculine(masculineCourseProgrammingCustoms);
        generalAnswers.setCourseProgrammingFemenine(femenineCourseProgrammingCustoms);
        generalAnswers.setAnotherLanguageFemenine(femenineAnotherLanguageCustoms);
        generalAnswers.setAnotherLanguageMasculine(masculineAnotherLanguageCustoms);
        generalAnswers.setBilingualSchoolMasculine(masculineBilingualSchoolCustoms);
        generalAnswers.setBilingualSchoolFemenine(femenineBilingualSchoolCustoms);
        generalAnswers.setAccessCourseNoteFemenine(femenineAccessCourseNoteCustoms);
        generalAnswers.setAccessCourseNoteMasculine(masculineAccessCourseNoteCustoms);
        generalAnswers.setClassroomsMasculine(masculineClassroomsCustoms);
        generalAnswers.setClassroomsFemenine(femenineClassroomsCustoms);
        generalAnswers.setLaboratoriesFemenine(femenineLaboratoriesCustoms);
        generalAnswers.setLaboratoriesMasculine(masculineLaboratoriesCustoms);
        generalAnswers.setLaboratoriesAssignmentMasculine(masculineLaboratoriesAssignmentsCustoms);
        generalAnswers.setLaboratoriesAssignmentFemenine(femenineLaboratoriesAssignmentsCustoms);
        generalAnswers.setLibraryServiceFemenine(femenineLibraryServiceCustoms);
        generalAnswers.setLibraryServiceMasculine(masculineLibraryServiceCustoms);
        generalAnswers.setTreatmentPlatformStaffMasculine(masculineTreatmentPlatformStaffCustoms);
        generalAnswers.setTreatmentPlatformStaffFemenine(femenineTreatmentPlatformStaffCustoms);
        generalAnswers.setQuickAttentionPlatformResponseFemenine(femenineQuickAttentionPlatformStaffCustoms);
        generalAnswers.setQuickAttentionPlatformResponseMasculine(masculineQuickAttentionPlatformStaffCustoms);
        generalAnswers.setKnowledgePlatformStaffMasculine(masculineKnowledgePlatformStaffCustoms);
        generalAnswers.setKnowledgePlatformStaffFemenine(femenineKnowledgePlatformStaffCustoms);
        generalAnswers.setPlatformStaffResponseFemenine(femeninePlatformStaffResponseCustoms);
        generalAnswers.setPlatformStaffResponseMasculine(masculinePlatformStaffResponseCustoms);
        generalAnswers.setCareerInformationMasculine(masculineCareerInformationCustoms);
        generalAnswers.setCareerInformationFemenine(femenineCareerInformationCustoms);
        generalAnswers.setPaymentInformationFemenine(femeninePaymentInformationCustoms);
        generalAnswers.setPaymentInformationMasculine(masculinePaymentInformationCustoms);
        generalAnswers.setPunctualityClassesMasculine(masculinePunctualityClassesCustoms);
        generalAnswers.setPunctualityClassesFemenine(femeninePunctualityClassesCustoms);
        generalAnswers.setKnowledgeCourseFemenine(femenineKnowledgeCourseCustoms);
        generalAnswers.setKnowledgeCourseMasculine(masculineKnowledgeCourseCustoms);
        generalAnswers.setUsedToolsClassesMasculine(masculineUsedToolsClassesCustoms);
        generalAnswers.setUsedToolsClassesFemenine(femenineUsedToolsClassesCustoms);
        generalAnswers.setSupportMaterialFemenine(femenineSupportMaterialCustoms);
        generalAnswers.setSupportMaterialMasculine(masculineSupportMaterialCustoms);
        generalAnswers.setCriteriaEvaluationFemenine(femenineCriteriaEvaluationCustoms);
        generalAnswers.setCriteriaEvaluationMasculine(masculineCriteriaEvaluationCustoms);
        generalAnswers.setProfessionalExperienceFemenine(femenineProfesionalExperienceCustoms);
        generalAnswers.setProfessionalExperienceMasculine(masculineProfesionalExperienceCustoms);
        generalAnswers.setKnowledgeStudyPlanMasculine(masculineKnowledgeStudyPlanCustoms);
        generalAnswers.setKnowledgeStudyPlanFemenine(femenineKnowledgeStudyPlanCustoms);
        generalAnswers.setUpportHeadCareerFemenine(femenineUpportHeadCareerCustoms);
        generalAnswers.setUpportHeadCareerMasculine(masculineUpportHeadCareerCustoms);
        generalAnswers.setExpertProgramCourseMasculine(masculineExpertProgramCourseCustoms);
        generalAnswers.setExpertProgramCourseFemenine(femenineExpertProgramCourseCustoms);
        generalAnswers.setContributionExpertProgramCourseFemenine(femenineContributionExpertProgramCourseCustoms);
        generalAnswers.setContributionExpertProgramCourseMasculine(masculineContributionExpertProgramCourseCustoms);
        generalAnswers.setContentCourseTopicFemenine(femenineContentCourseTopicsCustoms);
        generalAnswers.setContentCourseTopicMasculine(masculineContentCourseTopicsCustoms);
        generalAnswers.setAdequateCoursePaymentMasculine(masculineAdequateCoursePaymentCustoms);
        generalAnswers.setAdequateCoursePaymentFemenine(femenineAdequateCoursePaymentCustoms);
        generalAnswers.setJetsParticipationFemenine(femenineJetsParticipationCustoms);
        generalAnswers.setJetsParticipationMasculine(masculineJetsParticipationCustoms);

        return generalAnswers;
    }

    @Override
    public Answers careerReport(long idCareer) {

        Answers careerAnswers = new Answers();

        SQLQuery femenineIsWorking = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(is_working) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(is_working) = 'NO'");

        femenineIsWorking.addScalar("quantity", LongType.INSTANCE);
        femenineIsWorking.addScalar("response", StringType.INSTANCE);

        femenineIsWorking.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineIsWorkingCustoms = femenineIsWorking.list();

        SQLQuery masculineIsWorking = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(is_working) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(is_working) = 'NO'");

        masculineIsWorking.addScalar("quantity", LongType.INSTANCE);
        masculineIsWorking.addScalar("response", StringType.INSTANCE);

        masculineIsWorking.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineIsWorkingCustoms = masculineIsWorking.list();

        SQLQuery femenineSuggestionsMet = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and suggestions_met = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and suggestions_met = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and suggestions_met = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and suggestions_met = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and suggestions_met = '5' and gender = 'F'");

        femenineSuggestionsMet.addScalar("quantity", LongType.INSTANCE);
        femenineSuggestionsMet.addScalar("response", StringType.INSTANCE);

        femenineSuggestionsMet.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSuggestionsMetCustoms = femenineSuggestionsMet.list();

        SQLQuery masculineSuggestionsMet = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and suggestions_met = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and suggestions_met = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and suggestions_met = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and suggestions_met = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and suggestions_met = '5' and gender = 'M'");

        masculineSuggestionsMet.addScalar("quantity", LongType.INSTANCE);
        masculineSuggestionsMet.addScalar("response", StringType.INSTANCE);

        masculineSuggestionsMet.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSuggestionsMetCustoms = masculineSuggestionsMet.list();

        SQLQuery masculineCourseProgramming = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and course_programming = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and course_programming = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and course_programming = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and course_programming = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and course_programming = '5' and gender = 'M'");

        masculineCourseProgramming.addScalar("quantity", LongType.INSTANCE);
        masculineCourseProgramming.addScalar("response", StringType.INSTANCE);

        masculineCourseProgramming.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCourseProgrammingCustoms = masculineCourseProgramming.list();

        SQLQuery femenineCourseProgramming = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and course_programming = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and course_programming = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and course_programming = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and course_programming = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and course_programming = '5' and gender = 'F'");

        femenineCourseProgramming.addScalar("quantity", LongType.INSTANCE);
        femenineCourseProgramming.addScalar("response", StringType.INSTANCE);

        femenineCourseProgramming.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCourseProgrammingCustoms = femenineCourseProgramming.list();

        SQLQuery femenineAnotherLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(another_language) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(another_language) = 'SI'");

        femenineAnotherLanguage.addScalar("quantity", LongType.INSTANCE);
        femenineAnotherLanguage.addScalar("response", StringType.INSTANCE);

        femenineAnotherLanguage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAnotherLanguageCustoms = femenineAnotherLanguage.list();

        SQLQuery masculineAnotherLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(another_language) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(another_language) = 'SI'");

        masculineAnotherLanguage.addScalar("quantity", LongType.INSTANCE);
        masculineAnotherLanguage.addScalar("response", StringType.INSTANCE);

        masculineAnotherLanguage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAnotherLanguageCustoms = masculineAnotherLanguage.list();

        SQLQuery masculineBilingualSchool = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(bilingual_school) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(bilingual_school) = 'SI'");

        masculineBilingualSchool.addScalar("quantity", LongType.INSTANCE);
        masculineBilingualSchool.addScalar("response", StringType.INSTANCE);

        masculineBilingualSchool.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineBilingualSchoolCustoms = masculineBilingualSchool.list();

        SQLQuery femenineBilingualSchool = currentSession().createSQLQuery("SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(bilingual_school) = 'NO'\n" +
                "union \n" +
                "SELECT count(*) as quantity, 'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(bilingual_school) = 'SI'");

        femenineBilingualSchool.addScalar("quantity", LongType.INSTANCE);
        femenineBilingualSchool.addScalar("response", StringType.INSTANCE);

        femenineBilingualSchool.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineBilingualSchoolCustoms = femenineBilingualSchool.list();

        SQLQuery femenineAccesCourseNote = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and access_course_note = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and access_course_note = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and access_course_note = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and access_course_note = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and access_course_note = '5' and gender = 'F'");

        femenineAccesCourseNote.addScalar("quantity", LongType.INSTANCE);
        femenineAccesCourseNote.addScalar("response", StringType.INSTANCE);

        femenineAccesCourseNote.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAccessCourseNoteCustoms = femenineAccesCourseNote.list();

        SQLQuery masculineAccesCourseNote = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and access_course_note = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and access_course_note = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and access_course_note = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and access_course_note = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and access_course_note = '5' and gender = 'M'");

        masculineAccesCourseNote.addScalar("quantity", LongType.INSTANCE);
        masculineAccesCourseNote.addScalar("response", StringType.INSTANCE);

        masculineAccesCourseNote.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAccessCourseNoteCustoms = masculineAccesCourseNote.list();

        SQLQuery masculineClassrooms = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and classrooms = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and classrooms = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and classrooms = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and classrooms = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and classrooms = '5' and gender = 'M'");

        masculineClassrooms.addScalar("quantity", LongType.INSTANCE);
        masculineClassrooms.addScalar("response", StringType.INSTANCE);

        masculineClassrooms.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineClassroomsCustoms = masculineClassrooms.list();

        SQLQuery femenineClassrooms = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and classrooms = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and classrooms = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and classrooms = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and classrooms = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and classrooms = '5' and gender = 'F'");

        femenineClassrooms.addScalar("quantity", LongType.INSTANCE);
        femenineClassrooms.addScalar("response", StringType.INSTANCE);

        femenineClassrooms.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineClassroomsCustoms = femenineClassrooms.list();

        SQLQuery femenineLaboratories = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and laboratories = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories = '5' and gender = 'F'");

        femenineLaboratories.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratories.addScalar("response", StringType.INSTANCE);

        femenineLaboratories.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesCustoms = femenineLaboratories.list();

        SQLQuery masculineLaboratories = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and laboratories = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories = '5' and gender = 'M'");

        masculineLaboratories.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratories.addScalar("response", StringType.INSTANCE);

        masculineLaboratories.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesCustoms = masculineLaboratories.list();

        SQLQuery masculineLaboratoriesAssignments = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '5' and gender = 'M'");

        masculineLaboratoriesAssignments.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratoriesAssignments.addScalar("response", StringType.INSTANCE);

        masculineLaboratoriesAssignments.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesAssignmentsCustoms = masculineLaboratoriesAssignments.list();

        SQLQuery femenineLaboratoriesAssignments = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories_assignment = '5' and gender = 'F'");

        femenineLaboratoriesAssignments.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratoriesAssignments.addScalar("response", StringType.INSTANCE);

        femenineLaboratoriesAssignments.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesAssignmentsCustoms = femenineLaboratoriesAssignments.list();

        SQLQuery femenineLibraryService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and library_service = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and library_service = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and library_service = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and library_service = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and library_service = '5' and gender = 'F'");

        femenineLibraryService.addScalar("quantity", LongType.INSTANCE);
        femenineLibraryService.addScalar("response", StringType.INSTANCE);

        femenineLibraryService.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLibraryServiceCustoms = femenineLibraryService.list();

        SQLQuery masculineLibraryService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and library_service = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and library_service = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and library_service = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and library_service = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and library_service = '5' and gender = 'M'");

        masculineLibraryService.addScalar("quantity", LongType.INSTANCE);
        masculineLibraryService.addScalar("response", StringType.INSTANCE);

        masculineLibraryService.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLibraryServiceCustoms = masculineLibraryService.list();

        SQLQuery masculineTreatmentPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '5' and gender = 'M'");

        masculineTreatmentPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineTreatmentPlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineTreatmentPlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineTreatmentPlatformStaffCustoms = masculineTreatmentPlatformStaff.list();

        SQLQuery femenineTreatmentPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and treatment_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and treatment_platform_staff = '5' and gender = 'F'");

        femenineTreatmentPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineTreatmentPlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineTreatmentPlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineTreatmentPlatformStaffCustoms = femenineTreatmentPlatformStaff.list();

        SQLQuery femenineQuickAttentionPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '5' and gender = 'F'");

        femenineQuickAttentionPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineQuickAttentionPlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineQuickAttentionPlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineQuickAttentionPlatformStaffCustoms = femenineQuickAttentionPlatformStaff.list();

        SQLQuery masculineQuickAttentionPlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and quick_attention_platform_staff = '5' and gender = 'M'");

        masculineQuickAttentionPlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineQuickAttentionPlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineQuickAttentionPlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineQuickAttentionPlatformStaffCustoms = masculineQuickAttentionPlatformStaff.list();

        SQLQuery masculineKnowledgePlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '5' and gender = 'M'");

        masculineKnowledgePlatformStaff.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgePlatformStaff.addScalar("response", StringType.INSTANCE);

        masculineKnowledgePlatformStaff.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgePlatformStaffCustoms = masculineKnowledgePlatformStaff.list();

        SQLQuery femenineKnowledgePlatformStaff = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_platform_staff = '5' and gender = 'F'");

        femenineKnowledgePlatformStaff.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgePlatformStaff.addScalar("response", StringType.INSTANCE);

        femenineKnowledgePlatformStaff.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgePlatformStaffCustoms = femenineKnowledgePlatformStaff.list();

        SQLQuery femeninePlatformStaffResponse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and platform_staff_response = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '5' and gender = 'F'");

        femeninePlatformStaffResponse.addScalar("quantity", LongType.INSTANCE);
        femeninePlatformStaffResponse.addScalar("response", StringType.INSTANCE);

        femeninePlatformStaffResponse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePlatformStaffResponseCustoms = femeninePlatformStaffResponse.list();

        SQLQuery masculinePlatformStaffResponse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and platform_staff_response = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and platform_staff_response = '5' and gender = 'M'");

        masculinePlatformStaffResponse.addScalar("quantity", LongType.INSTANCE);
        masculinePlatformStaffResponse.addScalar("response", StringType.INSTANCE);

        masculinePlatformStaffResponse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePlatformStaffResponseCustoms = masculinePlatformStaffResponse.list();

        SQLQuery masculineCareerInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and career_information = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and career_information = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and career_information = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and career_information = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and career_information = '5' and gender = 'M'");

        masculineCareerInformation.addScalar("quantity", LongType.INSTANCE);
        masculineCareerInformation.addScalar("response", StringType.INSTANCE);

        masculineCareerInformation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCareerInformationCustoms = masculineCareerInformation.list();

        SQLQuery femenineCareerInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and career_information = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and career_information = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and career_information = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and career_information = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and career_information = '5' and gender = 'F'");

        femenineCareerInformation.addScalar("quantity", LongType.INSTANCE);
        femenineCareerInformation.addScalar("response", StringType.INSTANCE);

        femenineCareerInformation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCareerInformationCustoms = femenineCareerInformation.list();

        SQLQuery femeninePaymentInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and payment_information = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and payment_information = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and payment_information = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and payment_information = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and payment_information = '5' and gender = 'F'");

        femeninePaymentInformation.addScalar("quantity", LongType.INSTANCE);
        femeninePaymentInformation.addScalar("response", StringType.INSTANCE);

        femeninePaymentInformation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePaymentInformationCustoms = femeninePaymentInformation.list();

        SQLQuery masculinePaymentInformation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and payment_information = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and payment_information = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and payment_information = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and payment_information = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and payment_information = '5' and gender = 'M'");

        masculinePaymentInformation.addScalar("quantity", LongType.INSTANCE);
        masculinePaymentInformation.addScalar("response", StringType.INSTANCE);

        masculinePaymentInformation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePaymentInformationCustoms = masculinePaymentInformation.list();

        SQLQuery masculinePunctualityClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and punctuality_classes = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '5' and gender = 'M'");

        masculinePunctualityClasses.addScalar("quantity", LongType.INSTANCE);
        masculinePunctualityClasses.addScalar("response", StringType.INSTANCE);

        masculinePunctualityClasses.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePunctualityClassesCustoms = masculinePunctualityClasses.list();

        SQLQuery femeninePunctualityClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and punctuality_classes = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and punctuality_classes = '5' and gender = 'F'");

        femeninePunctualityClasses.addScalar("quantity", LongType.INSTANCE);
        femeninePunctualityClasses.addScalar("response", StringType.INSTANCE);

        femeninePunctualityClasses.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePunctualityClassesCustoms = femeninePunctualityClasses.list();

        SQLQuery femenineKnowledgeCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_course = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_course = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_course = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_course = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_course = '5' and gender = 'F'");

        femenineKnowledgeCourse.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeCourse.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeCourseCustoms = femenineKnowledgeCourse.list();

        SQLQuery masculineKnowledgeCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_course = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_course = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_course = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_course = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_course = '5' and gender = 'M'");

        masculineKnowledgeCourse.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeCourse.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeCourseCustoms = masculineKnowledgeCourse.list();

        SQLQuery masculineUsedToolsClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and used_tools_classes = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '5' and gender = 'M'");

        masculineUsedToolsClasses.addScalar("quantity", LongType.INSTANCE);
        masculineUsedToolsClasses.addScalar("response", StringType.INSTANCE);

        masculineUsedToolsClasses.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUsedToolsClassesCustoms = masculineUsedToolsClasses.list();

        SQLQuery femenineUsedToolsClasses = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and used_tools_classes = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and used_tools_classes = '5' and gender = 'F'");

        femenineUsedToolsClasses.addScalar("quantity", LongType.INSTANCE);
        femenineUsedToolsClasses.addScalar("response", StringType.INSTANCE);

        femenineUsedToolsClasses.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUsedToolsClassesCustoms = femenineUsedToolsClasses.list();

        SQLQuery femenineSupportMaterial = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and support_material = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and support_material = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and support_material = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and support_material = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and support_material = '5' and gender = 'F'");

        femenineSupportMaterial.addScalar("quantity", LongType.INSTANCE);
        femenineSupportMaterial.addScalar("response", StringType.INSTANCE);

        femenineSupportMaterial.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSupportMaterialCustoms = femenineSupportMaterial.list();

        SQLQuery masculineSupportMaterial = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and support_material = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and support_material = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and support_material = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and support_material = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and support_material = '5' and gender = 'M'");

        masculineSupportMaterial.addScalar("quantity", LongType.INSTANCE);
        masculineSupportMaterial.addScalar("response", StringType.INSTANCE);

        masculineSupportMaterial.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSupportMaterialCustoms = masculineSupportMaterial.list();

        SQLQuery masculineCriteriaEvaluation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and criteria_evaluation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '5' and gender = 'M'");

        masculineCriteriaEvaluation.addScalar("quantity", LongType.INSTANCE);
        masculineCriteriaEvaluation.addScalar("response", StringType.INSTANCE);

        masculineCriteriaEvaluation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCriteriaEvaluationCustoms = masculineCriteriaEvaluation.list();

        SQLQuery femenineCriteriaEvaluation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and criteria_evaluation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and criteria_evaluation = '5' and gender = 'F'");

        femenineCriteriaEvaluation.addScalar("quantity", LongType.INSTANCE);
        femenineCriteriaEvaluation.addScalar("response", StringType.INSTANCE);

        femenineCriteriaEvaluation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCriteriaEvaluationCustoms = femenineCriteriaEvaluation.list();

        SQLQuery femenineProfesionalExperience = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and professional_experience = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and professional_experience = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and professional_experience = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and professional_experience = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and professional_experience = '5' and gender = 'F'");

        femenineProfesionalExperience.addScalar("quantity", LongType.INSTANCE);
        femenineProfesionalExperience.addScalar("response", StringType.INSTANCE);

        femenineProfesionalExperience.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineProfesionalExperienceCustoms = femenineProfesionalExperience.list();

        SQLQuery masculineProfesionalExperience = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and professional_experience = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and professional_experience = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and professional_experience = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and professional_experience = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and professional_experience = '5' and gender = 'M'");

        masculineProfesionalExperience.addScalar("quantity", LongType.INSTANCE);
        masculineProfesionalExperience.addScalar("response", StringType.INSTANCE);

        masculineProfesionalExperience.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineProfesionalExperienceCustoms = masculineProfesionalExperience.list();

        SQLQuery masculineKnowledgeStudyPlan = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_study_plan = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '5' and gender = 'M'");

        masculineKnowledgeStudyPlan.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeStudyPlan.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeStudyPlan.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeStudyPlanCustoms = masculineKnowledgeStudyPlan.list();

        SQLQuery femenineKnowledgeStudyPlan = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_study_plan = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_study_plan = '5' and gender = 'F'");

        femenineKnowledgeStudyPlan.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeStudyPlan.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeStudyPlan.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeStudyPlanCustoms = femenineKnowledgeStudyPlan.list();

        SQLQuery femenineUpportHeadCareer = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and upport_head_career = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and upport_head_career = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and upport_head_career = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and upport_head_career = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and upport_head_career = '5' and gender = 'F'");

        femenineUpportHeadCareer.addScalar("quantity", LongType.INSTANCE);
        femenineUpportHeadCareer.addScalar("response", StringType.INSTANCE);

        femenineUpportHeadCareer.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUpportHeadCareerCustoms = femenineUpportHeadCareer.list();

        SQLQuery masculineUpportHeadCareer = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and upport_head_career = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and upport_head_career = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and upport_head_career = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and upport_head_career = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and upport_head_career = '5' and gender = 'M'");

        masculineUpportHeadCareer.addScalar("quantity", LongType.INSTANCE);
        masculineUpportHeadCareer.addScalar("response", StringType.INSTANCE);

        masculineUpportHeadCareer.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUpportHeadCareerCustoms = masculineUpportHeadCareer.list();

        SQLQuery masculineExpertProgramCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and expert_program_course = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and expert_program_course = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and expert_program_course = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and expert_program_course = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and expert_program_course = '5' and gender = 'M'");

        masculineExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        masculineExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        masculineExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineExpertProgramCourseCustoms = masculineExpertProgramCourse.list();

        SQLQuery femenineExpertProgramCourse = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and expert_program_course = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and expert_program_course = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and expert_program_course = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and expert_program_course = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and expert_program_course = '5' and gender = 'F'");

        femenineExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        femenineExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        femenineExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineExpertProgramCourseCustoms = femenineExpertProgramCourse.list();

        SQLQuery femenineContributionExpertProgramCourse = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(contribution_expert_program_course) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(contribution_expert_program_course) = 'NO'");

        femenineContributionExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        femenineContributionExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        femenineContributionExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionExpertProgramCourseCustoms = femenineContributionExpertProgramCourse.list();

        SQLQuery masculineContributionExpertProgramCourse = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(contribution_expert_program_course) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(contribution_expert_program_course) = 'NO'");

        masculineContributionExpertProgramCourse.addScalar("quantity", LongType.INSTANCE);
        masculineContributionExpertProgramCourse.addScalar("response", StringType.INSTANCE);

        masculineContributionExpertProgramCourse.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionExpertProgramCourseCustoms = masculineContributionExpertProgramCourse.list();

        SQLQuery femenineContentCourseTopics = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and content_course_topics = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and content_course_topics = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and content_course_topics = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and content_course_topics = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and content_course_topics = '5' and gender = 'F'");

        femenineContentCourseTopics.addScalar("quantity", LongType.INSTANCE);
        femenineContentCourseTopics.addScalar("response", StringType.INSTANCE);

        femenineContentCourseTopics.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContentCourseTopicsCustoms = femenineContentCourseTopics.list();

        SQLQuery masculineContentCourseTopics = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and content_course_topics = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and content_course_topics = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and content_course_topics = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and content_course_topics = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and content_course_topics = '5' and gender = 'M'");

        masculineContentCourseTopics.addScalar("quantity", LongType.INSTANCE);
        masculineContentCourseTopics.addScalar("response", StringType.INSTANCE);

        masculineContentCourseTopics.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContentCourseTopicsCustoms = masculineContentCourseTopics.list();

        SQLQuery masculineAdequateCoursePayment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and adequate_course_payment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '5' and gender = 'M'");

        masculineAdequateCoursePayment.addScalar("quantity", LongType.INSTANCE);
        masculineAdequateCoursePayment.addScalar("response", StringType.INSTANCE);

        masculineAdequateCoursePayment.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAdequateCoursePaymentCustoms = masculineAdequateCoursePayment.list();

        SQLQuery femenineAdequateCoursePayment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and adequate_course_payment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and adequate_course_payment = '5' and gender = 'F'");

        femenineAdequateCoursePayment.addScalar("quantity", LongType.INSTANCE);
        femenineAdequateCoursePayment.addScalar("response", StringType.INSTANCE);

        masculineAdequateCoursePayment.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAdequateCoursePaymentCustoms = femenineAdequateCoursePayment.list();

        SQLQuery femenineJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and jets_participation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and jets_participation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and jets_participation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and jets_participation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and jets_participation = '5' and gender = 'F'");

        femenineJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineJetsParticipationCustoms = femenineJetsParticipation.list();

        SQLQuery masculineJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and jets_participation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and jets_participation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and jets_participation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and jets_participation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and jets_participation = '5' and gender = 'M'");

        masculineJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineJetsParticipationCustoms = masculineJetsParticipation.list();

        SQLQuery masculineContributionJetsParticipation = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(contribution_jets_participation) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(contribution_jets_participation) = 'NO'");

        masculineContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionJetsParticipationCustoms = masculineContributionJetsParticipation.list();

        SQLQuery femenineContributionJetsParticipation = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(contribution_jets_participation) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(contribution_jets_participation) = 'NO'");

        femenineContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionJetsParticipationCustoms = femenineContributionJetsParticipation.list();

        SQLQuery masculineSadisfaccionContributionJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '5' and gender = 'M'");

        masculineSadisfaccionContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        masculineSadisfaccionContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        masculineSadisfaccionContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineSadisfaccionContributionJetsParticipationCustoms = masculineSadisfaccionContributionJetsParticipation.list();

        SQLQuery femenineSadisfaccionContributionJetsParticipation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and sadisfaccion_contribution_jets_participation = '5' and gender = 'F'");

        femenineSadisfaccionContributionJetsParticipation.addScalar("quantity", LongType.INSTANCE);
        femenineSadisfaccionContributionJetsParticipation.addScalar("response", StringType.INSTANCE);

        femenineSadisfaccionContributionJetsParticipation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineSadisfaccionContributionJetsParticipationCustoms = femenineSadisfaccionContributionJetsParticipation.list();

        SQLQuery femenineKnowledgeUniversitySocialResponsability = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(knowledge_university_social_responsibility) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(knowledge_university_social_responsibility) = 'NO'");

        femenineKnowledgeUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeUniversitySocialResponsabilityCustoms = femenineKnowledgeUniversitySocialResponsability.list();

        SQLQuery masculineKnowledgeUniversitySocialResponsability = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(knowledge_university_social_responsibility) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(knowledge_university_social_responsibility) = 'NO'");

        masculineKnowledgeUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        masculineKnowledgeUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        masculineKnowledgeUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineKnowledgeUniversitySocialResponsabilityCustoms = masculineKnowledgeUniversitySocialResponsability.list();

        SQLQuery femenineContributionUniversitySocialResponsability = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '5' and gender = 'F'");

        femenineContributionUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        femenineContributionUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        femenineContributionUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionUniversitySocialResponsabilityCustoms = femenineContributionUniversitySocialResponsability.list();

        SQLQuery maculineContributionUniversitySocialResponsability = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and contribution_university_social_responsibility = '5' and gender = 'M'");

        maculineContributionUniversitySocialResponsability.addScalar("quantity", LongType.INSTANCE);
        maculineContributionUniversitySocialResponsability.addScalar("response", StringType.INSTANCE);

        maculineContributionUniversitySocialResponsability.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> maculineContributionUniversitySocialResponsabilityCustoms = maculineContributionUniversitySocialResponsability.list();

        SQLQuery maculineKnowledgeUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '5' and gender = 'M'");

        maculineKnowledgeUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        maculineKnowledgeUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        maculineKnowledgeUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> maculineKnowledgeUniversityInternacionalizationCustoms = maculineKnowledgeUniversityInternacionalization.list();

        SQLQuery femenineKnowledgeUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and knowledge_university_internationalization = '5' and gender = 'F'");

        femenineKnowledgeUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        femenineKnowledgeUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        femenineKnowledgeUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineKnowledgeUniversityInternacionalizationCustoms = femenineKnowledgeUniversityInternacionalization.list();

        SQLQuery femenineContributionUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '5' and gender = 'F'");

        femenineContributionUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        femenineContributionUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        femenineContributionUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineContributionUniversityInternacionalizationCustoms = femenineContributionUniversityInternacionalization.list();

        SQLQuery masculineContributionUniversityInternacionalization = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and contribution_university_internationalization = '5' and gender = 'M'");

        masculineContributionUniversityInternacionalization.addScalar("quantity", LongType.INSTANCE);
        masculineContributionUniversityInternacionalization.addScalar("response", StringType.INSTANCE);

        masculineContributionUniversityInternacionalization.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineContributionUniversityInternacionalizationCustoms = masculineContributionUniversityInternacionalization.list();

        SQLQuery masculineStudentUniversityPride = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and student_university_pride = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and student_university_pride = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and student_university_pride = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and student_university_pride = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and student_university_pride = '5' and gender = 'M'");

        masculineStudentUniversityPride.addScalar("quantity", LongType.INSTANCE);
        masculineStudentUniversityPride.addScalar("response", StringType.INSTANCE);

        masculineStudentUniversityPride.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineStudentUniversityPrideCustoms = masculineStudentUniversityPride.list();

        SQLQuery femenineStudentUniversityPride = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and student_university_pride = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and student_university_pride = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and student_university_pride = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and student_university_pride = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and student_university_pride = '5' and gender = 'F'");

        femenineStudentUniversityPride.addScalar("quantity", LongType.INSTANCE);
        femenineStudentUniversityPride.addScalar("response", StringType.INSTANCE);

        femenineStudentUniversityPride.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineStudentUniversityPrideCustoms = femenineStudentUniversityPride.list();

        SQLQuery femenineProfessionalImage = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and professional_image = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and professional_image = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and professional_image = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and professional_image = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and professional_image = '5' and gender = 'F'");

        femenineProfessionalImage.addScalar("quantity", LongType.INSTANCE);
        femenineProfessionalImage.addScalar("response", StringType.INSTANCE);

        femenineProfessionalImage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineProfessionalImageCustoms = femenineProfessionalImage.list();

        SQLQuery masculineProfessionalImage = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and professional_image = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and professional_image = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and professional_image = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and professional_image = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and professional_image = '5' and gender = 'M'");

        masculineProfessionalImage.addScalar("quantity", LongType.INSTANCE);
        masculineProfessionalImage.addScalar("response", StringType.INSTANCE);

        masculineProfessionalImage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineProfessionalImageCustoms = masculineProfessionalImage.list();

        SQLQuery masculineCareerQuality = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and career_quality = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and career_quality = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and career_quality = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and career_quality = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and career_quality = '5' and gender = 'M'");

        masculineCareerQuality.addScalar("quantity", LongType.INSTANCE);
        masculineCareerQuality.addScalar("response", StringType.INSTANCE);

        masculineCareerQuality.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineCareerQualityCustoms = masculineCareerQuality.list();

        SQLQuery femenineCareerQuality = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and career_quality = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and career_quality = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and career_quality = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and career_quality = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and career_quality = '5' and gender = 'F'");

        femenineCareerQuality.addScalar("quantity", LongType.INSTANCE);
        femenineCareerQuality.addScalar("response", StringType.INSTANCE);

        femenineCareerQuality.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineCareerQualityCustoms = femenineCareerQuality.list();

        SQLQuery femenineInfrastructure = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and infrastructure = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and infrastructure = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and infrastructure = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and infrastructure = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and infrastructure = '5' and gender = 'F'");

        femenineInfrastructure.addScalar("quantity", LongType.INSTANCE);
        femenineInfrastructure.addScalar("response", StringType.INSTANCE);

        femenineInfrastructure.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineInfrastructureCustoms = femenineInfrastructure.list();

        SQLQuery masculineInfrastructure = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and infrastructure = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and infrastructure = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and infrastructure = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and infrastructure = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and infrastructure = '5' and gender = 'M'");

        masculineInfrastructure.addScalar("quantity", LongType.INSTANCE);
        masculineInfrastructure.addScalar("response", StringType.INSTANCE);

        masculineInfrastructure.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineInfrastructureCustoms = masculineInfrastructure.list();

        SQLQuery masculineClassSchedule = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and class_schedule = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and class_schedule = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and class_schedule = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and class_schedule = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and class_schedule = '5' and gender = 'M'");

        masculineClassSchedule.addScalar("quantity", LongType.INSTANCE);
        masculineClassSchedule.addScalar("response", StringType.INSTANCE);

        masculineClassSchedule.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineClassScheduleCustoms = masculineClassSchedule.list();

        SQLQuery femenineClassSchedule = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and class_schedule = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and class_schedule = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and class_schedule = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and class_schedule = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and class_schedule = '5' and gender = 'F'");

        femenineClassSchedule.addScalar("quantity", LongType.INSTANCE);
        femenineClassSchedule.addScalar("response", StringType.INSTANCE);

        femenineClassSchedule.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineClassScheduleCustoms = femenineClassSchedule.list();

        SQLQuery femeninePrestigeInstitution = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and prestige_institution = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and prestige_institution = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and prestige_institution = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and prestige_institution = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and prestige_institution = '5' and gender = 'F'");

        femeninePrestigeInstitution.addScalar("quantity", LongType.INSTANCE);
        femeninePrestigeInstitution.addScalar("response", StringType.INSTANCE);

        femeninePrestigeInstitution.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femeninePrestigeInstitutionCustoms = femeninePrestigeInstitution.list();

        SQLQuery masculinePrestigeInstitution = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and prestige_institution = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and prestige_institution = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and prestige_institution = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and prestige_institution = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and prestige_institution = '5' and gender = 'M'");

        masculinePrestigeInstitution.addScalar("quantity", LongType.INSTANCE);
        masculinePrestigeInstitution.addScalar("response", StringType.INSTANCE);

        masculinePrestigeInstitution.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculinePrestigeInstitutionCustoms = masculinePrestigeInstitution.list();

        SQLQuery masculineGreatTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and great_teachers = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and great_teachers = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and great_teachers = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and great_teachers = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and great_teachers = '5' and gender = 'M'");

        masculineGreatTeachers.addScalar("quantity", LongType.INSTANCE);
        masculineGreatTeachers.addScalar("response", StringType.INSTANCE);

        masculineGreatTeachers.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineGreatTeachersCustoms = masculineGreatTeachers.list();

        SQLQuery femenineGreatTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and great_teachers = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and great_teachers = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and great_teachers = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and great_teachers = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and great_teachers = '5' and gender = 'F'");

        femenineGreatTeachers.addScalar("quantity", LongType.INSTANCE);
        femenineGreatTeachers.addScalar("response", StringType.INSTANCE);

        femenineGreatTeachers.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineGreatTeachersCustoms = femenineGreatTeachers.list();

        SQLQuery femenineModularSystem = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and modular_system = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and modular_system = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and modular_system = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and modular_system = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and modular_system = '5' and gender = 'F'");

        femenineModularSystem.addScalar("quantity", LongType.INSTANCE);
        femenineModularSystem.addScalar("response", StringType.INSTANCE);

        femenineModularSystem.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineModularSystemCustoms = femenineModularSystem.list();

        SQLQuery masculineModularSystem = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and modular_system = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and modular_system = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and modular_system = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and modular_system = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and modular_system = '5' and gender = 'M'");

        masculineModularSystem.addScalar("quantity", LongType.INSTANCE);
        masculineModularSystem.addScalar("response", StringType.INSTANCE);

        masculineModularSystem.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineModularSystemCustoms = masculineModularSystem.list();

        SQLQuery masculineAccesiblePrice = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and accesible_price = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and accesible_price = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and accesible_price = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and accesible_price = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and accesible_price = '5' and gender = 'M'");

        masculineAccesiblePrice.addScalar("quantity", LongType.INSTANCE);
        masculineAccesiblePrice.addScalar("response", StringType.INSTANCE);

        masculineAccesiblePrice.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAccesiblePriceCustoms = masculineAccesiblePrice.list();

        SQLQuery femenineAccesiblePrice = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and accesible_price = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and accesible_price = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and accesible_price = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and accesible_price = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and accesible_price = '5' and gender = 'F'");

        femenineAccesiblePrice.addScalar("quantity", LongType.INSTANCE);
        femenineAccesiblePrice.addScalar("response", StringType.INSTANCE);

        femenineAccesiblePrice.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAccesiblePriceCustoms = femenineAccesiblePrice.list();

        SQLQuery femenineReferences = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and census_student.references = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and census_student.references = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and census_student.references = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and census_student.references = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and census_student.references = '5' and gender = 'F'");

        femenineReferences.addScalar("quantity", LongType.INSTANCE);
        femenineReferences.addScalar("response", StringType.INSTANCE);

        femenineReferences.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineReferencesCustoms = femenineReferences.list();

        SQLQuery masculineReferences = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and census_student.references = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and census_student.references = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and census_student.references = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and census_student.references = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and census_student.references = '5' and gender = 'M'");

        masculineReferences.addScalar("quantity", LongType.INSTANCE);
        masculineReferences.addScalar("response", StringType.INSTANCE);

        masculineReferences.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineReferencesCustoms = masculineReferences.list();

        SQLQuery femenineLaboratoriesEquipment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and laboratories_equipment = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '5' and gender = 'F'");

        femenineLaboratoriesEquipment.addScalar("quantity", LongType.INSTANCE);
        femenineLaboratoriesEquipment.addScalar("response", StringType.INSTANCE);

        femenineLaboratoriesEquipment.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineLaboratoriesEquipmentCustoms = femenineLaboratoriesEquipment.list();

        SQLQuery masculineLaboratoriesEquipment = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and laboratories_equipment = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and laboratories_equipment = '5' and gender = 'M'");

        masculineLaboratoriesEquipment.addScalar("quantity", LongType.INSTANCE);
        masculineLaboratoriesEquipment.addScalar("response", StringType.INSTANCE);

        masculineLaboratoriesEquipment.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineLaboratoriesEquipmentCustoms = masculineLaboratoriesEquipment.list();

        SQLQuery masculineComunication = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and comunication = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and comunication = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and comunication = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and comunication = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and comunication = '5' and gender = 'M'");

        masculineComunication.addScalar("quantity", LongType.INSTANCE);
        masculineComunication.addScalar("response", StringType.INSTANCE);

        masculineComunication.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineComunicationCustoms = masculineComunication.list();

        SQLQuery femenineComunication = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and comunication = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and comunication = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and comunication = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and comunication = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and comunication = '5' and gender = 'F'");

        femenineComunication.addScalar("quantity", LongType.INSTANCE);
        femenineComunication.addScalar("response", StringType.INSTANCE);

        femenineComunication.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineComunicationCustoms = femenineComunication.list();

        SQLQuery femenineGraduateGreatStudent = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and graduate_great_student = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '5' and gender = 'F'");

        femenineGraduateGreatStudent.addScalar("quantity", LongType.INSTANCE);
        femenineGraduateGreatStudent.addScalar("response", StringType.INSTANCE);

        femenineGraduateGreatStudent.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineGraduateGreatStudentCustoms = femenineGraduateGreatStudent.list();

        SQLQuery masculineGraduateGreatStudent = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and graduate_great_student = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and graduate_great_student = '5' and gender = 'M'");

        masculineGraduateGreatStudent.addScalar("quantity", LongType.INSTANCE);
        masculineGraduateGreatStudent.addScalar("response", StringType.INSTANCE);

        masculineGraduateGreatStudent.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineGraduateGreatStudentCustoms = masculineGraduateGreatStudent.list();

        SQLQuery masculineAdequateRequirementStudies = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '5' and gender = 'M'");

        masculineAdequateRequirementStudies.addScalar("quantity", LongType.INSTANCE);
        masculineAdequateRequirementStudies.addScalar("response", StringType.INSTANCE);

        masculineAdequateRequirementStudies.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineAdequateRequirementStudiesCustoms = masculineAdequateRequirementStudies.list();

        SQLQuery femenineAdequateRequirementStudies = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and adequate_requirement_studies = '5' and gender = 'F'");

        femenineAdequateRequirementStudies.addScalar("quantity", LongType.INSTANCE);
        femenineAdequateRequirementStudies.addScalar("response", StringType.INSTANCE);

        femenineAdequateRequirementStudies.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineAdequateRequirementStudiesCustoms = femenineAdequateRequirementStudies.list();

        SQLQuery femenineQualificationTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and qualification_teachers = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '5' and gender = 'F'");

        femenineQualificationTeachers.addScalar("quantity", LongType.INSTANCE);
        femenineQualificationTeachers.addScalar("response", StringType.INSTANCE);

        femenineQualificationTeachers.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineQualificationTeachersCustoms = femenineQualificationTeachers.list();

        SQLQuery masculineQualificationTeachers = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and qualification_teachers = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and qualification_teachers = '5' and gender = 'M'");

        masculineQualificationTeachers.addScalar("quantity", LongType.INSTANCE);
        masculineQualificationTeachers.addScalar("response", StringType.INSTANCE);

        masculineQualificationTeachers.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineQualificationTeachersCustoms = masculineQualificationTeachers.list();

        SQLQuery masculineStudentService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and student_service = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and student_service = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and student_service = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and student_service = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and student_service = '5' and gender = 'M'");

        masculineStudentService.addScalar("quantity", LongType.INSTANCE);
        masculineStudentService.addScalar("response", StringType.INSTANCE);

        masculineStudentService.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineStudentServiceCustoms = masculineStudentService.list();

        SQLQuery femenineStudentService = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and student_service = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and student_service = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and student_service = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and student_service = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and student_service = '5' and gender = 'F'");

        femenineStudentService.addScalar("quantity", LongType.INSTANCE);
        femenineStudentService.addScalar("response", StringType.INSTANCE);

        femenineStudentService.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineStudentServiceCustoms = femenineStudentService.list();

        SQLQuery femenineReputationMiddleCity = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and reputation_middle_city = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '5' and gender = 'F'");

        femenineReputationMiddleCity.addScalar("quantity", LongType.INSTANCE);
        femenineReputationMiddleCity.addScalar("response", StringType.INSTANCE);

        femenineReputationMiddleCity.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineReputationMiddleCityCustoms = femenineReputationMiddleCity.list();

        SQLQuery masculineReputationMiddleCity = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and reputation_middle_city = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and reputation_middle_city = '5' and gender = 'M'");

        masculineReputationMiddleCity.addScalar("quantity", LongType.INSTANCE);
        masculineReputationMiddleCity.addScalar("response", StringType.INSTANCE);

        masculineReputationMiddleCity.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineReputationMiddleCityCustoms = masculineReputationMiddleCity.list();

        SQLQuery masculineUniversitySupervision = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and university_supervision = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and university_supervision = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and university_supervision = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and university_supervision = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and university_supervision = '5' and gender = 'M'");

        masculineUniversitySupervision.addScalar("quantity", LongType.INSTANCE);
        masculineUniversitySupervision.addScalar("response", StringType.INSTANCE);

        masculineUniversitySupervision.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUniversitySupervisionCustoms = masculineUniversitySupervision.list();

        SQLQuery femenineUniversitySupervision = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and university_supervision = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and university_supervision = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and university_supervision = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and university_supervision = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and university_supervision = '5' and gender = 'F'");

        femenineUniversitySupervision.addScalar("quantity", LongType.INSTANCE);
        femenineUniversitySupervision.addScalar("response", StringType.INSTANCE);

        femenineUniversitySupervision.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUniversitySupervisionCustoms = femenineUniversitySupervision.list();

        SQLQuery femenineUniversityRecommendation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and university_recommendation = '1' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and university_recommendation = '2' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and university_recommendation = '3' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and university_recommendation = '4' and gender = 'F'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and university_recommendation = '5' and gender = 'F'");

        femenineUniversityRecommendation.addScalar("quantity", LongType.INSTANCE);
        femenineUniversityRecommendation.addScalar("response", StringType.INSTANCE);

        femenineUniversityRecommendation.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineUniversityRecommendationCustoms = femenineUniversityRecommendation.list();

        SQLQuery masculineUniversityRecommendation = currentSession().createSQLQuery("select \"count\"(*) AS quantity, '1' as response " +
                "from census_student where id_career = "+idCareer+" and university_recommendation = '1' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '2' as response from census_student where id_career = "+idCareer+" and university_recommendation = '2' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '3' as response from census_student where id_career = "+idCareer+" and university_recommendation = '3' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '4' as response from census_student where id_career = "+idCareer+" and university_recommendation = '4' and gender = 'M'\n" +
                "UNION ALL\n" +
                "select \"count\"(*) AS quantity, '5' as response from census_student where id_career = "+idCareer+" and university_recommendation = '5' and gender = 'M'");

        masculineUniversityRecommendation.addScalar("quantity", LongType.INSTANCE);
        masculineUniversityRecommendation.addScalar("response", StringType.INSTANCE);

        masculineUniversityRecommendation.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineUniversityRecommendationCustoms = masculineUniversityRecommendation.list();

        SQLQuery femenineTypeLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(type_language) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'F' AND UPPER(type_language) = 'NO'");

        femenineTypeLanguage.addScalar("quantity", LongType.INSTANCE);
        femenineTypeLanguage.addScalar("response", StringType.INSTANCE);

        femenineTypeLanguage.setResultTransformer(Transformers.aliasToBean(FemenineAnswers.class));
        List<FemenineAnswers> femenineTypeLanguageCustoms = femenineTypeLanguage.list();

        SQLQuery masculineTypeLanguage = currentSession().createSQLQuery("SELECT count(*) as quantity,'SI' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(type_language) = 'SI' " +
                "UNION " +
                "SELECT count(*) as quantity, 'NO' as response from census_student where id_career = "+idCareer+" and gender = 'M' AND UPPER(type_language) = 'NO'");

        masculineTypeLanguage.addScalar("quantity", LongType.INSTANCE);
        masculineTypeLanguage.addScalar("response", StringType.INSTANCE);

        masculineTypeLanguage.setResultTransformer(Transformers.aliasToBean(MasculineAnswers.class));
        List<MasculineAnswers> masculineTypeLanguageCustoms = masculineTypeLanguage.list();

        careerAnswers.setTypeLanguageMasculine(masculineTypeLanguageCustoms);
        careerAnswers.setTypeLanguageFemenine(femenineTypeLanguageCustoms);
        careerAnswers.setUniversityRecommendationMasculine(masculineUniversityRecommendationCustoms);
        careerAnswers.setUniversityRecommendationFemenine(femenineUniversityRecommendationCustoms);
        careerAnswers.setUniversitySupervisionFemenine(femenineUniversitySupervisionCustoms);
        careerAnswers.setUniversitySupervisionMasculine(masculineUniversitySupervisionCustoms);
        careerAnswers.setReputationMiddleCityMasculine(masculineReputationMiddleCityCustoms);
        careerAnswers.setReputationMiddleCityFemenine(femenineReputationMiddleCityCustoms);
        careerAnswers.setStudentServiceFemenine(femenineStudentServiceCustoms);
        careerAnswers.setStudentServiceMasculine(masculineStudentServiceCustoms);
        careerAnswers.setQualificationTeachersMasculine(masculineQualificationTeachersCustoms);
        careerAnswers.setQualificationTeachersFemenine(femenineQualificationTeachersCustoms);
        careerAnswers.setAdequateRequirementStudiesFemenine(femenineAdequateRequirementStudiesCustoms);
        careerAnswers.setAdequateRequirementStudiesMasculine(masculineAdequateRequirementStudiesCustoms);
        careerAnswers.setGraduateGreatStudentMasculine(masculineGraduateGreatStudentCustoms);
        careerAnswers.setGraduateGreatStudentFemenine(femenineGraduateGreatStudentCustoms);
        careerAnswers.setCommunicationFemenine(femenineComunicationCustoms);
        careerAnswers.setCommunicationMasculine(masculineComunicationCustoms);
        careerAnswers.setLaboratoriesEquipmentMasculine(masculineLaboratoriesEquipmentCustoms);
        careerAnswers.setLaboratoriesEquipmentFemenine(femenineLaboratoriesEquipmentCustoms);
        careerAnswers.setReferencesMasculine(masculineReferencesCustoms);
        careerAnswers.setReferencesFemenine(femenineReferencesCustoms);
        careerAnswers.setAccesiblePriceFemenine(femenineAccesiblePriceCustoms);
        careerAnswers.setAccesiblePriceMasculine(masculineAccesiblePriceCustoms);
        careerAnswers.setModularSystemMasculine(masculineModularSystemCustoms);
        careerAnswers.setModularSystemFemenine(femenineModularSystemCustoms);
        careerAnswers.setGreatTeachersFemenine(femenineGreatTeachersCustoms);
        careerAnswers.setGreatTeachersMasculine(masculineGreatTeachersCustoms);
        careerAnswers.setPrestigeInstitutionMasculine(masculinePrestigeInstitutionCustoms);
        careerAnswers.setPrestigeInstitutionFemenine(femeninePrestigeInstitutionCustoms);
        careerAnswers.setClassScheduleFemenine(femenineClassScheduleCustoms);
        careerAnswers.setClassScheduleMasculine(masculineClassScheduleCustoms);
        careerAnswers.setInfrastructureMasculine(masculineInfrastructureCustoms);
        careerAnswers.setInfrastructureFemenine(femenineInfrastructureCustoms);
        careerAnswers.setCareerQualityFemenine(femenineCareerQualityCustoms);
        careerAnswers.setCareerQualityMasculine(masculineCareerQualityCustoms);
        careerAnswers.setProfessionalImageMasculine(masculineProfessionalImageCustoms);
        careerAnswers.setProfessionalImageFemenine(femenineProfessionalImageCustoms);
        careerAnswers.setStudentUniversityPrideFemenine(femenineStudentUniversityPrideCustoms);
        careerAnswers.setStudentUniversityPrideMasculine(masculineStudentUniversityPrideCustoms);
        careerAnswers.setContributionUniversityInternacionalizationMasculine(masculineContributionUniversityInternacionalizationCustoms);
        careerAnswers.setContributionUniversityInternacionalizationFemenine(femenineContributionUniversityInternacionalizationCustoms);
        careerAnswers.setKnowledgeUniversityInternacionalizationFemenine(femenineKnowledgeUniversityInternacionalizationCustoms);
        careerAnswers.setKnowledgeUniversityInternacionalizationMasculine(maculineKnowledgeUniversityInternacionalizationCustoms);
        careerAnswers.setContributionUniversitySocialResponsabilityMasculine(maculineContributionUniversitySocialResponsabilityCustoms);
        careerAnswers.setContributionUniversitySocialResponsabilityFemenine(femenineContributionUniversitySocialResponsabilityCustoms);
        careerAnswers.setKnowledgeUniversitySocialResponsabilityFemenine(femenineKnowledgeUniversitySocialResponsabilityCustoms);
        careerAnswers.setKnowledgeUniversitySocialResponsabilityMasculine(masculineKnowledgeUniversitySocialResponsabilityCustoms);
        careerAnswers.setSadisfaccionContributionJetsParticipationFemenine(femenineSadisfaccionContributionJetsParticipationCustoms);
        careerAnswers.setSadisfaccionContributionJetsParticipationMasculine(masculineSadisfaccionContributionJetsParticipationCustoms);
        careerAnswers.setContributionJetsParticipationFemenine(femenineContributionJetsParticipationCustoms);
        careerAnswers.setContributionJetsParticipationMasculine(masculineContributionJetsParticipationCustoms);
        careerAnswers.setIsWorkingFemenine(femenineIsWorkingCustoms);
        careerAnswers.setIsWorkingMasculine(masculineIsWorkingCustoms);
        careerAnswers.setSuggestionsMetFemenine(femenineSuggestionsMetCustoms);
        careerAnswers.setSuggestionsMetMasculine(masculineSuggestionsMetCustoms);
        careerAnswers.setCourseProgrammingMasculine(masculineCourseProgrammingCustoms);
        careerAnswers.setCourseProgrammingFemenine(femenineCourseProgrammingCustoms);
        careerAnswers.setAnotherLanguageFemenine(femenineAnotherLanguageCustoms);
        careerAnswers.setAnotherLanguageMasculine(masculineAnotherLanguageCustoms);
        careerAnswers.setBilingualSchoolMasculine(masculineBilingualSchoolCustoms);
        careerAnswers.setBilingualSchoolFemenine(femenineBilingualSchoolCustoms);
        careerAnswers.setAccessCourseNoteFemenine(femenineAccessCourseNoteCustoms);
        careerAnswers.setAccessCourseNoteMasculine(masculineAccessCourseNoteCustoms);
        careerAnswers.setClassroomsMasculine(masculineClassroomsCustoms);
        careerAnswers.setClassroomsFemenine(femenineClassroomsCustoms);
        careerAnswers.setLaboratoriesFemenine(femenineLaboratoriesCustoms);
        careerAnswers.setLaboratoriesMasculine(masculineLaboratoriesCustoms);
        careerAnswers.setLaboratoriesAssignmentMasculine(masculineLaboratoriesAssignmentsCustoms);
        careerAnswers.setLaboratoriesAssignmentFemenine(femenineLaboratoriesAssignmentsCustoms);
        careerAnswers.setLibraryServiceFemenine(femenineLibraryServiceCustoms);
        careerAnswers.setLibraryServiceMasculine(masculineLibraryServiceCustoms);
        careerAnswers.setTreatmentPlatformStaffMasculine(masculineTreatmentPlatformStaffCustoms);
        careerAnswers.setTreatmentPlatformStaffFemenine(femenineTreatmentPlatformStaffCustoms);
        careerAnswers.setQuickAttentionPlatformResponseFemenine(femenineQuickAttentionPlatformStaffCustoms);
        careerAnswers.setQuickAttentionPlatformResponseMasculine(masculineQuickAttentionPlatformStaffCustoms);
        careerAnswers.setKnowledgePlatformStaffMasculine(masculineKnowledgePlatformStaffCustoms);
        careerAnswers.setKnowledgePlatformStaffFemenine(femenineKnowledgePlatformStaffCustoms);
        careerAnswers.setPlatformStaffResponseFemenine(femeninePlatformStaffResponseCustoms);
        careerAnswers.setPlatformStaffResponseMasculine(masculinePlatformStaffResponseCustoms);
        careerAnswers.setCareerInformationMasculine(masculineCareerInformationCustoms);
        careerAnswers.setCareerInformationFemenine(femenineCareerInformationCustoms);
        careerAnswers.setPaymentInformationFemenine(femeninePaymentInformationCustoms);
        careerAnswers.setPaymentInformationMasculine(masculinePaymentInformationCustoms);
        careerAnswers.setPunctualityClassesMasculine(masculinePunctualityClassesCustoms);
        careerAnswers.setPunctualityClassesFemenine(femeninePunctualityClassesCustoms);
        careerAnswers.setKnowledgeCourseFemenine(femenineKnowledgeCourseCustoms);
        careerAnswers.setKnowledgeCourseMasculine(masculineKnowledgeCourseCustoms);
        careerAnswers.setUsedToolsClassesMasculine(masculineUsedToolsClassesCustoms);
        careerAnswers.setUsedToolsClassesFemenine(femenineUsedToolsClassesCustoms);
        careerAnswers.setSupportMaterialFemenine(femenineSupportMaterialCustoms);
        careerAnswers.setSupportMaterialMasculine(masculineSupportMaterialCustoms);
        careerAnswers.setCriteriaEvaluationFemenine(femenineCriteriaEvaluationCustoms);
        careerAnswers.setCriteriaEvaluationMasculine(masculineCriteriaEvaluationCustoms);
        careerAnswers.setProfessionalExperienceFemenine(femenineProfesionalExperienceCustoms);
        careerAnswers.setProfessionalExperienceMasculine(masculineProfesionalExperienceCustoms);
        careerAnswers.setKnowledgeStudyPlanMasculine(masculineKnowledgeStudyPlanCustoms);
        careerAnswers.setKnowledgeStudyPlanFemenine(femenineKnowledgeStudyPlanCustoms);
        careerAnswers.setUpportHeadCareerFemenine(femenineUpportHeadCareerCustoms);
        careerAnswers.setUpportHeadCareerMasculine(masculineUpportHeadCareerCustoms);
        careerAnswers.setExpertProgramCourseMasculine(masculineExpertProgramCourseCustoms);
        careerAnswers.setExpertProgramCourseFemenine(femenineExpertProgramCourseCustoms);
        careerAnswers.setContributionExpertProgramCourseFemenine(femenineContributionExpertProgramCourseCustoms);
        careerAnswers.setContributionExpertProgramCourseMasculine(masculineContributionExpertProgramCourseCustoms);
        careerAnswers.setContentCourseTopicFemenine(femenineContentCourseTopicsCustoms);
        careerAnswers.setContentCourseTopicMasculine(masculineContentCourseTopicsCustoms);
        careerAnswers.setAdequateCoursePaymentMasculine(masculineAdequateCoursePaymentCustoms);
        careerAnswers.setAdequateCoursePaymentFemenine(femenineAdequateCoursePaymentCustoms);
        careerAnswers.setJetsParticipationFemenine(femenineJetsParticipationCustoms);
        careerAnswers.setJetsParticipationMasculine(masculineJetsParticipationCustoms);

        return careerAnswers;
    }
}
