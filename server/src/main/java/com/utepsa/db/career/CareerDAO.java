package com.utepsa.db.career;

import com.utepsa.models.Career;

import java.util.List;

/**
 * Created by David on 18/01/2017.
 */
public interface CareerDAO {
    long create(Career career) throws Exception;
    List<Career> getAll() throws Exception;
    List<Career> getByGrade(String grade) throws Exception;
    Career getByCodeUtepsa(String codeUtepsa) throws Exception;
    Career getById(long id) throws Exception;
}
