package com.utepsa.db.notification;

import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Notification;

import java.util.List;

/**
 * Created by shigeots on 03-11-16.
 */
public interface NotificationDAO {
    long create (Notification notification) throws Exception;
    Notification getById (long id) throws Exception;
    List<Notification> getAll() throws Exception;
    List<Notification> getByStudent(String registerNumber, long idCareer, int pageSize, int startRow) throws Exception;
    void updateNotifications(long id, String content,CategoryNotification idCategory)throws Exception;
    List<Notification> getByCreator (long idCreator) throws Exception;
}
