package com.utepsa.db.notification;

import com.google.inject.Inject;
import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Notification;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by shigeots on 03-11-16.
 */
public class NotificationRealDAO extends AbstractDAO<Notification> implements NotificationDAO {
    @Inject
    public NotificationRealDAO (SessionFactory factory) {
        super(factory);
    }

    @Override
    public long create(Notification notification) {
        final Notification created = persist(notification);
        if(created == null) try {
            throw new Exception("The notification was not created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return created.getId();
    }

    @Override
    public Notification getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<Notification> getAll() throws Exception {
        return currentSession().createCriteria(Notification.class).list();
    }

    @Override
    public List<Notification> getByStudent(String registerNumber, long idCareer, int pageSize, int startRow) throws Exception {
        return list(namedQuery("com.utepsa.models.Notification.getByStudent")
                .setParameter("registerNumber", registerNumber )
                .setParameter("idCareer", Long.toString(idCareer))
                .setFirstResult(startRow)
                .setMaxResults(pageSize));
    }

    @Override
    public void updateNotifications(long id, String content, CategoryNotification idCategory)throws Exception
    {
        Notification notification = get(id);
        notification.setCategory(idCategory);
        notification.setContent(content);
    }

    @Override
    public List<Notification> getByCreator(long idCreator) throws Exception {
        SQLQuery notifications = currentSession().createSQLQuery("(SELECT n.* FROM notification n INNER JOIN credential_administrator cadm " +
                "on n.id_creator = cadm.id WHERE cadm.id = "+idCreator+") " +
                "UNION (SELECT n.* FROM notification n INNER JOIN notification_audience na on na.id_notification = n.id  " +
                "INNER JOIN type_audience ta on ta.id = na.id_type WHERE ta.id = 1)");
        return notifications.addEntity(Notification.class).list();
    }
}
