package com.utepsa.db.photo;

import com.utepsa.models.Photo;

import java.util.List;

/**
 * Created by leonardo on 21-07-17.
 */
public interface PhotoDAO {

    Photo getById(long id) throws Exception;
    long create(Photo photo) throws Exception;
    List<Photo> getByAcademicEvent(long idAcademicEvent) throws Exception;
}
