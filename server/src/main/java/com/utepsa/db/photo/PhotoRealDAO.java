package com.utepsa.db.photo;

import com.google.inject.Inject;
import com.utepsa.models.Photo;
import com.utepsa.models.AcademicEvent;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by leonardo on 21-07-17.
 */
public class PhotoRealDAO extends AbstractDAO<Photo> implements PhotoDAO {

    @Inject
    public PhotoRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Photo photo) throws Exception{
        return persist(photo).getId();
    }

    @Override
    public Photo getById(long id) throws Exception{
        return getById(id);
    }

    @Override
    public List<Photo> getByAcademicEvent(long idAcademicEvent) throws Exception{
        return currentSession().createSQLQuery(String.format("select photo.id,id_event,direction from photo,academic_event where photo.id_event = %d", idAcademicEvent)).addEntity(Photo.class).list();
    }

}
