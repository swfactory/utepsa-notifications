package com.utepsa.db.careerCourse;

import com.utepsa.models.CareerCourse;

import java.util.List;

/**
 * Created by David on 22/01/2017.
 */
public interface CareerCourseDAO {
    void create(CareerCourse careerCourse) throws Exception;
    List<CareerCourse> getAll() throws Exception;
    List<CareerCourse> getCoursesByCareer(long idCareer, int pensum) throws Exception;
    CareerCourse getCourseByCoursePensum(long idCourse, long idCareer, int pensum) throws Exception;
}
