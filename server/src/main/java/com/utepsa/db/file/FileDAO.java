package com.utepsa.db.file;
import com.utepsa.models.File;

import java.util.List;

/**
 * Created by shigeots on 09-11-16.
 */
public interface FileDAO {
    long create(File file) throws Exception;
    File getById(long id) throws Exception;
    List<File> getAvailableByStudent(long idCareer) throws Exception;
    List<File> getAllAvailable() throws Exception;
}
