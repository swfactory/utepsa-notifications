package com.utepsa.db.file;

import com.google.inject.Inject;
import com.utepsa.api.views.ViewFile;
import com.utepsa.models.File;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by shigeots on 09-11-16.
 */
public class FileRealDAO extends AbstractDAO<File> implements FileDAO {
    @Inject
    public FileRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(File file) throws Exception {
        final File created = persist(file);
        if(created == null) throw new Exception("The file was not created.");
        return created.getId();
    }

    @Override
    public File getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<File> getAvailableByStudent(long idCareer) throws Exception {
        return list(namedQuery("com.utepsa.models.File.getAvailableByStudent")
                .setParameter("idCareer", Long.toString(idCareer)));
    }

    @Override
    public List<File> getAllAvailable() throws Exception {
        return currentSession().createCriteria(File.class)
                .add(Restrictions.eq("state", true)).list();
    }
}
