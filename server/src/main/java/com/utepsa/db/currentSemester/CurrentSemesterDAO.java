package com.utepsa.db.currentSemester;

import com.utepsa.models.CurrentSemester;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
public interface CurrentSemesterDAO {

    CurrentSemester getCurrentSemester() throws Exception;
}
