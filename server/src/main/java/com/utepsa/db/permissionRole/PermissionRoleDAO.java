package com.utepsa.db.permissionRole;

import com.utepsa.models.PermissionRole;

import java.util.List;

/**
 * Created by Gerardo on 14/02/2017.
 */
public interface PermissionRoleDAO {
    List<PermissionRole> getByIdRole(long id) throws Exception;
    PermissionRole getByRoleAndPermission (long idRole, long idPermission)throws Exception;
    void modify(List<PermissionRole> permissionRoles) throws Exception;
}
