package com.utepsa.db.eventCourse;

import com.google.inject.Inject;
import com.utepsa.models.EventCareer;
import com.utepsa.models.EventCourse;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Leonardo on 22/07/2017.
 */
public class EventCourseRealDAO extends AbstractDAO<EventCourse> implements EventCourseDAO {

    @Inject
    public EventCourseRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(EventCourse eventCourse) throws Exception{
        return persist(eventCourse).getId();
    }

    @Override
    public EventCourse getById(long id) throws Exception {
        return getById(id);

    }
}
