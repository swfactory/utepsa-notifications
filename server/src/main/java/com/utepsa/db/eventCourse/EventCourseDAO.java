package com.utepsa.db.eventCourse;

import com.utepsa.models.EventCourse;

/**
 * Created by Leonardo on 22/07/2017.
 */
public interface EventCourseDAO {
    EventCourse getById(long id) throws Exception;
    long create(EventCourse eventCourse) throws Exception;

}
