package com.utepsa.db.eventType;

import com.utepsa.models.EventType;

/**
 * Created by leonardo on 21-07-17.
 */
public interface EventTypeDAO {
    EventType getById(long id) throws Exception;
    long create(EventType eventType) throws Exception;

}
