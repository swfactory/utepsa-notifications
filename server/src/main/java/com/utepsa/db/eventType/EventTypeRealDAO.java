package com.utepsa.db.eventType;

import com.google.inject.Inject;
import com.utepsa.models.EventType;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by leonardo on 21-07-17.
 */
public class EventTypeRealDAO extends AbstractDAO<EventType> implements EventTypeDAO {

    @Inject
    public EventTypeRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public EventType getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public long create(EventType eventType) throws Exception{
        return persist(eventType).getId();
    }

}
