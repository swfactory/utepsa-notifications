package com.utepsa.db.credential;

import com.utepsa.models.Credential;
import com.utepsa.models.Student;

import java.util.List;

/**
 * Created by david on 19/10/16.
 */
public interface CredentialDAO {
    long create(Credential credential);
    Credential login(String username, String password) throws Exception;
    void resetPasswordOfCredential(Credential credential) throws Exception;
    Credential getById(long id) throws Exception;
    Credential getByIdStudent(Student idStudent) throws Exception;
    void registerLastConnection(Credential credential) throws Exception;
    void registerGcmId(long idCredential, String gcmId) throws Exception;
    List<String> getAllGcmId() throws Exception;
    List<String> getGcmIdByCareer(long idCareer) throws Exception;
    List<String> getGcmIdByStudent(long idStudent) throws Exception;
    List<Credential> getAllCredentials() throws Exception;
    void changePasswordForced(Credential credential, String newPassword) throws Exception;
}
