package com.utepsa.db.credential;

import com.google.inject.Inject;
import com.utepsa.models.Credential;
import com.utepsa.models.Student;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by david on 19/10/16.
 */
public class CredentialRealDAO extends AbstractDAO<Credential> implements CredentialDAO {

    @Inject
    public CredentialRealDAO(SessionFactory factory) {super(factory);}

    public long create( Credential credential){
        return persist(credential).getId();
    }

    @Override
    public Credential login(String username, String password) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.Credential.login").setParameter("username", username).setParameter("password", password));
    }

    @Override
    public void resetPasswordOfCredential(Credential credential) throws Exception{
        Credential credentialUpdate = get(credential.getId());
        persist(credentialUpdate);
    }

    @Override
    public Credential getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public Credential getByIdStudent(Student idStudent) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.Credential.getByIdStudent").setParameter("idStudent", idStudent));
    }

    @Override
    public void registerLastConnection(Credential credential) throws Exception {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        credential.setLastConnection(timeStamp);
        persist(credential);
    }

    @Override
    public void registerGcmId(long idCredential, String gcmId) throws Exception {
        Credential credential = get(idCredential);
        credential.setGcmId(gcmId);
        persist(credential);
    }

    @Override
    public List<String> getAllGcmId() throws Exception {
        Query query = currentSession().getNamedQuery("com.utepsa.models.ViewGcmStudent.getAll");
        return query.list();
    }

    @Override
    public List<String> getGcmIdByCareer(long idCareer) throws Exception {
        Query query = currentSession().getNamedQuery("com.utepsa.models.ViewGcmStudent.getByCareer");
        query.setParameter("idCareer", idCareer);
        return query.list();
    }

    @Override
    public List<String> getGcmIdByStudent(long idStudent) throws Exception {
        Query query = currentSession().getNamedQuery("com.utepsa.models.ViewGcmStudent.getByStudent");
        query.setParameter("idStudent", idStudent);
        List l = query.list();
        return query.list();
    }

    @Override
    public List<Credential> getAllCredentials() throws Exception {
        return currentSession().createCriteria(Credential.class).list();
    }

    @Override
    public void changePasswordForced(Credential credential, String newPassword) throws Exception{
        credential.setPassword(newPassword);
        credential.setChangePasswordForced(false);
        persist(credential);
    }

}