package com.utepsa.db.student;


import com.utepsa.api.custom.MissingCourse;
import com.utepsa.models.GroupsOffered;
import com.utepsa.models.Student;

import java.util.List;

/**
 * Created by shigeo on 01-09-16.
 */
public interface StudentDAO {
    long create(Student student) throws Exception;
    List<Student> getAll() throws Exception;
    Student getById(long id) throws Exception;
    Student getByRegisterNumber(String registerNumber) throws Exception;
    List<Student> getByAgendCode(String agendCode) throws Exception;
    List getRegisterNumbersWhitoutHistoryNotes(int limit) throws Exception;
    List getActiveStudents(String semester) throws Exception;
    List<Student> searchStudentByCareerFullnameOrRegisterNumber(String query) throws Exception;
    String getFirstSemesterByStudent(long idStudent);
    List<MissingCourse> getMissingCoursesByStudent(long idStudent, int pensum, long idCareer) throws Exception;
    List<GroupsOffered> getGroupsOfferedByStudent(long idCareer, int pensum, String module) throws Exception;
}
