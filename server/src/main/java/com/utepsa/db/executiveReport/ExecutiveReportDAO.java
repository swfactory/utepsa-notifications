package com.utepsa.db.executiveReport;

import com.utepsa.api.custom.*;

import java.util.List;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public interface ExecutiveReportDAO {
    String getAverageByCareer(String semester, Long idCareer) throws Exception;
    List<StudentCustom> getStudentCustom() throws Exception;
    Long getActiveStudents(String semester) throws Exception;
    Long getActiveStudentsByCareer(String semester, Long idCsreer) throws Exception;
    Long getActiveStudentsByGender(String semester, Long idCareer, String gender) throws Exception;
    Long getTotalSemesterCourses(String semester) throws Exception;
    Long getTotalDisapprovedCourses(String semester) throws Exception;
    Long getTotalApprovedCourses(String semester) throws Exception;
    Long getTotalAbandonedCourses(String semester) throws Exception;
    Long getSemesterCourses(String semester, Long idCareer) throws Exception;
    Long getDisapprovedCourses(String semester, Long idCareer) throws Exception;
    Long getApprovedCourses(String semester, Long idCareer) throws Exception;
    Long getAbandonedCourses(String semester, Long idCareer) throws Exception;
    Long getActiveStudentsByModule(String semester, Long idCareer, String module) throws Exception;
    ReportYear getActiveStudentsByYear(String year) throws Exception;
    ReportYear getActiveStudentsByFirstSemester(String year) throws Exception;
    ReportYear getActiveStudentsBySecondSemester(String year) throws Exception;
    ReportYear getActiveStudentsBySummer(String year) throws Exception;
    ReportYear getActiveStudentsByWinter(String year) throws Exception;
    ReportYear getActiveStudentsByYearAndCareer(String year, Long idCareer) throws Exception;
    ReportYear getActiveStudentsByFirstSemesterAndCareer(String year, Long idCareer) throws Exception;
    ReportYear getActiveStudentsBySecondSemesterAndCareer(String year, Long idCareer) throws Exception;
    ReportYear getActiveStudentsBySummerAndCareer(String year, Long idCareer) throws Exception;
    ReportYear getActiveStudentsByWinterAndCareer(String year, Long idCareer) throws Exception;
    ReportYear getTotalCoursesByYear(String year) throws Exception;
    ReportYear getTotalDisapprovedCoursesByYear(String year) throws Exception;
    ReportYear getTotalApprovedCoursesByYear(String year) throws Exception;
    ReportYear getTotalAbandonedCoursesByYear(String year) throws Exception;
    ReportYear getCoursesByYear(String year, Long idCareer) throws Exception;
    ReportYear getDisapprovedCoursesByYear(String year, Long idCareer) throws Exception;
    ReportYear getApprovedCoursesByYear(String year, Long idCareer) throws Exception;
    ReportYear getAbandonedCoursesByYear(String year, Long idCareer) throws Exception;
    ReportYear getAverageByYear(String year, Long idCareer) throws Exception;
    List<CourseModuleReport> getCourseModule(String semester, String module, Long idCareer) throws Exception;
    Long getTotalStudentByModule(String semester, String module) throws Exception;
}
