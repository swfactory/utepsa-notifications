package com.utepsa.db.executiveReport;

import com.google.inject.Inject;
import com.utepsa.api.custom.*;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public class ExecutiveReportRealDAO extends AbstractDAO<ExecutiveReport> implements ExecutiveReportDAO {

    @Inject
    public ExecutiveReportRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getAverageByCareer(String semester, Long idCareer) throws Exception {
        SQLQuery average = currentSession().createSQLQuery("select avg(history_note.note) from history_note INNER join student on student.id = history_note.id_student " +
                "where history_note.semester = '"+semester+"' and student.id_career = "+idCareer+"");
        DecimalFormat df = new DecimalFormat("0.00");
        String  avarage = df.format(average.uniqueResult()).toString();
        return avarage;
    }

    @Override
    public List<StudentCustom> getStudentCustom() throws Exception {
        SQLQuery studentCustom = currentSession().createSQLQuery("SELECT s.id as idStudent, s.id_career as idCareer, s.gender as gender FROM student s " +
                "INNER JOIN career cc on cc.id = s.id_career " +
                "WHERE cc.career_active = TRUE");
        studentCustom.addScalar("idStudent", LongType.INSTANCE);
        studentCustom.addScalar("idCareer", LongType.INSTANCE);
        studentCustom.addScalar("gender", StringType.INSTANCE);

        studentCustom.setResultTransformer(Transformers.aliasToBean(StudentCustom.class));

        List<StudentCustom> studentCustoms = studentCustom.list();
        if (studentCustoms.size() < 1)
        {
            return null;
        }
        return studentCustoms;
    }

    @Override
    public Long getActiveStudents(String semester) throws Exception {

        SQLQuery activeStudents = currentSession().createSQLQuery("select count(activos) from " +
                "(select student_course_register.id_student from student_course_register inner join student " +
                "on student_course_register.id_student = student.id " +
                "where student_course_register.state = 'CONFIRMADA' " +
                "and student_course_register.semester = '"+semester+"' " +
                "group by student_course_register.id_student) As activos");

        String quantity = activeStudents.uniqueResult().toString();

        if (quantity.isEmpty())
        {
            return null;
        }
        return Long.valueOf(quantity);
    }

    @Override
    public Long getActiveStudentsByCareer(String semester, Long idCsreer) throws Exception {
        SQLQuery activeStudentsByCareer = currentSession().createSQLQuery("select count(activos) from " +
                "(select student_course_register.id_student from student_course_register inner join student " +
                "on student_course_register.id_student = student.id " +
                "where student.id_career = "+idCsreer+" and student_course_register.state = 'CONFIRMADA' " +
                "and student_course_register.semester = '"+semester+"' " +
                "group by student_course_register.id_student) As activos");
        String quantity = activeStudentsByCareer.uniqueResult().toString();

        if (quantity.isEmpty())
        {
            return null;
        }
        return Long.valueOf(quantity);
    }

    @Override
    public Long getActiveStudentsByGender(String semester, Long idCareer, String gender) throws Exception {
        SQLQuery activeStudentsByGender = currentSession().createSQLQuery("select count(activos) from " +
                "(select student_course_register.id_student from student_course_register inner join student " +
                "on student_course_register.id_student = student.id " +
                "where student.id_career = "+idCareer+" and student_course_register.state = 'CONFIRMADA' AND student.gender = '"+gender+"' " +
                "and student_course_register.semester = '"+semester+"' " +
                "group by student_course_register.id_student) As activos");
        String quantity = activeStudentsByGender.uniqueResult().toString();

        if (quantity.isEmpty())
        {
            return null;
        }
        return Long.valueOf(quantity);
    }

    @Override
    public Long getTotalSemesterCourses(String semester) throws Exception {
        SQLQuery studentsSemester = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(studentsSemester.list().size());
    }

    @Override
    public Long getTotalDisapprovedCourses(String semester) throws Exception {
        SQLQuery disapproved = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on sc.id_student = s.id " +
                "where sc.note < 51 and sc.note != 0 and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(disapproved.list().size());
    }

    @Override
    public Long getTotalApprovedCourses(String semester) throws Exception {
        SQLQuery approved = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where sc.note > 50 and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(approved.list().size());
    }

    @Override
    public Long getTotalAbandonedCourses(String semester) throws Exception {
        SQLQuery studentsSemester = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where sc.note = 0 and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(studentsSemester.list().size());
    }

    @Override
    public Long getSemesterCourses(String semester, Long idCareer) throws Exception {
        SQLQuery studentsSemester = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where s.id_career = "+idCareer+" and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(studentsSemester.list().size());
    }

    @Override
    public Long getDisapprovedCourses(String semester, Long idCareer) throws Exception {
        SQLQuery disapproved = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on sc.id_student = s.id " +
                "where s.id_career = "+idCareer+" and sc.note < 51 and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(disapproved.list().size());
    }

    @Override
    public Long getApprovedCourses(String semester, Long idCareer) throws Exception {
        SQLQuery approved = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where s.id_career = "+idCareer+" and sc.note > 50 and sc.state = 'CONFIRMADA' and sc.semester = '"+semester+"'");
        return Long.valueOf(approved.list().size());
    }

    @Override
    public Long getAbandonedCourses(String semester, Long idCareer) throws Exception {
        SQLQuery studentsSemester = currentSession().createSQLQuery("select s.id from student_course_register sc inner join student s on s.id = sc.id_student " +
                "where sc.note = 0 and sc.state = 'CONFIRMADA' and s.id_career = "+idCareer+" and sc.semester = '"+semester+"'");
        return Long.valueOf(studentsSemester.list().size());
    }

    @Override
    public ReportYear getActiveStudentsByYear(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year1+"%' GROUP BY id_student)as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year2+"%' GROUP BY id_student)as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year3+"%' GROUP BY id_student)as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();

        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsByFirstSemester(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year1+"-1' GROUP BY id_student)as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year2+"-1' GROUP BY id_student)as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year3+"-1' GROUP BY id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();

        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsBySecondSemester(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year1+"-2' GROUP BY id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year2+"-2' GROUP BY id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year3+"-2' GROUP BY id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();

        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsBySummer(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year1+"-V' GROUP BY id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year2+"-V' GROUP BY id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year3+"-V' GROUP BY id_student) as activos");
        String resultYear3 =studentsSemester3.uniqueResult().toString();

        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsByWinter(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year1+"-I' GROUP BY id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year2+"-I' GROUP BY id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT id_student from history_note WHERE semester like '"+year3+"-I' GROUP BY id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();

        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsByYearAndCareer(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester like '"+year1+"%' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester like '"+year2+"%' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester like '"+year3+"%' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsByFirstSemesterAndCareer(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year1+"-1' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year2+"-1' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year3+"-1' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsBySecondSemesterAndCareer(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year1+"-2' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year2+"-2' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year3+"-2' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsBySummerAndCareer(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year1+"-V' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year2+"-V' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year3+"-V' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getActiveStudentsByWinterAndCareer(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year1+"-I' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year2+"-I' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear2 =studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("SELECT COUNT (activos) as ActivosYear from " +
                "(SELECT hn.id_student from history_note hn INNER JOIN student s on s.id = hn.id_student " +
                "WHERE hn.semester = '"+year3+"-I' and s.id_career = "+idCareer+" GROUP BY hn.id_student) as activos");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getTotalCoursesByYear(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn "+
                "WHERE semester like '"+year1+"%'");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year2+"%'");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year3+"%'");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getTotalDisapprovedCoursesByYear(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn "+
                "WHERE semester like '"+year1+"%' and hn.note < 51 and hn.note > 0");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year2+"%' and hn.note < 51 and hn.note > 0");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year3+"%' and hn.note < 51 and hn.note > 0");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getTotalApprovedCoursesByYear(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn "+
                "WHERE semester like '"+year1+"%' and hn.note > 51");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year2+"%' and hn.note > 51");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year3+"%' and hn.note > 51");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getTotalAbandonedCoursesByYear(String year) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn "+
                "WHERE semester like '"+year1+"%' and hn.note = 0");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year2+"%' and hn.note = 0");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn " +
                "WHERE semester like '"+year3+"%' and hn.note = 0");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getCoursesByYear(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year1+"%' and cc.id = "+idCareer+"");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year2+"%' and cc.id = "+idCareer+"");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year3+"%' and cc.id = "+idCareer+"");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getDisapprovedCoursesByYear(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year1+"%' and cc.id = "+idCareer+" and hn.note < 51 and hn.note > 0");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year2+"%' and cc.id = "+idCareer+" and hn.note < 51 and hn.note > 0");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year3+"%' and cc.id = "+idCareer+" and hn.note < 51 and hn.note > 0");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getApprovedCoursesByYear(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year1+"%' and cc.id = "+idCareer+" and hn.note > 51");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year2+"%' and cc.id = "+idCareer+" and hn.note > 51");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year3+"%' and cc.id = "+idCareer+" and hn.note > 51");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getAbandonedCoursesByYear(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year1+"%' and cc.id = "+idCareer+" and hn.note = 0");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year2+"%' and cc.id = "+idCareer+" and hn.note = 0");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select COUNT(hn.id_student) from history_note hn inner join student s " +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year3+"%' and cc.id = "+idCareer+" and hn.note = 0");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears =new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public ReportYear getAverageByYear(String year, Long idCareer) throws Exception {
        int year1 = Integer.parseInt(year);
        SQLQuery studentsSemester1 = currentSession().createSQLQuery("select ROUND(AVG(hn.note),2) from history_note hn inner join student s \n" +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year1+"%' and cc.id = "+idCareer+"");
        String resultYear1 = studentsSemester1.uniqueResult().toString();

        int year2 = year1 - 1;
        SQLQuery studentsSemester2 = currentSession().createSQLQuery("select ROUND(AVG(hn.note),2) from history_note hn inner join student s \n" +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year2+"%' and cc.id = "+idCareer+"");
        String resultYear2 = studentsSemester2.uniqueResult().toString();

        int year3 = year2 - 1;
        SQLQuery studentsSemester3 = currentSession().createSQLQuery("select ROUND(AVG(hn.note),2) from history_note hn inner join student s \n" +
                "on hn.id_student = s.id inner join career cc on cc.id = s.id_career " +
                "WHERE semester like '"+year3+"%' and cc.id = "+idCareer+"");
        String resultYear3 = studentsSemester3.uniqueResult().toString();


        ReportYear quantityYears = new ReportYear();
        quantityYears.setYearnumber1(resultYear1);
        quantityYears.setYearnumber2(resultYear2);
        quantityYears.setYearnumber3(resultYear3);
        return quantityYears;
    }

    @Override
    public List<CourseModuleReport> getCourseModule(String semester, String module, Long idCareer) throws Exception {
        SQLQuery courseModule = currentSession().createSQLQuery("select count(student_course_register.id_student) as studentRegister, " +
                "student_course_register.group_course as courseGroup, cc.name as courseName, sch.schedules as scheduleCourse " +
                "from student_course_register inner join student " +
                "on student_course_register.id_student = student.id inner join course cc on cc.id = student_course_register.id_course " +
                "inner join schedule sch on sch.id = student_course_register.schedule " +
                "where student.id_career = "+idCareer+" and student_course_register.state = 'CONFIRMADA' " +
                "and student_course_register.semester = '"+semester+"' " +
                "and student_course_register.module = '"+module+"'" +
                "group by courseGroup, courseName, scheduleCourse");

        courseModule.addScalar("studentRegister", LongType.INSTANCE);
        courseModule.addScalar("courseGroup", StringType.INSTANCE);
        courseModule.addScalar("courseName", StringType.INSTANCE);
        courseModule.addScalar("scheduleCourse", StringType.INSTANCE);

        courseModule.setResultTransformer(Transformers.aliasToBean(CourseModuleReport.class));

        List<CourseModuleReport> courseModuleList = courseModule.list();

        return courseModuleList;
    }

    @Override
    public Long getTotalStudentByModule(String semester, String module) throws Exception {
        SQLQuery activeStudents = currentSession().createSQLQuery("SELECT count(t.student) as studentRegister FROM " +
                "(SELECT sr.id_student as student FROM student_course_register sr " +
                "WHERE sr.semester = '"+semester+"' AND sr.state = 'CONFIRMADA' and sr.module = '"+module+"' " +
                "GROUP BY sr.id_student) t");

        String quantity = activeStudents.uniqueResult().toString();

        if (quantity.isEmpty())
        {
            return null;
        }
        return Long.valueOf(quantity);
    }

    @Override
    public Long getActiveStudentsByModule(String semester, Long idCareer, String module) throws Exception {
        SQLQuery activeStudentsByModule = currentSession().createSQLQuery("select count(activos) from " +
                "(select student_course_register.id_student from student_course_register inner join student " +
                "on student_course_register.id_student = student.id " +
                "where student.id_career = "+idCareer+" and student_course_register.state = 'CONFIRMADA' AND student_course_register.module = '"+module+"' " +
                "and student_course_register.semester = '"+semester+"' " +
                "group by student_course_register.id_student) As activos");
        String quantity = activeStudentsByModule.uniqueResult().toString();

        if (quantity.isEmpty())
        {
            return null;
        }
        return Long.valueOf(quantity);
    }
}
