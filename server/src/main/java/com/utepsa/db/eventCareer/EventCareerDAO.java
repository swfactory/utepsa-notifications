package com.utepsa.db.eventCareer;

import com.utepsa.models.EventCareer;

/**
 * Created by Leonardo on 22/07/2017.
 */
public interface EventCareerDAO {
    EventCareer getById(long id) throws Exception;
    long create(EventCareer eventCareer) throws Exception;

}
