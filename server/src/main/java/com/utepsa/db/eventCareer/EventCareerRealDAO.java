package com.utepsa.db.eventCareer;

import com.google.inject.Inject;
import com.utepsa.models.EventCareer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Leonardo on 22/07/2017.
 */
public class EventCareerRealDAO extends AbstractDAO<EventCareer> implements EventCareerDAO{

    @Inject
    public EventCareerRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(EventCareer eventCareer) throws Exception{
        return persist(eventCareer).getId();
    }
    @Override
    public EventCareer getById(long id) throws Exception {
        return getById(id);

    }
}
