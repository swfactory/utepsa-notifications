package com.utepsa.db.historyNote;

import com.google.inject.Inject;
import com.utepsa.api.custom.HistoryNotesCustom;
import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.List;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public class HistoryNotesRealDAO extends AbstractDAO<HistoryNotes> implements HistoryNotesDAO {

    @Inject
    public HistoryNotesRealDAO(SessionFactory factory) {
        super(factory);
    }

    public long create(HistoryNotes historyNotes) throws Exception{
        final HistoryNotes created = persist(historyNotes);
        if(created == null) throw new Exception("The notification was not created.");
        return created.getId();
    }

    @Override
    public List<HistoryNotes> getByStudent(long idStudent) throws Exception {
       return currentSession().createCriteria(HistoryNotes.class)
               .add(Restrictions.eq("idStudent", idStudent))
               .list();
    }

    @Override
    public List<Course> getCoursesMissingByStudent(long idStudent, long idCareer, long pensum) throws Exception {
        return currentSession().createSQLQuery( String.format("SELECT cc.id_course as id, c.name, c.initials " +
                "FROM (SELECT * FROM career_course WHERE id_career = %d AND pensum = %d) cc " +
                "INNER JOIN course c ON cc.id_course = c.id " +
                "WHERE cc.id_course NOT IN (SELECT hn.id_course FROM history_note hn WHERE hn.id_student = %d AND note > 50)",  idCareer, pensum, idStudent)).addEntity(Course.class).list();
    }

    @Override
    public List<HistoryNotesCustom> getHistoryNotesByPensum(long idStudent, long idCarrer, long idPensum) throws Exception {
        SQLQuery query = currentSession().createSQLQuery( String.format("SELECT hn.id_student as idStudent, c.id as idCourse, c.name as course, hn.note, " +
                "hn.minimun_note as minimunNote, hn.semester, hn.module, cc.pensum_level as pensumLevel, cc.pensum_order as pensumOrder " +
                "FROM career_course cc " +
                "LEFT JOIN ( SELECT * FROM history_note hn WHERE hn.id_student = %d AND hn.note > 50) hn ON cc.id_course = hn.id_course " +
                "LEFT JOIN course c ON cc.id_course = c.id " +
                "WHERE cc.id_career = %d AND cc.pensum = %d",idStudent, idCarrer,idPensum));

        query.addScalar("idStudent", LongType.INSTANCE);
        query.addScalar("idCourse", LongType.INSTANCE);
        query.addScalar("course", StringType.INSTANCE);
        query.addScalar("note", LongType.INSTANCE);
        query.addScalar("minimunNote", LongType.INSTANCE);
        query.addScalar("semester", StringType.INSTANCE);
        query.addScalar("module", StringType.INSTANCE);
        query.addScalar("pensumLevel", LongType.INSTANCE);
        query.addScalar("pensumOrder", LongType.INSTANCE);

        query.setResultTransformer(Transformers.aliasToBean(HistoryNotesCustom.class));

        List<HistoryNotesCustom> historyNotesCustomList =query.list();
        for ( HistoryNotesCustom historyNotesCustom: historyNotesCustomList)
        {
            historyNotesCustom.setIdStudent(idStudent);
        }
        return historyNotesCustomList;
    }

    @Override
    public List<HistoryNotes> getAllHistoryNotesByCourse(long idStudent, Course idCourse) throws Exception {
        return  list(namedQuery("com.utepsa.models.historyNotes.getAllHistoryNotesByCourse").setParameter("idStudent", idStudent).setParameter("idCourse", idCourse));
    }

    @Override
    public List<HistoryNotes> getAllHistoryNotesByStudentAndSemester(long idStudent, String semester) throws Exception {
        return  list(namedQuery("com.utepsa.models.historyNotes.getAllHistoryNotesByStudentAndSemester").setParameter("idStudent", idStudent).setParameter("semester", semester));
    }
}
