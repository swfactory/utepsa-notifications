package com.utepsa.db.historyNote;


import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;

import java.util.List;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public interface HistoryNotesDAO {

    long create(HistoryNotes historyNotes) throws Exception;
    List<HistoryNotes> getByStudent(long idStudent) throws Exception;
    List<Course> getCoursesMissingByStudent(long idStudent, long idCareer, long pensum) throws Exception;
    List getHistoryNotesByPensum(long idStudent, long idCarrer, long idPensum) throws Exception;
    List<HistoryNotes> getAllHistoryNotesByCourse(long idStudent, Course idCourse)throws Exception;
    List<HistoryNotes> getAllHistoryNotesByStudentAndSemester(long idStudent, String semester) throws Exception;
}
