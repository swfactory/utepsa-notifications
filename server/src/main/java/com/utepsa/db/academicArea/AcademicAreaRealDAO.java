package com.utepsa.db.academicArea;

import com.google.inject.Inject;
import com.utepsa.models.AcademicArea;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 24/04/2017.
 */
public class AcademicAreaRealDAO extends AbstractDAO<AcademicArea> implements AcademicAreaDAO {

    @Inject
    public AcademicAreaRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public AcademicArea getById(long id) throws Exception{
        return get(id);
    }

    @Override
    public AcademicArea getByCode(String code) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.AcademicArea.getByCode").setParameter("code", code));
    }

    @Override
    public List<AcademicArea> getAll() throws Exception {
        return namedQuery("com.utepsa.models.AcademicArea.getAll").list();
    }
}
