package com.utepsa.db.book;

import com.google.inject.Inject;
import com.utepsa.models.Book;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 19/09/2017.
 */
public class BookRealDAO extends AbstractDAO<Book> implements BookDAO {

    @Inject
    public BookRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Book book) throws Exception {
        final Book created = persist(book);
        if(created == null) throw new Exception("The Book was not created.");
        return created.getId();
    }

    @Override
    public Book getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<Book> getAll() throws Exception {
        return list(namedQuery("com.utepsa.models.Book.getAll"));
    }

    @Override
    public List<Book> searchBook(String query) throws Exception{
        query = query.replace(" ", "%");

        return currentSession().createSQLQuery("SELECT b.\"id\", b.author, b.title, b.quantity, b.publication_date, b.\"content\", b.\"location\", b.description, b.editorial, b.pages, b.volume_number, b.number_of_tomes, b.isbn, b.number_edition " +
                "FROM book As b " +
                "WHERE (b.author || b.title || b.\"content\" || b.description) ILIKE '%" + query + "%' ORDER BY b.title ASC").addEntity(Book.class).list();
    }
}
