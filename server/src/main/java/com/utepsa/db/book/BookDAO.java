package com.utepsa.db.book;

import com.utepsa.models.Book;

import java.util.List;

/**
 * Created by Gerardo on 19/09/2017.
 */
public interface BookDAO {
    long create(Book book) throws Exception;
    Book getById(long id) throws Exception;
    List<Book> getAll() throws Exception;
    List<Book> searchBook(String query) throws Exception;
}
