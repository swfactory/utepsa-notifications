package com.utepsa.db.permissionCredential;

import com.google.inject.Inject;
import com.utepsa.models.PermissionCredential;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 13/02/2017.
 */
public class PermissionCredentialRealDAO extends AbstractDAO<PermissionCredential> implements PermissionCredentialDAO {
    @Inject
    public PermissionCredentialRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(PermissionCredential permissionCredential) throws Exception {
        return persist(permissionCredential).getId();
    }

    @Override
    public List<PermissionCredential> getByIdCredential(long idCredential) throws Exception{
        return list(namedQuery("com.utepsa.models.PermissionCredential.getByIdCredential").setParameter("idCredential",idCredential));
    }

    @Override
    public PermissionCredential getByIdCredentialPermission (long idCredential, long idPermission) throws Exception{
        return uniqueResult(namedQuery("com.utepsa.models.PermissionCredential.getByIdCredentialPermission").setParameter("idCredential", idCredential).setParameter("idPermission", idPermission));
    }

    @Override
    public void modify(List<PermissionCredential> permissionCredentials) throws Exception {
        for (PermissionCredential permissionCredential:permissionCredentials) {
            PermissionCredential permissionCredentialeModify =getByIdCredentialPermission(permissionCredential.getCredentialAdministrator(),permissionCredential.getPermission().getId());
            permissionCredentialeModify.setState(permissionCredential.isState());
            persist(permissionCredentialeModify);
        }
    }
}
