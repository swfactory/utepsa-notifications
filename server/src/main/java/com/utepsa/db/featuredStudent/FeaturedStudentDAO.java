package com.utepsa.db.featuredStudent;

import com.utepsa.api.custom.BestAveragePerSemester;
import com.utepsa.api.custom.StudentParExcellence;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Gerardo on 11/08/2017.
 */
public interface FeaturedStudentDAO {
    List<BestAveragePerSemester> getBestAverageStudentsBySemesterAndCareer(String year, String semester, long idCareer, String station) throws Exception;
    List<StudentParExcellence> getStudentParExcellence(String semester, long idCareer) throws Exception;
    String getFirstSemesterByStudent(long idStudent)throws Exception;
    BigInteger getTotalCarriedCourse(long idStudent) throws Exception;
    BigInteger getCareerTotalCourse(long idCareer, long pensum) throws Exception;
    BigInteger getNumbersOfSemester(long idCareer, long pensum) throws Exception;
}
