package com.utepsa.db.featuredStudent;

import com.google.inject.Inject;
import com.utepsa.api.custom.BestAveragePerSemester;
import com.utepsa.api.custom.StudentParExcellence;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.math.BigInteger;
import java.util.List;
/**
 * Created by Gerardo on 11/08/2017.
 */
public class FeaturedStudentRealDAO extends AbstractDAO<StudentParExcellence> implements FeaturedStudentDAO {

    @Inject
    public FeaturedStudentRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<BestAveragePerSemester> getBestAverageStudentsBySemesterAndCareer(String year, String semester, long idCareer, String station) throws Exception {
        SQLQuery bestAveragePerSemester = currentSession().createSQLQuery("SELECT COUNT(*) carriedCourse, ROUND(avg(hn.note),2) average, hn.id_student As idStudent, st.name As name, " +
                "st.father_lastname As fatherLastname, st.mother_lastname As motherLastname, st.register_number As registerNumber, st.agend_code As agendCode, st.pensum As pensum " +
                "FROM history_note As hn inner join student As st on st.id = hn.id_student " +
                "WHERE st.id_career = " +idCareer+" "+
                "AND hn.id in (SELECT hn2.id from history_note hn2 WHERE hn2.semester = '"+year+"-"+semester+"'" +
                "UNION SELECT hn3.id from history_note hn3 WHERE hn3.semester = '"+year+"-"+station+"')" +
                "and hn.id_student NOT IN (" +
                "select hnn.id_student from history_note hnn where hnn.module = '0' GROUP BY hnn.id_student having COUNT(hnn.id_course) > 2) " +
                "group by hn.id_student, st.name, st.father_lastname, st.mother_lastname, st.register_number, st.agend_code, st.pensum " +
                "having count(*) > 4 " +
                "order by average desc " +
                "LIMIT 10");

        bestAveragePerSemester.addScalar("idStudent", LongType.INSTANCE);
        bestAveragePerSemester.addScalar("average", DoubleType.INSTANCE);
        bestAveragePerSemester.addScalar("name", StringType.INSTANCE);
        bestAveragePerSemester.addScalar("fatherLastname", StringType.INSTANCE);
        bestAveragePerSemester.addScalar("motherLastname", StringType.INSTANCE);
        bestAveragePerSemester.addScalar("registerNumber", StringType.INSTANCE);
        bestAveragePerSemester.addScalar("carriedCourse", LongType.INSTANCE);
        bestAveragePerSemester.addScalar("agendCode", StringType.INSTANCE);
        bestAveragePerSemester.addScalar("pensum", LongType.INSTANCE);

        bestAveragePerSemester.setResultTransformer(Transformers.aliasToBean(BestAveragePerSemester.class));

        List<BestAveragePerSemester> bestAveragePerSemesters = bestAveragePerSemester.list();

        for (BestAveragePerSemester data: bestAveragePerSemesters) {
            data.setFirstSemester(this.getFirstSemesterByStudent(data.getIdStudent()));
            data.setTotalCarriedCourse(this.getTotalCarriedCourse(data.getIdStudent()));
            data.setCareerTotalCourse(this.getCareerTotalCourse(idCareer, data.getPensum()));
        }

        return bestAveragePerSemesters;
    }

    @Override
    public List<StudentParExcellence> getStudentParExcellence(String semester, long idCareer) throws Exception {
        SQLQuery studentExcellence = currentSession().createSQLQuery("SELECT s.id, s.register_number as registerNumber, s.name As name, " +
                "s.father_lastname As fatherLastname, s.mother_lastname As motherLastname, s.agend_code As agendCode, " +
                "ROUND(AVG(hn.note),2) as average, s.pensum As pensum " +
                "FROM history_note hn " +
                "inner join student s on s.id = hn.id_student " +
                "inner join career_course cc on cc.id_career = s.id_career " +
                "WHERE s.id_career = "+idCareer+" and note >= 51 and s.id not in (SELECT s.id as student " +
                "FROM history_note hn inner join student s on s.id = hn.id_student " +
                "WHERE s.id_career = "+idCareer+" GROUP BY s.id " +
                "HAVING AVG(hn.note) <= 85) and s.id in (SELECT hn.id_student FROM history_note hn " +
                "WHERE hn.note >= 51 " +
                "GROUP BY id_student " +
                "HAVING COUNT(hn.id_course) >= 30 and s.id in (SELECT sr.id_student from student_course_register sr " +
                "inner join student s on s.id = sr.id_student WHERE s.id_career = "+idCareer+" and sr.semester = '"+semester+"')) " +
                "GROUP BY s.id, s.register_number, s.name, s.father_lastname, s.mother_lastname, s.agend_code, s.pensum " +
                "HAVING AVG(hn.note) >= 85" +
                "ORDER BY average DESC");

        studentExcellence.addScalar("id", LongType.INSTANCE);
        studentExcellence.addScalar("registerNumber", StringType.INSTANCE);
        studentExcellence.addScalar("agendCode", StringType.INSTANCE);
        studentExcellence.addScalar("name", StringType.INSTANCE);
        studentExcellence.addScalar("fatherLastname", StringType.INSTANCE);
        studentExcellence.addScalar("motherLastname", StringType.INSTANCE);
        studentExcellence.addScalar("average", DoubleType.INSTANCE);
        studentExcellence.addScalar("pensum", LongType.INSTANCE);

        studentExcellence.setResultTransformer(Transformers.aliasToBean(StudentParExcellence.class));

        List<StudentParExcellence> studentExcellenceList = studentExcellence.list();

        for (StudentParExcellence data:studentExcellenceList) {
            data.setFirstSemester(this.getFirstSemesterByStudent(data.getId()));
            data.setTotalCarriedCourse(this.getTotalCarriedCourse(data.getId()));
            data.setCareerTotalCourse(this.getCareerTotalCourse(idCareer, data.getPensum()));
            data.setNumberOfSemesters(this.getNumbersOfSemester(idCareer, data.getPensum()));
        }

        return studentExcellenceList;
    }

    @Override
    public String getFirstSemesterByStudent(long idStudent) throws Exception {
        return (String) currentSession().createSQLQuery(String.format("select semester from history_note where id_student = %d ORDER BY semester, module LIMIT 1", idStudent)).uniqueResult();
    }

    @Override
    public BigInteger getTotalCarriedCourse(long idStudent) throws Exception{
        return (BigInteger) currentSession().createSQLQuery(String.format("select COUNT(*) from " +
                "history_note inner join student on student.id = history_note.id_student " +
                "where student.id = %d", idStudent)).uniqueResult();
    }

    @Override
    public BigInteger getCareerTotalCourse(long idCareer, long pensum) throws Exception{
        return (BigInteger) currentSession().createSQLQuery(String.format("select COUNT(*) from " +
                "career_course cc where cc.id_career = %d and cc.pensum = %d", idCareer, pensum)).uniqueResult();
    }

    @Override
    public BigInteger getNumbersOfSemester(long idCareer, long pensum) throws Exception{
        return (BigInteger) currentSession().createSQLQuery(String.format("select MAX(pensum_level) from career_course cc where cc.id_career = %d and cc.pensum = %d", idCareer, pensum)).uniqueResult();
    }
}
