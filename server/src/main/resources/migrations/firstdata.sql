-- RESET career
TRUNCATE career CASCADE;
ALTER SEQUENCE career_id_seq RESTART WITH 1;

-- RESET courses and career_course
TRUNCATE course CASCADE;
ALTER SEQUENCE course_id_seq RESTART WITH 1;
ALTER SEQUENCE career_course_id_seq RESTART WITH 1;

-- RESET student and credential
TRUNCATE student CASCADE;
ALTER SEQUENCE student_id_seq RESTART WITH 1;
ALTER SEQUENCE credential_id_seq RESTART WITH 1;
-- RESET document_student
TRUNCATE document_student CASCADE;
ALTER SEQUENCE document_student_id_seq RESTART WITH 1;

-- RESET document_student
TRUNCATE student_course_register CASCADE;
ALTER SEQUENCE student_course_register_id_seq RESTART WITH 1;

-- INSERT DATA faculty
INSERT INTO faculty (id, name, abbreviation, grade) SELECT 1, 'FACULTAD DE CIENCIA Y TECNOLOGÍA', 'FCT', 'PRE' WHERE NOT EXISTS (SELECT 1 FROM faculty WHERE id = 1);
INSERT INTO faculty (id, name, abbreviation, grade) SELECT 2, 'FACULTAD DE CIENCIAS EMPRESARIALES', 'FCE', 'PRE' WHERE NOT EXISTS (SELECT 1 FROM faculty WHERE id = 2);
INSERT INTO faculty (id, name, abbreviation, grade) SELECT 3, 'FACULTAD DE CIENCIAS JURÍDICAS Y SOCIALES', 'FCJS', 'PRE' WHERE NOT EXISTS (SELECT 1 FROM faculty WHERE id = 3);
INSERT INTO faculty (id, name, abbreviation, grade) SELECT 4, 'COLEGIO DE POST GRADO', 'CPG', 'POST' WHERE NOT EXISTS (SELECT 1 FROM faculty WHERE id = 4);
INSERT INTO faculty (id, name, abbreviation, grade) SELECT 5, 'SECCION ACADEMICA NO DEFINIDA', 'N/D', 'POST' WHERE NOT EXISTS (SELECT 1 FROM faculty WHERE id = 5);

-- INSERT DATA document
INSERT INTO document (id, code_utepsa, description, related) VALUES (1, '001', 'FOTOCOPIA LEGALIZADA DEL TITULO DE BACHILLER', 1);
INSERT INTO document (id, code_utepsa, description, related) VALUES (2, '002', 'CERTIFICADOS DE ESTUDIOS DE 4TO. MEDIO', 2);
INSERT INTO document (id, code_utepsa, description, related) VALUES (3, '003', 'LIBRETA DE CUARTO MEDIO', 3);
INSERT INTO document (id, code_utepsa, description, related) VALUES (4, '004', 'FOTOCOPIA  DE LA CEDULA DE IDENTIDAD', 4);
INSERT INTO document (id, code_utepsa, description, related) VALUES (5, '005', 'FOTOCOPIA LEGALIZADA DE LIBRETA DEL SERVICIO MILITAR', 5);
INSERT INTO document (id, code_utepsa, description, related) VALUES (6, '006', '2 FOTOGRAFIAS DE 3 X 4 CM. COLOR (CON TRAJE FORMAL Y FONDO CLARO)', 6);
INSERT INTO document (id, code_utepsa, description, related) VALUES (7, '007', 'FOTOCOPIA LEGALIZADA CEDULA DE IDENTIDAD PARA EXTRANJEROS', 7);
INSERT INTO document (id, code_utepsa, description, related) VALUES (8, '008', 'PASAPORTE VIGENTE CON VISA CONSULAR DE ESTUDIANTE', 8);
INSERT INTO document (id, code_utepsa, description, related) VALUES (9, '009', 'CERTIFICADO DE ANTECEDENTES', 9);
INSERT INTO document (id, code_utepsa, description, related) VALUES (10, '010', 'CERTIFICADO DE REGISTRO DOMICILIARIO', 10);
INSERT INTO document (id, code_utepsa, description, related) VALUES (11, '011', 'CERTIFICADO MEDICO ELISA', 11);
INSERT INTO document (id, code_utepsa, description, related) VALUES (12, '012', 'CERTIFICADO DE NACIMIENTO', 12);
INSERT INTO document (id, code_utepsa, description, related) VALUES (13, '013', 'FORMULARIO DE SOLICITUD DE ADMISION', 13);
INSERT INTO document (id, code_utepsa, description, related) VALUES (14, '014', 'FOTOCOPIA LEGALIZADA DEL TITULO UNIVERSITARIO', 14);
INSERT INTO document (id, code_utepsa, description, related) VALUES (15, '015', 'FOTOCOPIA LEGALIZADA DEL TITULO EN PROVISION NACIONAL', 15);
INSERT INTO document (id, code_utepsa, description, related) VALUES (16, '016', '3 FOTOGRAFIAS DE 3 X 4 CM. COLOR', 16);
INSERT INTO document (id, code_utepsa, description, related) VALUES (17, '017', 'CURRICULUM VITAE ACTUALIZADO', 17);
INSERT INTO document (id, code_utepsa, description, related) VALUES (18, '018', 'CARTA DE RECOMENDACION', 18);
INSERT INTO document (id, code_utepsa, description, related) VALUES (19, '019', 'ENTREVISTA', 19);
INSERT INTO document (id, code_utepsa, description, related) VALUES (20, '020', 'CERTIFICADO DE NOTAS 4TO MEDIO', 20);
INSERT INTO document (id, code_utepsa, description, related) VALUES (21, '021', 'RESOLUCION ADMINISTRATIVA (SEDUCA)', 21);
INSERT INTO document (id, code_utepsa, description, related) VALUES (22, '022', 'CONSTANCIA DE TRAMITE DE TITULO DE BACHILLER', 22);
INSERT INTO document (id, code_utepsa, description, related) VALUES (23, '023', 'OTROS DOCUMENTOS', 23);
INSERT INTO document (id, code_utepsa, description, related) VALUES (24, '024', 'RESOLUC.ADM. DE RECONOCIMIENTO DE ESTUDIO (HOMOLOG. CICLO MEDIO)', 24);
INSERT INTO document (id, code_utepsa, description, related) VALUES (25, '025', 'CERTIFICADO DE TRABAJO', 25);
INSERT INTO document (id, code_utepsa, description, related) VALUES (26, '026', 'FOTOCOPIA A COLOR DEL PASAPORTE', 26);

-- INSERT DATA role
INSERT INTO role (id, name) VALUES (1, 'Administrator');
INSERT INTO role (id, name) VALUES (2, 'Head Career');
INSERT INTO role (id, name) VALUES (3, 'Student');

-- INSERT DATA type_audience
INSERT INTO type_audience (id, name) VALUES (1, 'PUBLIC');
INSERT INTO type_audience (id, name) VALUES (2, 'STUDENT');
INSERT INTO type_audience (id, name) VALUES (3, 'CAREER');
INSERT INTO type_audience (id, name) VALUES (4, 'GROUP');

-- INSERT DATA category_notification
INSERT INTO category_notification (id, name, id_parent) VALUES (1, 'Notificacion', null);

-- INSERT DATA type_file
INSERT INTO type_file (id, name) VALUES (1, 'PDF');
INSERT INTO type_file (id, name) VALUES (2, 'DOC');
INSERT INTO type_file (id, name) VALUES (3, 'XLS');
INSERT INTO type_file (id, name) VALUES (4, 'PPT');
INSERT INTO type_file (id, name) VALUES (5, 'IMG');
INSERT INTO type_file (id, name) VALUES (6, 'VIDEO');
INSERT INTO type_file (id, name) VALUES (7, 'WWW');


-- INSERT DATA type_migrationlog
INSERT INTO type_migrationlog (id, name) VALUES (1, 'MIGRATION STUDENT WITH CREDENTIAL');
INSERT INTO type_migrationlog (id, name) VALUES (2, 'MIGRATION STUDENT HISTORY NOTE');
INSERT INTO type_migrationlog (id, name) VALUES (3, 'MIGRATION STUDENT DOCUMENTS');
INSERT INTO type_migrationlog (id, name) VALUES (4, 'UPDATE STUDENT HISTORY NOTE');
INSERT INTO type_migrationlog (id, name) VALUES (5, 'UPDATE STUDENT DOCUMENTS');
INSERT INTO type_migrationlog (id, name) VALUES (6, 'MIGRATION_STUDENT_COURSE_REGISTERED');
INSERT INTO type_migrationlog (id, name) VALUES (7, 'UPDATE_STUDENT_COURSE_REGISTERED');
-- DELETE duplicates history_notes
/*DELETE FROM history_note h3 WHERE h3.id IN (SELECT h1.id FROM history_note h1, history_note h2
WHERE h1.id_course = h2.id_course and h1.id_student = h2.id_student and h1.semester = h2.semester and h1.module = h2.module and h1.note = h2.note and h1.minimun_note = h2.minimun_note
AND h1.id > h2.id ORDER BY h1.id_student);*/

-- DELETE duplicates student_migrationlog with history_notes
/*DELETE FROM student_migrationlog WHERE id IN (SELECT s1.id FROM student_migrationlog s1, student_migrationlog s2
WHERE s1.id_student = s2.id_student and s1.id_type = s2.id_type and s1.id_type = 2
and s1.id > s2.id) */


--INSERT DATA permission--
INSERT INTO permission VALUES(1,'Crear usuario','CREATE_USER');
INSERT INTO permission VALUES(2,'Crear notificación','CREATE_NOTIFICATION');
INSERT INTO permission VALUES(3,'Modificar usuario','UPDATE_USER');
INSERT INTO permission VALUES(4,'Modificar notificación','UPDATE_NOTIFICATION');
INSERT INTO permission VALUES(5,'Eliminar usuario','DELETE_USER');
INSERT INTO permission VALUES(6,'Eliminar notificación','DELETE_NOTIFICATION');
INSERT INTO permission VALUES(7,'Consultar notificaciones','QUERY_NOTIFICATION');
INSERT INTO permission VALUES(8,'Consultar alumnos','QUERY_STUDENT');
INSERT INTO permission VALUES(9,'Dashboard','QUERY_DASHBOARD');
INSERT INTO permission VALUES(10,'Consulta a reporte Gerencial','QUERY_REPORT_MANAGMENT');
INSERT INTO permission VALUES(11,'Consultar usuarios','QUERY_USER');
INSERT INTO permission VALUES(12,'Consultar permisos de usuario','QUERY_PERMISSIONS_USER');
INSERT INTO permission VALUES(13,'Editar permisos de usuario','UPDATE_PERMISSION_USER');

--INSERT DATA role_administrator--
INSERT into role_administrator VALUES(1,'Super Administrator');
INSERT into role_administrator VALUES(2,'Administrator');
INSERT into role_administrator VALUES(3,'Moderator');
INSERT into role_administrator VALUES(4,'Executive');
INSERT into role_administrator VALUES(5,'Head Career');
INSERT into role_administrator VALUES(6,'Professor');
INSERT into role_administrator VALUES(7,'Staff');

--INSERT DATA permission_role--
INSERT INTO permission_role VALUES(1,1,1,TRUE);
INSERT INTO permission_role VALUES(2,1,2,TRUE);
INSERT INTO permission_role VALUES(3,1,3,TRUE);
INSERT INTO permission_role VALUES(4,1,4,TRUE);
INSERT INTO permission_role VALUES(5,1,5,TRUE);
INSERT INTO permission_role VALUES(6,1,6,TRUE);
INSERT INTO permission_role VALUES(7,1,7,TRUE);
INSERT INTO permission_role VALUES(8,1,8,TRUE);
INSERT INTO permission_role VALUES(9,1,9,TRUE);
INSERT INTO permission_role VALUES(10,1,10,TRUE);
INSERT INTO permission_role VALUES(11,1,11,TRUE);
INSERT INTO permission_role VALUES(12,1,12,TRUE);
INSERT INTO permission_role VALUES(13,1,13,TRUE);

INSERT INTO permission_role VALUES(14,2,1,TRUE);
INSERT INTO permission_role VALUES(15,2,2,TRUE);
INSERT INTO permission_role VALUES(16,2,3,TRUE);
INSERT INTO permission_role VALUES(17,2,4,TRUE);
INSERT INTO permission_role VALUES(18,2,5,TRUE);
INSERT INTO permission_role VALUES(19,2,6,TRUE);
INSERT INTO permission_role VALUES(20,2,7,TRUE);
INSERT INTO permission_role VALUES(21,2,8,TRUE);
INSERT INTO permission_role VALUES(22,2,9,TRUE);
INSERT INTO permission_role VALUES(23,2,10,TRUE);
INSERT INTO permission_role VALUES(24,2,11,TRUE);
INSERT INTO permission_role VALUES(25,2,12,TRUE);
INSERT INTO permission_role VALUES(26,2,13,TRUE);

INSERT INTO permission_role VALUES(27,3,1,FALSE);
INSERT INTO permission_role VALUES(28,3,2,FALSE);
INSERT INTO permission_role VALUES(29,3,3,TRUE);
INSERT INTO permission_role VALUES(30,3,4,TRUE);
INSERT INTO permission_role VALUES(31,3,5,FALSE);
INSERT INTO permission_role VALUES(32,3,6,FALSE);
INSERT INTO permission_role VALUES(33,3,7,TRUE);
INSERT INTO permission_role VALUES(34,3,8,TRUE);
INSERT INTO permission_role VALUES(35,3,9,FALSE);
INSERT INTO permission_role VALUES(36,3,10,FALSE);
INSERT INTO permission_role VALUES(37,3,11,TRUE);
INSERT INTO permission_role VALUES(38,3,12,TRUE);
INSERT INTO permission_role VALUES(39,3,13,FALSE);

INSERT INTO permission_role VALUES(40,4,1,FALSE);
INSERT INTO permission_role VALUES(41,4,2,FALSE);
INSERT INTO permission_role VALUES(42,4,3,FALSE);
INSERT INTO permission_role VALUES(43,4,4,FALSE);
INSERT INTO permission_role VALUES(44,4,5,FALSE);
INSERT INTO permission_role VALUES(45,4,6,FALSE);
INSERT INTO permission_role VALUES(46,4,7,FALSE);
INSERT INTO permission_role VALUES(47,4,8,TRUE);
INSERT INTO permission_role VALUES(48,4,9,TRUE);
INSERT INTO permission_role VALUES(49,4,10,TRUE);
INSERT INTO permission_role VALUES(50,4,11,FALSE);
INSERT INTO permission_role VALUES(51,4,12,FALSE);
INSERT INTO permission_role VALUES(52,4,13,FALSE);

INSERT INTO permission_role VALUES(53,6,1,FALSE);
INSERT INTO permission_role VALUES(54,6,2,FALSE);
INSERT INTO permission_role VALUES(55,6,3,FALSE);
INSERT INTO permission_role VALUES(56,6,4,FALSE);
INSERT INTO permission_role VALUES(57,6,5,FALSE);
INSERT INTO permission_role VALUES(58,6,6,FALSE);
INSERT INTO permission_role VALUES(59,6,7,FALSE);
INSERT INTO permission_role VALUES(60,6,8,FALSE);
INSERT INTO permission_role VALUES(61,6,9,FALSE);
INSERT INTO permission_role VALUES(62,6,10,FALSE);
INSERT INTO permission_role VALUES(63,6,11,FALSE);
INSERT INTO permission_role VALUES(64,6,12,FALSE);
INSERT INTO permission_role VALUES(65,6,13,FALSE);

INSERT INTO permission_role VALUES(66,7,1,FALSE);
INSERT INTO permission_role VALUES(67,7,2,TRUE);
INSERT INTO permission_role VALUES(68,7,3,FALSE);
INSERT INTO permission_role VALUES(69,7,4,TRUE);
INSERT INTO permission_role VALUES(70,7,5,FALSE);
INSERT INTO permission_role VALUES(71,7,6,FALSE);
INSERT INTO permission_role VALUES(72,7,7,TRUE);
INSERT INTO permission_role VALUES(73,7,8,TRUE);
INSERT INTO permission_role VALUES(74,7,9,TRUE);
INSERT INTO permission_role VALUES(75,7,10,TRUE);
INSERT INTO permission_role VALUES(76,7,11,FALSE);
INSERT INTO permission_role VALUES(77,7,12,FALSE);
INSERT INTO permission_role VALUES(78,7,13,FALSE);

INSERT INTO permission_role VALUES(79,5,1,FALSE);
INSERT INTO permission_role VALUES(80,5,2,FALSE);
INSERT INTO permission_role VALUES(81,5,3,FALSE);
INSERT INTO permission_role VALUES(82,5,4,FALSE);
INSERT INTO permission_role VALUES(83,5,5,FALSE);
INSERT INTO permission_role VALUES(84,5,6,FALSE);
INSERT INTO permission_role VALUES(85,5,7,FALSE);
INSERT INTO permission_role VALUES(86,5,8,FALSE);
INSERT INTO permission_role VALUES(87,5,9,FALSE);
INSERT INTO permission_role VALUES(88,5,10,FALSE);
INSERT INTO permission_role VALUES(89,5,11,FALSE);
INSERT INTO permission_role VALUES(90,5,12,FALSE);
INSERT INTO permission_role VALUES(100,5,13,FALSE);

INSERT INTO academic_area VALUES (1,'84','ADMINISTRACION');
INSERT INTO academic_area VALUES (2,'60','ADMINISTRATIVA');
INSERT INTO academic_area VALUES (3,'85','AUDITORIA');
INSERT INTO academic_area VALUES (4,'80','CIENCIAS BASICAS EMPRESARIALES');
INSERT INTO academic_area VALUES (5,'66','CIENCIAS BASICAS PARA INGENIERIA');
INSERT INTO academic_area VALUES (6,'02','COMERCIAL');
INSERT INTO academic_area VALUES (7,'97','COMERCIO INTERNACIONAL');
INSERT INTO academic_area VALUES (8,'48','COMUNICACION');
INSERT INTO academic_area VALUES (9,'62','DERECHO');
INSERT INTO academic_area VALUES (10,'91','DISEÑO GRAFICO');
INSERT INTO academic_area VALUES (11,'40','ELECTRICA');
INSERT INTO academic_area VALUES (12,'64','ELECTRONICA');
INSERT INTO academic_area VALUES (13,'68','FINANCIERA');
INSERT INTO academic_area VALUES (14,'65','IDIOMAS');
INSERT INTO academic_area VALUES (15,'83','INDUSTRIAL');
INSERT INTO academic_area VALUES (16,'63','INFORMATICA');
INSERT INTO academic_area VALUES (17,'90','MARKETING');
INSERT INTO academic_area VALUES (18,'70','MECANICA');
INSERT INTO academic_area VALUES (19,'96','PETROLERA');
INSERT INTO academic_area VALUES (20,'69','POSTGRADO');
INSERT INTO academic_area VALUES (21,'67','PRODUCTIVA');
INSERT INTO academic_area VALUES (22,'94','REDES');
INSERT INTO academic_area VALUES (23,'61','RELACIONES INTERNACIONALES');
INSERT INTO academic_area VALUES (24,'88','SISTEMAS');
INSERT INTO academic_area VALUES (25,'59','TEC');
INSERT INTO academic_area VALUES (26,'98','TURISMO');

--UPDATE DATA career--
UPDATE career SET career_active=TRUE WHERE code_utepsa='006';
UPDATE career SET career_active=TRUE WHERE code_utepsa='044';
UPDATE career SET career_active=TRUE WHERE code_utepsa='038';
UPDATE career SET career_active=TRUE WHERE code_utepsa='004';
UPDATE career SET career_active=TRUE WHERE code_utepsa='020';
UPDATE career SET career_active=TRUE WHERE code_utepsa='022';
UPDATE career SET career_active=TRUE WHERE code_utepsa='008';
UPDATE career SET career_active=TRUE WHERE code_utepsa='046';
UPDATE career SET career_active=TRUE WHERE code_utepsa='030';
UPDATE career SET career_active=TRUE WHERE code_utepsa='032';
UPDATE career SET career_active=TRUE WHERE code_utepsa='014';
UPDATE career SET career_active=TRUE WHERE code_utepsa='016';
UPDATE career SET career_active=TRUE WHERE code_utepsa='012';
UPDATE career SET career_active=TRUE WHERE code_utepsa='055';
UPDATE career SET career_active=TRUE WHERE code_utepsa='028';
UPDATE career SET career_active=TRUE WHERE code_utepsa='053';
UPDATE career SET career_active=TRUE WHERE code_utepsa='050';
UPDATE career SET career_active=TRUE WHERE code_utepsa='512';
UPDATE career SET career_active=TRUE WHERE code_utepsa='555';
UPDATE career SET career_active=TRUE WHERE code_utepsa='528';
UPDATE career SET career_active=TRUE WHERE code_utepsa='553';
UPDATE career SET career_active=TRUE WHERE code_utepsa='550';

UPDATE career SET same_career=29 WHERE code_utepsa='012';
UPDATE career SET same_career=29 WHERE code_utepsa='512';
UPDATE career SET same_career=1820 WHERE code_utepsa='555';
UPDATE career SET same_career=1820 WHERE code_utepsa='055';
UPDATE career SET same_career=610 WHERE code_utepsa='028';
UPDATE career SET same_career=610 WHERE code_utepsa='528';
UPDATE career SET same_career=812 WHERE code_utepsa='053';
UPDATE career SET same_career=812 WHERE code_utepsa='553';
UPDATE career SET same_career=711 WHERE code_utepsa='050';
UPDATE career SET same_career=711 WHERE code_utepsa='550';

UPDATE career SET plan='T' WHERE code_utepsa='512';
UPDATE career SET plan='T' WHERE code_utepsa='555';
UPDATE career SET plan='T' WHERE code_utepsa='528';
UPDATE career SET plan='T' WHERE code_utepsa='553';
UPDATE career SET plan='T' WHERE code_utepsa='550';

-----------CREATE VIEW AVEAGE BY CAREER--------------
CREATE VIEW aveage_career
as
SELECT cc.id as id_career, AVG(hn.note) as average_student FROM student s
INNER JOIN history_note hn on s.id = hn.id_student
INNER JOIN career_course ccu on hn.id_course = ccu.id_course
INNER JOIN career cc on ccu.id_career = cc.id
WHERE cc.id IN (SELECT cc.id FROM career cc WHERE cc.career_active = TRUE)
GROUP BY cc.id;
------------CREATE INDEX BY NOTE---------------------
CREATE INDEX note_index ON history_note (note);