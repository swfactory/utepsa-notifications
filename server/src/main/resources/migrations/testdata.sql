
--insert data faculty
INSERT INTO faculty VALUES(1, 'FACULTAD DE CIENCIA Y TECNOLOGÍA', 'FCT', 'PRE' );
INSERT INTO faculty VALUES(2, 'FACULTAD DE CIENCIAS EMPRESARIALES', 'FCE', 'PRE');
INSERT INTO faculty VALUES(3, 'FACULTAD DE CIENCIAS JURÍDICAS Y SOCIALES', 'FCJS', 'PRE');
INSERT INTO faculty VALUES(4, 'COLEGIO DE POST GRADO', 'CPG', 'POST');
INSERT INTO faculty VALUES(5, 'SECCION ACADEMICA NO DEFINIDA', 'N/D', 'POST');

--insert data career
INSERT INTO career VALUES(1,'123','Ing de Sistemas',1,1);
INSERT INTO career VALUES(2,'526','Administracion General',2,2);
INSERT INTO career VALUES(3,'956','Ing Financiera',2,2);
INSERT INTO career VALUES(4,'569','Ing Electronica y Sistemas',2,1);
INSERT INTO career VALUES(5,'623','Redes y Telecominicaciones',2,1);

--insert data courses
INSERT INTO Course VALUES (1,'00003','ADMINISTRACION 140','AAD-140');
INSERT INTO Course VALUES (2,'00003','ADMINISTRACION 140','AAD-140');
INSERT INTO Course VALUES (3,'00003','ADMINISTRACION 140','AAD-140');
INSERT INTO Course VALUES (4,'00004','CALCULO 100','CCL-100');
INSERT INTO Course VALUES (5,'00005','ORGANIZACION PERSONAL 120','ORP-120');
INSERT INTO Course VALUES (6,'00006','PROGRAMACION 120 120','PRG-120');

--insert data student
INSERT INTO student VALUES(1,'jose','Zurita','Paz','16/03/1992','M','Jose237@gmail.com','jose.Zurita@gmail.com','75694412','74456325','455896','533269','1','1','1',1);
INSERT INTO student VALUES(2,'Maria','Martinez','Casona','02/06/1994','F','marinita523@gmail.com','M.Martinez@gmail.com','79564823','78954462','366452','566482','2','2','2',2);
INSERT INTO student VALUES(3,'Tatiana','Quido','Mensilla','03/12/1996','F','tati523@gmail.com','T.Quido@gmail.com','79564823','78954462','455623','566482','2','2','2',2);
INSERT INTO student VALUES(4,'Marco','Castro','Sosa','02/06/1992','M','marco523@gmail.com','M.Castro@gmail.com','79564823','78954462','136249','263156','2','2','2',3);
INSERT INTO student VALUES(5,'Sergio','Ardaya','Ferioli','12/09/1995','M','sergio523@gmail.com','S.Ardaya@gmail.com','79564823','78954462','562134','566482','2','2','2',4);
INSERT INTO student VALUES(6,'Oscar','Fisher','Mendez','25/10/1991','M','oscar523@gmail.com','O.Fisher@gmail.com','79564823','78954462','965482','566482','2','2','2',4);

--insert data professor
INSERT INTO professor VALUES(1,'TORREZ MARAÑON',TRUE);
INSERT INTO professor VALUES(2,'ESCOBAR BISCARRA',TRUE);
INSERT INTO professor VALUES(3,'CASTRO MEDINA',TRUE);

--insert data student_course_registration
INSERT INTO student_course_register VALUES(1,1,1,1,'2017-1','1','1','6589942','TARDE','n-220',0,30,'CONFIRMADA');
INSERT INTO student_course_register VALUES(2,2,2,2,'2017-1','1','5','6524485','MAÑANA','e-220',0,20,'CONFIRMADA');
INSERT INTO student_course_register VALUES(3,3,3,3,'2017-1','1','2','6661231','MAÑANA','e-500',0,15,'RETIRADA');
INSERT INTO student_course_register VALUES(4,4,4,2,'2017-1','1','4','6661235','NOCHE','n-401',0,20,'PENDIENTE');
INSERT INTO student_course_register VALUES(5,5,5,2,'2017-1','1','3','6661238','TARDE','n-220',0,18,'CONFIRMADA');
INSERT INTO student_course_register VALUES(6,1,6,3,'2017-1','1','6','6661239','NOCHE','n-504',0,12,'CONFIRMADA');
INSERT INTO student_course_register VALUES(7,4,4,2,'2017-1','1','4','6661235','TARDE','n-401',0,15,'A CONFIRMAR');
INSERT INTO student_course_register VALUES(8,5,3,2,'2017-1','1','3','6661238','TARDE','n-220',0,15,'CONFIRMADA');

--insert data current_semester
INSERT INTO current_semester VALUES (1,'2016-1',1,FALSE);
INSERT INTO current_semester VALUES (2,'2016-2',2,FALSE);
INSERT INTO current_semester VALUES (3,'2014-1',1,FALSE);
INSERT INTO current_semester VALUES (4,'2015-1',1,FALSE);
INSERT INTO current_semester VALUES (5,'2015-2',2,FALSE);
INSERT INTO current_semester VALUES(6,'2017-1',1,TRUE);

--insert data credential
INSERT INTO credential VALUES(2,'M.Martinez','123','token2','DUNXj0pBy5bT2duPEYjw_P3MCNnBjzVbA4xMjFGrmFwv6w1Pr33JhIUlKsDLRVbt8chLUb2vOKWJyBFLZrdBiel46ZRsnEHVwNdv9Sg9RyaSw9F3dFA9ncO4x0LvRvT89xI','2',20,TRUE,2,2,NULL);
INSERT INTO credential VALUES(3,'T.Quido','123','token3','DUNXj0pBy5bT2duPEYjw_P3MCNnBjzVbA4xMjFGrmFwv6w1Pr33JhIUlKsDLRVbt8chLUb2vOKWJyBFLZrdBiel46ZRsnEHVwNdv9Sg9RyaSw9F3dFA9ncO4x0LvRvT89xI','3',15,TRUE,2,3,NULL);
INSERT INTO credential VALUES(4,'M.Castro','123','token4','DUNXj0pBy5bT2duPEYjw_P3MCNnBjzVbA4xMjFGrmFwv6w1Pr33JhIUlKsDLRVbt8chLUb2vOKWJyBFLZrdBiel46ZRsnEHVwNdv9Sg9RyaSw9F3dFA9ncO4x0LvRvT89xI','4',18,TRUE,2,4,NULL);
INSERT INTO credential VALUES(5,'S.Ardaya','123','token5','DUNXj0pBy5bT2duPEYjw_P3MCNnBjzVbA4xMjFGrmFwv6w1Pr33JhIUlKsDLRVbt8chLUb2vOKWJyBFLZrdBiel46ZRsnEHVwNdv9Sg9RyaSw9F3dFA9ncO4x0LvRvT89xI','5',16,TRUE,2,5,NULL);
INSERT INTO credential VALUES(6,'O.Fisher','123','token6','DUNXj0pBy5bT2duPEYjw_P3MCNnBjzVbA4xMjFGrmFwv6w1Pr33JhIUlKsDLRVbt8chLUb2vOKWJyBFLZrdBiel46ZRsnEHVwNdv9Sg9RyaSw9F3dFA9ncO4x0LvRvT89xI','6',12,TRUE,2,6,NULL);

--insert data permission
INSERT INTO permission VALUES (1,'crear usuarios','tag1');
INSERT INTO permission VALUES (2,'crear notificaciones','tag2');
INSERT INTO permission VALUES (3,'modificar usuarios','tag3');
INSERT INTO permission VALUES (4,'modificer nitificaciones','tag4');
INSERT INTO permission VALUES (5,'consultar usuarios','tag5');
INSERT INTO permission VALUES (6,'consultar notificaciones','tag6');

--Select data duplicate History Notes By id Student
(select id_student, id_course, semester, module, note from history_note WHERE id_student = 38046) EXCEPT ALL
(select distinct id_student, id_course, semester, module, note from history_note WHERE id_student = 38046);

--duplicate data history notes--
select "count"(*), id_student, semester, id_course, note, "module"
from history_note
group by id_student, semester, id_course, note, "module"
HAVING "count"(*) > 1

--give all permissions to utepsa
GRANT SELECT ON ALL TABLES IN SCHEMA public TO utepsa;