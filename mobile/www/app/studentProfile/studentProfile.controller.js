;(function() {

    angular.module("utepsa-notifications").controller('StudentProfileController', StudentProfileController);

    StudentProfileController.$inject = ['$scope', 'resizeTemplate','studentProfileService', 'AuthenticationService','status_code','$localStorage','$state'];

    function StudentProfileController($scope, resizeTemplate,studentProfileService, AuthenticationService,status_code,$localStorage,$state) {
        $scope.itemsStudentProfile=[];
        $scope.studentProfile=[];
        resizeTemplate.resize();
        getStudenProfile();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerStudentProfile = true;
        $scope.initRegisterStudent=[];
        $scope.photoRegisterStudent="";
        $scope.registerCodeWithoutZero="";
        $scope.agendCodeWithoutZero="";

        $scope.logout = logout;

        function logout(){
            AuthenticationService.Logout();
        }

        var formatRegisterCode = function(accountOrRegisterCode, tamano) {
            var register="";
            for(var i=0;i<tamano ; i++){
                if(accountOrRegisterCode.charAt(i) > 0){
                    if(accountOrRegisterCode.substring(i).length == 2){ //si el registro contiene 2
                        register= accountOrRegisterCode.substring(i-2,tamano-2);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 3){ //si el registro contiene 3
                        register= accountOrRegisterCode.substring(i-2,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 4){ //si el registro contiene 4
                        register= accountOrRegisterCode.substring(i-1,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 5){ //si el registro contiene 5
                        register= accountOrRegisterCode.substring(i,tamano-3);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 6){ //si el registro contiene 6
                        register= accountOrRegisterCode.substring(i,tamano-4);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 7){ //si el registro contiene 7
                        register= accountOrRegisterCode.substring(i,tamano-5);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 8){ //si el registro contiene 8
                        register= accountOrRegisterCode.substring(i,tamano-6);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 9){ //si el registro contiene 9
                        register= accountOrRegisterCode.substring(i,tamano-7);
                        break;
                    }
                    if(accountOrRegisterCode.substring(i).length == 10){ //si el registro contiene 10
                        register= accountOrRegisterCode.substring(i,tamano-8);
                        break;
                    }
                }
            }
            return register;
        }

        function getStudenProfile() {
            var promise = studentProfileService.getStudentProfile();
            if (promise) {
                promise.then(function (result) {
                    $scope.itemsStudentProfile = result;
                    $scope.spinnerStudentProfile = false;
                    if ($scope.itemsStudentProfile.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsStudentProfile.code == status_code.OK) {

                        $scope.studentProfile =$scope.itemsStudentProfile.data;
                        $scope.initRegisterStudent = formatRegisterCode($scope.studentProfile.agendCode, 10);
                        $scope.photoRegisterStudent=$scope.studentProfile.agendCode;
                        $scope.registerCodeWithoutZero=splitRegisterCode($scope.studentProfile.registerNumber,10);
                        $scope.agendCodeWithoutZero=splitAgendCode($scope.studentProfile.agendCode,10);

                    }
                    if ($scope.itemsStudentProfile.code == status_code.NOT_FOUND) {
                        $scope.messageAlert = "  Estudiante no encontrado";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsStudentProfile.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsStudentProfile.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }
        function splitRegisterCode (result,tamano){
            var newRegister="";
            for(var i=0;i<tamano; i++) {
                if (result.charAt(i) > 0) {
                    newRegister= result.substring(i,tamano);
                    break;
                }
            }
            return  newRegister;
        }
        function splitAgendCode (result,tamano){
            var newRegister="";
            for(var i=0;i<tamano; i++) {
                if (result.charAt(i) > 0) {
                    newRegister= result.substring(i,tamano);
                    break;
                }
            }
            return  newRegister;
        }

        var dateFull = new Date();
        var day = dateFull.getDate();
        var month = dateFull.getMonth();
        var year = dateFull.getFullYear();
        var hour = dateFull.getHours();
        var minutes = dateFull.getMinutes();
        var seconds = dateFull.getSeconds();

        $scope.hourToTimestamp="";
        humanToTime();
        function humanToTime(){
            var humDate = new Date(Date.UTC(year,
                (month),
                (day),
                (hour),
                (minutes),
                (seconds)));
            $scope.hourToTimestamp = (humDate.getTime()/1000.0);
        }

        $scope.totalHour=0;
        verifyDate();
        function verifyDate() {
            try {
                $scope.stateUpdateStudent = $localStorage.lastUpdate.state;
                $scope.dataUpdateStudent = $localStorage.lastUpdate.data;

                dateStart =  new Date($scope.hourToTimestamp*1000);
                dateEnd = new Date ($scope.dataUpdateStudent*1000);
                segundos = (dateStart - dateEnd) / 1000;
                minus = segundos /60;
                hours = minus / 60;
                hours = Math.round (hours);
                days = hours / 24;
                days = Math.round (days);

                $scope.totalHour = hours;
                if ($scope.totalHour >= 6) {
                    $localStorage.lastUpdate.state = true;
                }
            } catch (e) {
                console.log(e)
                logout();
                $state.transitionTo('login');
            }
        }

        $scope.updateStudent="";
        $scope.updateStudentError=false;
        $scope.updateStudentMessage="";

        $scope.updateStudentInformation=function (){
            $scope.spinnerStudentProfile=true;
            if($scope.totalHour == 6 || $scope.stateUpdateStudent==true){
                studentProfileService.updateInfoStudent(function (result) {
                    $scope.updateStudent = result;
                    $scope.spinnerStudentProfile=false;
                    if ($scope.updateStudent.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.updateStudent.data.code == status_code.OK) {
                        $state.reload();
                        $localStorage.lastUpdate ={
                            state:false,
                            data:$scope.hourToTimestamp
                        };
                    }
                    if ($scope.updateStudent.data.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert ="  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.updateStudent.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert="  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }else{
                $scope.spinnerStudentProfile=false;
            }
        }
    }

})();