;(function() {

	angular.module("utepsa-notifications").factory("studentProfileService", studentProfileService);

	studentProfileService.$inject = ['$http','$stateParams','API','$localStorage'];

	function studentProfileService($http,$stateParams,API,$localStorage){

		function getStudentProfile(){

			return $http({
			  method: 'GET',
			  url: API.url+'student/'+ $localStorage.currentUser.student,
			  withCredentials: true,
	          headers: {
	          	'Content-Type': 'application/json; charset=utf-8'
	          }
			}).then(function(result) {
			  return result.data;
			})
			.catch(function(error) {
			  return error;
			});
		}

        function updateInfoStudent(callback){
            return $http({
                method: 'PUT',
                url: API.url+'student/'+ $localStorage.currentUser.student+'/sync',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        var service = {
            getStudentProfile: getStudentProfile,
            updateInfoStudent:updateInfoStudent
        };

		return service;
	}

})();