;(function() {

    angular.module("utepsa-notifications").controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$state','$scope', 'resizeTemplate','dashboardService','notificationsService','status_code','API','$localStorage','AuthenticationService'];

    function DashboardController($state,$scope, resizeTemplate, dashboardService, notificationsService,status_code,API,$localStorage,AuthenticationService) {
        $scope.showMessage = $localStorage.showMessageInApplicationMobile;
        $scope.notificationss = [];
        $scope.itemsNotificationss="";
        $scope.itemsFiles ="";
        $scope.files=[];
        $scope.spinnerNotificationsInDashboard = true;
        $scope.spinnerFile = true;
        $scope.messageAlertNotification = "";
        $scope.alertErrorNotification = false;
        $scope.messageAlertFile= "";
        $scope.alertErrorFile = false;
        $scope.alertErrorUpdateInfoStudent = false;
        $scope.itsChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        resizeTemplate.resize();
        getNotificatios();
        getFilesAvailable();

        function reloadPage(){
            if($state.current.name !== 'notifications'){
                $state.go('notifications');
            }else{
                $state.reload();
            }
        }

        document.addEventListener('deviceready', OnDeviceready, false);

        function  OnDeviceready() {

            document.addEventListener("offline", onOffline, false);
            document.addEventListener("online", onOnline, false);
            var push = PushNotification.init({
                "android": {
                    "senderID": API.sender_id
                },
                "ios": {
                    "senderID": API.sender_id,
                    "sound": true,
                    "vibration": true,
                    "badge": true,
                    "gcmSandbox":true,

                }
            });

            push.on('registration', function (data) {
                var oldRegId = $localStorage.registrationGCM;
                if (oldRegId !== data.registrationId) {
                    // Save new registration ID
                    $localStorage.registrationGCM =data.registrationId;

                    // Post registrationId to your app server as the value has changed
                    if (!$localStorage.postGcm) {
                        AuthenticationService.GcmRegistration($localStorage.currentUser.idCredential,
                            $localStorage.registrationGCM, function (result) {
                                if (result === true) {
                                    $localStorage.postGcm = {state: 'send'};
                                } else {

                                }
                            });
                    }
                }
            });

            push.on('notification', function(data) {
                navigator.notification.alert(
                    data.message,         // message
                    reloadPage,       // callback
                    data.title,           // title
                    'Ok'                  // buttonName
                );
            });

            push.on('error', function (e) {
            });
        }

        function onOffline (){
            $('#myModal').modal('show');
        }

        function onOnline() {
            $('#myModal').modal('hide');
            var networkState = navigator.connection.type;
            if (networkState !== Connection.NONE) {
                $state.reload();
            }
        }

        $scope.DownloadFile=function (url){
            if($scope.itsChrome){
                $scope.spinnerFile = true;
                window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fileEntry) {
                    var uris = encodeURI(url);
                    var res = uris.split("/");
                    var fileName=res[2];
                    var filepath = fileEntry.toURL() + fileName;
                    var fileTransfer = new FileTransfer();

                    fileTransfer.download(uris, filepath,
                        function (fileEntry) {
                            openUrl();
                        },
                        function (error) {
                            navigator.notification.alert(
                                "No se pudo Descargar el archivo "+fileName,       // message
                                reloadPage(),       // callback
                                'Error:',           // title
                                'Ok'                  // buttonName
                            );
                        },
                        true,
                        {}
                    );
                });

                function openUrl() {
                    $scope.spinnerFile = false;
                    window.open(url, "_system");
                    $state.reload();
                }

                function reloadPage() {
                    $state.reload();
                }
            }
        };

        function getNotificatios(){
            var promise = notificationsService.getNotifications(0);
            if(promise){
                promise.then(function (result) {
                    try {
                        $scope.showMessage = $localStorage.showMessageInApplicationMobile;
                        $scope.itemsNotificationss = result;
                        $scope.spinnerNotificationsInDashboard = false;
                        if ($scope.itemsNotificationss.status == -1) {
                            $scope.messageAlertNotification = "  Problema de conexión";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.code == status_code.OK) {
                            angular.forEach($scope.itemsNotificationss.data, function (item) {
                                $scope.notificationss.push(item);
                            });
                        }
                        if ($scope.itemsNotificationss.code == status_code.NOT_FOUND) {
                            $scope.messageAlertNotification = "  Estudiante no encontrado";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.code == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlertNotification = "  Problema interno del servidor";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.status == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlertNotification = "  Problema interno del servidor";
                            $scope.alertErrorNotification = true;
                        }
                    }catch (e){

                    }
                });
            }
        }

        function getFilesAvailable(){
            var promise = dashboardService.getFilesAvailables();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsFiles = result;
                    $scope.spinnerFile = false;
                    if ($scope.itemsFiles.status == -1) {
                        $scope.messageAlertFile = "  Problema de conexión";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.code == status_code.OK) {
                        angular.forEach($scope.itemsFiles.data, function(item){
                            $scope.files.push(item);
                        });
                    }
                    if ($scope.itemsFiles.code == status_code.NOT_FOUND) {
                        $scope.messageAlertFile = "  Estudiante no encontrado";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertFile = "  Problema interno del servidor";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertFile = "  Problema interno del servidor";
                        $scope.alertErrorFile = true;
                    }
                });
            }
        }

        var stopActions = function ($event) {
            if ($event.stopPropagation) {
                $event.stopPropagation();
            }
            if ($event.preventDefault) {
                $event.preventDefault();
            }
            $event.cancelBubble = true;
            $event.returnValue = false;
        };

        $scope.next = function ($event) {
            stopActions($event);
            openSidebar();
        };

        $scope.close = function ($event) {
            stopActions($event);
            hideSidebar();
        };

        $scope.stop = function ($event) {
            stopActions($event);
        };

        $scope.changeState = function () {
            $localStorage.showMessageInApplicationMobile = false;
            $scope.showMessage = $localStorage.showMessageInApplicationMobile;
        };
    }
})();