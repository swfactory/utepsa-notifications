;(function () {
    angular.module('utepsa-notifications').factory('materialsOfferedService', materialsOfferedService);

    materialsOfferedService.$inject = ['$http', 'API', '$localStorage'];

    function materialsOfferedService($http, API, $localStorage) {

        function getStudentById() {
            return $http({
                method: 'GET',
                url: API.url + 'student/' + $localStorage.currentUser.student,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error.data;
            });
        }

        function getMaterialsOffered(dataOfStudent, module ) {
            return $http({
                method: 'GET',
                url: API.url+'student/' + dataOfStudent.idCareer + '/'+dataOfStudent.pensum+'/GroupsOffered?module='+module
            }).then(function (result) {
                return result.data;
            }).catch(function (error) {
                return error.data;
            })

        }

        return  {
            getStudentById: getStudentById,
            getMaterialsOffered: getMaterialsOffered
        };
    }
})();