;(function () {
    angular.module('utepsa-notifications').controller('materialsOfferedController', materialsOfferedController);

    materialsOfferedController.$inject = ['$scope', 'materialsOfferedService', 'status_code'];

    function materialsOfferedController($scope , materialsOfferedService, status_code) {
        $scope.dataOfStudent = [];
        $scope.materialsOffered = [];
        $scope.messageAlert = '';
        $scope.alert = false;
        $scope.spinnerOfGetStudent = true;
        $scope.spinnerGetCourseOffered = false;
        getStudentById();

        function getStudentById() {
            let promise = materialsOfferedService.getStudentById();
            if(promise) {
                promise.then(function (result) {
                    $scope.spinnerOfGetStudent = false;
                    if(result.status == -1) {
                        $scope.messageAlert = 'Problema de conexion.';
                        $scope.alert = true;
                    }
                    if (result.code == status_code.OK) {
                        $scope.dataOfStudent = {
                            idCareer: result.data.career.id,
                            pensum: result.data.pensum
                        }
                    }
                    if (result.code == status_code.NO_CONTENT) {
                        $scope.messageAlert = 'No se encontro ninguna materia.';
                        $scope.alert = true;
                    }
                    if (result.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = 'Problema interno del servidor.';
                        $scope.alert = true;
                    }
                })
            }
        }

        $scope.getOffered = function (dataOfStudent, module) {
            $scope.spinnerGetCourseOffered = true;
            let promise = materialsOfferedService.getMaterialsOffered(dataOfStudent, module);
            if (promise) {
                promise.then(function (result) {
                    $scope.spinnerGetCourseOffered = false;
                    if(result.status == -1) {
                        $scope.messageAlert = 'Problema de conexion.';
                        $scope.alert = true;
                    }
                    if (result.code == status_code.OK) {
                        $scope.materialsOffered = result.data;
                    }
                    if (result.code == status_code.NO_CONTENT) {
                        $scope.messageAlert = 'No se encontro ninguna materia.';
                        $scope.alert = true;
                    }
                    if (result.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = 'Problema interno del servidor.';
                        $scope.alert = true;
                    }
                })
            }
        };

        $scope.showInformationMaterialsOffered = function (materials) {
            if(materials.classroom == '') materials.classroom = 'N/A';
            swal({
                title: materials.course.name,
                html: '<div class="content-sweetAlert">' +
                '<h4><strong>Sigla:</strong></h4>' +
                '<p>' + materials.course.initials + '</p>' +
                '<h4><strong>Grupo:</strong></h4>' +
                '<p>' + materials.group + '</p>' +
                '<h4><strong>Horario:</strong></h4>' +
                '<p>' + materials.schedule.description +' - '+ materials.schedule.schedule +'</p>' +
                '<h4><strong>Cantidad de inscritos:</strong></h4>' +
                '<p>' + materials.numberEnrolled +'</p>' +
                '<h4><strong>Aula:</strong></h4>' +
                '<p >' + materials.classroom +'</p>' +
                '<h4><strong>Semestre:</strong></h4>' +
                '<p>' + materials.semester + '</p></div>'
            });
        };
    }})();