/**
 * Created by BURGOS on 24/10/2017.
 */
;(function () {
    angular.module('utepsa-notifications').factory('virtualBooksService', virtualBooksService);
    virtualBooksService.$inject = ['$http', 'API'];
    function virtualBooksService($http, API) {

        function getSearchBook(search) {
            return $http({
                method: 'GET',
                url: API.url+'virtualBooks/search?fullName='+ search,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        return {
            getSearchBook: getSearchBook
        }
    }
})();