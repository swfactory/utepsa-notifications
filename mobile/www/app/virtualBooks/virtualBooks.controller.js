/**
 * Created by BURGOS on 24/10/2017.
 */
;(function () {
    angular.module('utepsa-notifications').controller('virtualBooksController', virtualBooksController);
    virtualBooksController.$inject = ['$scope', 'virtualBooksService', 'status_code'];
    function virtualBooksController($scope, virtualBooksService, status_code) {

        $scope.getSearchBook = function (search) {
            $scope.spinner = true;
            $scope.alert = false;
            $scope.messageAlert = '';
            $scope.books = [];
            let promise = virtualBooksService.getSearchBook(search);
            if(promise) {
                promise.then(function (result) {
                    $scope.spinner = false;
                    if(result.status == -1) {
                        $scope.alert = true;
                        $scope.messageAlert = 'Problema de conexion.'
                    }
                    if(result.code == status_code.OK) {
                        $scope.books = result.data;
                    }
                    if(result.code == status_code.NO_CONTENT) {
                        $scope.alert = true;
                        $scope.messageAlert = 'No se encontro ningun libro.'
                    }
                    if (result.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = 'Problema interno del servidor.';
                        $scope.alert = true;
                    }
                })
            }
        };

        $scope.showPdfVisor = function (book) {
            swal({
                showCloseButton: false,
                html: '<div class="iframe-content"><iframe class="iframe" src="http://docs.google.com/gview?' +
                'url=' +book.url + '&embedded=true&zoom=false" frameborder="0"></iframe></div>'
            });
        }
    }
})();