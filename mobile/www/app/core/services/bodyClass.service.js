;(function() {

	angular.module("utepsa-notifications").factory("bodyClass", bodyClass);

	
	
	function bodyClass(){
		
		function change(class){
			$.AdminLTE.layout.activate();
    		$.AdminLTE.layout.fix();
    		$.AdminLTE.layout.fixSidebar();
		}

		var service = {
			resize: resize
		};

		return service;
	}

})();