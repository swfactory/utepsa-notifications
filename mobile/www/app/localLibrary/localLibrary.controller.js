;(function () {
angular.module('utepsa-notifications').controller('localLibraryController', localLibraryController);

localLibraryController.$inject = ['$scope', 'localLibraryService', 'status_code',];

function localLibraryController($scope , localLibraryService, status_code) {

    $scope.getBook = function (search) {
        $scope.books = [];
        $scope.spinner = true;
        $scope.messageAlert = '';

        let promise = localLibraryService.getSearchBook(search);
        if (promise) {
            promise.then(function (result) {
                $scope.spinner = false;
                if(result.status == -1) {
                    $scope.messageAlert = 'Problema de conexion.';
                    $scope.alert = true;
                }
                if (result.code == status_code.OK) {
                    $scope.books = result.data;
                }
                if (result.code == status_code.NO_CONTENT) {
                    $scope.messageAlert = 'No se encontro ningun libro.';
                    $scope.alert = true;
                }
                if (result.code == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert = 'Problema interno del servidor.';
                    $scope.alert = true;
                }
            })
        }
    };

    $scope.showInformationBook = function (book) {
        swal({
            title: book.title,
            html: '<div class="content-sweetAlert"><h4>Ubicación</h4>' +
            '<p>' + book.location + '</p>' +
            '<h4>Autor</h4>' +
            '<p>' + book.author + '</p>' +
            '<h4>Contenido</h4>' +
            '<p>' + book.content + '</p>' +
            '<h4>Descripción</h4>' +
            '<p>' + book.description + '</p></div>'
        });
    };
}})();