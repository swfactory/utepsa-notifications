;(function() {

  	angular.module("utepsa-notifications").controller('LoginController', LoginController);

  	LoginController.$inject = ['$scope', '$state', 'resizeTemplate', 'AuthenticationService', '$localStorage','status_code'];

  	function LoginController($scope, $state, resizeTemplate, AuthenticationService, $localStorage,status_code) {
    	resizeTemplate.resize();
    	$scope.userAgent="";
    	$scope.messageAlert="";
        $scope.username="";
        $scope.password="";
        $scope.login=login;
        $localStorage.showMessageInApplicationMobile = false;

        var formatRegisterCode = function(accountOrRegisterCode, tamano) {
                return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
        }

        var dateFull = new Date();
        var day = dateFull.getDate();
        var month = dateFull.getMonth();
        var year = dateFull.getFullYear();
        var hour = dateFull.getHours();
        var minutes = dateFull.getMinutes();
        var seconds = dateFull.getSeconds();

        $scope.hourToTimestamp="";
        humanToTime();
        function humanToTime(){
            var humDate = new Date(Date.UTC(year,
                (month),
                (day),
                (hour),
                (minutes),
                (seconds)));
            $scope.hourToTimestamp = (humDate.getTime()/1000.0);
        }

        function login() {
            $scope.spinnerLogin=true;
            $scope.alertError = false;
            $localStorage.showMessageInApplicationMobile = false;
            var user = formatRegisterCode($scope.username, 10);
           AuthenticationService.Login(user,  CryptoJS.SHA512($scope.password), function (result) {
               try {
                   $scope.userAgent = result;
                   $scope.spinnerLogin=false;
                   if ($scope.userAgent.status == -1) {
                       $scope.messageAlert = "  Problema de conexión";
                       $scope.alertError = true;
                       cleanInputs();
                   }
                   if ($scope.userAgent.data.code == status_code.OK) {
                       $localStorage.showMessageInApplicationMobile = false;
                       $localStorage.currentUser = {
                           student: $scope.userAgent.data.data.student.id,
                           idCredential: $scope.userAgent.data.data.id
                       };
                       $localStorage.lastUpdate ={
                           state:true,
                           data:$scope.hourToTimestamp
                       };
                       $state.go('dashboard');
                   }
                   if ($scope.userAgent.data.code == status_code.FORBIDDEN) {
                       $localStorage.showMessageInApplicationMobile = true;
                       $localStorage.tempUser = {student: $scope.userAgent.data.data.id};
                       $localStorage.currentUser = {
                           student: $scope.userAgent.data.data.student.id,
                           idCredential: $scope.userAgent.data.data.id
                       };
                       $localStorage.lastUpdate ={
                           state:true,
                           data:$scope.hourToTimestamp
                       };
                       $state.go('resetPassword');
                   }
                   if ($scope.userAgent.data.code == status_code.NOT_FOUND) {
                       $scope.messageAlert = "  Código de registro y/o contraseña incorrectos";
                       $scope.alertError = true;
                       cleanInputs();
                   }
                   if ($scope.userAgent.data.code == status_code.INTERNAL_SERVER_ERROR) {
                       $scope.messageAlert = "  Problema interno del servidor";
                       $scope.alertError = true;
                       cleanInputs();
                   }
                   if ($scope.userAgent.status == status_code.INTERNAL_SERVER_ERROR) {
                       $scope.messageAlert = "  Problema interno del servidor";
                       $scope.alertError = true;
                       cleanInputs();
                   }
               }catch (e) {

               }
           })
       };
        function cleanInputs() {
            try{
                $scope.password="";
            }catch(e){

            }
        }
  	}
})();