;(function() {

  angular.module("utepsa-notifications").controller('NotificationsController', NotificationsController);

    NotificationsController.$inject = ['resizeTemplate','$scope', 'notificationsService','status_code','$timeout'];

    function NotificationsController(resizeTemplate,$scope,notificationsService,status_code,$timeout) {
        var vm= this;
        $scope.spinnerNotifications = true;
        $scope.notifications = [];
        $scope.itemsNotifications = "";
        vm.busy = false;
        $scope.rows = 0;
        $scope.alertError=false;
        $scope.messageAlert="";
        resizeTemplate.resize();

        $scope.loadMore = function(){
            if (vm.busy) return;
            vm.busy = true;
            var promise = notificationsService.getNotifications($scope.rows);
            if(promise) {
                promise.then(function (result) {
                    try {
                        $scope.itemsNotifications = result;
                        if ($scope.itemsNotifications.status == -1) {
                            $scope.messageAlert = "  Problema de conexión";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsNotifications.code == status_code.OK) {
                            $scope.spinnerNotifications = false;
                            angular.forEach($scope.itemsNotifications.data, function (item) {
                                $scope.notifications.push(item);
                            });
                            $scope.rows = $scope.rows + $scope.itemsNotifications.data.length;
                            vm.busy = false;
                            return;
                        }
                        if ($scope.itemsNotifications.code == status_code.NOT_FOUND) {
                            $scope.messageAlert = "  Estudiante no encontrado";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsNotifications.code == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsNotifications.status == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                    } catch (e) {

                    }
                })
            }
        };
    }
})();